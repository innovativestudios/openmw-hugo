{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-03-24T23:01:15+00:00",
   "title": "OpenMW w okresie od 19 do 25 marca",
   "type": "post",
   "url": "/2012/openmw-okresie-od-19-25-marca/"
}
Nienawidzę zmian czasu. Brak im uzasadnienia, są denerwujące i rozbijające. Przynajmniej przez tydzień będę neptykiem.

Na szczęście ilekroć patrzę na to co się dzieje w OpenMW od razu poprawia mi się humor; bo dzieje się wiele, ale to naprawdę wiele dobrego&#8230;

Zini ukończył implementację pojemników. Rzecz jasna brak GUI dla ekwipunku skutecznie niweluje spektakularność tego osiągnięcia, ale to mimo wszystko to wciąż ważne zadanie. Tak ważne, że aż potrzebne. ;-)

Obiecałem, wam, że poinformuję o wszelkich postępach w pracach nad minimapą i dziś mogę z zadowoleniem powiedzieć, że scrawl nie zawiódł!  
Po pierwsze: migracja OpenMW do MyGUI 3.2. To wcale nie było tak proste jak mogłoby się zdawać, bo MyGUI to biblioteka tworzona w obłąkany sposób &#8211; jej deweloperzy entuzjastycznie zmieniają około połowy funkcji przy okazji każdego wydania. Przystosowanie OpenMW by działało poprawnie z tą wersją to wysiłek podjęty w całości przez scrawla.  
Po drugie: minimapa została ukończona. Jest strzałka, jest nawet teren (w odpowiedniej gałęzi). Kompas w prawym dolnym rogu ekranu również działa. Całość prezentuje się <a title="przyjemnie" href="http://dl.dropbox.com/u/2899105/mini.jpg" target="http://dl.dropbox.com/u/2899105/mini.jpg">elegancko</a>.

Ponieważ wiele osób narzekało na wygląd fontów w GUI, a pomimo migracji na MyGUI 0.13.0 wciąż nie sposób użyć oryginalnych pikselowych czcionek, pvdk znalazł zupełnie nową. W grze wygląda naprawdę <a href="http://oi39.tinypic.com/2ywxxzs.jpg" target="_blank">ładnie</a>. Zmieniliśmy również font na stronie. Spójrzcie tylko jak słodko wygląda literka Q. Aż chce się ją pogłaskać ;-)

Nowa czcionka ma znaki dla wielu języków, w tym oczywiście dla polskiego.

Jhooks1 wciąż męczy fizykę. Mamy drobny problem z błędną implementacją: osie współrzędnych w Aedra i OpenMW nie działają dokładnie tak samo, więc wciąż pozostaje wiele do zrobienia. Jhooks1 nie jest nawet pewny, czy będzie w stanie ukończyć to zadanie&#8230; ale za to ja nie mam nawet cienia wątpliwości co do talentu i wytrwałości tego konkretnego programisty. ;-)

Github ostatnio stał się sceną dla nowego, zamaskowanego bohatera. Kromgrat sforkował OpenMW i naprawił kilka bugów, w tym błąd numer 1 (niewłaściwa orientacja niektórych obiektów). Tajemnicza postać co prawda zarejestrowała się na forum, ale wciąż nie napisała ani jednej wiadomości, nie pojawiła się też wcale na kanale IRC.

Dziękujemy ci Kromgrat, twoja pomoc jest nieoceniona!

gus dokończył dialogi. Finał! Fajerwerki! Szampan! Dialogi to killer feature wydania 0.13.0, dają wrażenie o wiele większej interakcji z otoczeniem. Skoro zadanie jest ukończone, nic nie powstrzymuje nas przed wydaniem nowego OpenMW.

I to się dzieje. Teraz. Paczkerzy pakują. WeirdSexy zrobił wideo z prezentacją OpenMW i wszystkim nam bardzo podoba się jego dzieło. W przyszłym tygodniu, może nawet w poniedziałek, wszyscy zobaczycie i z pewnością pokochacie OpenMW 0.13.0. :-)