{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-10-15T04:23:48+00:00",
   "title": "Zini has arrived.",
   "type": "post",
   "url": "/2013/zini-arrived/"
}
Część czytelników bloga pewnie zastanawiało się ostatnim dniami, cóż to za kataklizm zdruzgotał OpenMW. W niniejszym wpisie postaram się ograniczyć te rozważenia, choć jednocześnie nie mam złudzeń co do szans na całkowicie ich rozwianie.

Otóż przez okres dwóch tygodni Zini nie był dostępny online. Choć dokładne przyczyny tego wydarzenia nie są nam znane, z pewnością wiemy o utracie zasilenia połowy miasta, a niesprawdzone plotki mówią o wybuchu jądrowym; inwazji kosmitów oraz innych efektach specjalnych.

Na szczęście Zini uratował świat przed niechybną zgubą, i w końcu może powrócić do rzeczy naprawdę istotnych ─ takich jak OpenMW. Jest to oczywiście bardzo korzystne, choćby dla tego, że liczba gałęzi do zmergowania zaczynała przekraczać normy wzniesione z takim trudem przez drobnomieszczańską, ale jednak; cnotę ─ RZETELNOŚĆ.

Brak ziniego oznaczał znaczne spowolnienie rozwoju OpenMW. Warto jednak zwrócić uwagę, na wysiłki gusa w kierunku wprowadzenia sztucznej inteligencji walki.

Mam nadzieję, że posty na blogu będą pojawiać się dawnym trybem, bez większych przerw.