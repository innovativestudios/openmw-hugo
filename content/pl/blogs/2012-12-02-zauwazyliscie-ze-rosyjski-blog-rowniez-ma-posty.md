{
   "author": "sir_herrbatka",
   "date": "2012-12-02T17:01:05+00:00",
   "title": "Zauważyliście, że Rosyjski blog również ma posty?",
   "type": "post",
   "url": "/2012/zauwazyliscie-ze-rosyjski-blog-rowniez-ma-posty/"
}
Cześć,

Zacznę od rzeczy bardzo istotnej, o której zapomniałem wspomnieć w poście anglojęzycznym. O to tromboncot rozwiązał problem długich nazw przedmiotów wyświetlanych w dymkach podpowiedzi. Ponieważ obraz mówi więcej niż i nawet tysiąc słów o to krótka prezentacja http://www.youtube.com/watch?v=ftwThEF09vI&feature=youtu.be Wideo nie przedstawia aktualnego stanu projektu ─ tekst jest przewijany z większą prędkością, ale jednak dopiero po kilku sekundowym opóźnieniu, by można spokojnie zapoznać się z treścią.

Tymczasem Mark76 kontynuuje swoje prace nad wsparciem dla wielu plików danych jednocześnie. Zagadnienie jest dość obszerne i polecam odwiedzić nasze forum gdzie programista umieścił wyczerpujący opis zarówno swoich osiągnięć jak i dalszych planów. Niestety plany będą musiały poczekać bo Mark będzie zajęty, przynajmniej przez jakiś czas.

Scrawl dodał obsługę istnego mrowia instrukcji skryptów (PcExpell, PcExpelled, PcClearExpelled, RaiseRank, LowerRank, GetWerewolfKills, ModScale, SetDelete, GetSquareRoot…).

gus dalej zajmuje się renderowanie broni. Można już dobyć oręża, tarcza również jest widoczna. Niestety pozostaje wciąż wiele do zrobienia, jak choćby obsługa przenośnych źródeł światła (pochodnie) i perspektywa pierwszoosobowa która na dodatek będzie wymagała dość delikatnych zmian w animacjach.

Zini z kolei doszedł do sensownego w sumie wniosku, że OpenMW ma zespół silny, liczny, sprawny i solidny w przeciwieństwie do edytora którym zajmuje się w zasadzie jedynie Eli2. W związku z tym podjął kontrowersyjną decyzję o rozpoczęciu prac nad OpenCS od nowa, jednak z zamierzeniem wykorzystania elementów starego edytora. Intencje są z pewnością słuszne i dobre: dzięki zmianom zini ma nadzieję dostarczyć edytor wraz z OpenMW 1.0 tak, by w przyszłych wydaniach można będzie już swobodnie wykorzystać możliwości nowego formatu pliku wzbogaconego o dodatkowe funkcje dla OpenMW post 1.0 (nazwa kodowa: Yeti). 

Zobaczymy co z tego wyniknie: sam jestem bardzo ciekaw i na pewno będę monitorować w sytuację.