{
   "author": "sir_herrbatka",
   "date": "2013-03-17T22:33:13+00:00",
   "title": "Mam tu wpisać tytuł?",
   "type": "post",
   "url": "/2013/mam-tu-wpisac-tytul/"
}
Niczym trzęsienie ziemi, niczym burza, niczym huragan nadciąga wielkie wydanie 0.22.0. Więcej niż po prostu nieproporcjonalne niczym pająk w oczach przerażonej panienki czy też nawet prącie aktora porno w optyce kamery. Nie moi drodzy! Wielkie niczym King Kong albo Godzilla.

Aktualnie 0.22.0 jest w fazie RC, co oznacza, że już wkrótce zostanie dodany w sekcji „Do ściągnięcia” w wersjach kompilowanych dla systemów Linux, Windows, OSX ─ aby każdy zainteresowany będzie mógł wypróbować nowe funkcje. A funkcje są tym razem wręcz przytłaczające. 

Przede wszystkim postać główna potrafi już chodzić i nie dryfuje nad ziemią jak gdyby występowała w teledysku do „What else is there?” zespołu <a href="http://www.youtube.com/watch?v=KLpkXtM-VI8" title="Pomimo ö w nazwie nie grają metalu lub hard rocka, po prostu pochodzą z Norwegii." target="_blank">Röyksopp</a>. Co więcej: już w 0.23.0 zobaczymy chodzących npc.

Jakby było wam jeszcze mało to mamy kolejną funkcję wagi ciężkiej: wsparcie dla ładowania wielu plików gry równocześnie. Jeśli macie ochotę na spacer po Twierdzy Smutków, Forcie Śnieżnym albo nawet Firewatch absolutnie nic już nie stoi na przeszkodzie.

A to przecież nie wszystko! Są inne funkcje, i to nawet takie, które śmiało mogłyby uchodzić za hity w pomniejszym wydaniu. Oczywiście przy okazji deweloperzy rozgnietli na miazgę rekordową liczbę bugów, ale kto by o tym nawet wspominał…

Sumarycznie wydanie 0.22.0 można określić jako najbardziej spektakularne od czasów pojawienia się renderowania terenu, a być może nawet większe.

Programiści kontynuują prace nad OpenMW również teraz, śmiem nawet twierdzić, że nie zwolnili tempa ani o jotę. Pewności nadaje mi krótka wizyta na naszym bugtrackerze.

<a href="https://bugs.openmw.org/projects/openmw/activity" title="Kliknij." target="_blank"></a>

Jak widzicie pojawił się klawisz włączający tryb automatycznego biegania, punkty pancerza, nowe; liczne raporty o błędach i równie liczne poprawki.

Poza tym gus radzie sobie znakomicie z implementowaniem AI. Funkcja AItravel działa, przynajmniej w odniesieniu do postaci znajdujących się w komórkach wnętrz. Kwestia plenerów jest nieco bardziej złożona, ale nie ma wątpliwości, że gus ostatecznie poradzi sobie również i z nią.