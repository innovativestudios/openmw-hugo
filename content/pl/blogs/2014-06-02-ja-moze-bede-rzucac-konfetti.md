{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-06-02T17:58:37+00:00",
   "title": "To ja może będę rzucać konfetti…",
   "type": "post",
   "url": "/2014/ja-moze-bede-rzucac-konfetti/"
}
Gdybyście nie zauważyli, to mamy już wydanie 0.30.0. Wspaniałe! Jego długa lista zmian, niech będzie moim usprawiedliwieniem: tłumaczenie tego dla Polskich czytelników wymaga więcej wysiłku, niż jestem w stanie poświęcić. Za to mogę przetłumaczyć wam bardzo ciekawy post Ziniego, opisujący obrazowo aktualny stan projektu.

> Jak możecie zobaczyć pod adresem https://forum.openmw.org/viewtopic.php?f=2&t=2199 jesteśmy bardzo blisko pełnej reimplementacji funkcjonalności Morrowinda. Wszystko idzie w dobrym kierunku i… nie mam nic do dodania odnośnie OpenMW.
> 
> OpenCS to odmienna kwestia. Brakuje nam siły roboczej. Aktualnie większość pracy jest realizowana przez sirherrbatkę i mnie, a to zwyczajnie nie wystarcza. O ile nie chciałbym odciągać deweloperów od OpenMW by pracowali nad edytorem, o tyle zachęcam nieaktywnych lub byłych programistów OpenMW do pomocy. Nowe osoby są oczywiście również mile widziane.

Tak. To są nasze perspektywy. Prawdę powiedziawszy, nie mogę oprzeć się wrażeniu, że OpenMW; wraz z ostatnim wydaniem dotarł do etapu który z jednej strony jest nudny: bo do wydania 1.0.0 najpewniej nie będę mieć okazji do opisania szczególnie wielu podniecających zmian w programie, ale z drugiej strony czeka nas masa pracy przy doprowadzaniu całości do pełni grywalności.

I nie przestajemy pracować! Scrawl, przykładowo jest super-aktywny: naprawia bugi jeden za drugim niczym bohater filmów akcji. Didimaster również naprawił wiele błędów, zaś puppykevin pracuje nad wprowadzeniem animacji twarzy do OpenMW. Aktualny ich stan jest… zdaje się że sami wiecie jaki.

Zini wprowadził bardzo istotną zmianę do edytora. Otóź obecnie obsługuje on zapis również odniesień (referencji) co jest funkcją fundamentalną i niezbędną dla dalszego rozwoju OpenCS.

Najważniejsza nowość jaką mam do przekazania nie dotyczy jednak OpenMW: Lgro, za dnia programista, wieczorami mistrz sztuk walki i nasz niezawodny admin się żeni! Z tego miejsca życzę mu szczęścia i pomyślności na nowej drodze życia, a wszelkie zawodzące wniebogłosy czytelniczki informuję, że ponoć scrawl jest jeszcze wolny… Nie krępujcie się dodać własne życzenia dla państwa Gromanowskich!