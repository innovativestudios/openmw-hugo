{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-06-11T07:20:45+00:00",
   "title": "Junk data",
   "type": "post",
   "url": "/2014/3679/"
}
Witajcie ponownie,

Projekt pędzi do przodu niczym naszprycowany Armstrong w swoich najlepszych czasach. Nie lubicie kolarstwa? Uważacie je za nudne? Ja też. Ale tym bardziej dopełnia to analogii.

Bo rozwój OpenMW jest w gruncie rzeczy powtarzalny jak pory roku i co za tym idzie: tydzień w tydzień muszę pisać o tych samych rzeczach. Scrawl znowu rozwiązuje niezliczone bugi, greye przeprowadza refaktoryzację kodu sztucznej inteligencji, pvdk stara się pracować nad starterem (a dokładnie nad instalatorem Morrowinda), zini wprowadza zmiany do OpenCS – natury tych zmian nie sposób zrozumieć bez znajomości struktury tej aplikacji.

Całe szczęście mam w zanadrzu jeszcze kilka ciekawostek. Po pierwsze: jeśli chcielibyście być pierwszą osobą która przejdzie Morrowinda uruchomionego na naszym silniku… nic z tego. Rebel-raider już tego dokonał.

Po drugie: scrawl ostatnio wspominał, że musi się użerać z byle-jakością modów. Pokiwaliśmy ze zrozumieniem głową, nie spodziewaliśmy się ujrzeć <a href="https://openmw.org/wp-content/uploads/2014/06/screenshot_19-00-00.png" title="hmmm" target="_blank">tego</a>.

Tak, mody zawierają niekiedy &#8220;junk data&#8221;. Biedny scrawl.