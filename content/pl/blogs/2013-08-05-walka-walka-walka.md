{
   "author": "sir_herrbatka",
   "date": "2013-08-05T09:16:10+00:00",
   "title": "Walka, walka, walka!",
   "type": "post",
   "url": "/2013/walka-walka-walka/"
}
Od momentu przejęcia rozwoju walki w OpenMW przez niezastąpionego Chrisa prace posuwają się dość szybko. Mamy już pierwszego przeciwnika zabitego mieczem, i choć wciąż pozostaje wiele do zrobienia, świadczy to w sposób jasny o skali postępu jaki został dokonany!

Ponieważ jesteśmy obecnie w fazie RC, nie ma mowy by w nadchodzącym wydaniu znalazła się ta funkcja. Jednakże obecne tempo jasno wskazuje, że kolejne wydanie pozwoli już na zupełnie swobodne atakowanie przeciwników. Następnie trzeba będzie wprowadzić sztuczną inteligencję walki oraz rzucanie zaklęć.

Prawdę powiedziawszy Chris przy okazji rozpoczął już prace nad rzucaniem zaklęć, jednak by posunąć się dalej koniecznym jest rozwiązanie pewnych czysto technicznych zagadnień silnika.

Tak więc znajdujemy się w dość emocjonującym punkcie, gdzie w końcu OpenMW naprawdę zaczyna przypominać rozgrywkę znaną z morrowinda.

Oprócz tego z ciekawszych wiadomości:

Zini nadal pracuje nad edytorem. Niestety nie są to dokonania o których jest sens się rozpisywać.

Potatomaster z kolei naprawił uciążliwy i obecny bardzo długo w OpenMW bug powodujący natychmiastową śmierć serca Lorkhan po wejściu bohatera głównego do komnaty.