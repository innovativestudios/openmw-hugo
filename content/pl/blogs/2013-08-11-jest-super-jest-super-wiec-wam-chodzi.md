{
   "author": "sir_herrbatka",
   "date": "2013-08-11T18:09:58+00:00",
   "title": "Jest super, jest super… więc o co wam chodzi?",
   "type": "post",
   "url": "/2013/jest-super-jest-super-wiec-wam-chodzi/"
}
Za nami bardzo udany (i upalny) tydzień. 

Kolega Pl_Kolek pochylił się nad problemem utonięć. Co prawda kwestii topielców w świecie realnym nie rozwiązał (ani nawet chyba nie próbował) ale przynajmniej zaimplementował mechanizm tonięcia w OpenMW — czego niewątpliwe walory dydaktyczne podważa jedynie stan pernamentnej nieśmietelności bohatera głównego. Ale to się zmieni, z czasem…

Tymczasem Chris postanowił wprowadzić do gry wilkołaki. I, o dziwo działają! Wymagalo to sporego nakładu sił i pracy ale efekt jest znakomity! Jednocześnie, nijako przy okazji zaimplentowane zostały przenośne źródła światła, takie jak pochodnie.

Zini zaś wprowadza coraz to nowe funkcje do edytora OpenMW, co jest rzeczą z pewnością dobrą.

Oprócz powyższych nowych funkcji, do OpenMW wprowadzono kilka poprawek błędów.