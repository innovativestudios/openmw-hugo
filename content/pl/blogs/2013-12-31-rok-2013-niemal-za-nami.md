{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-12-31T08:10:16+00:00",
   "title": "Rok 2013 niemal za nami.",
   "type": "post",
   "url": "/2013/rok-2013-niemal-za-nami/"
}
Rok 2013 nie obfitował w wybitnie optymistyczne wydarzenia na świecie, za to dla OpenMW był on udany jak żaden poprzedni. Udało się nam wprowadzić nowe funkcje, uspraweniania; naprawiliśmy setki odnalezionych błędów. Obecny status projektu jasno świadczy naszych postępach: jedynie (sic!) trzy (3) umiejętności nie są jeszcze zaimplementowane, możemy już ładować wiele plików gry, animacje działają doskonale ─ dzięki czemu mogliśmy w końcu stworzyć poprawną implementację kontrolera postaci. Obsługa sterowania również została całkowicie odmieniona. Oprócz tego gra, ukazuje potencjał w dziedzinie usprawnień graficznych: normal mapping, specular mapping, widoczny odległy teren. A na dodatek OpenCS! Edytor nie istniał w obecnej postaci rok temu, a dziś doskonale się rozwija.

Tak, mijający rok to niezwykle udany okres dla OpenMW. W 2014 na pewno ukaże się wersja 1.0.0 ─ nie potrafię wyobrazić sobie innej możliwości. Ale oznacza to zupełnie nowe wyzwania: optymalizację, naprawę wciąż licznych błędów i wprowadzenie nielicznych, ale nadal brakujących elementów rozgrywki. Brzmi ekscytująco, czyż nie?