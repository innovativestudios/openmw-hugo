{
   "author": "sir_herrbatka",
   "date": "2013-07-24T14:50:28+00:00",
   "title": "No to po staremu…",
   "type": "post",
   "url": "/2013/po-staremu/"
}
Wracają do normalności na blogu publikuję cotygodniowy post w niedzielę. Od środy nie uświadczyliśmy jakichś wielkich przełomów, jednak jest o czym pisać.

  * Otóż pojawiły się odgłosy kroków! Zasługa to rzecz jasna Chrisa.
  * Zini przebudował menu główne edytora oraz poprawił konfigurację widoku kolumn.
  * Kolzi zaimplementował bardzo interesującą funkcję: można już przybliżać i oddalać kamerę widoku TPP.