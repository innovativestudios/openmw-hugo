{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-11-13T18:34:33+00:00",
   "title": "Wczoraj było święto narodowe → wczoraj nie było posta",
   "type": "post",
   "url": "/2012/wczoraj-bylo-swieto-narodowe-→-wczoraj-nie-bylo-posta/"
}
A gdy stało się tak, że wydana została również wersja 0.19.0 już nic nigdy nie było takie samo. Powstała nowa siła zdolna wlać czystą trwogę do serc nawet i najmężniejszych. Bo o to narodziła się Śmierć. 

Szkoda tylko, że jak się chwilę później okazało w dialogach brakuje bardzo wielu istotnych filtrów bez których nie może być mowy o wykonaniu nawet pierwszego zadania gildii wojowników w Balmorze.

Na szczęście mamy już gotowy plan rozwiązania tych problemów. Składa się jedynie z dwóch punktów, ale prostota często bywa cechą prawdziwego geniuszu:

  * Zajmie się tym Zini.
  * Zini rozwiąże wszystkie problemy.
Jak widać plan jest w pełni przemyślany i trudno znaleźć powód dla którego może nie zadziałać. Kill counter z którego braku nie działa większość zadań w których należy coś (lub kogoś) zabić już funkcjonuje. Z pewnością wkrótce zacznie też działać reszta brakujących elementów.

Oprócz tego Zini dodał również garść nowych statystyk postaci, to jest nagrodę za głowę postaci oraz choroby przez nią przenoszone. Jak zapewne pamiętacie chore postacie są witane przez NPC w „odmienny” sposób ─ to samo dotyczy postaci za których głowę wyznaczona jest nagroda.

Gus zaś zajął się handlem. Co prawda samo okienko jest już obecne od dość dawna, ale jednak bez targowania się. Teraz uległo to zmianie, można już uzyskać lepszą cenę; zupełnie jak w oryginalnym morrowind.

Nastawienie to temat którym aktualnie zajmuje się scrawl. Zajmuje się właśnie implementacją perswazji, a ponadto pomógł gusowi w pracach nad handlem, konkretnie dodając tymczasowe obniżenie nastawienia handlarzy po odrzuceniu przez nich oferty.

Tymczasem greye stara się ukrócić ironiczne „Happy Role Playing” z filmików WeirdSexy. Pora w końcu wdrożyć widoczną zmianę rasy postaci. Oczywiście wymagało to również dodania kolejnej porcji fundamentalnych i niewidocznych dla gracza elementów, ale teraz greye może już pracować nad czymś co nareszcie będzie wyglądało ładnie na zrzutach ekranu! 

Jestem pewny, że bardzo mu tego brakowało. ;-)