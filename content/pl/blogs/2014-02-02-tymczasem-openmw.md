{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-02-02T18:49:57+00:00",
   "openid_comments": [
      "a:1:{i:0;i:528;}"
   ],
   "title": "Tymczasem w OpenMW…",
   "type": "post",
   "url": "/2014/tymczasem-openmw/"
}
A więc, drogi czytelniku, po raz kolejny, lub; oczywiście ─ to również zupełnie możliwe: dopiero po raz pierwszy, odwiedziłeś blog OpenMW by dowiedzieć się cóż nowego słychać w projekcie, i czy oby podstępnie nie wypuściliśmy wersji 1.0.0 pod twoją nieobecność. Już na wstępie mogę zdradzić, że nic podobnego nie miało miejsca. Bardziej szczegółowe informacje znajdują się w dalszej części tekstu ─ zainteresowanych nie trzeba zachęcać, a inni już pewnie zdążyli opuścić stronę: tak więc do rzeczy!

Scrawl, jeden z głównych motorów rozwoju OpenMW, zdążył zakończyć prace nad obszarowymi efektami magicznymi, jak również wprowadził szereg istotnych poprawek. Warto zwrócić uwagę, na zmianę w sposobie poruszania się npc: skręcając nie zmieniają kierunku natychmiast, ale zgodnie z prawami fizyki stopniowo obracają się. Do uzyskania tego efektu przyczynił się również gus.

Zini, wrócił właśnie do pracy nad zapisem stanu gry: nowością jest zapamiętywanie również zmian zawartości pojemników.

Graffy nadal prowadzi prace nad oknem wyboru plików gry. Aktualnie model jest już gotowy, rozmieszczenie poszczególnych elementów interfejsu doczekało się poprawek.

Klonowanie rekordów w OpenCS jest już możliwe i działa zgodnie z oczekiwaniami.

I to tyle. Wynik, moim zdaniem, godny uznania!