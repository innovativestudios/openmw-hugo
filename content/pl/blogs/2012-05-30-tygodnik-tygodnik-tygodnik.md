{
   "author": "sir_herrbatka",
   "date": "2012-05-30T09:50:36+00:00",
   "title": "Tygodnik, tygodnik, tygodnik!",
   "type": "post",
   "url": "/2012/tygodnik-tygodnik-tygodnik/"
}
Scrawl co prawda wciąż dodaje nowe funkcje w GUI (dodał nawet okno alchemii które jest jednak dopiero w stadium Brzydkiego Kaczątka) ale oprócz tego zajął się także implementacją opcji ustawień w launcherze.

Edmondo to osoba odpowiedzialna za openmw dla gentoo. Oprócz tego zaproponował poprawki dla skryptu ale cmake zostały one odrzucone gdyż:

> We don&#8217;t have any formatting rules for cmake files and considering that even with the currently limited number of code layout and naming rules (some) people have difficulties following them, I don&#8217;t see how adding more will be of any help. But without new rules the cmake files will quickly degenerate into the inconsistent state we have now.
> 
> On top of that these kind of code cleanup usually leads to a lot of merge problems. Therefore they should be avoided unless absolutely necessary.

gus dłubie w kierunku skalowania i obracania obiektów w grze. To niezbędne funkcje.

jhooks1 tradycyjnie już kontynuuje wysiłki związane z animacjami. Wygląda na to, że jeszcze wiele wody w Wiśle upłynie zanim będzie działać idealnie.

Aleksander pracuje zaś nad okienkiem z zaklęciami. Tempo nie jest zbyt szybkie bo może programować tylko w weekendy.

mark76 (deweloper crystall scrolls) jak twierdzi, poczynił znaczne postępy w sprawie ładowania wielu esp i esm. Co prawda wciąż nie dodał kodu do githuba więc nikt nie może tego wypróbować&#8230;

spyboot zaofiarował pomoc w tłumaczeniu newsów na język niemiecki, więc od teraz możecie również czytać wordpressa w tym jakże pięknym języku.

A co do wydania 0.15.0&#8230;

![Że niby 0.15.0?][1] 

O cholera, mamy nowe wydanie.

 [1]:  http://4.bp.blogspot.com/-0Frs8LGFUzc/T39GFr76LeI/AAAAAAAAFYg/BIQCaOuVvWo/s1600/cjp.jpeg