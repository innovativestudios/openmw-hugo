{
   "author": "Vedyimyn",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2014-10-16T20:53:57+00:00",
   "title": "Dziwactw ciąg dalszy",
   "type": "post",
   "url": "/2014/dziwactw-ciag-dalszy/"
}
Minęło trochę czasu od ostatniej aktualizacji, więc pewnie jesteście ciekawi, co się wydarzyło w tym czasie. Zatem: mamy 108 problemów (w dziale „issues” na trackerze) zamkniętych w wersji 0.33.0, i załatanych jeszcze więcej bugów, które się tam nie pojawiły. Przyjrzyjmy się kilku z nich…

Jeden ze szczególnie interesujących to taki, w którym Dagoth Ur jest martwy… wcześniej niż powinien. Przed zabiciem głównego łotra, gracz powinien pokonać siedmiu jego kumpli. Powinien działać skrypt, który obniżał zdrowie Dagoth Ura o 50 punktów po zabiciu każdego z jego popleczników &#8211; łącznie 350 punktów obrażeń dla Sharmata (tak Vivek zwykł nazywać Dagoth Ura). W oryginalnym Morrowindzie skrypt był uszkodzony, obrażenia nie były zadawane. OpenMW od początku działał jak należy, skrypt funkcjonował. Problem w tym, że Dagoth miał tylko 300 pkt. zdrowia, toteż po zabiciu kolegów siłą rzeczy był martwy, zanim go spotkaliśmy w pieczarze. Ironia &#8211; błąd musiał zostać odtworzony, żeby gra działała jak trzeba.

Był także problem z deszczem, efektem wizualnym, który jest de facto picem na wodę. Raczej nie powinieneś/aś zauważyć, że nie pada wszędzie. Nie ma sensu męczyć zasobów, generując więcej kropelek, dla niewielkiej poprawy efektu wizualnego. Deszcz jest generowany tylko tam, gdzie idzie gracz. W perspektywie pierwszej osoby nie ma z tym problemu, ale po przełączeniu na widok zza pleców postaci, widać było coś w tym rodzaju: [zrzut ekranu][1]. Deszcz był generowany tylko w najbliższym (dosłownie) otoczeniu gracza. Naprawiono to. Obecnie pada tam, gdzie jest ustawiona kamera, a nie tam, gdzie jest postać.

Inny problem dotyczył NPC-ów podążających za graczem. Normalnie po teleportacji lub podróży łazikiem, „podążacz” powinien być przeniesiony pierwszy, żeby nie było widać jego skokowego pojawienia się. Działo się to niemal równocześnie. Problem w tym, że skoro i gracz, i towarzysz mieli ten sam docelowy punkt podróży (co do metra), postać gracza pojawiała się na głowie kompana. Pół biedy, jeśli to się działo na dworze. Gorzej po teleportacji w gildii magów &#8211; gracz budził się z górną częścią ciała w suficie. Zostało to naprawione, koniec dziwnych eksperymentów.

Parę newsów na temat OpenCS. Dodano rendering terenu ([link][2]), co oznacza, że można teraz oglądać poprawnie wyświetlany teren w OpenCS. Teksty w prostokątach zawierają współrzędne (x, y) oraz nazwę komórki (cell). Dzięki temu łatwo możesz się zorientować, gdzie jesteś, gdy lecisz nad terenem.

Cc9cii spędził nieco czasu nad czyszczeniem kodu, a Lazaroth bawił się shaderami. Zastanawiał się, co by było, gdyby zachowanie wody zależało od pogody. Na przykład: gdy pada, woda powinna zachowywać się tak, jakby spadały na nią krople deszczu, prawda? Udało mu się już osiągnąć taki efekt ([zrzut ekranu][3]), ale obecnie tylko jeden może zostać użyty. Obecnie nie da się dodawać różnych, w zależności od pogody. Scrawl uważa jednak, że przy odrobinie pracy nie jest to duży problem. Z pewnością jest to rzecz warta zrobienia, ponieważ pozwoli developerom nie tylko zmieniać kształt powierzchni wody, ale także dodawać inne niesamowite efekty, jak połysk ulicy zmoczonej deszczem…

A co do Scrawla: zaczął pisać bloga. Jest on nie tylko kroniką technicznych aspektów OpenMW, ale także innego projektu, nad którym pracuje. Ostatni post dotyczył portowania OpenMW na Ogre 2.0, które powinno zaskutkować olbrzymim wzrostem wydajności. Jeśli masz nieco doświadczenia w programowaniu, powinno Cię to zainteresować. Blog Scrawla jest dostępny pod tym adresem: <http://scrawl.bplaced.net/blog/>.

To tyle. Wersja 0.33.0 robi duży postęp. Duża liczba załatanych błędów może zwiastować nadejście kolejnej wersji, więc bądźcie czujni.

Zapraszamy do komentowania [tutaj][4]!

 [1]: https://bugs.openmw.org/attachments/download/608/2014-07-26_00007.jpg
 [2]: https://github.com/OpenMW/openmw/pull/312
 [3]: http://i.imgur.com/JNfv62K.jpg
 [4]: https://forum.openmw.org/viewtopic.php?f=38&t=2516