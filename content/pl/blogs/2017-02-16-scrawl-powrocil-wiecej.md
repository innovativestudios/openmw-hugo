{
   "author": "Vedyimyn",
   "categories": [
      "Uncategorized"
   ],
   "date": "2017-02-16T15:45:19+00:00",
   "title": "Powrót scrawla… i więcej",
   "type": "post",
   "url": "/2017/scrawl-powrocil-wiecej/"
}
Wygląda na to, że wróciły stare dobre czasy. Zgodnie z informacjami dostępnymi na [GitHubie][1], każdego dnia, przez około dwa ostatnie tygodnie, otrzymywaliśmy kilka commitów od scrawla. Analizując wprowadzone przez niego optymalizacje, można się spodziewać wzrostu liczby klatek na sekundę w&nbsp;następnej wersji OpenMW.

Jedna rzecz rzuca się w oczy: można konfigurować _culling_ (ukrywanie obiektów przy renderowaniu sceny) poprzez modyfikację zawartości pliku settings.cfg. Początkowo ustawiono na sztywno, że obiekty o rozmiarze mniejszym niż 2×2 piksele będą pomijane przy rysowaniu. Teraz można samodzielnie zdecydować ile pikseli chce się poświęcić dla zwiększenia płynności gry. W połączeniu z uproszczoną siatką terenu, nad którą scrawl pracował przed swoją przerwą, oznacza to, że możemy się wkrótce spodziewać funkcji odległego lądu (_distant land_).

Przechodząc do innych spraw&#8230; Allofich naprawił pewien dość irytujący [błąd][2]. Jeśli ktoś poszedł popływać i zignorował rybę, w tle wciąż odgrywana była muzyka towarzysząca walce, nawet gdy wyszło się już na ląd. Obecnie każdy, kto nie miał czasu zająć się rybką, może znowu cieszyć głównym motywem Morrowinda po wyjściu z wody.

Na koniec warto wspomnieć o osiągnięciach projektu [tes3mp][3] (jest to próba utworzenia z Morrowinda gry wieloosobowej). Udało im się zorganizować serwer zachowujący zmiany wprowadzane przez graczy. Gracze mogą wpływać na otoczenie nie tylko poprzez upuszczanie przedmiotów z ekwipunku, ale również dodając obiekty przez konsolę. Deweloperzy zrobili kilka zrzutów ekranu. Zachęcam do ich obejrzenia [tutaj][4].

##### Zapraszamy do komentowania [tutaj][5].

 [1]: https://github.com/OpenMW/openmw/commits/master
 [2]: https://bugs.openmw.org/issues/2678
 [3]: https://www.reddit.com/r/tes3mp/
 [4]: https://imgur.com/a/6aTIl
 [5]: https://forum.openmw.org/viewtopic.php?f=38&t=4134