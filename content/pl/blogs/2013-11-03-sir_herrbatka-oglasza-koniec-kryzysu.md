{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-11-03T17:22:03+00:00",
   "openid_comments": [
      "a:1:{i:0;i:436;}"
   ],
   "title": "Sir_Herrbatka ogłasza koniec kryzysu",
   "type": "post",
   "url": "/2013/sir_herrbatka-oglasza-koniec-kryzysu/"
}
Otóż to: kryzys się skończył, a teraz będzie tylko lepiej. Oczywiście, nie mówię o kryzysie ekonomicznym.

Tak czy owak, wydaje się, że OpenMW na nowo nabiera rozpędu, by z impetem ruszyć w kierunku wyśnionego 1.0. Na poparcie tej, jakże śmiałej, tezy mam dowody pewniejsze niż ekonomiści na poparcie swoich i nie zawaham się ich użyć w poniższym tekście.

Z perspektywy rodaków preferujących język ojczysty najciekawszą informacją będzie z pewnością wieść o najnowszej poprawce autorstwa nieocenionego lgro. Likwiduje ona błąd, który wlekł się za nami zdecydowanie zbyt długo. O ile tylko uważnie czytaliście informacje dotyczące nowych wydań, utkwiła wam w pamięci informacja o niekompatybilności kodu OpenMW ze zlokalizowaną, Polską wersją Morrowind.esm. Opisywanie tego problemu na łamach poniższego bloga, wiązało się z poczuciem wstydu i pewnego zażenowania u autora; bo jakże to: blog i owszem, a tylko program nie akceptuje spolszczonych plików? Na szczęście lgro poświęcił parę godzin swej uwagi i wybawił mnie opresji, a wam, moi drodzy, umożliwił uruchomienie polskiego Morrowinda. Wyznania miłosne, laurki oraz wszelkie inne akty dziękczynne proszę zamieszczać w komentarzach.

Pamiętajcie, że motywacja do pracy w projektach niekomercyjnych bierze się również ze wdzięczności użytkowników. Co więcej, lgro przy okazji odkrył inny błąd powodujący zakończenie programu, więc każda szczypta motywacji jest użyteczna.

Gus tymczasem właśnie zakończył prace nad funkcją LOS (line of sight, służy do potwierdzenia obecności bohatera głównego w polu widzenia postaci niezależnych oraz stworzeń występujących w grze) oraz powrócił do pracy nad sztuczną inteligencją w walce ─ i to z nadspodziewanie dobrym rezultatem. Co prawda funkcja nie jest jeszcze kompletna, ale najtrudniejsza i największa cześć prac została zamknięta ─ a to wynik godny uznania.

Jakby tego jeszcze było mało: wydaje się, że scrawl uaktywnił się ponownie, w związku z czym oczekiwać można by kolejnych spektakularnych osiągnięć. I owszem, faktycznie właśnie obserwujemy nadejście długo oczekiwanej funkcji: renderer dla OpenCS. Innymi słowy już wkrótce będzie można obejrzeć edytowane komórki, co jest opcją nieobecną w innych, alternatywnych względem oryginalnego Construction Set Bethesdy, edytorach.

Zini oraz graffy również nie próżnują. Graffy pracuje nad nową odsłoną okna wyboru plików (wykorzystywanego zarówno przez edytor, jak starter), a zini poprawia różne tabele obecne w OpenCS.