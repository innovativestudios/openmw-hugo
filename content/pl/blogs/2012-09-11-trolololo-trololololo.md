{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-09-10T22:45:39+00:00",
   "title": "Trolololo… Trololololo…",
   "type": "post",
   "url": "/2012/trolololo-trololololo/"
}
Ponieważ jestem osobą z gruntu przeżartą zarazą lenistwa w najgorszym wydaniu, będę posiłkować się już od samego początku wypowiedzią jhooks1.

> I am still working on the NPC/creature correct collision shapes task.
> 
> I had the task done, but I decided instead of scrapping the PhysicActor class that I would redesign it (too much valuable code). The new class is going to incorporate the Rigid Bodies (based on box shapes) and use pmove to allow the creature/NPC to collide with the world around it. Right now only the player gets to collide with the world through pmove, so this work is essential to having NPCs and creatures running about. 
> 
> This is a lot to take in and change around though (and it seems like 3x the work of the original task I set out to do). I hope I can get a solution up and running soon.

Co jak widzicie, właśnie popełniłem. :-)

Niestety oznacza to ni mniej, ni więcej a tyle, że animacja chodu musi poczekać. Niepokoić może ewentualność całkowitego wypadnięcia tej funkcji z nowego wydania, więc wszyscy trzymajmy kciuki za jhooksa!

Oprócz tego chciałbym dodać, iż poprawka wygładzająca animacje o której wspominałem w jednym z dawnych postów, została włączona do gałęzi głównej. Zachęcam do wypróbowania, obserwacji i porównywania wyglądu względem starszej wersji.

Tymczasem Zini pracował nad zagadnieniem numer 370 dotyczącym przeniesienia „Target Handling” do klasy „Action”. Zadanie zostało zakończone, a pozycja na liście zamknięta: efekt prac możecie podziwiać w gałęzi master.

Eli2 tradycyjnie pracuje nad edytorem, kierując większość wysiłków w stronę obsługi filtrów. Nie powinno to dziwić osób znających dyrektywy i koncepcje interfejsu opracowane przez nasz zespół, gdyż filtry stanowią jeden z filarów na którym wspierać ma się użytkownik korzystając z edytora. Oczywiście status elementu wiąże się również z kłębowiskiem dyskusji rosnącym, i karmiącym się nawet drobnymi wątpliwościami przy każdym wprowadzaniu istotnych zmian &#8212; nie inaczej jest i tym razem, ale jak sami rozumiecie ta sytuacja naturalnie wynika z troski podzielanej przez programistów.

Hrnchamd kontynuuje zbożne dzieło patroszenia Morrowind w poszukiwaniu szczegółowych informacji o mechanizmach rozgrywki. Na czole listy priorytetów był, rzecz jasna, koszt ćwiczenia umiejętności u trenerów, jako, że funkcja ta figuruje w planie na najbliższe wydanie. 

Dgdiniz, jak widać na załączonym <a href="http://i649.photobucket.com/albums/uu212/dgdiniz3/trainingWindow.jpg" title="Fajne!" target="_blank">obrazku</a> dokonał znakomitych postępów w implementacji tego okienka. Warto zwrócić uwagę na obecność, całkowicie pozbawionego sensu, paska przewijania w oryginalnym interfejsie. Oczywiście nie musimy duplikować absurdów, do której to kategorii zaliczyć można, jak wszyscy wiecie, nie tylko przewijanie trójelementowej listy&#8230;

Może was to zaskoczy, ale dopiero Scrawl uświadomił mi zbędność paska w oknie treningu; do tej pory przechodziłem nad nim do porządku dziennego. Z drugiej jednak strony nie jest to jedyny fakt z którego nie zdawałem sobie sprawę&#8230;

Przykładowo, jak się ostatnio (przynajmniej dla mnie, Zini z pewnością pisał już o tym na forum) okazało mamy o to kolejny przykład wyższości OpenMW nad Morrowindem, tym razem dotyczący skryptów. 

Stworzenie skryptu przenoszącego gracza do komórki, w punkt o zadanych koordynatach jest zadaniem trywialnym &#8212; o ile tylko nie zamierzacie użyć zmiennych ze skryptu jako położenia, w takim bowiem wypadku okaże się to po prostu niemożliwe. To głupie, sztuczne ograniczenie doprowadzające modderów do szału już od dekady, ten błąd który jako jeden z wielu skatalizował powstanie MSE&#8230; jest nieobecny w OpenMW. 

Po prostu. 

Zadziwiające obejścia zastosowane np. w modzie dodającym do dyspozycji gracza przenośny namiot popielnych, nie będą potrzebne w OpenMW.

Nareszcie!