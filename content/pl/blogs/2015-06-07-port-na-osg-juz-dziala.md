{
   "author": "Vedyimyn",
   "categories": [
      "Uncategorized"
   ],
   "date": "2015-06-07T21:40:05+00:00",
   "title": "Port na OSG już działa!",
   "type": "post",
   "url": "/2015/port-na-osg-juz-dziala/"
}
Scrawl powiadomił, że nowy port już działa. Wykonał także kilka testów. Można o tym poczytać [tutaj][1]. Postaram się przetłumaczyć tego posta w sobotę. Niestety nawał obowiązków nie pozwoli wcześniej :(

 [1]: https://openmw.org/2015/openscenegraph-port-playable/