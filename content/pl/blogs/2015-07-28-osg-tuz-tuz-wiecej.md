{
   "author": "Vedyimyn",
   "categories": [
      "Uncategorized"
   ],
   "date": "2015-07-28T19:55:16+00:00",
   "title": "OSG tuż-tuż i więcej",
   "type": "post",
   "url": "/2015/osg-tuz-tuz-wiecej/"
}
Ci, którzy mają możliwość skompilowania OpenMW z gałęzi OpenSceneGraph pewnie już tego próbowali. Nowy OpenMW jest sprawny, szybki i oczywiście, co najważniejsze, bardzo funkcjonalny. Nie jednakże zbyt ładny. Wersja Ogre miała shadery, w tym niesamowity shader „Shiny” &#8211; dzieło Scrawla &#8211; który nadawał wodzie niesamowity wygląd. Niestety nie da się ich łatwo przeportować na wersję OSG. Jest to jednakże tylko uciecha dla oka. Co prawda jest to krok w tył, ale tylko tymczasowy. Biorąc pod uwagę wzrost wydajności i liczne poprawki, jakie zawdzięczamy migracji, port OSG stanowi kilka kroków naprzód. Jest to wystarczająco dużo, by następne wydanie, 0.37.0, skorzystało z nowego silnika. Przygotujcie się, OpenMW-OSG niedługo ujrzy światło dzienne!

W czasie pisania tego posta, 0.37.0 zbliża się do solidnej liczby 138 problemów (issues) na trackerze. Jak zostało wspomniane przez Scrawla w ostatnim poście, NPCe mają wreszcie właściwy rozmiar. Krople deszczu [nie czynią ognia przezroczystym][1], a ogień [nie nadaje rzeczom czarnego koloru][2]. Towarzysze nie będą się już oburzać, gdy weźmiesz coś niezupełnie swojego do kieszeni.

Nowo przybyły Kunesj przygotował fajne małe dodatki, które zostały już zintegrowane: celownik i podpowiedzi w dymkach, wskazujące, czy obiekt ma posiadacza, czy nie ([foto][3]). Nie działa to na razie tak, jak powinno, więc na razie trzeba edytować parę plików konfiguracyjnych, żeby ta funkcja zadziałała. Jest to jednak wspaniała zapowiedź czegoś, co wielu graczy przyjmnie z entuzjazmem.

Pracowano też trochę nad OpenMW-CS. Umożliwiono edycję wielu parametrów NPC-ów, np. rangi w organizacji, poziomu, umiejętności, itp. Niedogodności interfejsu, takie jak początkowy rozmiar głównego okna, czy też przycisk „Anuluj” zamykający program, zamiast wrócić do poprzedniego ekranu, zostały naprawione. Efekty wyszukiwania mogą być teraz sortowane, co powinno uczynić funkcję globalnego wyszukiwania jeszcze użyteczniejszą. A &#8211; jeszcze jedno. Dodano dynksa do pobierania kolorów ([foto][4])!

Jak widać, drużyna cały czas prze naprzód, ale oprócz silnika OpenMW i Cs-a, powstały też dwa inne projekty.

Najpiew dajmy się wykrzyczeć TravDarkowi, twórcy moda [Ashes of the Apocalypse][5], i Envy123, twórcy nieoficjalnego rozszerzenia do AoA zwanego [Desert Region 2][6]. Udzielili oni hojnie przyzwolenia na rozwój i reorganizację tych projektów tak, by mogły stać się niezależnymi grami na silniku OpenMW. By tak było, konieczna będzie podmiana wszystkich zasobów autorstwa Bethesdy i innych moderów. Jest dużo do zrobienia. [Praca już się rozpoczęła][7].

I tak dochodzimy do drugiego projektu: minimalny pakiet dla twórców gier. Jako że OpenMW to po prostu silnik, potrafi obsługiwać nie tylko Morrowinda. Aby powstała gra z jego użyciem, potrzeba pewnego plikowego minimum, np. nieba, kilku animacji. Zapraszamy na forumowy wątek projektu [OpenMW Game Template][8], projekuu, który ma zapewnić minimum plików koniecznych do użycia OpenMW. Oprócz bycia dobrą podstawą do pracy dla developerów, będzie także fantastycznym testem samego silnika. Ludzie próbujący tworzyć gry mogą mieć problemy z silnikiem, mogą być w nim błędy. Bliski związek projektu OpenMW Game Template z zespołem tworzącym OpenMW umożliwia dokumentowanie i rozwiązywanie problemów zanim staną się kłodą u nogi autorom pełnoprawnych, większych gier.

Tak to więc wygląda: dwa projekty na boku, CS rozszerzony, OpenMW prawie przeportowany, szybszy i sprawniejszy. Jest wiele powodów do ekscytacji, nawet na tak późnym etapie rozwoju.

**Zapraszamy do komentowania [tutaj][9].**

 [1]: http://bugs.openmw.org/issues/2111
 [2]: http://bugs.openmw.org/issues/2140
 [3]: https://cloud.githubusercontent.com/assets/7138527/8763449/534ecda8-2d9c-11e5-81ab-186d7ac4dc55.png
 [4]: http://bugs.openmw.org/attachments/download/1377/color_picking.png
 [5]: http://www.moddb.com/mods/ashes-of-apocalypse
 [6]: http://www.moddb.com/mods/desert-region-2-the-final-frontier
 [7]: https://forum.openmw.org/viewtopic.php?f=2&t=2990
 [8]: https://forum.openmw.org/viewtopic.php?f=20&t=3003
 [9]: https://forum.openmw.org/viewtopic.php?f=38&t=3015