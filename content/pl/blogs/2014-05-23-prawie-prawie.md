{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-05-23T14:15:42+00:00",
   "title": "Prawie, prawie…",
   "type": "post",
   "url": "/2014/prawie-prawie/"
}
Wydanie 0.30.0 zajmuje zdecydowanie więcej czasu niż powinno. Nie traktuję was jak głupców: zakładam, że domyślacie się trudności jakie musieliśmy napotkać po drodze. Wiedzcie jednak, że poradziliśmy sobie. Wymagało to co prawda hotfixa dla OpenCS, ale to już detal większego obrazu: OpenMW 0.30.0 nadciąga nieubłaganie, bezlitośnie miażdżąc…

Ah, wybaczcie, tak naprawdę chciałem pisać o scrawlu i jego iście heroicznych bojach z bugami OpenMW, aktualnie występującymi w zdecydowanym nadmiarze. Scrawl był bardzo aktywny i rozwiązał ich całe tuziny, zaciekle szarżując na pozycje… 

Może jednak zostawmy scrawl w spokoju. Greye również pomógł nam uporać się z istotnym problemami, występującymi jednak jedynie na platformie windows. Nie wnikając w szczegóły były one powiązane z kodowaniem znaków ścieżek plików – a w samej swej ogólnej postaci wynikają z niedostatków testowania. Faktycznie, windows sprawia nam problemy: większość programistów pracuje na systemach Linux, a co za tym idzie nie bardzo wiedzą czy kod który piszą oby na pewno działa poprawnie na innych systemach. Wspomniany wcześniej hotfix również adresowany był do problemu występującego na windowsie – i zapewniam was, że nie był to problem trudny do zauważenia. Naprawienie pewnie też nie było trudne, ale znalezienie osoby zdolnej do wprowadzenia poprawki już tak. Uratował nas cc9cii.

Naprawdę: potrzebujemy więcej testerów skorych do raportowania błędów. Jeśli chcesz nam pomóc, ale do tej pory nie wiedziałeś jak: oto odpowiedź.

Kończąc wpis chciałbym również wspomnieć o naszej ukochanej AI. Strażnicy potrafią już atakować stworzenia, dzięki czemu w razie gdyby gonił was skrzekacz możecie w końcu szukać schronienia w mieście. Zabawne, że strażnicy nie są zainteresowani walką z jawnie łamiącymi prawo osobnikami jak choćby zabójcy mrocznego bractwa.