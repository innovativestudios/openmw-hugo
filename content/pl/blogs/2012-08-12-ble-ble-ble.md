{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-08-12T14:26:45+00:00",
   "title": "ble ble ble",
   "type": "post",
   "url": "/2012/ble-ble-ble/"
}
Nowe forum OpenMW ma dokładnie rok. Drobna rocznica, wagi rzec by się chciało piórkowej, ale jednak rocznica, a każde tego typu święto powinno być przede wszystkim okazją by wspomnieć minione wydarzenia.

Stare forum OpenMW przeżyło atak wandala, inwazję spambotów reklamujących ruskie porno, viagrę niewiadomego pochodzenia oraz inne produkty podobnego sortu, a na dodatek działało z szybkością otyłego ślimaka o słabym sercu. Ostatecznie admin porzucił projekt, do którego to wniosku doszliśmy po miesiącu naszych nieudanych prób kontaktu. 

Lgro zawodowo nie jest administratorem, ale zapewniam was, że profesjonalizmu mu nie brakuje. Postawił nowe forum, przeniósł tematy ze starego forum do archiwum (ręcznie), a na dodatek odzyskał znaczną część wiki (która to padła znacznie wcześniej). Ostatecznie OpenMW zyskało nową domenę, nową stronę i wszystko działało jak w zegarku do czasu gdy serwery nie wytrzymały obciążenia po niesławnym artykule na slashdot. Lgro jednak rozwiązał i ten problem, tak, że teraz możemy śmiało powiedzieć: Lgro, jesteś wielki!

A stare forum&#8230; Obumarłe i suche niczym wylinka ale wciąż obecne w internecie z tego tylko powodu, że nie ma osoby która byłaby w stanie je ostatecznie zamknąć.

Wracając jednak do wydarzeń bieżących, bo to przecież one i tylko one stanowią krew odżywiającą wszelkie tkanki tego projektu &#8212; napędzają nasze marzenia i plany.

Ci spośród was którzy używają dystrybucji Gentoo wiedzą zapewne o istnieniu overlay autorstwa Edmondo, ale jeśli próbowaliście zainstalować ostatnio w ten sposób OpenMW najpewniej spotkało was rozczarowanie. Od kiedy (szeroko opisywany na łamach tego bloga) nowy system shaderów został włączony do głównej gałęzi powróciliśmy, przynajmniej tymczasowo, do submodułów git. Na szczęście Edmondo wciąż jest gotów służyć pomocą i poprawkami więc, overlay powinien znowu działać jak należy.

Eli2 kontynuuje prace nad edytorem: auto uzupełnianie zostało dodane, filtry mogą być także wczytywane z plików. Dodatkowo do zadania przyłączył się Lgro: zaczął od wprowadzenia kilku poprawek oraz umożliwienia wykorzystania innych kodowań znaków. Mam cichą nadzieję, że teraz edytor naprawdę wystartuje &#8212; nadzieję może i cichą ale jednak artykułowaną bo jak pokazują doświadczenia szczypta oczekiwań działa mobilizująco. ;-)

greye jest na półmetku zadania „player control”: na zaimplementowanie wciąż czekają węzły kamery, a tymczasem programista zajął się naprawianiem błędów w funkcji przekraczania granic obszaru.

Corristo naprawił Ogre3D dla OSX, dzięki czemu kursor menu pod tym systemem powinien poruszać się z jednakową prędkością niezależnie od liczby FPS. 

Gus kontynuuje swoją pracę z poprzedniego tygodnia.

Scrawl tymczasem dodał podstawowe menu główne wyświetlane po naciśnięciu klawisza esc. Zawiera jedynie trzy przyciski: Return, Options, Quit.

Jhooks1 wprowadził nową metodę wykrywania kolizji o której pisałem tydzień temu także dla stworzeń.

Zini znowu refaktoryzuje kod, tym razem w mw-subsystem. Ktoś to MUSI robić.

Jeśli śledzicie OpenMW wystarczająco starannie obrazek taki jak <a href="https://dl.dropbox.com/u/2899105/127.png" title="ten" target="https://dl.dropbox.com/u/2899105/127.png">ten</a> może wydawać się znajomy. Otóż jak widać właśnie mergujemy wszystkie gotowe gałęzie co stanowi preludium do&#8230; tak! OpenMW 0.17.0. Co prawda nie wiele będzie nowych „gameplayowych” funkcji ale jednak wprowadziliśmy daleko idące usprawnienia w czeluściach silnika więc intensywna seria testów jest bardzo potrzebna, a każdy z was może nam pomóc raportując napotkane błędy.

Dzięki wykorzystaniu biblioteki shiny poszerzyliśmy zakres sprzętu na którym działa gra, nowe system animacji jest o wiele wydajniejszy niż stary. 

Cóż, czy muszę jeszcze zachęcać?