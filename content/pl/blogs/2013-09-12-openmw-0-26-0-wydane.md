{
   "author": "sir_herrbatka",
   "categories": [
      "Nowa wersja"
   ],
   "date": "2013-09-11T15:05:31+00:00",
   "title": "OpenMW 0.26.0 wydane!",
   "type": "post",
   "url": "/2013/openmw-0-26-0-wydane/"
}
Co w tym tygodniu?

To słabe…

To nieciekawe…

Tego nie rozumiem…

A to? Tak, to będzie dobre!

Panie i panowie! Chłopcy i dziewczęta! O to, w całej swej krasie OpenMW 0.26.0! Zalecam pobranie paczek na stronie [do ściągnięcia][1].

Zalecam jednak ostrożność. OpenMW 0.26.0 to najbardziej brütalne wydanie w historii projektu, otwiera możliwość zadawania bólu, cierpienia i śmierci z wykorzystaniem broni białej; wilkołactwo; mechanizm tonięcia. Wideo nagrane przez jak zwykle fantastycznego WeirdSexy oddaje to w całej, okrutnej i przerażającej głębi.



**Inne rzecz na które należy zwrócić uwagę:**

  * W trakcie przechodzenia pomiędzy komórkami niektórzy gracze obserwują drżenie ekranu. W celu obejścia problemu polecamy włączenie antyaliasingu.
  * Polska wersja językowa morrowinda nadal wywołuje niespodziewane zakończenie OpenMW (lepiej znane jako &#8220;crash&#8221;).

**Changelog:**

  * Implemented Melee Combat
  * Implemented Lycanthropy
  * Implemented auto-initialization of AI Packages
  * Implemented Drowning
  * Implemented Data File installation in the Launcher (Mac and Linux only)
  * Implemented mouse wheel transitions between first/third person
  * Improvements to UI window sizing and input
  * Improvements to sliders in the enchanting window
  * Fixed already-dead NPCs not equipping clothing/items
  * Fixed missing terrain in Tamriel Rebuilt, South of Tel Oren
  * Fixed health calculation of NPCs, fixing Heart of Lorkhan acting like a dead body
  * Fixed strange behavior when leaving water
  * Fixed terrain rendering to more accurately emulate vanilla Morrowind
  * Fixed no clip players causing NPCs to leviate
  * Fixed door open/close sound not cutting itself
  * Fixed no clip player preventing doors from opening
  * Fixed keypress during startup freezing the game
  * Fixed combat magic stances being available prematurely during character creation
  * Fixed naked Dark Brotherhood assassins
  * Fixed Rest dialog showing &#8220;Until Healed&#8221; option when player has full stats
  * Fixed invisible equipped torches
  * Fixed sheath weapon sound playing when leaving magic stance
  * Fixed some boots not producing footstep sounds
  * Fixed FPS bar alignment
  * Fixed printscreen not working
  * Fixed being able to jump while sneaking
  * Fixed blank movie variables in Morrowind.ini crashing the game
  * Fixed dancing girls in &#8220;Suran, Desele&#8217;s House of Earthly Delights&#8221; so they dance
  * Fixed repeating idle animations
  * Fixed sticking to certain surfaces, improves movement when swimming close to collidable objects
  * Fixed animation problem while swimming at the surface of water and looking up
  * Fixed capitalization of names in inventory
  * Fixed an issue with the spell effect area layout not updating properly
  * Fixed a Tamriel Rebuilt crash on load
  * Fixed rain sound persisting into new games
  * Various compilation fixes

 [1]: https://openmw.org/downloads/