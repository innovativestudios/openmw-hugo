{
   "author": "sir_herrbatka",
   "date": "2013-03-05T14:30:07+00:00",
   "title": "wiadomości",
   "type": "post",
   "url": "/2013/wiadomosci/"
}
Cześć! 

<p style="text-align: center;">
  <img loading="lazy" class="aligncenter" alt="FSM" src="https://dl.dropbox.com/u/2899105/zrzut%20ekranu30.png" width="496" height="293" />
</p>

Github potrafi przedstawiać takie o to właśnie wykresy, których niezaprzeczalną zaletą jest prezentowanie ogromnej ilości informacji w sposób kojarzący się z praktykami voodoo co z kolei gwarantuje dla osoby potrafiącej odczytać takie spaghetti szacunek godny najwyższego kapłana.

Co by tu dużo mówić, widać już na pierwszy rzut oka, że rozwiązanych zostało mrowie błędów których nie chce mi nawet się wyliczać, całe szczęście że oprócz tego są też ciekawsze nowiny.

Na dobry początek: OpenMW właśnie ma kolejną, drobniutką funkcję. Przyciski po kliknięciu wydają już znajomy dźwięk. Ciekawi mnie cz zauważyliście właściwie, ten brak?

A czy zauważyliście, że OpenMW nie potrafiło odtwarzać sampli które nie są 16 bitowe? Nie? Wasza szansa przepadła bo Chris właśnie to naprawił. Poza tym od teraz wspieramy także tekstury bmp, choć niech Bóg świadkiem wcale tego nie chcemy. BMP to najgorszy możliwy format dla gier komputerowych, ale niestety Bethesda przeszło dekadę temu zaimplementowała obsługę tekstur BMP w Morrowind; przypuszczalnie sądząc, że będzie to zabawne. Skutkiem tego są mody które wykorzystują bitmapowe tekstury. Wizard Island swojego czasu doprowadzał graczy do zgrzytania zębami z powodu szalonych wymagań sprzętowych, jeśli nadal się zastanawiacie nad przyczyną tej nędzy to podaję ją wam na tacy niczym głowę Jana Chrzciciela. Co gorsza Tamriel Rebuilt także wykorzystuje tekstury bmp, choć w mniejszym stopniu. Tak czy inaczej mam dwa komunikaty. Do Bethesdy: to nie było zabawne. Do TR: 

<p style="text-align: center;">
  <img loading="lazy" class="aligncenter" alt="Yes, that's right. If you are using BMP textures for your mod you are a bad person." src="https://i0.wp.com/weknowmemes.com/generator/uploads/generated/g1362230410871025715.jpg?resize=320%2C240" width="320" height="240" data-recalc-dims="1" />
</p>

Poza tym: scrawl podpalił Atronachy! Od teraz potwory mogą emitować cząsteczki takie jak płomienie Atronachów ognia.

Prace nad OpenCS również posuwają się do przodu.

Ale to jeszcze nic. Hitem jest fakt zmergowania gałęzi z animacjami do master. Od teraz możemy już pracować nad AI. 

[WorldSynth][1]. Znacie? To narzędzie do generowanie terenu do pliku esp z pliku PNG. Narzędzie jest napisane w pythonie i w przyszłości może stać się pluginem do naszego edytora.

 [1]: http://www.mindwerks.net/2013/02/introducing-worldsynth-for-your-world-building-needs/ "WorldSynth"