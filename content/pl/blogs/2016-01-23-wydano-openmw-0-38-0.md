{
   "author": "Vedyimyn",
   "categories": [
      "Nowa wersja"
   ],
   "date": "2016-01-23T13:11:22+00:00",
   "title": "Wydano OpenMW 0.38.0",
   "type": "post",
   "url": "/2016/wydano-openmw-0-38-0/"
}
Zespół OpenMW ma zaszczyt ogłosić wydanie wersji 0.38.0! Do pobrania, jak zwykle, [tutaj][1], dla wszystkich systemów operacyjnych. Depcząc po piętach poprzedniego, to wydanie przynosi wiele poprawek i usprawnień, szczególnie ruch obiektów pomiędzy komórkami oraz wsparcie dla formatu modeli natywnego dla OSG.

**Znane problemy:**

  * nie zaimplementowano cieni, wyświetlania odległego lądu (distant land) i shaderów obiektów (object shaders),
  * w OpenMW-CS zaimplementowano tylko podstawowe sposoby kontroli kamery, brakuje renderowania siatki ścieżek NPC i znaczników komórek, podobnie jak poruszania instancjami obiektów.

## Skrócona lista zmian w OpenMW:

**Dodano:**

  * pole widzenia wilkołaka,
  * poruszanie się obiektów między komórkami, co naprawia kilka błędów,
  * obsługę modeli w formacie natywnym dla OSG,
  * powiadomienie, gdy gra jest zapisywana.

**Naprawiono:**

  * obiekt gracza był usuwalny,
  * sprawdzanie dystansu przez AiCombat bierze pod uwagę bryłę kolizji,
  * piła w Kopule Kasji nie raniła stacjonarnego gracza,
  * szczury lewitowały, gdy zabite blisko drzwi,
  * źródło głosu NPC nie było umieszczone w głowie,
  * instalator OpenMW czasem się wysypywał,
  * zaklęcie absorpcji działa tylko raz na efekt,
  * zaklęte przedmioty przetasowywały się za każdym razem, gdy klikano opcję „Handel”,
  * nie można było wskrzesić gracza poprzez konsolę, jeśli zdrowie miało wartość zerową,
  * naprawiono zachowanie broni dystansowych pod wodą,
  * przedmioty pod wodą nie tonęły,
  * dialog nie działał poprawnie w modzie _Julan Ashlander Companion_,
  * strażnicy przyjmowali grzywnę nawet, gdy gracz nie posiadał odpowiedniej kwoty,
  * źródło dźwięky gracza było w stopach,
  * naprawiono usterki renderowania widoku FPP przy dalekim polu widzenia,
  * gra wysypywała się przy wyborze klasy postaci (przy zaczynaniu gry),
  * brak tekstur dziennika jeśli nie zainstalowano dodatków Trójca i Przepowiednia,
  * dialogi ze stworzeniami działały niepoprawnie,
  * naprawiono wysypywanie się gry, gdy wchodzi się do klasztoru Holamayan z zainstalowanymi inny mi siatkami (mesh replacer),
  * śniący pojawiali się zbyt wcześnie,
  * polecenia następna/poprzednia broń/zaklęcie oraz Przygotuj zaklęcie działały w postaci wilkołaka,
  * wyeliminowano możliwość złapania duszy stwora więcej niż raz,
  * przyzwane stwory i przedmioty znikały o północy.

## Lista zmian OpenMW-CS:

**Dodano:**

  * możliwość wyborów kolorów do podświetlania składni,
  * ukrywanie listy błędów w skrypcie, gdy nie ma żadnego,
  * możliwość tworzenia wtyczek (_omwaddons_) z kropkami w nazwie pliku,
  * możliwość zmiany rozmiaru panelu błędów skryptu,
  * dodano dymki z podpowiedziami w trójwymiarowym podglądzie sceny.

**Naprawiono:**

  * problem z obsługę DELE subrecords,
  * funkcja _Verify_ nie sprawdzała, czy w kontenerze istnieje obiekt o danym ID.

Pełną listę zmian (po angielsku), wraz z możliwością komentowania, można znaleźć [tutaj][2].

 [1]: https://openmw.org/downloads/
 [2]: https://forum.openmw.org/viewtopic.php?f=38&t=3338