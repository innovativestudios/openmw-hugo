{
   "author": "Vedyimyn",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2014-10-27T18:39:38+00:00",
   "title": "Gotujcież się na wersyję trzydzieści i trzy",
   "type": "post",
   "url": "/2014/gotujciez-sie-na-wersyje-trzydziesci-trzy/"
}
Jest już włączona blokada dla wersji 0.33.0, co oznacza, że nowe wydanie jest bliskie. Wraz z wydaniem pojawi się pełna lista zmian. Jeśli śledzicie „Wydarzenia”, to o wielu z nich pewnie już wiecie. Przyjrzyjmy się jednak, co jeszcze nowego się pojawi.

16 funkcji zostało w pełni zaimplementowanych w tym wydaniu („features” na trackerze), w tym 11 dotyczy OpenCS. Liczby te są stosunkowo małe, ale proszę nie sądzić, że to oznaka spowolnienia prac nad OpenMW. Brakujące funkcje spoza edytora są po prostu nieliczne. Większość zmian to bugfixy.

Poprawiono mechanizm rzucania zaklęć przez stwory niedwunożne. To oznacza, że zarówno Dremora jak i Pełzacz będą używać na Tobie swoich zaklęć, jeśli uznają to za stosowne.

Użytkownicy zgłaszali ostatnio, że filmiki są odtwarzane bez dźwięku. To dlatego, że program używany przez OpenMW do ich wyświetlania, ffmpeg, nieco zmienił swoją wewnętrzną strukturę po ostatnich aktualizacjach, co zaskutkowało problemem w odtwarzaniu filmów z gry. Zdecydowaliśmy się na inny sposób obsługi dźwięków, co zaowocuje niedługo naprawieniem niedogodności i powrotem pełnej przyjemności z oglądania.

Poprawiono kompatybilność z masą kolejnych modów. BTB wprowadza wiele znaczących zmian w funkcjonowaniu Morrowinda, także dotyczących zaklęć. W ostatnich wersjach zaklęcia przezeń wprowadzane były dodawane do zbioru czarów, jednakże intencją BTB było zastąpienie niektórych oryginalnych. Teraz OpenMW będzie najpierw stosownie czyścił listę zaklęć przed dodawaniem magii z BTB. Dalej: Naprawiono problem z jedną z lokacji z Tamriel Rebuilt, powodujący „wywalenie się” gry. Poza tym, niektóre lokacje z TR były bezimienne (w edytorze nie było ich nazw, wiele questów się przez to gubiło), co również zostało poprawione.

Ruszyła praca nad 0.34.0, ale o tym kiedy indziej. 0.33.0 jest bardzo blisko, więc jeśli chcesz, zajrzyj wkrótce na bloga i/lub zacznij sprawdzać codziennie menedżer pakietów.

Zapraszamy do komentowania [tutaj][1]!

 [1]: https://forum.openmw.org/viewtopic.php?f=38&t=2538