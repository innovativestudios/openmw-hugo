{
   "author": "Vedyimyn",
   "categories": [
      "Uncategorized"
   ],
   "date": "2015-10-14T18:22:03+00:00",
   "title": "Parę nowości",
   "type": "post",
   "url": "/2015/pare-nowosci/"
}
Witajcie! Parę informacji, bo dawno żadnych nie było. Przejście na OSG wymagało sporego nakładu pracy. Sporo z nich jest mało spektakularnych (chyba, że ktoś jest programistą i chce zaglądać głębiej w rozwój projektu), toteż nie doczekały się posta o sobie. Pierwsza nowość: w OpenMW-CS można teraz wybierać obiekt z podglądu sceny. Sceny mogą zawierać wiele obiektów, stąd ich wyszukiwanie na liście bywało kłopotliwe. Nad tą funkcją pracowali Scrawl i Zini. Da się też wybierać elementy nierenderowane, np. ścieżki i markery. Nie da się ich jeszcze przesuwać &#8211; to zadanie na inną aktualizację.

Jeśli nie jesteś moderem, jedynie grasz w tę grę, ucieszy Cię druga część newsów: Scrawl przywrócił słoneczne rozbłyski! Poniżej obrazki „przed” i „po”:

[<img src="https://i0.wp.com/i.imgur.com/mVxl6WZ.png?w=300" alt="" data-recalc-dims="1" />][1] [<img src="https://i0.wp.com/i.imgur.com/oU1zKrY.png?w=300" alt="" data-recalc-dims="1" />][2]

Słońce nie jest już płaskim dyskiem na niebie &#8211; pięknie rozmazuje się na ekranie. Śliczności!

Każdy krok w kierunku upiększenia gry jest dobrym krokiem, toteż drużyna (szczególnie magik od OSG i shaderów &#8211; Scrawl) planuje zdziałać dużo więcej! Do zrobienia są shadery wody i obiektów. Te ostatnie mogą być trudne nieco trudniejsze do wykonania. Nie da się ich przenieść z istniejącej implementacji bez utraty wydajności. Gdy shadery obiektów będą zrobione, będzie można pracować nad ich cieniami. Może nie wydaje się to być rzeczą bardzo znaczącą, ale robi niesamowitą różnicę. W późniejszych wersjach będzie można powiedzieć więcej na ten temat.

Pozbywanie się bugów jest, oczywiście, również istotną sprawą, bez której nie ma kolejnych wydań OpenMW. Licząc tylko błędy z trackera, usunięto ich 149 (niektóre są usuwane zaraz po znalezieniu, wtedy tam nie trafiają). Pozostałe 40 błędów z trackera, które zamknięto, to to drobne optymalizacje, skalowalne GUI, różne funkcje OpenMW-CS.

Do wydania wersji 1.0 OpenMW zostało mniej niż 100 problemów (issues) do załatwienia. Oczywiście są to sprawy różnych rozmiarów &#8211; jedne są małe, inne ogromne. Ogólnie jednak, linia mety zarysowuje się coraz bliżej. Nie zamierzam kłamać: portowanie jest rzeczą trudną, i finisz nie jest kwestią tygodnia/dwóch. Ekipa pracuje ciężko, starając się w następnym wydaniu zawrzeć optymalizację terenu i podstawowy shader wody &#8211; dla Waszej uciechy.

Czas od poprzedniego wydania wydaje się być długi, ale wiedzcie, że drużyna wytrwale pracuje, a ta praca ma zaowocować jednymi z największych, z punktu widzenia graczy, zmian od implementacji tekstur i fizyki. Dziękujemy za cierpliwość i do zobaczenia następnym razem.

**Zapraszamy do komentowania [tutaj][3].**

 [1]: https://i0.wp.com/i.imgur.com/mVxl6WZ.png
 [2]: https://i0.wp.com/i.imgur.com/oU1zKrY.png
 [3]: https://forum.openmw.org/viewtopic.php?f=38&t=3131