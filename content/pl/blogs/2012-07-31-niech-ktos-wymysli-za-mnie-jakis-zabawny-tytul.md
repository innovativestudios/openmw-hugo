{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-07-31T12:39:50+00:00",
   "title": "Niech ktoś wymyśli za mnie jakiś zabawny tytuł.",
   "type": "post",
   "url": "/2012/niech-ktos-wymysli-za-mnie-jakis-zabawny-tytul/"
}
Za nami bardzo udany tydzień, obfitujący w osiągnięcia będące podsumowaniem wytężonej pracy zespołu programistów, której (co bardzo istotne) początek datuje się całe miesiące temu. Gdy jednak do pokonania jest tak długa droga potrzeba wiele czasu by dojść do upragnionego celu. Ta prosta zasada w żadnym stopniu nie umniejsza sukcesu &#8212; wręcz przeciwnie, nadaje mu drogocennego blasku.

Innymi słowy dotarliśmy nareszcie do finału Odysei znanej lepiej jako „OGRE3D animuje modele nif”.

Czy muszę jeszcze przypominać jak wiele pracy trzeba było włożyć w tą funkcję? Nie wystarczył jeden deweloper: potrzeba było połączonych sił jhooks1 i chrisa by dojść do obecnego punktu. Teraz owoce tego trudu oczekują w gałęzi main na śmiałków chcących przeprowadzić testy.

Dla osób nie w temacie: co prawda OpenMW miał animacje od dłuższego czasu, ale system opierał się na ręcznym manipulowaniu szkieletami modeli &#8212; nie na dobrodziejstwach jakie niesie ze sobą biblioteka OGRE3D. Stara metoda była więc zupełnie nie efektywna, a na domiar złego nie wszystkie stworzenia mogliśmy animować poprawnie &#8212; przykładowo animacja wilków dodanych w Bloodmoonie była kompletnie schrzaniona.

Zostawiliśmy te problemy za sobą. Animacje wyglądają prawidłowo, a wydajność nie wypada wcale beznadziejnie; nawet pomimo braku hardware skinningu. Pełen sukces!

Jeśli wam mało to mam dla was kolejną bombę: nowy, ulepszony system shaderów również wylądował w gałęzi main. Co prawda po dziś dzień nie pofatygowałem się wyjaśnić o co tu w zasadzie chodzi, ale teraz postaram się to nadrobić.

Do tej pory powszechnie używa się trzech języków do pisania shaderów: HLSL (wynalazek MS, tylko DX), GLSL (tylko OpenGL) oraz CG (od nVidia, DX oraz OpenGL). Ułomność sterowników OpenGL dla radeonów to fakt powszechnie znany, a deweloperzy gier którzy chcą by ich produkty sprawnie działały u każdego gracza muszą wspierać DX. Oczywiście nie jesteśmy wyjątkiem, tyle tylko, że my nie możemy zrezygnować z OpenGL, a pisanie wszystkich shaderów w dwóch językach równolegle to znaczące utrudnienie.

Wydawać by się mogło, że rozwiązaniem jest nVidia CG. Można używać go razem z DirectX i OpenGL, i to na karcie każdego producenta &#8212; fajnie no nie? 

No właśnie nie! 

Po pierwsze: nVidia CG to własnościowy, zamknięty toolkit dostępny na Linux, Windows, OSX ale już nie na BSD, haiku i inne systemy. Po drugie: jak się okazało nVidia CG działa naprawdę dobrze na kartach (niespodzianka!) nVidii (oddychaj! Wiemy, że jesteś w szoku) i znacznie gorzej na produktach konkurencji. Oprócz tego CG ma inne wady które ujawniły się aż nazbyt wyraźnie, bo niestety ale używaliśmy CG.

Na szczęście scrawl znalazł wyjście z tego bagna.

Biblioteka shiny pozwala, zależnie od potrzeby, specyfikacji sprzętowej oraz platformy skompilować shader napisany w specjalnym meta-języku zarówno jako GLSL jak i HLSL. Już sama ta możliwość jest ogromną pomocą, a zalety są szersze:

> &#8211; High-level layer on top of OGRE&#8217;s material system. It allows you to generate multiple techniques for all your materials from a set of high-level per-material properties.
> 
> &#8211; Several available Macros in shader source files. Just a few examples of the possibilities: binding OGRE auto constants, binding uniforms to material properties, foreach loops (repeat shader source a given number of times), retrieving per-material properties in an #if condition, automatic packing for vertex to fragment passthroughs. These macros allow you to generate even very complex shaders (for example the Ogre::Terrain shader) without assembling them in C++ code. 
> 
> &#8211; Integrated preprocessor (no, I didn&#8217;t reinvent the wheel, I used boost::wave which turned out to be an excellent choice) that allows me to blend out macros that shouldn&#8217;t be in use because e.g. the shader permutation doesn&#8217;t need this specific feature.
> 
> &#8211; User settings integration. They can be set by a C++ interface and retrieved through a macro in shader files.
> 
> &#8211; Automatic handling of shader permutations, i.e. shaders are shared between materials in a smart way. 
> 
> &#8211; An optional &#8220;meta-language&#8221; (well, actually it&#8217;s just a small header with some conditional defines) that you may use to compile the same shader source for different target languages. If you don&#8217;t like it, you can still code in GLSL / CG etc separately. You can also switch between the languages at runtime.
> 
> &#8211; On-demand material and shader creation. It uses Ogre&#8217;s material listener to compile the shaders as soon as they are needed for rendering, and not earlier.
> 
> &#8211; Shader changes are fully dynamic and real-time. Changing a user setting will recompile all shaders affected by this setting when they are next needed.
> 
> &#8211; Serialization system that extends Ogre&#8217;s material script system, it uses Ogre&#8217;s script parser, but also adds some additional properties that are not available in Ogre&#8217;s material system. 
> 
> &#8211; A concept called &#8220;Configuration&#8221; allowing you to create a different set of your shaders, doing the same thing except for some minor differences: the properties that are overridden by the active configuration. Possible uses for this are using simpler shaders (no shadows, no fog etc) when rendering for example realtime reflections or a minimap. You can easily switch between configurations by changing the active Ogre material scheme (for example on a viewport level).
> 
> &#8211; Fixed function support. You can globally enable or disable shaders at any time, and for texture units you can specify if they&#8217;re only needed for the shader-based path (e.g. normal maps) or if they should also be created in the fixed function path. 

Biblioteka zdaje się mieć przed sobą świetlaną przyszłość, być może nie tylko w ramach projektu OpenMW.

To, oczywiście, jeszcze nie koniec dobrych wieści&#8230;

Do zespołu dołączył nowy programista kryjący się pod nickiem \_greye i już na starcie wykazał się sprawnością i determinacją, szybko radząc sobie z pierwszym zadaniem: CreatureStats w oddzielnej klasie. Zajęło mu to ledwie dzień, a gdy Zini wyszukał nowe, zdecydowanie większe zadanie (uporządkowanie kodu odpowiadającego za upuszczanie przedmiotów) \_greye jedynie zakasał rękawy i wziął się znowu do pracy. Po kilku dniach ukończył zadanie. 

Efekty wysiłków _greye są już w gałęzi master.

Oprócz tego w zespole jest jeszcze jeden nowy programista. nhydock już pracuje nad zadaniem „camera control”.

Zini wciąż nie odszedł daleko od napojów magicznych, a dzięki kodowi dodanemu w tym tygodniu wypicie mikstury wreszcie wiąże się z odtworzeniem odpowiedniego dźwięku (siorbnięcia?). Oprócz tego Zini zrefaktoryzował klasę Action.

Corristo naprawił błędy występujące jedynie na platformie OSX, więc także ci spośród was którzy używają komputerów z logiem jabłka mogą „doświadczyć” OpenMW.

Łukasz, od czasu slashdot miał mniej czasu by bawić się swoim vamp-botem na kanale IRC, gdyż przenosiny na nowy serwer wymagały znalezienia oferty, która byłaby dla nas odpowiednia. Zadanie to samo w sobie niewdzięczne, stanowi jedynie przedsmak wyzwań stojących przed administratorem. Tak czy owak aktualnie OpenMW.org hostowane jest na mydevil.net, zaś bugtracker oraz forum na serwerze OVH. Dodatkową ochronę przed hordami odwiedzających zapewnia „proxy” Cloudflare CDN.