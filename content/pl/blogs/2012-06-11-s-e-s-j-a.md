{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-06-11T12:37:25+00:00",
   "title": "S E S J A :(",
   "type": "post",
   "url": "/2012/s-e-s-j-a/"
}
I stało się tak, że Mark, który niegdyś czynił cuda w swoim projekcie &#8220;Crystal Scrolls&#8221; dołączył do OpenMW z postanowieniem pracy nad wsparciem dla funkcji która przyczyniła się jak żadna inna do sukcesu Morrowinda i wszystkich następny gier serii wydanych na PC. Dopiero teraz jednak jego kod wylądował na githubie.

Funkcja jest prawie gotowa, i działa ale wciąż pozostało wiele pracy. Przykładowo łączenie dialogów wydaje się skomplikowane. Bloodmoon i tribunal mają także rozszerzony zakres instrukcji w skryptach co oznacza, że zini musi wrócić do pracy nad skryptami.

I wrócił. Włączenie nowej gałęzi jest samo w sobie potencjalnie trudne, a do tego dochodzą nowe zadania otwarte przez Mark76 ale skrypty są co najmniej równie ważne.

Garvek to nick nowego dewelopera który dołączył do nas po tym gdy DugeonHack musiał zmienić swój kształt. Wcześniej pracowali nad własnym silnikiem, w wyniku kryzysu w zespole zdecydowali się wykorzystać nasz, gdy będzie już oczywiście gotowy. Jak na razie zaczął od poprawek do naszego cmake oraz sprawieniu, że w końcu znowu można skompilować OpenMW za pomocą MSVC&#8230; przynajmniej przez jakiś czas.

Jhooks1 pracuje nad systemem fizyki w grze. Nic aż tak spektakularnego, a na domiar złego prace nad nowym systemem animacji utkwiły w martwym punkcie gdyż wciąż nie jesteśmy w stanie animować szat noszonych przez postacie oraz niektórych stworzeń.

Scrawl postanowił przepisać system shaderów tak by umożliwić pisanie ich w glsl. Ma powstać biblioteka podobna do RTShaderSystem dla Ogre ale znaczenia lepsza. 

Edmondo przygotował gentoo overlay dla openmw. Aktualnie jest w wersji alfa i jak sądzę pora na testy!