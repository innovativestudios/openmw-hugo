{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-01-20T08:29:52+00:00",
   "openid_comments": [
      "a:2:{i:0;i:517;i:1;i:521;}"
   ],
   "title": "Wydanie 0.28.0? Jakież to nudne!",
   "type": "post",
   "url": "/2014/3391/"
}
Witajcie ponownie!

Wydanie 0.28.0 okazało się być niezwykle udane, przynajmniej jeśli za miarę sukcesu uznamy ilość obejrzeń filmu na youtube, gdyż licznik odtworzeń zdążył już przekroczyć barierę piętnastu tysięcy. Nic w tym dziwnego ─ 0.28.0 przynosi dużo; długo oczekiwanych nowości i w sposób wyraźny zbliża nas do wersji 1.0.0. Bieżące zmiany są jednakże co najmniej równie interesujące.

W tym tygodniu, zgodnie z tradycją; obyczajem i dotychczasową statystyką, zdecydowanie najaktywniejszym programistą był scrawl. Dzięki jego pracy możemy już potężnym ciosem przewrócić przeciwnika (lub sami upaść w wyniku skutecznie wyprowadzonego ataku), użyć efektu magicznego &#8220;Powolne opadanie&#8221; lub podziwiać krew lejącą się z przeciwnika po udanym ataku (te śmieszne, małe, czerwone obłoczki widoczne po trafieniu bronią).

Tymczasem zini zakończył prace nad wstępem do zapisywania i ładowania stanu gry. Brakuje oczywiście bardzo wielu elementów ─ choćby jakiegokolwiek interfejsu. Z drugiej jednak strony, wydanie 1.0.0 jest już dość bliskie, a ten fakt ustanawia wyższe priorytety dla zadań powiązanych ze skryptami.

OpenCS doczekał się weryfikatora Referencables (to grupa obejmująca wszystkie obiekty które można umieścić w komórkach) który sam napisałem. Ja mam satysfakcję, a wy narzędzie dzięki któremu już nigdy nie zapomnicie dodać ikony do miecza.

Na pożegnanie, z racji na zaburzenie równowagi przez Piesła, oraz z dedykacją dla Lgro:  
<img loading="lazy" src="https://i0.wp.com/openmw.org/wp-content/uploads/2014/01/grumpy_cat.jpg?resize=400%2C400&#038;ssl=1" alt="grumpy_cat" width="400" height="400" class="aligncenter size-full wp-image-3403" srcset="https://i0.wp.com/openmw.org/wp-content/uploads/2014/01/grumpy_cat.jpg?w=400&ssl=1 400w, https://i0.wp.com/openmw.org/wp-content/uploads/2014/01/grumpy_cat.jpg?resize=150%2C150&ssl=1 150w, https://i0.wp.com/openmw.org/wp-content/uploads/2014/01/grumpy_cat.jpg?resize=300%2C300&ssl=1 300w" sizes="(max-width: 400px) 100vw, 400px" data-recalc-dims="1" />