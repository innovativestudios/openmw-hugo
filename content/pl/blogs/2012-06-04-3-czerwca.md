{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-06-04T12:26:24+00:00",
   "title": "3 czerwca",
   "type": "post",
   "url": "/2012/3-czerwca/"
}
Drodzy czytelnicy!

Sprawy przedstawiają się następująco: jest początek czerwca, a termometr za oknem pokazuje jasno, że temperatura powietrza na zewnątrz wynosi 14 stopni powyżej zera. Otuchy w nasze serca wlewa za to zespół deweloperów OpenMW.

Z tygodnia na tydzień, z dnia na dzień, (a wkrótce pewnie i z godziny na godzinę) coraz bardziej jasnym staje się fakt, że scrawl stał się zasadniczą i jakże potężną częścią siły napędowej projektu. Funkcje związane z GUI miały błyszczeć już w wersjach dawno (i z dużym opóźnieniem!) wydanych ale z racji na słomiany zapał programistów uzasadniony przykrym brakiem dokumentacji oraz wskazówek dla MyGUI, do niedawna nasze GUI wypadało bardzo ubogo. Pierwsze oznaki poprawy sytuacji dostarczył gus, a od kiedy za sprawę wziął się scrawl każdy tydzień przynosi nowości.

<img loading="lazy" class="alignleft" title="Wybierz zaklęcie..." src="https://i0.wp.com/openmw.org/wiki/images/5/5f/016-2.png?resize=643%2C402" alt="1" width="643" height="402" data-recalc-dims="1" />  
<img loading="lazy" class="alignleft" title="Gotuj z Viveciem" src=" http://openmw.org/wiki/images/4/4e/016-3.png" alt="2" width="645" height="403" /> 

Widzimy tu listę zaklęć z której można wybrać zarówno zaklęty przedmiot jak również czar znany postaci, drugi zrzut prezentuje działające okno tworzenia mikstur magicznych. Okno alchemii wygląda nieco inaczej niż oryginalne ale miejcie na uwadze, że używanie go powinno być zdecydowanie mniej frustrujące, bo nie wymaga już tak wielu kliknięć by choćby dodać składniki eliksiru.

Skoro już wspomniałem o gusie, teraz pracuje nad obracaniem i skalowaniem obiektów w grze. Co prawda dodał już instrukcje setange i setscale ale ponieważ wciąż pozostały problemy do rozwiązania nie czas jeszcze na włączenie tej gałęzi do gałęzi głównej.

Tymczasem jhooks1 zauważył, że pmove teoretycznie, przy odpowiedniej szybkości postaci może pozwolić dosłownie wbiec na pionową ścianę. Oczywiście nie jest to zachowanie nawet zbliżone do tego znanego z oryginalnego silnika (co prawda fizyka w morrowind jest momentami naprawdę wykręcona **ale akurat na to** nie pozwala), więc programista postanowił dodać dodatkowy kod sprawdzający czy oby kąt na który wspina się bohater nie jest zbyt duży.

Zini, jak zwykle pilnuje tego bałaganu, sprawdza, nadzoruje i kontroluje by mieć pewność, że nowo dodane funkcje działają dobrze i nie będą sprawiać **nieakceptowalnie** wielkich problemów w przyszłości.

No i na deser: na anglojęzycznej wersji bloga pojawił się wywiad z zinim, scrawlem i ace. W przeciągu tego milenium mam zamiar go przetłumaczyć na nasz język&#8230; Przepraszam was wszystkich ale sesja egzaminacyjna wcale mi nie pomaga.