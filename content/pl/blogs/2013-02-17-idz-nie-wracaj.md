{
   "author": "sir_herrbatka",
   "date": "2013-02-17T14:36:12+00:00",
   "title": "Idź i nie wracaj.",
   "type": "post",
   "url": "/2013/idz-nie-wracaj/"
}
Dzień dobry. Jeśli liczysz na eksplozje błyskotliwego humoru czeka cię zawód jak jamnika zakochanego w amstafie. Przyczyną tego smutnego stanu rzeczy jest moje zmęczenie. Wiedzcie jednak, że z narażeniem życia i zdrowia… 

Do rzeczy.

Na dobry początek, kolejny apetyczny błąd: https://dl.dropbox.com/u/2899105/lol.png

I jeszcze jeden: https://dl.dropbox.com/u/2899105/screenshot001.png

A teraz rzeczy mniej zabawne, ale równie radosne.

Po pierwsze: OpenMW 0.21.0 będzie wydany w przeciągu dni. 

Po drugie: otóż Chris finiszuje. Naprawdę. W OpenMW można już swobodnie biegać; pływać, a niedługo będzie można także skakać. Otwiera to nam możliwość implementacji AI, walki i masy innych funkcji których wciąż nam brakuje.

Po trzecie: dougmencken stworzył gałąź dla tych którzy nie potrafią oprzeć się pokusie i próbują ciasta zanim zostanie włożone do piekarnika. Gałąź nazywa się bigmix i zawiera w sobie wiele funkcji z gałęzi deweloperów. Są też bugi, rzecz jasna.

Przy okazji oczywiści zaprezentuje filmiki co by nie było, że kłamię.





Poza tym pozostali deweloperzy pracują w swoich tradycyjnych dziedzinach: blunted2night ─ dziennik, scrawl ─ grafika, pvdk ─ starter.

Graffy76 tymczasem zajmuje się edytorem, ale nic wielkiego na razie z tego nie wynika.

I to tyle. Przepraszam za ten tryb przyśpieszone, ale naprawdę miałem ciężki dzień.