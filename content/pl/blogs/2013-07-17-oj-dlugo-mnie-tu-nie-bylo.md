{
   "author": "sir_herrbatka",
   "date": "2013-07-17T20:50:23+00:00",
   "title": "Oj długo mnie tu nie było…",
   "type": "post",
   "url": "/2013/oj-dlugo-mnie-tu-nie-bylo/"
}
… ale jestem już z powrotem, czy to się wam podoba czy też nie. Mam nadzieję, że jednak w waszych ciepłych serduszkach znajdzie się choć odrobina sympatii.

Przejdźmy jednak do OpenMW.

Otóż nie dzieje się może specjalnie wiele, ale przynajmniej temat można uznać za ekscytujący. Nie ma wszak absolutnie niczego bardziej zajmującego niż okładanie napotkanych przeciwników po głowie mieczem. Otóż zespół złożony z chrisa, scrawla oraz gusa pracuje raźnie i skutecznie, posuwając się dostojnie w kierunku dodania upragnionej walki wręcz. Aktualnie działają już wszystkie animacje ataków ─ a wydaje się, że był to najbardziej wredny aspekt zagadnienia.

Oczywiście w tej chwili nie ma mowy o AI walki, więc nasi adwersarze nie wykażą się szczególną inicjatywą. Oczywiście nie odbiega to od normalnej rozgrywki w Morrowind…

Poza tym vorenon dodał maleńką, ale słodką funkcję. Od teraz okna pojemników można zamknąć poprzez wciśnięcie klawisza aktywacji (którym domyślnie jest spacja).

Co więcej zespół naprawił kilka błędów ─ ale to oczywiście nic nowego. Ciekawie prezentują się postępy w pracach nad edytorem: mamy już mapę regionów, a na dodatek w oknie edytora pojawiły się ikony.p