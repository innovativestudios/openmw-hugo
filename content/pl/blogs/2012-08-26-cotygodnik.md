{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-08-26T18:58:11+00:00",
   "title": "Cotygodnik.",
   "type": "post",
   "url": "/2012/cotygodnik/"
}
Niestety nowe wydanie wciąż nie jest dostępne między innymi z powodu opóźnionej tar-kulki dla Linux, ale bez obaw &#8212; w przyszłym tygodniu powinno się już udać. Z drugiej strony w sama porę powrócił do nas WeirdSexy, spiker gawędzący w kultowych już filmikach prezentujących nowe wersje OpenMW. Dopiero teraz mogliśmy uświadomić sobie, jak niezbędna jest to osoba &#8212; oczywiście zdawaliśmy sobie sprawę z zasięgu klipów na youtube ale czyj głos może zagwarantować status internetowego hitu? K1ll ze swym pełnokrwistym niemieckim akcentem, ap0 wcielając się w rolę francuskiego kochanka czy też może skusilibyśmy się na Prawdziwy Norweski Black Metalowy Growl w wykonaniu Rebel-Raider? 

Na szczęście los oszczędził nam tego dylematu.

Eli2 nieprzerwanie od niepamiętnych już czasów pracuje nad edytorem ale swą drogą kroczy w pojedynkę &#8212; ciężar wysiłku spoczywa na tylko jego barkach, więc brnie powoli&#8230; Kod edytora wciąż nie dojrzał do stanu w którym więcej osób mogłoby przy nim dłubać ale sam programista stwierdził, że nie obraziłby się gdyby znawca Qt4 przynajmniej dokładnie przejrzał aplikację i udzielił wskazówek. Czyta nas może jakiś rasowy Qt-hacker? 

Nasze demo (znane także jako „Example Suite” &#8212; nazwa godna Monthy Pythona) rozwija się nadspodziewanie dobrze. Greendogo uzyskał pozwolenie na wykorzystanie wysokiej jakości zasobów autorstwa Vurt (drzewa) oraz Westly (rasy). Nasza malutka TC może ostatecznie okazać się przydatna głównie z racji na modele w swoim archiwum i jednoczesnym dostępie za darmo: dzięki temu będzie mogła stanowić podstawę do budowy gier dla autorów którzy chcieliby całkowicie odciąć się od wymogu instalacji Morrowind na dysku. 

Oczywiście mógłbym także napisać o głębi portretów psychologicznych postaci, przemyślanej historii pełnej zaskakujących zwrotów akcji oraz innych walorach naszej produkcji ale nie chcę was okłamywać. W rzeczywistości Example Suite to pod względem fabularnym dzieło klasy B lub C.

Część z was na pewno zainteresuje ostatni pomysł Lgro: serwer jenkins automatycznie budujący paczki z OpenMW Nightly. Dzięki nim będziecie mogli być testować nowinki dopiero co dodane do gałęzi main bez konieczności własnoręcznego kompilowania. 

To tyle. Szkoda, że nie udało się zorganizować wydania na czas &#8212; żal mi szczególnie bo bardzo chciałbym podzielić się z wami planami na najbliższą przyszłość które to, zapewniam was, prezentują się wyjątkowo spektakularnie.