{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-12-31T08:05:47+00:00",
   "openid_comments": [
      "a:1:{i:0;i:478;}"
   ],
   "title": "OpenMW rządzi, OpenMW radzi, OpenMW nigdy cię nie zdradzi!",
   "type": "post",
   "url": "/2013/openmw-rzadzi-openmw-radzi-openmw-nigdy-cie-nie-zdradzi/"
}
Za nami kolejny tydzień ─ znowu bardzo szczęśliwy dla projektu.

Zacznijmy może od wyczynów scrawla. Ostatnio pracował dzielnie na obsługą brakujących typów animacji ─ głównie chodzi tu o… tak, zgadza się; o animacje twarzy mówiących postaci. Efekt tych wysiłków jest z pewnością widoczny, choć być może odbiega jeszcze nieco od ideału.

Przy okazji: strzeżcie się NPC! Od kiedy Lgro wprowadził automatycznie wyliczane statystyki postaci, wysokopoziomowi wojownicy naprawdę potrafią nieźle przywalić. Na kanale IRC słychać już pojękiwania w stylu &#8220;Kobieta mnie bije!&#8221;.

Chris powrócił do pracy nad fizyką w grze, i pozostaje mieć nadzieję, że tym razem uda się przezwyciężyć wszystkie trudności i poprawić naszą implementację. Zini również nie szczędzić trudu, pracując nad zapisem i wczytywaniem stanu gry. Ilość komplikacji napotkanych w trakcie rozwoju tej funkcji przekroczyła już nasze oczekiwania, a sam Zini zdaje się być lekko poirytowany sytuacją.

Poza tym: liczne naprawione błędy. Corristo wprowadził poprawki związane z systemem operacyjnym OSX, scrawl zaś; jak to ma w zwyczaju; zalał nas imponującą ilością bugfixów dotyczących różnorodnych części silnika OpenMW.