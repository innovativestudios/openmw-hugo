{
   "author": "sir_herrbatka",
   "date": "2013-08-27T20:00:17+00:00",
   "title": "Napisałem! Napisałem!",
   "type": "post",
   "url": "/2013/napisalem-napisalem/"
}
Witam wszystkich Czytelników bloga, w zeszłym tygodniu jeden skarżył się na brak działającego wideo we wpisie, a to świadczy niezbicie, że Czytelnicy jednak istnieją, ale po prostu czekają w ukryciu na możliwość ponarzekania. Serce na ten fakt urosło mi tak, że mogłoby zostać z powodzeniem przeszczepione słoniowi i całe moje jestestwo przepełnia te szczególne, słodkie ciepło… Ale do rzeczy.

Herr Zini (dla niewtajemniczonych przywódca, główny programista i dyktator OpenMW) właśnie zabsorbowany jest implementacją funkcji filtrowania w OpenCS. Jest to funkcja bardzo ważna – śmiem nawet twierdzić, że to właśnie wokół niej koncentrować się ma sposób użycia edytora. Oprócz tego nad edytorem pracuje również Graffy i Nomadic1 który przesłał nam kolejną porcję ikon.

Oprócz powyższego, w samym silniku gry również nie brakuje zmian, za sprawą aktywności Scrawla oraz Chrisa. Pierwszy udoskonala kod renderujący teren, co nasuwa mi myśl, że może jednak niedoszacowałem szans na implementację tej funkcji jeszcze w tym roku. Chris zaś zakończył na dzień dzisiejszy swoją przygodę z Bullet – co oznacza mianowicie, że być może już wkrótce animacja skoku zacznie naprawdę działać. Oczywiście ktoś musiałby to jeszcze zaimplementować, ale teraz nie będzie to aż takie trudne.

Na deser zaś, coś specjalnego i nowego. Instalator OpenMW! Choć, mówiąc zupełnie ściśle powinienem raczej napisać &#8220;instalator Morrowinda dla OpenMW&#8221;, chodzi bowiem o aplikację która wypakuje pliki konieczne do gry z płyty, bez konieczności używania do tego oryginalnego instalatora uruchomionego poprzez Wine, lub też ręcznej zabawny z konsolowym unshield. Tym samym, zostanie to docenione głównie przez użytkowników systemów Linux oraz OSX (niegdyś Mac OSX); i, prawdę powiedziawszy, tylko na tych systemach instalator jest w tej chwili dostępny. Użytkownicy windowsa nie muszą instalować wine by skorzystać z instalatora autorstwa Bethesdy, tak więc zdecydowaliśmy, że nie ma podstaw do dostarczania tego programu również na ten system. Jeśli uważasz inaczej, polecam krzyczeć. Najlepiej głośno.