{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-07-22T16:49:08+00:00",
   "title": "Tygodnik OpenMW czyli guar, alit i skrzekacz co niedzielę.",
   "type": "post",
   "url": "/2012/tygodnik-openmw-czyli-guar-alit-skrzekacz-niedziele/"
}
Wpatruję się w miękkie kształty chmur i słucham muzyki z płyty „Kind of Blue”. Czas płynie, tak jak zawsze: strumieniem; górą, dołem i między palcami, ale ja tkwię w miejscu i obserwuję bo na tym polega moja rola. Całe szczęście, że natura obdarzyła mnie inercją &#8212; więc gdy inni pędzą już naprzód ja nawet nie drgnąłem.

Pozostaje mi obserwować i dzielić się obserwacjami.

Widzę Scrawla, który jak zwykle pracuje nad shaderami. W zeszłym tygodniu wspomniałem, że używamy shaderów do renderowania terenu, a dziś mogę podać nową ciekawostkę: w ramach prac nad systemem shaderów powstała nowa implementacja terenu składająca się z o 3/4 mniejszej ilości linii kodu.

Ponadto wszystkie inne obszary, w których używamy shaderów siłą rzeczy również uległy zmianom. Cienie, woda &#8212; akurat tu już teraz mamy czym się pochwalić, i to nie tylko względem leciwego Morrowinda. W przyszłości zapewne uda się poprawić znacznie więcej.

Chris również zdziałał bardzo wiele w tym tygodniu. Na githubie znaleźć możecie świadectwo w postaci chociażby ilości commitów, ale co w istocie najważniejsze i nie oddawane przez gołe liczby to fakt, iż nowy system działa już w grze. Nie zrozumcie mnie źle &#8212; nie jest moim zamiarem tworzeniu fałszywego obrazu sytuacji: aktualnie brak jest integracji z fizyką, a oprócz tego wciąż obecne są drobne problemy które być może są banalnie proste w naprawie gdy już odkryje się ich przyczynę, ale jak pokazuje doświadczenie same znalezienie źródła takich, zdawać by się mogło, błahych kłopotów wymaga trudnej do przewidzenia ilości czasu.

Jednak przede wszystkim chciałbym podkreślić, że zdecydowana większość stworzeń (wliczając bestie spotykane w dodatku Bloodmoon), jak również npc (w tym postacie odziane w kłopotliwe od strony technicznej szaty) jest już animowana poprawnie.

Tym czasem Zini wprowadził do gry dwa efekty magiczne: Drain i Fortify, które zdolne są do modyfikowania statystyk postaci. Dodatkowo usunął też bug w kodzie automatycznie zakładającym zbroje i ubrania na postacie, jak również kilka innych błędów dotykających rozmaitych dziedzin silnika, uporządkował kwestię dynamicznie wyliczanych statystyk postaci i zmergował parę gałęzi.

Odkrywanie matematyki stosowanej przez Morrowinda postępuje dość dobrze. Dzięki Lazaroth przypuszczalnie wiemy już jak wyliczana jest siła efektu magicznego uzyskanego po zjedzeniu składnika, ale pewność uzyskamy dopiero gdy sprawdzimy jeszcze kilka kwestii. Wzory powiązane z akrobatyką są bardziej zagmatwane i wymagają więcej wysiłku, ale BrotherBrick użyczył nam swojego mocarnego serwera by zautomatyzować prace, a oddany sprawie Epsilon już zaciera ręce na myśl o wynikach które otrzyma.