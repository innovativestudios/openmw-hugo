{
   "author": "Vedyimyn",
   "categories": [
      "Uncategorized"
   ],
   "date": "2016-10-20T20:44:47+00:00",
   "title": "Świetlna ciekawostka",
   "type": "post",
   "url": "/2016/swietlna-ciekawostka/"
}
W ostatnim czasie dodano feature dotyczący [nieprzenośnych świateł w ekwipunku][1].

Jeśli nie słyszałeś/aś o tym wcześniej, to zapewne z uwagi na ciekawy fakt, że funkcja ta używana jest tylko raz w całej grze &#8211; przy użyciu Płomienia Prawdy, legendarnego miecza Nerevara Indorila.

[<img loading="lazy" class="alignnone size-medium wp-image-4720" src="https://i0.wp.com/openmw.org/wp-content/uploads/2016/10/N9rFc3P-300x169.jpg?resize=300%2C169&#038;ssl=1" alt="n9rfc3p" width="300" height="169" data-recalc-dims="1" />][2]

Jak możesz zauważyć, posiadanie miecza w aktywnym ekwipunku rozświetla mały obszar wokół gracza. Miecz sam w sobie nie jest źródłem światła. Istnieje skrypt dołączony do niego, który dodaje niewidzialne źródło światła do wyposażenia gracza, wytwarzające poświatę wokół niego.

To dało mi [DestinedToDie] do myślenia. W wielu grach światło otacza gracza w pewnym promieniu. Gdyby ktoś zamierzał zrobić mod lub zupełnie nową grę, mógłby użyć w tym celu opisanej wyżej mechaniki. Spróbowałem. Efekt nie był do końca taki, jak oczekiwałem&#8230;

[<img loading="lazy" class="alignnone size-medium wp-image-4721" src="https://i0.wp.com/openmw.org/wp-content/uploads/2016/10/TvoUoMq-300x169.jpg?resize=300%2C169&#038;ssl=1" alt="tvouomq" width="300" height="169" data-recalc-dims="1" />][3]

Znalazłem błąd. Najwyraźniej wszystko, co używa normalnej mapy, rozświetla się do pełnej jasności, co wygląda nieco dziwnie. Raport o błędzie znajduje się [tutaj][4], i mam nadzieję, że zostanie on wkrótce naprawiony.

Trzy godziny później Scrawl naprawił to! Ponowiłem eksperyment i otrzymałem małą różową otoczkę dla mojej niewidzialnej postaci.

[<img loading="lazy" class="alignnone size-medium wp-image-4722" src="https://i0.wp.com/openmw.org/wp-content/uploads/2016/10/WqdudZF-300x169.jpg?resize=300%2C169&#038;ssl=1" alt="wqdudzf" width="300" height="169" data-recalc-dims="1" />][5]

##### Zapraszamy do komentowania [tutaj][6].

 [1]: https://bugs.openmw.org/issues/2042
 [2]: https://i0.wp.com/openmw.org/wp-content/uploads/2016/10/N9rFc3P.jpg?ssl=1
 [3]: https://i0.wp.com/openmw.org/wp-content/uploads/2016/10/TvoUoMq.jpg?ssl=1
 [4]: https://bugs.openmw.org/issues/3597
 [5]: https://i0.wp.com/openmw.org/wp-content/uploads/2016/10/WqdudZF.jpg?ssl=1
 [6]: https://forum.openmw.org/viewtopic.php?f=38&t=3863