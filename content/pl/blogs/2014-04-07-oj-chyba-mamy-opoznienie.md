{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-04-07T09:45:40+00:00",
   "title": "Oj, chyba mamy opóźnienie…",
   "type": "post",
   "url": "/2014/oj-chyba-mamy-opoznienie/"
}
Z cotygodnika zrobił się co-dwó-tygodnik. Wina leży tylko i wyłącznie po mojej stronie i nie pozostaje nic innego, jak tylko was przeprosić.

Za to reszta zespołu, w żadnym razie nie próżnuje. Scrawl, chociażby domknął kwestię menu głównego. Ma ono teraz obrazek tła, znajomą muzykę (oczywiście nic nie stoi na przeszkodzie by podmienić pliki mp3, choćby po to by za każdym razem, przy uruchomieniu gry usłyszeć <a href="http://www.youtube.com/watch?v=IlKhFyBVtG4" target="_blank">http://www.youtube.com/watch?v=IlKhFyBVtG4</a>).

Inną, zauważalną zmianę, możecie dostrzec gdy tylko odwiedzicie kupca by wymienić swoje ciężko zagrabione mienie na gotówkę. Kupcy zachowują się teraz tak, jak w oryginalnej grze: ich złoto regeneruje się po upływie doby. To zasługa nowego członka zespołu: JyBego!

Sztuczna inteligencja w OpenMW, jak chyba powszechnie wiadomo, nie należy do światowej czołówki. Prawdę powiedziawszy, nadal jest wręcz żenująca. Jesteśmy tego świadomi i pracujemy nad ulepszeniami. Przykładowo, ostatnio gus wprowadził zmiany których celem jest zapobieżenie fali samobójstw operatorów łazików. Stojąc na wysokich podestach, są dosłownie o krok od zguby…

Prace nad OpenCS również posuwają się do przodu. Ja i graffy próbujemy usprawnić konfigurację edytora (z powodu naszych wymagań, nie jest to wcale takie trywialne zadanie), zaś zini… zini szaleje. Ostatnio mocno pracował nad panelem renderowania. Udało mu się wprowadzić tryby oświetlenia, oraz szereg innych zmian zbliżających nas do funkcjonalności znanej z edytora autorstwa Bethesdy.