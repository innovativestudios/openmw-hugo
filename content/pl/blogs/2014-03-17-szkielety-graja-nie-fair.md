{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-03-17T16:29:06+00:00",
   "title": "Szkielety grają nie-fair",
   "type": "post",
   "url": "/2014/szkielety-graja-nie-fair/"
}
… i tak, o to wydaliśmy 0.29.0 – najlepsze OpenMW od czasów poprzedniego! Spójrzcie tylko na listę zmian, imponuje samym rozmiarem i – co najważniejsze, zawiera długo wyczekiwaną opcję zapisu gry. Oczywiście, jednak wszyscy już o tym doskonale wiedzą, pora więc przejść do wydarzeń z ostatniego tygodnia.

Ciekawą nowością w edytorze OpenCS, jest pojawienie się wstępnej implementacji panelu podglądu modelu. Jak sama nazwa wskazuje, jest to narzędzie służące do podejrzenia wyglądu przedmiotu, bez konieczności umieszczania go w oknie renderowania komórki. Nie przypominam sobie, by zbliżona opcja dostępna była w oryginalnym Construction Set – a wydaje się przecież wręcz niezbędna!

Oprócz tego, do kodu OpenMW zawitały kolejne poprawki sztucznej inteligencji. Od teraz, postacie pod wpływem lewitacji oraz chodzenia po wodzie są w stanie wykorzystać te czary by poruszać się nad obszarem wody, stwory zaś potrafią radzić sobie w walce dystansowej – wystrzegajcie się szkieletów–łuczników! Czasami potrafią posiadać one magiczne, paraliżujące strzały. Pewnie każdy weteran gry zdążył już doświadczyć uroku tej broni…