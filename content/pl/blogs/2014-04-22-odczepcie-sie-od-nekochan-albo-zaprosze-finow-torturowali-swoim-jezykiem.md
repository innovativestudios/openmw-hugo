{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-04-22T14:50:30+00:00",
   "title": "Odczepcie się od nekochan, albo zaproszę finów by was torturowali swoim językiem…",
   "type": "post",
   "url": "/2014/odczepcie-sie-od-nekochan-albo-zaprosze-finow-torturowali-swoim-jezykiem/"
}
Wielkanoc za nami, jak tych prawie dwa tysiące innych. Chciałem napisać post wcześniej i poprosić by ci spośród was którzy się modlą, pomodlili się w intencji pokoju – Wielkanoc to najlepszy czas na takie prośby.

Ale post piszę dzisiaj. I ściśle na temat OpenMW.

W ostatnim tygodniu, podobnie jak ostatnio, doczekaliśmy się ulepszeń w dziedzinie sztucznej inteligencji. Od teraz npc będą obracać się w stronę gracza – bardzo potrzebna zmiana, bo nikt nie lubi być ignorowanym. Ponadto cc9cii naprawił bug w obsłudze fizyki. Nijako przy okazji okazało się, że wydajność silnika nieco się poprawiła.

Scrawl tymczasem skupił się na udoskonaleniach systemu przestępstw. I to tyle.

Zaskoczeni? Czasy gdy każdy tydzień przynosiły rewolucyjne zmiany minęły. Teraz musimy dokończyć i poprawić to co mamy i ostatecznie, w końcu, po latach pracy (tak, latach) wydać wersję 1.0 naszego silnika. Za to w OpenCS…

OpenCS nadal nie posiada wszystkich potrzebnych funkcji. Niestety, aktualnie jedynie zini pracuje nad rozwojem edytora – za to radzi sobie bardzo dobrze. Najnowsze zmiany dotyczą mapy regionów gdzie dodane zostały nowe akcje do menu: otwarcie w widoku tabeli oraz utworzenie nowej komórki. Funkcje potrzebne, ale wciąż zbyt nieliczne! Oj, trzeba brać się do roboty…