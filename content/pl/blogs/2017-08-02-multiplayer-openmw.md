{
   "author": "Vedyimyn",
   "categories": [
      "Uncategorized"
   ],
   "date": "2017-08-02T18:29:10+00:00",
   "title": "Multiplayer w OpenMW",
   "type": "post",
   "url": "/2017/multiplayer-openmw/"
}
**multiplayer**: „M&#8217;Aiq. nie zna tego słowa. Chcesz, żeby inni pomagali ci w zadaniach? Tchórz! Jeśli musisz, poszukaj Argonianina Im-Leeta, albo może wielkiego Norda, Rolfa Wielkiego. Z pewnością będą chcieli do ciebie dołączyć” &#8211; M&#8217;Aiq Łgarz, (tłum. własne)

M&#8217;Aiq mógł nas drażnić od 2002, ale teraz rzeczywiście można spotkać wspomniane przez niego postaci. Wszystko to dzięki siostrzanemu względem OpenMW projektowi TES3MP i jego deweloperom [Koncordowi][1] and [Davidowi Cernatovi][2]. Wcześniej tes3mp umożliwiał walkę pomiędzy graczami, ale ostatni patch dodał synchronizację npc-ów. Jakby tego było mało, dodano także synchronizację organizacji, do jakich należy gracz, dziennika, czasu, wtyczek&#8230; Obecnie moża grać w niemal całą grę, ale skrypty wciąż działają lokalnie, a obiekty utworzone za pomocą alchemii/zaklinania/tworzenia zaklęć nie są synchronizowane. [Pobierz tes3mp w wersji 0.6.0][3] i koniecznie przeczytaj ogłoszenie developerów [tutaj][4]!

[<img loading="lazy" class=" wp-image-4958 aligncenter" src="https://i0.wp.com/openmw.org/wp-content/uploads/2017/07/hA3oZhX-300x169.png?resize=709%2C399&#038;ssl=1" alt="" width="709" height="399" data-recalc-dims="1" />][5]

Instalacja jest bardzo łatwa. Jeśli masz już zainstalowane OpenMW, musisz tylko pobrać tes3mp i wypakowac archiwum do wybranego folderu. Jeśli nie masz jeszcze OpenMW, wystarczy uruchomić instalator i wskazać mu plik morrowind.esm. Kolejnym krokiem jest wystartowanie serwera, tj. pliku tes3mp-server.exe. Następnie musisz musisz otworzyć stosowne porty, by inni graczy mogli dołączyć do gry, korzystając np. z Hamachi. Można też dołączyć do serwera, korzystając z tes3mp-browser.exe.

[<img loading="lazy" class=" wp-image-4957 aligncenter" src="https://i0.wp.com/openmw.org/wp-content/uploads/2017/07/0225-015-1-300x160.jpg?resize=708%2C377&#038;ssl=1" alt="" width="708" height="377" data-recalc-dims="1" />][6]

Jak widać, do wyboru jest kilka opcji. Serwer moż być prywatny, chroniony hasłem, lub ogólnodostępny. Może mieć listę wtyczek wymaganych, by dołączyć do gry. Administrator może wymagać, by pliki .esp miały wymaganą sumę kontrolną, lub wymagać jedynie, by miały tę samą nazwę. Może on też zablokować konsolę na całym serwerze, lub tylko konkretnym graczom, jeśli to konieczne. Za pomocą skryptu LUA może zmienić poziom trudności lu miejsce startu dla graczy dołączajacych do serwera.

Skrypty LUA dają spore możliwości. Podczas testów alpha jeden z moderów utworzył za pomocą skryptów nowy tryb gry: _battle royale_. Polega to na tym, że gracze zaczynają w pomieszczeniu dla oczekujacych. Gdy dołączy minimum trzech graczy, rozpocznie się 60-cio sekundowe odliczanie. Gdy ten czas upłynie, gracze są teleportowani do losowych miejsc na Vvardenfell, mając 25 minut na zebranie uzbrojenia, po czym są nagle teleportowani na arenę, gdzie muszą zrobić z niego użytek. Wyobraźcie sobie, ile jest tutaj możliwości!

Ale załóżmy, że zaczynam zwykłą grę w tes3mp. Pierwsze, co napotykam, to Pelagiad i okienko pytające mnie o imię i hasło. Po ich wsprowadzeniu, informacja zostaje wysłana na serwer, to tworzy plik .json przechowujący informację o postaci. Chwilę później podaję rasę, klasę, znak zodiaku, po czym jestem teleportowany do Balmory.

Po pomyślnym zalogowaniu zostają zsynchronizowane informacje o wszystkich organizacjach, reputacji w organizacjach i zadaniach. Jeśli jakiś gracz dołączył do gildii złodziei, ja również będę do niej należał. Jesli ktoś został arcymagiem w gildii magów, każdy będzie arcymagiem. Jeśli ktoś zaczął quest, będę miał informacje z jego dziennika w swoim. Otwieram dziennik i widzę, że gracz wykonał zadanie ze szczurami dla gildii wojowników, ale nie zgłosił się jeszcze po nagrodę.

Ruszam więc do gildii wojowników. Gdy wchodzę do budynku, server wysyła mi plik cell.json opisujący zmiany pomieszczeń dokonane przez graczy względem stanu domyślnego. Np. fakt uśmiercenia NPC, upuszczony lub zabrany przedmiot. W moim wypadku było to zabranie wszystkiego ze skrzyni z zapasami. To w końcu multiplayer. Oddaję quest i dostaje awans.

Przyjmuję następne zadanie, związane ze złodziejami jaj. Czuję się za słaby na to zadanie, więc proszę na czacie o wsparcie. Zgasza się dwóch graczy: jeden z pingiem 356, drugi z 72, ja mam 301. Gracz drugi zostaje gospodarzem celi, bo ma najniższy ping. Oznacza to, że jeśli atakuje nas zwiadowca kwama, gracz 2 (gospodarz) wysyła informację na serwer, skąd informacji przechodzi do gracza pierwszego i do mnie. Tak działa synchronizacja NPC i stworów.

Po wykonaniu zadania jesteśmy ranni w różnym stopniu. Gracz 1 wymaga siedmiu godzin do wyzdrowienia poprzez odpoczynek, gracz 2 tylko jednej, ja trzech. Jak to rozwiązać? Odpoczywam przez 3 godziny, tak jak to jest normalnie w grze, ale po wypoczynku mój klient ponownie synchronizuje się z czasem serwera.

Wracamy do Balmory. Moi kumple byli na tylie sprytni, że wzięli ze sobą zwoje interwencji Almsivi. Ja nie byłem dość sprytny&#8230; Zamiast biec do Balmory użyłem komendy /suicide w oknie czatu, by skrypt LUA zakończył mój żywot. Ale nie przejmujcie się. Wróciłem do życia w najbliższej świątyni lub kaplicy, czyli właśnie w Balmorze. 

Wracamy do gildii wojowników po nagrodę. Zadanie może być wykonane tylko raz, więc nagrodę otrzymuje tylko jeden gracz. Dzielimy się więc złotem. Podobnie każdy przedmiot może być podniesiony tylko raz. Administrator serwera może jednak usunąć wybrane pliki .json by zresetować pomieszczenie lub obszar świata, co umożliwi graczom wykonanie zadań lub podniesienie przedmiotów, które przegapili.

Zaczynam bić się między sobą. To nie był dobry pomysł, ponieważ mieszkańcy miasta zaczęli zgłaszać nasze nieprawe zachowanie! Strażnik łapie mnie i stawia ultimatum: pieniądze albo więzienie. Czas nie może być zatrzymany w środowisku multiplayer, tak więc pozostali dwaj gracze mogą kontynuować grę, podczas gdy ja załatwiam sprawę ze strażnikiem. Może, nie wie, że jestem Nerevaryjczykiem (w każdym razie jednym z nich i przerywanie mojej pracy kosztuje.

Zabieram zbroję martwego strażnika i zakładam ją. I wtedy zauważam, że&#8230; wyglądam identycznie jak strażnik Hlaalu. Wyłączam caps-locka i zaczynam chodzić jak NPC. Nikt nie zauważy, że to podróba, dopóki nie podejdzie zbyt blisko, a ja nie wyciągnę ostrza. Jeśli zawitasz kiedyś do Balmory, pamiętaj, że ja tam będę. Będę tam chodził. Chodził jak NPC. Gotowy do ataku, gdy nie będziesz się spodziewać.

Patrząc, jak daleko zaszedł projekt tes3mp w stosunkowo krótkim czasie, ośmielę się stwierdzić, że zostało pokazane, iż implementacja trybu wieloosobowego i podobnych projektów na bazie OpenMW jest preferowaną ścieżką. Ludzie czekali wiele lat ma fanowską implementację jakieś formy trybu multiplayer w grach takich jak Morrowind, Oblivion, Skyrim, Fallout 3, Fallout 3 &#8211; New Vegas, a tes3mp jest pierwszym projektem, który dostarcza nie tylko synchronizację NPC, ale także górę innych możliwości, których inne projekty jeszcze nie oferują. Wystarczyło dwóch deweloperów i silnik Open Source, by osiągnąć to, z czym zmagają się profesjonalni programiści bazując na zamkniętym silniku.

OpenMW przez długi czas dążyło do tego, by mogły powstać projekty takie jak ten, więc to wielka radość, że na jego bazie powstał z powodzeniem projekt multiplayer. Marzenie o graniu w Morrowinda z przyjaciółmi spełniło się! Ale nie zamierzamy na tym poprzestać. Programiści tes3mp planują dalej rozwijać projekt, jak również pomagają rozwijać samo OpenMW. Każdy, kto chciałby utworzyć swój fork OprnMW i utworzyć projekt na jego bazie, tak jak uczynili autorzy tes3mp jest mile widziany.

##### Zapraszamy do komentowania [tutaj][7].

#####

 [1]: https://www.patreon.com/Koncord
 [2]: https://www.patreon.com/davidcernat
 [3]: https://github.com/TES3MP/openmw-tes3mp/releases/download/tes3mp-0.6.0/tes3mp.Win64.release.0.6.0.zip
 [4]: https://steamcommunity.com/groups/mwmulti#announcements/detail/1441567597386587897
 [5]: https://i0.wp.com/openmw.org/wp-content/uploads/2017/07/hA3oZhX.png?ssl=1
 [6]: https://i0.wp.com/openmw.org/wp-content/uploads/2017/07/0225-015-1.jpg?ssl=1
 [7]: https://forum.openmw.org/viewtopic.php?f=38&t=4463