{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2013-10-28T11:02:01+00:00",
   "title": "Nie panikować",
   "type": "post",
   "url": "/2013/nie-panikowac/"
}
Nie da się ukryć pewnych oczywistych faktów. Najlepszym przykładem, jest chyba wyraźne spowolnienie prac nad OpenMW w okresie ostatnich, chyba nawet już miesięcy. A skoro sytuacja jest zupełnie nie do ukrycia to warto przynajmniej spróbować ją wyjaśnić, choćby po to by ograniczyć liczbę teorii spiskowych.

Jak zapewne pamiętacie, jeszcze nie tak dawno pisałem wiele o przebudowie obsługi fizyki w grze. Przypuszczam, że część osób zastanawia się też, dlaczego na blogu nie pojawiają się kolejne emocjonujące wieści z tego frontu. Możliwe, iż pesymiści nawet doszli do wniosku, że najwyraźniej nie ma żadnych emocjonujących wieści. Niestety, pesymiści nie mają racji: ostatnie doniesienia potrafią podnieść ciśnienie krwi do niebezpiecznych poziomów i wycisnąć łzy z męskich oczu. Chris utkwił w martwym punkcie z powodu błędu w samym bullet.

Bullet to złożona biblioteka, i nie sądzę by osoba bez jej dogłębnej znajomości miała szansę na jej naprawienie. Oznacza to tyle, że nie możemy zrobić absolutnie nic i to nie z naszej winy. Bezsilność nie jest przyjemnym stanem, zapewniam was.

Nie pozostaje więc nic innego jak tylko odłożyć wszystko co wymagałoby usprawnień w kodzie fizyki na bliżej nieokreśloną przyszłość. Co gorsza, gus nie ma aż tak wiele czasu na rozwój sztucznej inteligencji walki; więc również i tu możemy przeanalizować wrażenia zrodzone z głębokiego rozczarowania.

Czas zweryfikować nasze możliwości i zoptymalizować plany. I tak: po pierwsze sztuczna inteligencja w walce zostaje odroczona, ponieważ funkcja sprawdzająca czy bohater jest widziany przez postacie w grze jest niezbędna dla innych funkcji ─ a przez to nabywa wyższy priorytet. Po drugie: OpenCS będzie miał swoją premierę już teraz, nawet pomimo tego, że jego użyteczność można określić co najwyżej jako &#8220;prawie działa&#8221;. Dzięki temu nowe wydanie OpenMW nie będzie wyglądać żenująco, i przynajmniej dostarczy nam nieco opinii na temat edytora.

Wierzcie lub nie: to nie jest pierwszy epizod trudności w historii projektu. Poradzimy sobie z nim tak, jak radziliśmy sobie w przeszłości. Przejściowe problemy nie mogą powstrzymać przyszłości.