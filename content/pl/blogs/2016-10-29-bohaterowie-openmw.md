{
   "author": "Vedyimyn",
   "categories": [
      "Uncategorized"
   ],
   "date": "2016-10-29T17:53:33+00:00",
   "title": "Bohaterowie OpenMW",
   "type": "post",
   "url": "/2016/bohaterowie-openmw/"
}
Przez długi czas scrawl był główną siłą napędową OpenMW, więc gdy tylko pojawiła się wiadomość, że zamierza przerwać pracę, by podreperować swoje zdrowie, wielu z nas pomyślało, że jakikolwiek postęp zostanie wstrzymany, i że następne wydania OpenMW będą tylko łatały bugi, jeżeli w ogóle…

Ale nic bardziej mylnego! Na horyzoncie pojawili się Allofich i MiroslavR. Ten duet nie tylko naprawił wiele błędów, ale zaimplementował też kilka nowych funkcji. Ponadto, scrawl wciąż jest aktywny w naszej społeczności &#8211; akceptuje pull requesty, czyta fora i okazyjnie zamknie jakiś błąd, gdy zdrowie na to pozwala. O ile tempo jest teraz nieco wolniejsze, prace nad projektem postępują, a OpenMW kontynuuje swoją podróż ku wersji 1.0.

[<img loading="lazy" class="wp-image-4741 aligncenter" src="https://i0.wp.com/openmw.org/wp-content/uploads/2016/10/bugtracker-300x133.jpg?resize=675%2C299&#038;ssl=1" alt="bugtracker" width="675" height="299" data-recalc-dims="1" />][1]

Jeśli chodzi o OpenMW-CS, to tutaj również pojawiło się parę usprawnień. Wcześniej okno przedstawiające scenę było jedynie statycznym podglądem świata, gdzie można było jedynie się porozglądać. Jednakże wkroczył Aeslywinn, przerobił system kamery, zaimplementował siatki ścieżek (pathgrids), renderowanie wody, poruszanie obiektami, tryby rotacji i skalowania. Co prawda zostawił nasz projekt, by czynić inne wielkie rzeczy, ale za to zostawił go z edytorem zdolnym do kreowania nowych światów.

[<img loading="lazy" class="wp-image-4742 aligncenter" src="https://i0.wp.com/openmw.org/wp-content/uploads/2016/10/Castle-300x161.jpg?resize=691%2C371&#038;ssl=1" alt="castle" width="691" height="371" data-recalc-dims="1" />][2]

Należy wspomnieć, że choć powyższy zrzut ekranu zrobiono używając OpenMW-CS, to wciąż jest kilka funkcji, które muszą być zaimplementowane w edytorze. Pamiętaj &#8211; jeśli masz umiejętność programowania w C++ lub QT i chciał(a)byś przyczynić się do realizacji projektu OpenMW, również możesz zostać jego bohaterem.

##### Zapraszamy do komentowania [tutaj][3].

 [1]: https://i0.wp.com/openmw.org/wp-content/uploads/2016/10/bugtracker.jpg?ssl=1
 [2]: https://i0.wp.com/openmw.org/wp-content/uploads/2016/10/Castle.jpg?ssl=1
 [3]: https://forum.openmw.org/viewtopic.php?f=38&p=43228#p43228