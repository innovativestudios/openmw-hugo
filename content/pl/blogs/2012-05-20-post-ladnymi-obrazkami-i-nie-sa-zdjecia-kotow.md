{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-05-20T17:31:38+00:00",
   "title": "Post z ładnymi obrazkami (i nie są to zdjęcia kotów)",
   "type": "post",
   "url": "/2012/post-ladnymi-obrazkami-i-nie-sa-zdjecia-kotow/"
}
<a href="http://openmw.org/wiki/index.php?title=File:015-6.png " title="To." target="_blank">http://openmw.org/wiki/index.php?title=File:015-6.png</a>  
<a href="http://openmw.org/wiki/index.php?title=File:015-4.png" title="I to!" target="_blank">http://openmw.org/wiki/index.php?title=File:015-4.png</a>  
<a href="http://openmw.org/wiki/index.php?title=File:015-5.png" title="I jeszcze to!" target="_blank">http://openmw.org/wiki/index.php?title=File:015-5.png</a>

Czyli co też wyczynia panicz scrawl? Jak widać mamy podpowiedzi, mamy okienko ekwipunku w którym działa nawet pojemność ustalana przez siłę naszego dzielnego herosa. Mało tego! Handel też działa! 

Nawet nie wiem czy jest sens opisywać to jak mocno scrawl przyczynił się do nadchodzącego wydania, bo powinno to być dla wszystkich oczywiste niczym wschód słońca.

Tak czy owak nowe funkcje związane z interfejsem nadciągają nieuchronnie dzięki wysiłkowi całego zespołu. Przykładowo pojemność ekwipunku działa dzięki temu, że Nasz Przywódca Który Widzi Więcej (Dzięki Diecie Bogatej w Witaminę A) wprowadził nową funkcję getEncumbrance którą mógł wykorzystać scrawl.

I tak o to ujawnia się modus operandi zespołu OpenMW. Zini przeważnie zajęty jest budowaniem fundamentów dla nowych funkcji by reszta zespołu poza naprawianiem starych, nudnych błędów mogła również dodać parę nowych, ekscytujących przy okazji dodawania zupełnie nowych funkcji.

I dokładnie to ma miejsce. Tym razem Zini zajęty jest przygotowaniami do wprowadzenia funkcji związanych z zaklęciami i efektami magicznymi by w (daj Boże) niedalekiej przyszłości, można by dodać je do gry. 

Ponieważ scrawl ukradł niczym Polski Hydraulik zadanie gusa, ten zajął się naprawianiem błędów. Niedawno zgłoszono bug w wykrywaniu kolizji objawiający się tym, że próba przejścia pod lampionami w Vivec kończyła się zderzeniem z nimi. Problem został już rozwiązany, a teraz gus planuje zająć się obracaniem przedmiotów (skrypty!).

A jhooks1 nadal katuje nowy system animacji. Nie ma dokumentacji, są problemy. Co prawda rozwiązania nadchodzą ale powoli, wtaczają się z wysiłkiem niczym ruszająca lokomotywa. Trudno spodziewać się szybszego postępu gdy błądzi się w ciemnościach, ale jhooks1 wiele już osiągną, a dzięki dodatkowej ekspertyzie Chrisa nie ma rzeczy niemożliwych.