{
   "author": "sir_herrbatka",
   "date": "2012-04-15T11:06:09+00:00",
   "title": "Tydzień po wielkanocy",
   "type": "post",
   "url": "/2012/tydzien-po-wielkanocy/"
}
Ostatnio naczelny facebookowicz OpenMW założył temat na forum by każdy z deweloperów mógł swobodnie wytłumaczyć czym się zajmuje i czym planuje się zająć w przyszłości. Ponieważ jestem leniwą bestią postanowiłem przetłumaczyć te posty.

**ACE**

> Pracuję nad poprawnym zapisywania rekordów esm, aktualnie część informacji jest tracona podczas tego procesu, więc teraz staram się wyśledzić przyczynę problemu. Docelowo powinniśmy być zdolni do tworzenia dokładnych kopii.  
> Gdy w końcu się uda, sądzę że wycofam się z rozwijania OpenMW, koncentrując się równocześnie na wydawaniu wersji na windows. Może spróbuję stworzyć nową paczkę z zależnościami.

**jhooks1**

> Teraz koncentruję się na wyświetlaniu pancerzy i odzienia.
> 
> Mam nadzieję, że w ramach prac nad 0.15.0 uda mi się dopracować nowy system fizyki który zaprezentowałem na kanale youtube.
> 
> Chciałbym także wprowadzić część z osiągnięć Chrisa w dziedzinie animowania stworzeń do OpenMW. Rzecz jasna, jeśli tylko Chris chce to właśnie on powinien objąć przywództwo.
> 
> Nawet jeśli nie uda się nam zmusić systemu OGRE do działania, to i tak sposób w jaki stosuje ciężar do elementów szkieletu jest efektywniejszy i być może uda się go zastosować w OpenMW.

Chris, jeśli już o nim mowa, wciąż usiłuje rzecz jasna wykorzystać system animacji ogre. Efekty są widoczne, można wręcz powiedzieć, że system nawet działa; jednak z wieloma bugami, problemami i kłopotami. Niestety .nif jest własnościowym typem plików, więc nie ma dostępu do dokumentacji, a Chris musi rozpracować wszystko własnoręcznie.

Aha, ale za to kolizja z gruntem już działa z nową fizyką.

**zini**

> Pracuję nad modelem świata; struktury informacji i przygotowania dla mechaniki rozgrywki. W gruncie rzeczy ta mniej ekscytująca robota.
> 
> Nie planowałem zajmować się tym w okresie przed 0.14.0 ale reszta przebija się przez podniecające zadania (grafika i mechanika widoczna od strony użytkownika) na tyle szybko, że zaczęło nam zagrażać wyczerpanie możliwości implementacji takich funkcji z powodu braków w podstawach.

Hircine pracował samodzielnie nad GUI dla pojemników i ekwipunku ale niestety jego umiejętności w zakresie C++ nie są wystarczające. Na szczęście na posterunku stał gus. Nowy duet poradzi sobie ze wszystkimi trudnościami, a przynajmniej taki jest plan.

Oczywiście gus skończył z pracą nad organizacjami. Gotowe!

**scrawl**

> Jestem już lekko znudzony grafiką, więc teraz zajmę się implementowaniem paru elementów GUI które marzyły mi się już od dawna. Głównie:
> 
>   * Podpowiedzi.
>   * Door Markers.
>   * Podpowiedzi dla obiektów wskazanych przez celownik
No i co ja mogę powiedzieć? W 0.14.0 będzie cała masa widocznego trudu scrawla. Minimapa, teren (razem z yacobym), ładna woda i nawet więcej.

Zbliżamy się wielkimi krokami do wydania 0.14.0, czekamy jeszcze na zakończenie ostatniego zadania (odzienie).