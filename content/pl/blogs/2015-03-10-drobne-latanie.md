{
   "author": "Vedyimyn",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2015-03-10T12:11:56+00:00",
   "title": "Drobne łatanie",
   "type": "post",
   "url": "/2015/drobne-latanie/"
}
Minął już około miesiąc od wypuszczenia wersji 0.35.0. W tym czasie otrzymaliśmy informacje o paru crashach tu i ówdzie. Zostały już naprawione, ale sytuacje takie, jak te, wręcz domagają się łatki, więc drużyna już nad nią pracuje. Wersja 0.35.1 nie tylko naprawi wspomniane crashe, ale będzie też zawierać poprawki dla wielu innych bugów.

Zostanie także dodana obsługa alternatywnych kontrolerów, takich jak joystick, czy gamepad. W zeszłym roku już nad tym pracowano, jednakże efekty, chociaż niezłe, nie były dość zadowalające, by dodać tę funkcję do OpenMW. Digmaster i Scrawl ulepszyli wsparcie dla kontrolerów na tyle, że można je umieścić w nowym wydaniu. Jeśli masz gamepada, to koniecznie spróbuj! Gdyby jakiś pad nie funkcjonował, jak należy, to chętnie się o tym dowiemy i temu zaradzimy.

Po wydaniu 0.35.1 zespół wróci do pracy nad 0.36.0.

Zapraszamy do komentowania [tutaj][1].

 [1]: https://forum.openmw.org/viewtopic.php?f=38&t=2801