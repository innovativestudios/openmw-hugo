{
   "author": "sir_herrbatka",
   "date": "2013-08-05T19:41:54+00:00",
   "title": "Wiadomości, wiadomości…",
   "type": "post",
   "url": "/2013/wiadomosci-wiadomosci/"
}
Tak więc, wydanie 0.25.0 jest już za nami i jest to z pewnością rzecz która nas wszystkich cieszy. Z drugiej jednak strony wydanie 0.26.0 będzie chyba jednak zdecydowanie fajniejsze.

Spytacie się zapewne &#8220;A czemuż to?&#8221;. Odpowiedź jest prosta: ponieważ będzie w nim można zabijać. Mieczem.

Jak do tej pory funkcja ta nie działa w sposób do końca prawidłowy ─ przykładowy dostarcza możliwość popełnienia samobójstwa poprzez atakowanie samego siebie, co choć bez wątpienia realistyczne i praktykowane w średniowiecznej Japonii przez samurajów nie jest jednak zgodne z mechaniką rozgrywki Morrowinda. Polecam jednak wypróbować OpenMW już teraz. Wszak historia dzieje się przed waszymi oczyma. To wymaga muzyki Straussa!



Poza tym mamy naprawdę sporo bugfixów, a na dodatek doczekaliśmy się też ikonki skradania się w gui wyświetlającą się (niespodzianka!) w trakcie skradania się. Prace nad edytorem również posuwają się do przodu.

Naprawdę nie ma na co narzekać.