{
   "author": "Vedyimyn",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2015-04-17T18:01:06+00:00",
   "title": "Nowy początek",
   "type": "post",
   "url": "/2015/nowy-poczatek/"
}
Jako że lista brakujących funkcji staje się coraz krótsza, zespój kieruje swoją uwagę na problemy dużo większe niż jakieś tam brakujące zachowania NPC-ów. Scrawl, na przykład, wytrwale przeportowuje projekt z silnika Ogre3D na OpenScreenGraph. Jest to dzieło mniej znane, ale bardzo elastyczne i wydajne, co umożliwi płynniejsze działanie OpenMW i łatwiejszy jego rozwój. O zmianie silnika Scrawl wypowiedział się szczegółowo [tutaj][1], ale jeden obrazek wart jest tysiąca słów:

<div style="text-align: center">
  <a href="https://i0.wp.com/scrawl.bplaced.net/temp/screen22-16-24.png"><img style="width: 500px" src="https://i0.wp.com/scrawl.bplaced.net/temp/screen22-16-24.png" alt="" data-recalc-dims="1" /></a> <a href="https://i0.wp.com/scrawl.bplaced.net/temp/screen22-07-19.png"><img style="width: 500px" src="https://i0.wp.com/scrawl.bplaced.net/temp/screen22-07-19.png" alt="" data-recalc-dims="1" /></a>
</div>

Jest to niemalże powrót do wersji 0.13.0, ale bez obaw ([film o wersji 0.13.0][2]). W przeciwieńswie do sytuacji z roku 2012, większość programu jest już gotowa. Obiecujemy, że powrót do niedawnego stanu (ale z nowym silnikiem) nie zajmie trzech lat ;) Tak swoją drogą, to w czasie pisania tego posta Scrawl sprawił, że bohaterowie niezależni są już renderowani ([dowód][3]). Postęp jest naprawdę szybki.

Rozwój OpenMW-CS również nie stoi w miejscu. Jeśli jesteś moderem, to koniecznie musisz zobaczyć dwie funkcje, jakie zostały ostatnio dodane. Dzięki pracy cc9ii, z wielką pomocą sirherrbatki, wiele możliwości, wcześniej niedostępnych, stoi przed Tobą otworem. Na przykład NPCe: ich opcje były dostępne tylko przez tabelę główną (main table), jednakże wielu rzeczy tam brakowało. Wybranie pozycji „Edytuj rekord” (Edit record) z menu kontekstowego pozwoli znaleźć więcej opcji, w tym na przykład możliwość wybrania towarów, które ten NPC skupuje. Dzięki pracy cc9ii możesz także z łatwością umieszczać przedmioty w jego ekwipunku, sprawdzać trasę, po której się porusza, i wiele więcej ([na GitHubie][4] można podejrzeć, co konkretnie).

Zini ogłosił z należnymi fanfarami ([na Twitterze][5]), że funkcja Global Search już działa (Globalne wyszukiwanie). Kiedyś poszukiwanie kamionkowej tacy z Biura Spisów i Opodatkowania w czeluściach zagraconego ekwipunku, było jak szukanie igły w stogu siana. Dało się co prawda szybko ją wyfiltrować pewnym zabiegiem, ale po co tak komplikować? Global Search pozwoli znaleźć pożądany przedmiot w mgnieniu oka. Co więcej, funkcja ta umożliwia też szybką podmianę (replace), zwalniając Cię z konieczności edytowania mikstur jedna po drugiej.

Zamieszczamy też dzieło społeczności skupionej wokół projektu. Sterling z kanału Old School RPG na YouTube zaczął kręcić serię Let&#8217;s Play Morrowind (Zagrajmy w Morrowind) na silniku OpenMW. Poniżej pierwszy odcinek. Zachęcamy do przejrzenia całego kanału ([link][6]).

Na razie to tyle.  Nie możemy się doczekać, by podać więcej informacji na temat nowego wydania, nad którym zespół wytrwale pracuje. Do następnego razu!

Zapraszamy do komentowania [tutaj][7].

 [1]: https://openmw.org/2015/zmiana-silnika-na-openscenegraph/
 [2]: https://www.youtube.com/watch?v=9gOqBU2oZnk
 [3]: http://i.imgur.com/jwhQAdr.png
 [4]: https://github.com/OpenMW/openmw/pull/553
 [5]: https://twitter.com/marczinnschlag/status/582217189966610434
 [6]: https://www.youtube.com/channel/UC6cCf0rNe7XK8V1rPcW9Bkw
 [7]: https://forum.openmw.org/viewtopic.php?f=38&t=2859