{
   "author": "sir_herrbatka",
   "date": "2012-04-16T10:16:23+00:00",
   "title": "Różnice pomiędzy OpenMW 0.11 i 0.14.0",
   "type": "post",
   "url": "/2012/roznice-pomiedzy-openmw-0-11-0-14-0/"
}
Znajdź siedem różnic.

<img loading="lazy" src="https://i0.wp.com/openmw.org/wiki/images/8/89/Openmw_0.11.1_2.png?resize=703%2C526" alt="Openmw 0.11" width="703" height="526" data-recalc-dims="1" /> 

<img loading="lazy" src="https://i0.wp.com/openmw.org/wiki/images/4/4f/014-12.png?resize=703%2C438" alt="Openmw 0.14" width="703" height="438" data-recalc-dims="1" /> 

&nbsp;

Dość oczywiste, czyż nie?