{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2013-10-21T11:20:59+00:00",
   "title": "Nowiny, nowiny i takie tam…",
   "type": "post",
   "url": "/2013/nowiny-nowiny-takie-tam/"
}
I tak o to, upłynął nam kolejny tydzień. Upłynął szybko, i niestety niekoniecznie produktywnie; ale do rzeczy.

Na początek miły akcent dla wszystkich użytkowników fedory: OpenMW właśnie trafił do repozytorium fusion ─ a to zmniejsza wysiłek związany z instalacją programu do minimum. W związku z tym: jeśli do tej pory nie miałeś cierpliwości by wypróbować OpenMW wiedz, że twoja wymówka padła niczym pod kopniakiem Chucka Norrisa.

Poza tym: w grze pojawiły się pochodnie gasnące po okresie użytkowania oraz zetknięciu z wodą. Wyrazy uznania wprost z ciemnych lochów kierować należy pod adresem rainChu.

Zini tymczasem właśnie wprowadza obsługę dialogów do naszego edytora. Co ciekawe, możliwe, że OpenMW jeszcze w wersji poprzedzającej 1.0 doczeka się poszerzonej obsługi plików dźwiękowych w dialogach ─ jest to ulepszenie trywialne do wykonania, a przy tym być może atrakcyjne dla niektórych autorów modyfikacji.