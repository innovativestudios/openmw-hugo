{
   "author": "sir_herrbatka",
   "date": "2012-11-07T19:00:10+00:00",
   "title": "Czasami cieszę się, że reszta zespołu nie zna naszego języka…",
   "type": "post",
   "url": "/2012/czasami-ciesze-sie-ze-reszta-zespolu-nie-zna-naszego-jezyka/"
}
_Żar lał się wprost z niebios niczym roztopiony ołów: skupiał się w każdej niecce, przelewał nad wzgórzami. Generał Zini mógł rejestrować te senne nurty obserwując falowanie rozgrzanego krajobrazu za wizjerem swojego transportera opancerzonego. </p> 

Droga była trudna, a co gorsza także długa; pancerze nie chroniły przed kurzem i odwodnieniem, wysiłek odbijał się na twarzach żołnierzy wyraźniej niż choroba. 

Pustkowia zdawały się żołnierzom bezkresne, misja zaś niejasna ─ tylko sam Zini wiedział dokąd zmierzają i jaką drogę obierają. Zaufanie musiało znieczulać wątpliwości niczym morfina ból.

Generał sprawdził po raz kolejny koordynaty i dopiero wtedy sięgnął po radio. Był już absolutnie pewny: nastała pora by rozstawić RC i przygotować się do zajęcia sektora 0.19.</em>

O tak! **0.19 nadciąga**! To pewne. Co prawda obraz dokonanych postępów może wzbudzić uczucie porażki w co bardziej pesymistycznie usposobionych czytelnikach ale zwróćcie uwagę na to, że celem nowych wydań jest ich przetestowanie. OpenMW wciąż nie jest grą.

Poza tym w tym tygodniu zespół kontynuuje prace z zeszłego tygodnia. Wychyliła się tylko para dwójka deweloperów: nowy członek zespołu o nicku emoose który zajął się zadaniem związanym z drzwiami w pomieszczeniach oraz wiarus scrawl który z kolei wprowadził do OpenMW najbardziej lubianą funkcję niektórych graczy: możliwość obrabowania zwłok. Oprócz tego Zini naprawił kilka mniej lub bardziej istotnych błędów.

I to na razie tyle.