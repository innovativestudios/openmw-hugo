{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-02-18T08:35:44+00:00",
   "title": "OpenMW nie jest nieuleczalne.",
   "type": "post",
   "url": "/2014/openmw-nie-jest-nieuleczalne/"
}
Zagadka: jaki procent całości skryptów jest kompilowanych przez OpenMW? 

Odpowiedź brzmi: 97,2682% (dziewięćdziesiąt-siedem-koma-dwa-tysiące-sześćset-osiemdziesiąt-dwa-procent) ─ lub jak kto woli: niemal wszystkie. I to chyba najlepiej ilustruje efekt ostatnich wysiłków Ziniego: jeszcze nie tak dawno wynik kręcił się w okolicach 80%. Warto nadmienić przy tym, że przed nami wciąż wiele wyzwań: nie wszystkie skompilowane skrypty działają, a osiągnięcie nawet tego rezultatu okupione było prawdziwym cierpieniem ─ świadectwem niech będzie wystukana z najwyższym trudem wiadomość od ziniego: &#8220;Niech ktoś mnie dobije, proszę&#8221;.

Ale oczywiście nikt nie zamachnie się na życie Ziniego. Nie utracimy naszego nieocenionego lidera! Będzie programować niczym niewolnik na galerze! Może nawet rozpoczniemy kampanię na kick starterze by zebrać fundusze na zakup nowoczesnych i stylowych kajdanów, a z czasem opatentujemy przykuwanie programistów do klawiatur? Mam wrażenie, że może się to przyjąć w szerokim, korporacyjnym świecie, a co za tym idzie; zarobimy na oferowaniu płatnych licencji. Wracając jednak do zasadniczego tematu bloga…

Zini, gdy skończył już użerać się z potworkami Bethesdy, rozwiązał wyjątkowo irytujący błąd w OpenCS ─ dzięki czemu tabela Referencables działa prawidłowo gdy załadowany jest więcej niż jeden plik (ma to miejsce zawsze gdy tworzymy nową modyfikację, gdyż musi ona bazować na pliku głównym, takim jak np. Morrowind.esm). Inną ciekawą nowością jest funkcja &#8220;Drag-and-Drop&#8221;: można w ten sposób szybko i przyjemnie zmienić, między innymi, rasę postaci niezależnej: wystarczy przeciągnąć rekord rasy do odpowiedniego pola ─ i gotowe!

Ponadto udało się nam naprawić dużą ilość błędów w samym silniku gry. Jak zapewne pamiętacie z ostatniego cotygodnika, szczególnie &#8220;interesujące&#8221; są problemy, powiązane ze sztuczną inteligencją. Dość powiedzieć, że na skutek działalności Gusa stały się one odrobinę mniej zabawne. Warto również wspomnieć o błędach naprawionych przez potatoemaster oraz MrCheko ─ co czyniąc kończę niniejszy wpis.

Do następnego tygodnia!