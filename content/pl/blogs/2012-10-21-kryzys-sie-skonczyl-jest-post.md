{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-10-21T18:02:09+00:00",
   "title": "Kryzys się skończył! Jest post!",
   "type": "post",
   "url": "/2012/kryzys-sie-skonczyl-jest-post/"
}
W zeszłym tygodniu nie było cotygodnika.

Oczywiście nie chodziło zaledwie o sprawdzenie czy ktokolwiek zauważy brak, jest to co prawda interesujące zagadnienie, ale jednak całkowicie oddzielne od moich problemów z komputerem.

Trudności udało mi się rozwiązać, ale wpisu napisać nie zdążyłem. Żałuję teraz, że nie zdecydowałem się zrobić tego w tygodniu, bo mogłem napisać artykuł który może i byłby spóźniony, ale jednak bez nie czułbym presji objęcia w jednym tekście dwóch tygodni.

Nie zrobiłem tego, a teraz mogę mieć tylko do siebie pretensje ─ tak można, i trzeba podsumować całą sytuację. Starczy więc tego zbędnego roztrząsania porażki, przejdźmy do INTERESUJĄCYCH wiadomości. Zachowajcie jednak świadomość niemal pewnej niekompletności tego konkretnego cotygodnika.

Scrawl ukończył okno tworzenia zaklęcia, a nawet odrobinę więcej bo dzięki dobremu planowaniu, w przyszłości przynajmniej część kodu będzie mogła zostać wykorzystana przy implementacji zaklinania przedmiotów. Gałąź została włączona do main.

Gus kontynuował prace nad szybką podróżą. Mamy już odpowiednie ceny, a nawet działające GUI; o tak moi drodzy: podróżowanie działa.

PVDK majstruje przy starterze OpenMW, sposób sortowania plików gry na liście uległ zmianie i jest teraz bardziej uporządkowany i przejrzystszy. Tylko co z tego, skoro sam silnik wciąż nie jest w stanie załadować niczego ponad jeden plik główny? 

Greye i Cary Cohen podjęli się zadań dotyczące mechanizmów ukrytych w trzewiach silnika, i choć jako takich nie eksponowanych w postaci funkcji, to jednak niezbędnych dla wielu elementów gry. 

I tak o to Greye pracuje nad dodaniem możliwości tworzenia w trakcie gry (a nie tylko ładowania z pliku) nowych elementów ESM. Jest to bardzo ważna funkcja, i to na wiele sposobów ─ dość powiedzieć, że bez niej nie sposób wprowadzić możliwość zmiany rasy postaci gracza.

Tymczasem Cary Cohen wprowadza zmiany w ESMTool. Pojawiły się etykiety zrozumiałe dla ludzkich istot, a ponadto zdumpowanie rekordów za pomocą ESMTool dostarczy więcej informacji.

Zini postanowił zwalczyć niestandardowe rozszerzenia GCC. Przy okazji wprowadził także flagi które mogą przełączyć kompilator w tryb C++11, to jest jeszcze-ciepły-i-pachnący-nowością standard C++. Oczywiście nie oznacza to jeszcze, że dopuszczalne jest stosowanie C++11 w OpenMW; standard staje się prawdziwym standardem nie wcześniej niż gdy zaczyna być poprawnie przekształcany do kodu maszynowego przez powszechnie używane kompilatory. Innymi słowy: w naszym wypadku przejście z całą pewnością nie nastąpi przed drugą połową przyszłego roku.

A tak na marginesie: zwróciliście uwagę na margines? Jest tam bardzo Web-dwa-zerowa chmurka tagów. Od tej pory taki np. Scrawl jest nie tylko Davidem Bowie programowania ale także tagiem na naszym blogu!