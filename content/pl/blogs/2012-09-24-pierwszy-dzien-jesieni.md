{
   "author": "sir_herrbatka",
   "date": "2012-09-24T21:00:49+00:00",
   "title": "Pierwszy dzień jesieni…",
   "type": "post",
   "url": "/2012/pierwszy-dzien-jesieni/"
}
W tym tygodniu nie mam zbyt wiele do przekazania, a to z racji szczególnych okoliczności. Wersja 0.18.0 jest już na ukończeniu, to jest w fazie RC. Cykl tej wersji trwał krócej niż zazwyczaj dzięki wysiłkowi programistów pracujących z godną podziwu (i naśladowania!) intensywnością. 

Niestety przed finalnym wydaniem powstrzymały nas zaskakująco liczne i dość poważne problemy występujące jedynie na systemach operacyjnych z rodziny OSX. 

Wśród członków zespołu znajduje się tylko jeden używający komputera Apple, a to oznacza kłopoty; tym poważniejsze im więcej zmian wprowadzono od ostatniego wydania. Zdolność przewidzenia przeszkody nie jest oczywiście jednoznaczna z możliwością jej uniknięcia. 

Na szczęście dzięki trudowi Scrawla i Corristo udało się już rozwiązać dolegliwości trapiące build dla OSX, a to oznacza, że lada dzień do pobrania zostanie udostępniona „najnowsza wersja OpenMW &#8212; najlepsza od czasów poprzedniej”.

Oprócz tego niespokojny duchem Scrawl ukończył dwie zupełnie nowe funkcje: korekcję gamma („chcesz widzieć przeciwników i tym samym mieć możliwość ich trafienia, czy też ci na tym nie zależy?”) oraz okno czekania i snu („trzy miecze w brzuchu i strzała w barku &#8212; pora na drzemkę!”); dzięki pomocy nieocenionego w takich momentach Hrnchamd mamy pewność, że szybkości regeneracji zdrowia oraz many pokrywają się z tymi obecnymi w Morrowind.

Ponadto Scrawl zajął się mapą globalną, status prac nad nią można określić jako połowicznie zakończony. Nie wygląda dokładnie tak samo jak w Morrowind ale osobiście nie uznaję tego za wadę, a wręcz przeciwnie; w tym przypadku OpenMW prezentuje się znowu lepiej niż Morrowind.

Tymczasem jhooks1 podąża ze swoimi pracami naprzód. Pmove powrócił, a kontrolery postaci znowu działają wraz z szeregiem powiązanych, istotnych funkcji.