{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2013-04-21T18:46:31+00:00",
   "title": "Właśnie zauważyłem, że mamy coś takiego jak kategorie na blogu. Wybacz Lgro: teraz już będę używać…",
   "type": "post",
   "url": "/2013/wlasnie-zauwazylem-ze-mamy-cos-takiego-jak-kategorie-na-blogu-wybacz-lgro-teraz-juz-bede-uzywac/"
}
Niektórzy z was (wierzę w to z siłą zrodzoną z desperacji) zapewne odwiedzają forum projektu, a skoro tak, to mogli zorientować się w planach wydania 0.23.0. Planujemy już od dłuższego momentu, ale jak do tej pory słowo nie stało się ciałem. 

Dlaczego?

Powody są proste. Tym razem proces jest wyjątkowo trudny, a przez to wymagający przedłużonego czasu. Głównym problemem były dwa błędy powodujące „nieoczekiwane zakończenie programu” albo, jak kto woli crash. Pierwszy bug był zaskakująco trudny do znalezienia, ale naprawienie okazało się na szczęście dużo prostsze więc scrawl uporał się z nim szybko i bez trudu. Drugi bug był prawdopodobnie najbardziej tajemniczym błędem projektu OpenMW w jego (zdecydowanie zbyt długiej jak na aktualny stan) historii.

Crash był wybredny i upodobał sobie jedynie system Chrisa, pozostali programiści nie mogli w żaden sposób odtworzyć scenariusza prowadzącego do jego wystąpienia. Już samo to jest co najmniej dziwne, jednak co gorsza okazało się, że zmiana kompilatora z gcc na clang skutkowała brakiem błędu. Wyglądało to bardzo nieprzyjemnie, a mówiąc wprost wręcz paskudnie. Zini zapowiadał już, że gotowy jest wyciąć z wydania 0.23.0 sztuczną inteligencję jeżeli rozwiązanie nie pojawi się na czas, a ja zastanawiałem się nad zaangażowanie Antoniego Macierewicza który jako osoba o intelekcie przenikliwym niczym zimne światło księżyca z pewnością rozwiązałaby i tą zagadkę. 

Na szczęście gus jakimś cudem naprawił bug i tym samym przewidywać należy rychłe wydanie wersji 0.23.0. Nie zawadzi też wspomnieć o planach odnośnie 0.24.0.

Aktualnie kluczowym wydaje się wprowadzenie warstw animacji oraz renderowania broni. Jeśli uda się tego dokonać lista zadań do wykonania dla wydania 0.25.0 będzie jednoelementowa: „cała reszta, bez określonej kolejności”.

W tej chwili Chris zajął się usprawnianiem naszej obsługi formatu nif, zwłaszcza w sferze cząsteczek których implementacja wciąż ma charakter ledwie szkieletu. Scrawl pracuje w wielu miejscach równocześnie by uporać się z pomniejszymi problemami przed wydaniem 0.23.0. Glorf także kontynuuje swoją krucjatę wymierzoną w bugi…

Zini jest co prawda chwilowo zajęty przygotowaniami do wydania 0.23.0 i nie ma zbyt wiele czasu do zagospodarowania, jednak pomiędzy jednym mergem, a drugim udało mu się zaimplementować tabelę komórek w edytorze. Jest to po prostu kolejny krok naprzód w długim marszu ─ bez takich prostych ruchów po prostu tkwilibyśmy w miejscu i nigdy nie dotarlibyśmy do mety.