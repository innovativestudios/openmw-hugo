{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-07-12T13:50:47+00:00",
   "title": "Obrazki",
   "type": "post",
   "url": "/2014/obrazki/"
}
Ponieważ, Lgro w swej mądrości zauważył, że:  
<img class="aligncenter" src="https://i0.wp.com/i1.kym-cdn.com/photos/images/original/000/349/104/00d.jpg" alt="Point taken." data-recalc-dims="1" /> 

… zostałem zmuszony do zamieszczenia kilku zrzutów z najnowszej wersji OpenCS.  
[Tu][1] możecie zobaczyć zrzut pokazujący zawartość pojemnika. Zaś [tu][2], możecie zobaczyć jedno okno OpenCS z dwoma widokami zawartości pojemnika (część osób może nie wiedzieć, że w OpenCS idziemy w kierunku multi-wszystko).

By być jeszcze bardziej dobitnym: trzy [widoki][3] w jednym oknie.

A o to [rendering][4]. A nawet [renrenderdering][5]. Rozmawiając z moderami zauważyłem, że wiele osób uskarża się na ograniczenie do jednego okna renderowania w CS. W OpenCS, jak widzicie to nie problem. Oprócz dowolnej liczby doków w oknie, pozwalamy również na dowolną liczbę okien &#8212; przypuszczalnie dobre rozwiązanie z perspektywy posiadaczy kilku ekranowych stanowisk (jak widzicie po antycznej rozdzielczości moich zrzutów, mój ekran jest bardzo stary i takie dywagacje wydają mi się odrobinę abstrakcyjne).

Mam nadzieję, że tych kilka zrzutów wystarczy. A jeśli nie wystarczy, to nic. Niedługo czeka nas wydanie nowej wersji OpenMW i każdy będzie mógł zobaczyć wszystko (i w dowolnej rozdzielczości).

 [1]: https://openmw.org/wp-content/uploads/2014/07/nestedtable.png
 [2]: https://openmw.org/wp-content/uploads/2014/07/nestedtable2.png
 [3]: https://openmw.org/wp-content/uploads/2014/07/nestedtable3.png
 [4]: https://openmw.org/wp-content/uploads/2014/07/rendering1.png
 [5]: https://openmw.org/wp-content/uploads/2014/07/rendering2.png "renrenderdering"