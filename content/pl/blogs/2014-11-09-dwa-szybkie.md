{
   "author": "Vedyimyn",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2014-11-09T09:56:51+00:00",
   "title": "Dwa szybkie…",
   "type": "post",
   "url": "/2014/dwa-szybkie/"
}
Dwa szybkie newsy.

Scrawl i WeirdSexy wspólnie przygotowali filmik o wersji 0.32.0:



Jest to może trochę nie na czasie, skoro jest już wersja 0.33.0, ale życzymy, by pomimo tego, oglądanie postępów było dla Was przyjemnością. Jesteśmy Wam wdzięczni, Scrawl i WeirdSexy!

Poza tym pojawiło się wydanie 0.33.1, które obejmuje tylko źródło (nie paczki). Zaszły po prostu drobne poprawki w kodzie. Szczegóły i źródło na naszym [Githubie][1].

Zapraszamy do komentowania [tutaj][2].

 [1]: https://github.com/OpenMW/openmw/releases/tag/openmw-0.33.1
 [2]: https://forum.openmw.org/viewtopic.php?p=28866#p28866