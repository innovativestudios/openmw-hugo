{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2013-04-15T00:24:28+00:00",
   "title": "Dzisiejszy wpis sponsoruje OpenCS",
   "type": "post",
   "url": "/2013/dzisiejszy-wpis-sponsoruje-opencs/"
}
Zeszły tydzień zaliczyć należy do tych udanych gdyż wydarzyło się wiele pozytywnych rzeczy, a projekt posunął się znacznie do przodu.

Zacznijmy od scrawla. Dodał on obsługę „leveled list”, to jest list określających zawartość skrzyń z losową, zależną od poziomu postaci zawartością.

Oprócz tego scrawl wprowadził również nową funkcję, nieobecną w oryginalnym silniku. Nie wnikając w szczegóły techniczne można ograniczyć się do stwierdzenia, że od teraz wspieramy nieograniczoną ilość tekstur, wliczając to również tekstury terenu. Dzięki temu w przyszłości, po stworzeniu ulepszonego formatu plików gry możliwy stanie się uzyskanie o wiele bardziej szczegółowych krajobrazów, co powinno wlać odrobinę radości do serc moderów oraz rozpalić w nich entuzjazm dla OpenMW ─ przynajmniej w ograniczonym zakresie.

Praca gusa wprowadzające podstawy AI została niedawno wprowadzona do gałęzi main.

Tymczasem lgro powrócił z powodzeniem do planu skonfigurowania serwera automatycznie budującego OpenMW. Jest to rzecz użyteczna ponieważ pozwala nam natychmiast zauważyć fakt utraty możliwości kompilacji na skutek błędów, a co więcej w przyszłości pozwoli wprowadzić testy jednostkowe. Co prawda testy jednostkowe nie są szczególnie przydatne dla gry wideo; jednak wciąż potrafią pomóc, a poza tym: oprócz samego silnika tworzymy przecież edytor OpenCS.

OpenCS rozwija się znakomicie dzięki ziniemu. Pojawiły się nowe tabele: skryptów, dźwięków, regionów jak również edytor skryptów z podświetlaną składnią.

Oprócz tego, jak co tydzień rozwiązaliśmy wiele bugów. Szczególne uznanie w tym względzie należy się Glorfowi, scrawlowi, pvdk oraz wheybags.