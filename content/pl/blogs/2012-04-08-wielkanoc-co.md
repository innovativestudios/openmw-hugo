{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-04-08T21:32:28+00:00",
   "title": "Wielkanoc. No i co?",
   "type": "post",
   "url": "/2012/wielkanoc-co/"
}
Wielkanoc! Święta czy dzień powszedni, choćby się waliło i paliło ─ cotygodnik jest zawsze na posterunku; a poza tym jajka są tak drogie, że w tym roku, zamiast je konsumować na trzysta sposobów przynajmniej część z was rzuci okiem w stronę Naszego Ulubionego Projektu Open Source.

Lokalny guru od spraw spektakularnej grafiki komputerowej (dla niewtajemniczonych: scrawl) w tym tygodniu zrobił naprawdę wiele. I nie będę pisać o jego osiągnięciach. Musiałbym naszpikować tekst fachową terminologią którą rozumie pewnie jakieś 10% czytelników, a poza tym warto chyba przypomnieć powiedzenie &#8220;Jeden obraz wyraża więcej niż tysiąc słów&#8221; z dobitnością wprost <a href="http://openmw.org/wiki/images/8/89/014-8.png" title="OMG! OMG!" target="_blank">niewiarygodną&#8230;</a>

Jeśli sprzęt jest niedostatecznie wydajny i nie macie ochoty na oglądanie pokazu slajdów, odbicia można wyłączyć całkowicie lub też wyłączyć jedynie częściowo ─ przy takiej konfiguracji w wodzie wciąż odbija się ziemia oraz niebo.

Parę takich romantycznych zrzutów ekranu i może w końcu zakończy się marudzenie o MGE wymieszane z namolnymi pytaniami o możliwość ulepszenia grafiki. Nie potrzeba nam MGE by renderować wodę bardziej zwierciadlaną niż w górskim jeziorze lub nawet odległego terenu. Zasadniczym powodem który powstrzymuje nas przed ulepszeniem grafiki do poziomu wywołującego opad szczęki, mięknięcie kolan i wzwód prącia jest lista funkcji oddzielających nas od w pełni działającego, grywalnego 1.0.0 połączona z naszym mocnym przekonaniem o tym, że gry służą do grania, a nie do podziwiania widoków.

Kromgrat znudził się najwyraźniej naprawianiem bugów i postanowił spróbować swoich sił w rozwijaniu nowej funkcji ─ opcją &#8220;zawsze widoczne&#8221; w oknach. W morrowind okna GUI mają w prawym górnym rogu niewielką ikonkę po naciśnięciu której okno jest widoczne również w trakcie gry. Jest to opcja użyteczna szczególnie w wypadku okna mapy: kompas w rogu wyświetla tylko maciupeńki fragment otoczenia, a na dodatek nie sposób zmusić go do wyświetlania globalnej mapy, tak przydatnej gdy przedzieramy się przez odludzia w poszukiwaniu odległych ruin&#8230;

ACE podjął się zadania które choć bardzo nudne i odkładane na bok przez kolejnych deweloperów jest bez cienia wątpliwości bardzo istotne: record saving.

gus zajął się logiczną kontynuacją dialogów, to jest implementacją gildii. Gdy ukończy swoje zadanie w końcu będziemy mogli zapisać się do gildii wojowników i podjąć się misji zabicia szczurów demolujących skład poduszek. Byłoby to bardzo sympatyczne rozszerzenie doznań płynących z testowania. ;-)

swick zamknął zadanie importowania ustawień z morrowind.ini. Corristo dokończył funkcję wyświetlania ścieżek ruchu npc. Dwa kolejne zadania zakończone. :-)

Chris, jak się okazało jest nie tylko czarodziejem OpenAL ─ jego doświadczenie obejmuje także inne dziedziny programowania przydatne w tworzeniu gier video. Podjął się więc próby zmuszenia OGRE do odczytania animacji nif. Co prawda już teraz (dzięki jhooks1) jesteśmy w stanie animować modele ale robimy to manualnie ─ nasza metoda jest niewydajna i odbija się na FPS.

Jeśli chodzi o jhooks1, cóż; pracuje nad wyświetlaniem odzienia. Miejmy nadzieję, że już wkrótce dopnie swego bo to zadanie było już odkładane zbyt wiele razy.

lgro nie ma zbyt wiele czasu, zdecydowanie za mało by rozpocząć pracę nad dużym zadaniem; ale broni nie składa i wciąż jest w stanie naprawić wiele bugów. I dokładnie to robi.

Zini dopiero co zaczął pracę na nowym zadaniem, o którym napiszę już w przyszłym tygodniu&#8230; 

Przynajmniej część z was pamięta Star-Demona i jego filmiki na youtube okraszone głosem dziwnie kojarzącym się z komentatorem sportowym. Okazało się, że oddzieliła go od nas awaria sprzętu &#8212; bez działającej karty graficznej nie ma mowy o nagrywaniu czegokolwiek. Na szczęście to już przeszłość i dawny załogant wznowił służbę na okręcie OpenMW ;-)