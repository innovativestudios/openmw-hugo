{
   "author": "Vedyimyn",
   "categories": [
      "Uncategorized"
   ],
   "date": "2017-09-09T14:58:26+00:00",
   "title": "Wielkość, która nadejdzie…",
   "type": "post",
   "url": "/2017/wielkosc-ktora-nadejdzie/"
}
Minął nieco ponad miesiąc od wydania OpenMW w wersji 0.42.0. Jeśli przypomnicie sobie, kiedy wydano wersję 0.41.0, to zauważycie, że stworzenie nowego wydania zajęło nam osiem miesięcy, co było jednym z największych odstępów pomiędzy wydaniami, pomimmo że nie różniło się ono znacząco pod względem objętości od wydań je poprzedzających.

Bynajmniej nie oznacza to, że deweloperzy OpenMW się obijali. Albo że przez te osiem miesięcy uczyniono mniejszy postęp niż zwykle. Powodem opóźnienia było to, że nasz komputer od edycji wideo miał awarię w wysoce niefortunnym czasie. Gdy dyskutowaliśmy, czy wydanie powinno mieć miejsce pomimo braku wideo, ustaliliśmy, że wstrzymamy je do czasu rozwiązania problemów ze sprzętem. W&nbsp;końcu jeśli ktoś chce zagrać w najnowszą wersję OpenMW nie musi czekać na wydanie, skoro może skorzystać z wydań nocnych, który ostatnio uczyniliśmy łatwiej dostępnymi poprzez podanie linku na stronie [„Do ściągnięcia”][1].

Dobra wiadomość jest taka, że o ile proces wydań został zawieszony, o tyle praca nad następną wersją już ruszyła, i znaleźliśmy się w takim punkcie, że mogliśmy wydać wersję 0.43.0 dzień po wersji 0.42.0. W&nbsp;momencie pisania tych słów mamy 85 zamkniętych problemów, co czyni to wydanie większym niż poprzednie. Spójrz na nasz [bugtracker][2], by zobaczyć szczegóły.

Nieco ponad miesiąc temu byliśmy również świadkami wydania TES3MP w wersji 0.6.0, a nieco później [łatki 0.6.1][3], z widokami na następną wersję już wkrótce, w połowie września. Odkąd TES3MP zaczęło bazować na silniku OpenMW, otrzymaliśmy część uwagi mediów i przybyło nam chętnych na deweloperów.

Dzięki nowym ludziom na pokładzie, wzrosło zainteresowanie poprawą dokumentacji kodu. Jeden z&nbsp;nowych programistów zaczął badać, czy byłoby możliwe rozszerzenie animacji .OSG lub sprawienie, by było możliwe czytanie przez OpenMW innych plików .nif, poza tymi z Morrowinda. Scrawl wypuścił swoje niekompletne eksperymenty z cieniami, a stary człone zespołu, o pseudonimie AnyOldName3 próbuje sprawić, by zaczęły działać. Chociaż nagli mnie, bym o tym wspomniał, nie ma gwarancji, że mu się uda. Powrócił także Aeslywinn, który dawniej włożył sporo pracy w OpenMW-CS i wrócił tam, gdzie skończył.

Jego nowa funkcja, już włączona do OpenMW, daje moderom komfort przeładowywania zasobów w&nbsp;OpenMW-CS bez przeładowywania programu. Jego następnym przedsięwzięciem wydają się być rekordy lądu, co byłoby krokiem naprzód w kierunku możliwości edycji krajobrazu, głównej brakującej funkcji OpenMW-CS.

Co prawda mamy wystarczająco dużo zamkniętych zgłoszeń, by wydać słusznych rozmiarów nową wersję, nie rozpoczęto jeszcze procesu wydawania nowej wersji, w końcu dopiero wypuściliśmy wersję 0.42.0. Prawdopodobnie nadchodzące wydanie będzie dużo większe, niż zwykle, i nastąpi za około dwa miesiące, ze wspomnianymi rekordami lądu i innymi funkcjami, nad którymi trwa teraz praca. Ponadto, obszary, które trwały w stagnacji przez ostatnie kilka miesięcy, znów są w widoczny sposób rozwijane, a&nbsp;przyszłość projektu wygląda całkiem dobrze.

##### Zapraszamy do komentowania [tutaj][4].

#####

 [1]: https://openmw.org/downloads/
 [2]: https://bugs.openmw.org/versions/52
 [3]: http://steamcommunity.com/groups/mwmulti#announcements/detail/1443822567710521369
 [4]: https://forum.openmw.org/viewtopic.php?f=38&t=4620