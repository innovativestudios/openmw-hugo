{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-01-06T11:11:59+00:00",
   "title": "Czary, choroby, gender i inne diabelstwa.",
   "type": "post",
   "url": "/2014/czary-choroby-gender-inne-diabelstwa/"
}
Nowe efekty magiczne. Chodzenie po wodzie, paraliż, dezintegracja, wszystkie powiązane z teleportacją (oznaczenie, przeniesienie, interwencje) jak również z AI (prowokacja, uspokojenie itp.) i atrybutami (przywrócenie oraz obniżenie). Jestem pod wrażeniem, a w związku z tym: pieseł.

<img src="https://i0.wp.com/openmw.org/wp-content/uploads/2014/01/wow-scrawl2.png?ssl=1" alt="Wow scrawl." data-recalc-dims="1" /> 

Jak łatwo się domyślić jest to wynik wysiłków scrawla, naszego naczelnego pracoholika. Oprócz tego, zajmuje się on teraz implementowaniem chorób ─ co oczywiście również łączy się z efektami magicznymi.

Jednak najważniejszą wiadomością, którą chciałbym Wam przekazać jest jednak zbliżanie się wydania 0.28.0. Będzie to pod względem wprowadzonych funkcji, najprawdopodobniej największa i najbardziej imponująca wersja w dotychczasowej historii projektu. Dość powiedzieć, że będzie w niej obecne rzucanie zaklęć oraz sztuczna inteligencja kontrolująca bohaterów niezależnych w trakcie walki.