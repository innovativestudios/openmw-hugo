{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-11-19T08:26:01+00:00",
   "title": "Niespodzianka!",
   "type": "post",
   "url": "/2013/niespodzianka/"
}
Witajcie moi drodzy. Spokojnie, nie mamy już niedzieli, a dopiero piątek! W drodze wyjątku postęp prac zostanie opisany już dzisiaj.

A dzieje się wiele. Przede wszystkim Scrawl wprowadził już zaklęcia o zasięgu &#8220;Dotyk&#8221;, a co więcej teraz przymierza się także do zaklęć działających na dystans ─ i to pomimo braku obecności broni miotającej pociski. Oprócz tego także poprawił kod animacji postaci oraz oczyścił obsługę ekwipunku.

Zini również dzielnie pracuje nad OpenCS ─ głównie dokańczając zadania, o których wspominałem w zeszłym tygodniu, ale również rozpoczął pracę nad kilkoma nowymi rejonami.

Niestety, sprawa biblioteki Bullet utkwiła w martwym punkcie. Błąd nadal się tam znajduje, i naprawdę nie jesteśmy go w stanie naprawić samodzielnie. Jedyna nadzieja w zespole samego Bullet. Pomóżcie nam, proszę zwrócić uwagę na ten problem.

[https://code.google.com/p/bullet/issues/detail?id=371][1]

Pod tym linkiem znajduje się wspominany bug. Chciałbym by część osób zachciała się pofatygować i oznaczyć go gwiazdką. Z góry dziękuję.

 [1]: https://code.google.com/p/bullet/issues/detail?id=371 "click me"