{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2012-07-10T17:02:53+00:00",
   "title": "Koniec czerwca…",
   "type": "post",
   "url": "/2012/koniec-czerwca/"
}
Witajcie wszyscy,

Okropnie zaniedbałem pisanie postów w języku ojca i dziada mojego, wierzę jednak wiarą silną i mocną, graniczącą z obłędem na rzece naiwności, że w sercach waszych kryje się dość miłosierdzia i litości by przebaczyć ten jeszcze jeden raz.

Zgodnie z utrwalonym w tradycji cyklem prac nad OpenMW jak zwykle, po miesiącu wydajemy nową wersję programu. Tym razem nosi numer 0.16.0. W związku z tym już wkrótce internet zaleją posty niespełnionych bloggerów takich jak ja, a kultowy WeirdSexy przygotował już kolejny kultowy odcinek z kultowej serii filmików w których prezentuje nowości obecne w nowym wydaniu i okrasza je swoim komentarzem wyartykułowanym dźwięcznym głosem profesjonalnego spikera radiowego. Dotychczasowe prezentacje cieszyły się zaskakująco dużą oglądalnością i wydaje się, że rozpropagowały wiedzę o OpenMW wśród internautów skuteczniej nawet niż wszystkie artykuły razem wzięte. 

Web 2.0, ot co.

Jeśli chodzi o aktualne wydarzenia nie związane z popychaniem numerków do przodu to muszę (tak! MUSZĘ!) wspomnieć o użytkowniku Myckel. Otóż dotarliśmy do punktu którego miało nie być. Zatrzymał nas brak znajomości morrowindowej matematyki, nie wiemy jak dokładnie obliczyć wiele parametrów używanych przez morrowinda. Odkrywanie formuł nie wymaga umiejętności programistycznych, ani znajomości C++ (wystarczy długopis, cierpliwość oraz wolny czas), a mimo to dotychczas nie było wielu osób które zdecydowały się tym zająć. Prawdę powiedziawszy to przytłaczająca większość zawartości naszej wiki w zakresie wzorów została dodana przez jednego tylko użytkownika. Oczywiście odwalił on niesamowicie wielki i potrzebny kawał roboty ale jednak nie całość. 

Gdy więc dotarliśmy do punktu gdy nastała pora na jedynie doszlifowanie naszej alchemii okazało się, że Zini nie może wykonać choćby jednego kroku naprzód. Z pomocą przyszedł właśnie Myckel który dzielnie pomagał: notował sukcesy i porażki przy określonych wartościach parametrów, wyszukiwał znaczenie pewnych wartości zaszytych w bebechach esm. 

Krótko mówiąc Myckel zawstydził nas, nie-programistów. A mnie chyba w szczególności.

Ponieważ Zini nie może posunąć się naprzód z Alchemią w międzyczasie rozpoczął kolejną rundę refaktoryzacji, tym razem w MW-subsystem. Wszystkie informacje które mogą być istotne z punktu widzenia dewelopera można znaleźć w tym [wątku na forum][1].

Niestety prace na nowym systemem animacji opartym na tym co oferuje biblioteka OGRE3D, zamiast na rozwiązaniach manualnych znowu stanęły w miejscu. Miejmy nadzieję, że nie na długo.

Od kiedy pvdk jest znowu obecny w sieci ponownie możemy pochwalić się zmianami w launcherze. Dodane zostały nowe opcje konfiguracyjne, a dzięki nowym grafikom program olśniewa jeszcze bardziej serca i dusze estetów, swym nieśmiertelnym blaskiem piękna&#8230; No dobrze, autorem nowych, lepszych grafik jest serpentine.

Scrawl tym razem skupił się na paru błędach. Przykładowo w 0.16.0 jeszcze będzie obecna brzydka regresja dopadająca użytkowników którzy używają DX (wine lub windows) przy renderowaniu: wszystkie tekstury są czarne. Co prawda rozwiązanie jest gotowe, ale gotowe zbyt późno by trafić do tego wydania&#8230;

Eli2 który poprzednio zyskał sławę swoimi zabawami z goglami 3D i OpenMW tym razem eksperymentuje z edytorem OpenMW. Długo oczekiwanym edytorem, jeśli pozwolicie, że to podkreślę&#8230; Wspaniale byłoby mieć w końcu edytor, a przynajmniej jego zaczątek przed OpenMW 1.0.0.

 [1]: http://openmw.org/forum/viewtopic.php?f=6&t=863 "http://openmw.org/forum/viewtopic.php?f=6&t=863"