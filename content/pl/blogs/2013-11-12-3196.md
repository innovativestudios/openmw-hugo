{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-11-12T06:58:41+00:00",
   "title": "Nowiny",
   "type": "post",
   "url": "/2013/3196/"
}
Jednym z elementów który zagnieździł się na stałe w pamięci chyba każdego gracza mającego styczność z Morrowindem są &#8220;Zwoje Lotu Ikara&#8221;. Jeśli jakimś cudem ich nie kojarzycie (bo najpewniej nie graliście jeszcze w tą grę) to już śpieszę z wyjaśnieniami: otóż zwój ten podnosił na kilka sekund akrobatykę o tysiąc poziomów, co umożliwia to wykonywanie gigantycznych skoków. A teraz na dodatek działają także z OpenMW!

<a href="https://www.youtube.com/watch?v=-6l_B98P7P8" title="kliknij proszę!" target="_blank">https://www.youtube.com/watch?v=-6l_B98P7P8</a>

To rezultat ostatnich osiągnięć scrawla: efekty magiczne podnoszące i obniżające statystyki postaci już działają. Poza tym scrawl pracuje nad implementacją czarów działających na odległość dotyku.

Oprócz tego mamy kilka innych, także ciekawych nowości: zini pracuje nad obsługą dialgów w OpenCS, zaś graffy przygotowuje ustawienia do wprowadzenia kolejnych funkcji.

Najważniejszą informacją jest jednak zbliżające się, kolejne wydanie OpenMW. Jak już pisałem tym razem pojawi się ono wraz z edytorem, ale oczywiście nie zabraknie nowych funkcji i poprawek na stare błędy.