{
   "author": "Vedyimyn",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2014-09-25T18:47:08+00:00",
   "title": "Kwestia czasu",
   "type": "post",
   "url": "/2014/kwestia-czasu/"
}
Po wypuszczeniu wersji 0.32.0, ruchliwy zespół rozpoczął marsz ku wersji 0.33.0. Załatwiono już 50 bugów i zaimplementowano kilka nowych funkcji.

Zini kontynuuje pracę nad OpenCS. Dodano doń innowacyjną funkcję, która ułatwi życie wielu moderom. Kiedy tworzy się wtyczkę, wypada ją przetestować, prawda? W tym celu należałoby uruchomić OpenMW, pójść w stosowne miejsce/wczytać grę i sprawdzić, czy mod działa. I tak kilka razy, jeśli się robi poprawki. Czy nie byłoby lepiej, gdyby istniał przycisk, który uruchomi OpenMW i ustawi gracza tam, gdzie była ustawiona kamera OpenCS? Ten przycisk już istnieje i jest zaimplementowany w OpenCS ;)

Do OpenMW samego w sobie też dodano ciekawe funkcje. Jedną z nich jest muzyka, gdy postać awansuje (o jejciu!) lub gdy umiera (buu&#8230;). Zakres naprawionych bugów rozciąga się od zjawisk dziwnych ([0 sztuk złota grzywny po odsiadce][1]), do wręcz przykrych, do jakich należy [topienie się pary NPC-ów po wykonaniu zadania][2].

Minął ledwie tydzień od wydania 0.32.0, a na horyzoncie majaczy już kolejne większe wydanie, 0.33.0. Kiedy spoglądamy wstecz, widać pozytywną tendencję. Jest rozwiązywana coraz większa liczba problemów. W wersji [0.29.0][3] rozwiązano ich 67, [0.30.0][4] &#8211; 84 błędy, [0.31.0][5] &#8211; rekordowa liczba 183 naprawionych bugów, [0.32.0][6] &#8211; też niemało, bo solidne 144 rozwiązane problemy.

Nie wszystkie zagadnienia są tu sobie równe. Funkcji (features) jest niewiele, ale stają się coraz trudniejsze w implementacji, teraz, gdy zaczyna już chodzić nie tyle o całokształt, co o szczególiki. Bugi są raczej małe, ale liczne. Odstęp czasowy między kolejnymi wydaniami znacznie się wydłużył. Kiedyś nowe wersje ukazywały się co miesiąc. Ostatnie wydanie zajęło więcej niż dwa miesiące. Bądźmy szczerzy. Chociaż ostatnie changelogi i duże liczby są fajne i w ogóle, to nie chodzi tu o robienie wrażenia. Wiesz zapewne, drogi Czytelniku, że OpenMW jest projektem, w którym chodzi o sprawy większe niż liczbę generowanych cieni, inaczej nie śledziłbyś tego wielkiego przedsięwzięcia. I Ty i ja jesteśmy ludźmi myślącymi praktycznie.

I taki jest nasz zespół. Ci ludzie wiedzą, że optymalne wykorzystanie czasu zakłada czasem zrobienie kroku w tył i ponowne przemyślenie planu wydań. Długi cykl wydań nie wygląda może „fajnie”, ale daje drużynie możliwość dopieszczenia wszystkiego i zaimplementowania bardziej skomplikowanych funkcji bez pośpiechu. Z drugiej strony częstsze wydania sprawiłyby, że gracze mieliby nowe funkcje dostępne wcześniej. To naprawdę pomaga w testowaniu, gdy każdy ma najnowszą wersję projektu. Musimy znaleźć złoty środek między tymi podejściami, zwłaszcza teraz, gdy zbliżamy się do wersji 1.0.

Co do testowania. Ludzie, którzy nie mają pojęcia o programowaniu, ale którzy grają, nieraz znajdują błędy, których nasz zespół nie znalazł. Często piszą nam, że jest jakiś problem z questem, albo ogólnie z gameplayem. Właśnie ci ludzie odwalają kawał dobrej roboty, która w przyszłości pozwoli nam cieszyć się wersją 1.0 wolną od błędów. Bardzo dziękujemy tym wszystkim testerom. Wasze bystre oczy i chęć pomocy zasługują na naszą wdzięczność. Trzykrotnie Wam błogosławimy!

Zapraszamy do komentowania [tutaj][7]!

 [1]: https://bugs.openmw.org/issues/1892
 [2]: https://bugs.openmw.org/issues/1911
 [3]: https://bugs.openmw.org/versions/20
 [4]: https://bugs.openmw.org/versions/22
 [5]: https://bugs.openmw.org/versions/23
 [6]: https://bugs.openmw.org/versions/24
 [7]: https://forum.openmw.org/viewtopic.php?f=38&t=2473