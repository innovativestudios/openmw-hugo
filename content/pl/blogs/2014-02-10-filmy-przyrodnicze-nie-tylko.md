{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-02-10T09:08:37+00:00",
   "title": "Filmy przyrodnicze i nie tylko.",
   "type": "post",
   "url": "/2014/filmy-przyrodnicze-nie-tylko/"
}
… a, wszędzie gdzie wkroczył scrawl, bugi padały pod jego potężnymi ciosami, ścieląc się u jego stóp o starannie przyciętych paznokciach… Nie: nie mogę tego, tak ciągnąć. Wszyscy czytelnicy powinni wymazać z pamięci to co przed chwilą przeczytali i przejść do następnego akapitu.

Faktycznie jednak: scrawl rozwiązał w mijającym tygodniu naprawdę wiele błędów, a ponadto włożył znaczący wkład w gui dla zapisywania i wczytywania stanu gry. Jakby tego było jeszcze mało, zaczął również prace nad obsługą broni dystansowej.

Tymczasem zini zajął się usprawnieniami w dziedzinie obsługi skryptów. Pojawił się &#8220;warning mode&#8221; kompilatora ─ funkcja bez wątpienia użyteczna. OpenMW obsługuje także skrypty wierniej oryginalnemu silnikowi ─ co niestety oznacza wkraczanie w obszar absurdu znanego z Morrowinda. By zachować wasze umysły w stanie względnie nie skażonym szaleństwem Bethesdy z przed ponad dekady, pominę szczegóły.

Ciekawie prezentują się za to ostatnie zmiany w obsłudze animacji postaci. W momencie gdy piszę ten post jeszcze nie znalazły się w gałęzi głównej ale już ewidentnie widać, że MrCheko poprawił wiele aspektów rozgrywki powiązanych z animacjami.

Od dawna na blogu nie było żadnych zabawnych filmików prezentujących bugi ─ a przecież to właśnie to tygryski lubią najbardziej. Tak czy owak, o to kolejny dzień z życia przyrody i mieszkańców prowincji Morrowind.

  
Tu, widzimy niezwykle trudny do zarejestrowania, taniec godowy netchów. Podekscytowany samiec demonstruje przed samicą swoją sprawność. Tubylcy pozostają w bezruchu by uniknąć agresywnych zachowań zwierząt.

  
Mroczne elfy znane są ze swej osobliwej, heretyckiej religii. Na oddawanie czci składa się wiele złożonych rytuałów, a integralnym i niezbywalnym elementem wielu spośród nich są wielokrotnie powtarzane pokłony.

  
Tu obserwujemy inny, tajemniczy rytuał którego znaczenie pozostaje dla nas nieznane.