{
   "author": "sir_herrbatka",
   "date": "2013-06-20T12:38:24+00:00",
   "title": "Zaniedbywałem bloga ─ przepraszam.",
   "type": "post",
   "url": "/2013/zaniedbywalem-bloga-─-przepraszam/"
}
Ale mam już więcej czasu!

Szkoda tylko, że nie dzieje się zbyt wiele niezwykle ciekawego. Chlubnym wyjątkiem są ostatnie wysiłki scrawla w kierunku wprowadzenia obsługi wejścia opartego na SDL2, a to pozwoli ostatecznie pogrzebać OIS ─ miejmy nadzieję, że także wszelkie bóle, problemy, kłopoty i braki jakie się z nim wiążą!

Poza tym wywołanie dziennika powoduje odtworzenie odpowiedniego dźwięku. Można także przewracać strony za pomocą kółka myszy: z pewnością sympatyczną opcją.

Glorf zaś pracuje nad edytorem. Co prawda jeszcze na dobrę się nie rozgrzał, ale widać jak rozwija swoje skrzydła…