{
   "author": "sir_herrbatka",
   "date": "2013-09-19T13:04:24+00:00",
   "title": "Trochę nuda.",
   "type": "post",
   "url": "/2013/troche-nuda/"
}
Witajcie!

Chyba najciekawszą wieścią z ubiegłego tygodnia, jest rozpoczęcie prac nad opcją zapisu w edytorze. Wprowadzenie tej funkcji umożliwi wykorzystanie edytora OpenCS do tworzenia i modyfikowanie pluginów gry, a to z kolei zachęci do testowania, i poprawi morale zespołu — podniesienie użyteczności aplikacji ponad poziom 0 po prostu musi wpłynąć pozytywnie na nastroje. 

Tymczasem Chris nadal odprawia rytuały voodoo w kodzie fizyki (nie znam się na voodoo, nawet na wódzie nie bardzo — nie pytajcie o co tu w zasadzie chodzi).

Prace nad implementacją TGM również są w toku, co na pewno ucieszy nałogowych cheaterów pomimo nawet braku zbyt wielu dróg do grobu bohatera głównego.

I to niestety wszystko. Ten tydzień nie był szczególnie udany.