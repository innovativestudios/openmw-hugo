{
   "author": "Vedyimyn",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2014-12-04T19:28:33+00:00",
   "title": "Wikitalizacja",
   "type": "post",
   "url": "/2014/wikitalizacja/"
}
Witojcie! Mamy nadzieję, że filmiki się podobały (pomimo opóźnienia). Bez słodkiego jak nektar, pięknego głosu WeirdSexy, to nie byłoby to samo… Gdy tylko wydano 0.33.0, drużyna nie spoczęła, lecz od razu rozpoczęła pracę nad 0.34.0. Ach, ci pracoholicy… Nie, żebym narzekał. Kolejne błędy, ku naszej radości, zostały naprawione. Przyjrzyjmy się kilku z nich.

Można pomyśleć, że Argonianie, po tym, co przeżyli na Vvardenfell, powinni się cieszyć z wyzwolenia z okowów. Jednakowoż byli raczej zazdrośni o swoje bransoletki, nawet po uwolnieniu. Po podniesieniu kajdan z podłogi, okazywali oburzenie. Nigdy więcej. Po poprawce będą raczej wdzięczni, niż pogardliwi.

Pojawiło się parę problemów z modem Fair Magicka Regen. OpenMW czyta skrypt poprawnie, ale sam mod zawiera buga ([link][1]). Mamy nadzieję, że autor gdzieś się tutaj zakręci, i umieści poprawioną wersję na Nexusie. :)

A skoro już jesteśmy przy modach, to warto wspomnieć, że nasza koleżanka, Archonaut Tinker, dodała na Wiki ([link][2]) listę kompatybilnych modów, wraz z opisem stopnia kompatybilności. Wiele z nich powinno działać bez komplikacji („prosto z pudełka”), ale dobrze mieć w jednym miejscu informację o tym. Jeśli nie potrafisz programować, ale lubisz pomagać i masz ku temu zapał, prosimy o wspomożenie nas w budowaniu tego repozytorium.

A teraz poruszmy inne sprawy. Nasza maszyna programistyczna (Scrawl) potrzebuje wymiany oleju. Dużo pracował dla OpenMW, a także poza OpenMW. Teraz dogoniły go problemy życia codziennego, ale gdy wszystko zostanie załatwione, ponownie będzie nas wspierać, co ma szansę stać się dość prędko. Czekamy na to niecierpliwie. Scrawl był z nami zawsze, więc możecie sobie wyobrazić, jak bardzo nam go brakuje.

Jeszcze jeden znajomy nick, Raevol (szczególnie znany naszym przyjaciołom na /r/morrowind). Otóż wpadł on na pomysł, żeby ukończyć wszystkie funkcje (features) pozostałe do zaimplementowania, i z fazy alfa przejść do fazy beta. Obecnie trwa dyskusja nad wadami i zaletami tego planu. Być może niedługo pojawi się na blogu informacja w tym temacie, więc zostańcie z nami!

Zapraszamy do komentowania [tutaj][3]!

 [1]: https://bugs.openmw.org/issues/2080
 [2]: https://wiki.openmw.org/index.php?title=Mod_status
 [3]: https://forum.openmw.org/viewtopic.php?f=38&t=2612