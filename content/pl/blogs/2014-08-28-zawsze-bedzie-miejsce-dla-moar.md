{
   "author": "Vedyimyn",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2014-08-28T19:44:42+00:00",
   "title": "Zawsze będzie miejsce dla MOAR",
   "type": "post",
   "url": "/2014/zawsze-bedzie-miejsce-dla-moar/"
}
Wiadomości z poprzedniego tygodnia o nowym projekcie (wstępnie nazwanym Morrowind Original Asset Replacement, w skrócie MOAR) umożliwiającym stworzenie nowych zasobów do Morrowinda zostały bardzo ciepło przyjęte. Zgłosiło się wielu ludzi, oferujących nie tylko swoje umiejętności, ale także wyrażających swoje opinie na temat projektu oraz na temat możliwych konsekwencji prawnych. Zastanawiano się też, jaka powinna być relacja projektu i OpenMW. Ostatecznie stwierdzono, że projekt będzie modem do Morrowinda, ale powinien działać z OpenMW. Jeśli chcesz się przyczynić do tej zacnej sprawy, zapraszamy na stronę nowego projektu ([link][1])  lub do wątku na forum ([tutaj][2]). Możesz też po prostu dołączyć do [tej][3] dyskusji i poczytać, co ludzie piszą.

Wracając do OpenMW. Popracowano nad interfejsem użytkownika &#8211; w aspekcie zarówno programowym, jak i sprzętowym. Jeśli chodzi o stronę sprzętową, poczyniono kilka eksperymentów nad obsługą gamepadów. Osiągnięto połowiczny sukces. Gamepady Wireless 360 działały bez zarzutu, ale PS3 dualshock już nie. To oznacza, że wsparcia jako takiego jeszcze nie ma, i że potrzebne jest jeszcze trochę rozmyślań na ten temat.

Od strony programowej uczyniono trochę mniejszych poprawek. Tu centrowanie linii z tekstem, tu zmiana rozmiaru przycisków, tam „dymka” z podpowiedzią&#8230; Z tym wszystkim obcuje się praktycznie cały czas w trakcie gry, dlatego musi być wykonane bardzo dobrze. Poprawiono jeszcze kilka innych drobnostek. Można od teraz elegancko kartkować książki kółkiem myszki, albo dokładnie umiejscawiać komentarze na mapie, tak jak to jest możliwe w oryginalnym Morrowindzie. Udoskonalono też mechanizm walki. Kiedy kondycja NPCa osiągnie poziom zerowy, ten zemdleje. Ponadto będzie możliwość obrabowania go, zanim wróci do zmysłów.

Wszystkie te drobiazgi razem wzięte oraz fakt, że została niedawno wypuszczona nowa wersja MyGUI (poprawiona także pod względem wspomnianych ostatnio wycieków pamięci), mogą zwiastować powstanie wkrótce OpenMW w wersji 0.32.0, o czym są już pogłoski wśród developerów. Zawsze dobrze to słyszeć.

Zapraszamy do komentowania [tutaj][4]!

 [1]: http://inglip.org/
 [2]: https://forum.openmw.org/viewtopic.php?f=2&t=2349
 [3]: https://forum.openmw.org/viewtopic.php?f=2&t=2318
 [4]: https://forum.openmw.org/viewtopic.php?f=38&t=2354