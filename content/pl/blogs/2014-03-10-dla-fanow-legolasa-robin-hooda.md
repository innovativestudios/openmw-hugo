{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-03-10T18:34:28+00:00",
   "title": "Dla fanów Legolasa i Robin Hooda",
   "type": "post",
   "url": "/2014/dla-fanow-legolasa-robin-hooda/"
}
Czy są pośród nas i tacy, co ponad rozkosz walki twarzą w twarz z wrogiem preferują broń dystansową? Łuki, broń miotaną, a może kusze? Na pewno są, i na pewno się ucieszą na tę o tą nowinę: od teraz możecie strzelać i rzucać do woli! OpenMW właśnie doczekało się obsługi wszelkiego ustrojstwa z kategorii &#8220;Celność&#8221;. Oczywiście stoi za tym nie kto inny, jak sam scrawl – nie pomyślcie jednak, że reszta zespołu grzeszy lenistwem.

Przykładowo gus wprowadził kolejną porcję poprawek i ulepszeń przeznaczonych na bolączki, wciąż lekko niedomagającej sztucznej inteligencji postaci w grze. Zini udoskonala, wspomnianą w zeszłym tygodniu, nawigację 3D pola renderowania; a ja tymczasem pracuję nad widokiem edycji rekordu – co też prezentuję na poniższym zrzucie.  
[<img loading="lazy" src="https://i0.wp.com/openmw.org/wp-content/uploads/2014/03/zrzut961.png?resize=300%2C240&#038;ssl=1" alt="zrzut96" width="300" height="240" class="alignnone size-medium wp-image-3490" srcset="https://i0.wp.com/openmw.org/wp-content/uploads/2014/03/zrzut961.png?resize=300%2C240&ssl=1 300w, https://i0.wp.com/openmw.org/wp-content/uploads/2014/03/zrzut961.png?resize=1024%2C819&ssl=1 1024w, https://i0.wp.com/openmw.org/wp-content/uploads/2014/03/zrzut961.png?w=1280&ssl=1 1280w" sizes="(max-width: 300px) 100vw, 300px" data-recalc-dims="1" />][1]  
I tym optymistycznym akcentem zakańczam niniejszy wpis.

 [1]: https://i0.wp.com/openmw.org/wp-content/uploads/2014/03/zrzut961.png?ssl=1