{
   "author": "Vedyimyn",
   "categories": [
      "Nowa wersja"
   ],
   "date": "2017-12-06T20:15:16+00:00",
   "title": "Wersja 0.43.0 i pożegnanie",
   "type": "post",
   "url": "/2017/wersja-0-43-0-pozegnanie/"
}
Zespół OpenMW ma zaszcyt ogłosić wydanie wersji 0.43.0! Do pobrania, jak zwykle, [tutaj][1], dla wszystkich systemów operacyjnych. Dodano wiele nowych funkcji i poprawek, poprawiono sztuczną inteligencję i&nbsp;skróty klawiszowe dla menu.

Koniecznie zobaczcie [film o OpenMW][2] i [film o OpenMW-CS][3], autorstwa czcigodnego Atahualpy.





By zobaczyć listę zmian, zapraszam na angielską wersję blogu. Zapraszam tam również czytelników polskiej strony, zainteresowanych kolejnymi newsami.

Od kilku lat tłumaczyłem posty z angielskiej strony. Cieszę się, że mogłem umożliwić lekturę osobom, które z żywym zainteresowaniem chcą śledzić dalsze losy projektu, czytając materiały na ten temat w&nbsp;swoim własnym języku. Myślę, że nadszedł dla mnie czas, bym ustąpił z tego stanowiska.

Polska strona może być od tej pory rzadziej aktualizowana, w związku z czym proszę o śledzenie strony angielskiej. Gdyby ktoś chciał kontynuować tłumaczenia, lub pisać własne posty w języku polskim, oczywiście zapraszam do kontaktu na forum. Być może znajdzie się nawet ktoś, kto miałby chęć i&nbsp;umiejętności, by dodać polskie napisy do filmów chcigodnego Atahualpy, kto wie? ;) Myślę, że byłoby to znaczącym dziełem.

Z pozdrowieniami  
i życzeniami wytrwałości  
dla Czytelników  
i całego zespołu OpenMW,  
Vedyimyn

##### Zapraszamy do komentowania <a target="_blank" href="https://forum.openmw.org/viewtopic.php?f=38&t=4801">tutaj</a>.

 [1]: https://openmw.org/downloads/
 [2]: https://www.youtube.com/watch?v=F0MPLXdl0y4
 [3]: https://www.youtube.com/watch?v=88ywx8BSyKg