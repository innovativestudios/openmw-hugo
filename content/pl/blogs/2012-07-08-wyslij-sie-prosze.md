{
   "author": "sir_herrbatka",
   "date": "2012-07-08T19:43:36+00:00",
   "title": "Wyślij się, proszę.",
   "type": "post",
   "url": "/2012/wyslij-sie-prosze/"
}
Jak pewnie (mam cichą, nieśmiałą i taką śmietankową nadzieję, że ktoś to właściwie czyta) część z was zauważyła, że strona openmw.org była niedostępna. Wynikała to z nagłego, gwałtownego i skokowego wzrostu ilości odsłon strony po tym, gdy na slashdot.org pojawił się news z linkiem. 

Czytelnicy klikali link, przechodzili na naszą stronę, a ponieważ były ich istne hordy wkrótce jedyne co mógł zrobić admin zalogowany na serwer to bezradne obserwować z sercem zdjętym trwogą sforkowane procesy apache.

Po chwili strona była całkowicie zablokowana skuteczniej niż w następstwie ataku anonimowych, ale mimo tej niedogodności zespół programistów kontynuował prace po staremu.

Zini w ramach oczyszczania kodu niespodziewanie odkrył, że w źródłach OpenMW znajduje się wiele niepotrzebnych include co niestety przekłada się na czas kompilacji aplikacji. Wielu spośród użytkowników marudzi, że zbudowanie OpenMW zajmuje wieczność i choć są to w większości posiadacze tych mniej potężnych procesorów (oraz być może zbyt skromnych pokładów cierpliwości) to nie ma sensu ukrywać, że przyśpieszenie kompilacji nie powinno nikomu zaszkodzić. 

Usunięcie zbędnych include ograniczyło czas potrzebny do skompilowania OpenMW o blisko połowę.

Następnie Zini przystąpił do pracy nad właściwą implementacją rozwoju umiejętność. Co prawda jak na razie zestaw umiejętności które można zobaczyć w grze zawiera jedynie Alchemię (a i ta zdolność nie jest ukończona) ale wkrótce ulegnie to zmianie. Nastała pora przygotowań dla nadchodzących funkcji.

Scrawl majstruje przy shaderach, i co ciekawe odkrył iż użycie GLSL do pisania shaderów skraca czas kompilacji dwadzieścia jeden razy. Ponieważ asortyment shaderów OpenMW można określić co najwyżej jako &#8220;skromny&#8221; przełożenie na całkowity czas kompilacji jest znikome, ale sądzę, że mogło to was zaciekawić.

jhooks1 wciąż szarpie się z animacjami, a równolegle Chris poznaje szczegóły nifload by móc dołączyć rozwoju.

Rozszyfrowanie formuł z morrowind nareszcie nabrało rozpędu i posuwa się do przodu tak, jak powinno było od samego początku. Myckel dzielnie pracuje nad alchemią, do roboty zabrali się także nowi koledzy z forum i zapewniam was, że ten wysiłek nie pójdzie na marne.