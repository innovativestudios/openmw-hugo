{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-12-31T08:06:25+00:00",
   "title": "Bez kolęd poproszę",
   "type": "post",
   "url": "/2013/bez-koled-poprosze/"
}
Adwent, nie adwent, a OpenMW programować trzeba i należy.

Zacznijmy może od pewnej nowości w dziedzinie obsługi plików. Część osób z pewnością orientuje się w ograniczeniu ilości modów, jakie może załadować Morrowind. Limit ten wynosi dokładnie 255 plików. Prawdę mówiąc całe lata operowałem na założeniu, że jest to wartość na tyle wysoka, że w praktyce nie do osiągnięcia ─ w internecie krążą jednak legendy o ludziach którzy w swej zapalczywości zainstalowali więcej modów niż Bethesda zezwoliła, a że w każdej plotce jest, jak to mawiają, ziarno prawdy; najwyraźniej 255 w tym kontekście nie jest wcale liczbą tak abstrakcyjną jak uważałem do tej pory. W takim więc razie ograniczenie te powinno zostać zniesione, i tak właśnie się stało. Od dzisiaj możecie instalować tyle modów, ile dusza zapragnie ─ pod warunkiem jednakże, iż żaden z nich nie będzie miał więcej niż 255 zależności. Jeśli jakimś cudem okaże się, że to zbyt mało ─ również to ograniczenie może zostać zniesione.

Poza tym: bugi, bugi, bugi. Cała chmara uciążliwych błędów została unicestwiona zbiorowym wysiłkiem naszego zespołu, i jest to bez wątpienia coś, z czego możemy być dumni. Z drugiej jednak strony, jest jeszcze wiele nawet gorszych pomyłek, które tylko czekają na to by je rozwiązać, trudno więc ogłosić całkowity sukces i na tym poprzestać.

Postęp nad fizyką w grze pozostawia sporo do życzenia. Nie jest to wieść zaskakująca, o ile tylko uwzględni się jak trudną biblioteką jest bullet, oraz jak złożona jest matematyka zaangażowana do obliczeń fizycznych.

Ale po co te marsowe miny! Nie ma potrzeby się smucić. Na deser, specjalnie dla was, garść przeuroczych <a href="https://plus.google.com/u/0/b/105268208967912754238/photos/105268208967912754238/albums/5960234896492093137" title="click me" target="_blank">zrzutów</a> prezentujących teren z dodanymi nowoczesnymi efektami (specular, parallax, normal mapping) i całkiem zwyczajnymi teksturami.