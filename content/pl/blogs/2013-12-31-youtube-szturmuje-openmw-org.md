{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-12-31T08:04:34+00:00",
   "title": "Youtube szturmuje OpenMW.org!",
   "type": "post",
   "url": "/2013/youtube-szturmuje-openmw-org/"
}
Cześć!

Zacznijmy od krótkiego filmu… a może nawet lepiej od dwóch!





Część osób zapewne właśnie się zastanawia cóż takiego właśnie zobaczyło. Otóż, jeśli się zdarza wam się pomarzyć o tym, jak wyglądałyby dziesięcioletnie tekstury i modele, ale wzbogacone o możliwość wykorzystania współczesnych technologii takich jak &#8220;normal mapping&#8221;; &#8220;specular mapping&#8221; i &#8220;paralax mapping&#8221;, to właśnie zobaczyliście odpowiedź. Dodatkowo macie też dowód na to, że nie oszukuję was bezczelnie i rzucanie czarów naprawdę działa.

Filmy nie pokazują wszystkich nowości, rzecz jasna. O ile pamiętacie wygląd efektów cząsteczkowych w komorach teleportacyjnych twierdz, ucieszy was fakt iż wyglądają teraz poprawnie. Dodatkowo OpenMW obsługuje już &#8220;Nifflip&#8221;, dzięki czemu poszerzyliśmy obsługę modów o kilka nowych &#8212; wcześniej zupełnie nie działających. Wszystko dzięki scrawlowi.

Tymczasem Zini pracuje nad obsługą zapisu i wczytywania gry. Po przetarciu szlaku i reorganizacji podstaw wymaganych dla wprowadzenia tej nowości, prace idą już co najmniej zadowalającym tempem &#8212; a to może tylko cieszyć.

Pvdk &#8212; osoba odpowiedzialna za nasz program startowy, w końcu na poważnie zabrał się za udoskonalony interfejs kreatora instalacji Morrowind.