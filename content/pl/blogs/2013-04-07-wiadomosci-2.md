{
   "author": "sir_herrbatka",
   "categories": [
      "Cotygodnik"
   ],
   "date": "2013-04-07T17:37:03+00:00",
   "title": "Wiadomości",
   "type": "post",
   "url": "/2013/wiadomosci-2/"
}
…i tak o to stała się rzecz niezwykła i zgoła niesłychana, choć w oczach świeżych i nie znużonych horyzontem stałego obserwatora OpenMW być może jawiąca się jako wręcz nieunikniona. O to bowiem Chris najwyraźniej wydoił ostatnią porcję frajdy jaka jeszcze pozostawała w Skyrim i zajął się na powrót programowaniem OpenMW.

A efekty tego są zaiste znakomite. Nowa instrukcja isWerewolf została zaimplementowana co jest oczywiście rzeczą dobrą, mimo, że niezbyt wybitnie prezentującą się na zrzutach ekranów. Na szczęście oprócz tego pojawiły się efekty cząsteczkowe co oznacza, że w końcu można zobaczyć płomienie świec i pochodni. Co prawda obecny status trudno określić jako ukończone ale przynajmniej jest co pokazać na [filmie.][1]

Co prawda efekty cząsteczkowe wciąż pozostają nie do końca poznane, jednak Chris posuwa się do przodu pewnie i przypuszczać należy, że raczej prędzej niż później rozwikła całą kwestię.

Oprócz tego umiejętność zaklinania została ukończona i bez wątpliwości ujrzymy ją w wydaniu 0.23.0. 

Prace nad OpenCS postępują sprawnie. Zini dodał tabelę frakcji, zawierającą informacje je charakteryzujące. Nomadic1 rysuje śliczne ikony, a graffy ślicznie zaimplementował ustawienia użytkownika.

Gus tymczasem nabył nowy komputer dzięki czemu może zmagać się z nowymi trudnościami w dziedzinie AI. Tym razem okazało się że wprowadzone ostatnio zmiany dość efektywnie uniemożliwiają działanie napisanego kodu. 

Scrawl dodał obsługę dla bump maps i glow maps, ponadto ustanowił nową hierarchię znaczenia plików zgodnie z którą pliki zarchiwizowane w BSA mają niższy priorytet niż niespakowane. Oznacza to, że mody podmieniające tekstury powinny zacząć działać.

Oprócz tego rozwiązaliśmy dość sporą gromadkę problemów która uzbierała się na naszym bugtrackerze, pojawiło się nowe zadanie do wykonania; tym razem dotyczy uprzątnięcia mwgui.

 [1]: http://www.youtube.com/watch?v=RLTLT1l6wK4&list=PL14E8F94A73754549&index=4 "click me"