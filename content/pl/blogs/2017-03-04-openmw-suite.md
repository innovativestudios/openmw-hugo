{
   "author": "Vedyimyn",
   "categories": [
      "Uncategorized"
   ],
   "date": "2017-03-04T18:47:13+00:00",
   "title": "OpenMW Example Suite",
   "type": "post",
   "url": "/2017/openmw-suite/"
}
OpenMW jest znany jako darmowa, otwarta reimplementacja silnika gry _TESIII: Morrowind_. Oprócz stworzenia silnika umożliwiającego grę w Morrowinda, celem projektu jest utworzenie naszej własnej, usprawnionej implementacji construction seta, nazwanego OpenMW-CS, a także uczynienie z OpenMW bazy, na podstawie której będzie można tworzyć nowe gry.

Zestaw przykładowy (_The Example Suite_) to gra zupełnie niezależna od plików Morrowinda, umożliwiająca demonstrację możliwości silnika OpenMW. Gra ta będzie dostępna za darmo, a gracze będą ją mogli łatwo uruchomić z silnikiem OpenMW.

Ostatnio jeden z członków zespołu, o pseudonimie DestinedToDie, zgłosił się na lidera modelowania 3d dla tego projektu. Poskładał wstępny plan pierwszego obszaru, na razie z zastosowaniem placeholderów.

<blockquote class="imgur-embed-pub" lang="en" data-id="Blynpmv">
  <p>
    <a href="http://imgur.com/Blynpmv">View post on imgur.com</a>
  </p>
</blockquote>



DestinedToDie opisał swoją wizję _The Example Suite_ [tutaj][1].

W celu zrealizowania projektu, założył on [stronę][2] w serwisie Patreon. Wsparcie za pomocą tej strony (lub poprzez [PayPal][3]) umożliwi mu poświęcenie czasu na ten projekt i doprowadzenie go do postaci grywalnej.

Projekt jest dostępny dla wszystkich na licencji CC-BY-3.0, co oznacza, że każdy może przyczynić się do niego lub utworzyć na jego bazie własną grę, nawet komercyjną.

Podsumowując, można się spodziewać OpenMW grywalnego „prosto z pudełka”, z dostępnym za darmo zestawem testowym.

##### Zapraszamy do komentowania [tutaj][4].

 [1]: http://forum.openmw.org/viewtopic.php?f=28&p=46098#p46098
 [2]: https://www.patreon.com/destinedtodie
 [3]: http://www.paypal.me/DestinedToDie
 [4]: https://forum.openmw.org/viewtopic.php?f=38&t=4181