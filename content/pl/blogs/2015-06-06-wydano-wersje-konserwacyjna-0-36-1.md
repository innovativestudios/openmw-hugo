{
   "author": "Vedyimyn",
   "categories": [
      "Nowa wersja"
   ],
   "date": "2015-06-06T11:37:40+00:00",
   "title": "Wydano wersję konserwacyjną 0.36.1",
   "type": "post",
   "url": "/2015/wydano-wersje-konserwacyjna-0-36-1/"
}
Zespół OpenMW ogłasza wydanie wersji konserwacyjnej 0.36.1. Dodatkowe skrypty startowe nie uruchamiały się &#8211; zostało to naprawione. Nową wersję można pobrać [tutaj][1], dla wszystkich systemów operacyjnych.

**Znane problemy:**

  * Przełączenie się z pełnego ekranu na tryb okienkowy, przy użyciu silnika renderującego D3D9, powoduje wysypanie się programu.
  * Wysypywanie się OpenCS przy próbie włączenia podglądu cella na systemie OS X.

## Lista zmian:

**Naprawiono:**

  * Dodatkowe skrypty startowe nie uruchamiały się.

##### **Zapraszamy do komentowania [tutaj][2]!**

 [1]: https://openmw.org/downloads/
 [2]: https://forum.openmw.org/viewtopic.php?f=38&t=2927