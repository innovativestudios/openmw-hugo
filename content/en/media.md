{
   "date": "2022-01-13T07:09:41-06:00",
   "draft": false,
   "title": "Media",
   "toc": false
}

## Release Commentaries
Our official YouTube channel, named [MrOpenMW](https://www.youtube.com/user/MrOpenMW), is where the release commentaries are uploaded. We try to make two videos (one for OpenMW and one for OpenMW-CS) for every release, covering the most important changes to the engine. The commentaries are currently made by the brilliant Atahualpa, and have been for the last few years. Most videos up to version 0.35 were made by the legendary WeirdSexy. Filling in for Atahualpa for the 0.46.0 release, the terrific johnnyhostile has now joined the team to collaborate with Atahualpa in future OpenMW videos.

Below you can find the release commentary for the latest release, 0.46.0:
{{<youtube kMGjf6ZONE8>}}

Want some OpenMW history? Head over to the YouTube channel and check out the earliest videos! :)

## Video Specials
Still not enough OpenMW content for you? How about some relaxing [Gource](https://gource.io/) animation showing the progression of OpenMW's codebase over the years - starting in July 2008 and ending in early 2021, right before the project entered the 0.47.0 release phase.
{{<youtube UeJc3e_qQWY>}}

## Videos Showing Specific Features
### Normal, specular, and parallax maps
{{<youtube nk1YmN70jfc>}}

### Shaders
{{<youtube NjfCxa2tpjY>}}

### Graphics Improvements
{{<youtube Yj6jK6TuCCI>}}

More videos to come! Feel free to contribute if you are able to make videos showing keyboard UI navigation, cell preloading or anything else you might think of.