{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2012-01-01T18:09:43+00:00",
   "tags": [
      "bullet",
      "Gus",
      "jhooks",
      "journal",
      "K1ll",
      "physics",
      "Zini"
   ],
   "type": "post",
   "url": "/2012/160/"
}
Welcome in the year 2012. Zini already wrote the summary of openmw status after 2011. 

Well, 0.12.0 is quite close now. Jhooks1 is probably amazing himself (not me, I&#8217;m already confident about him) with his developing skills. To quote zini:

> I am away for just a couple of days and suddenly tons of stuff happens.

Improvement is really significant and a large step in the right direction. It seems that some other problems left are related to drivers and libbullet so **suffering level** (this sounds so cool) depends on the version of driver/lib. 

And there is almost nothing we can do with it besides reporting bugs. K1ll reports that actually it WORKS since the bullet bug seems to be a fixed in the last svn revision. And that nasty bug could cause quite massive fps drops. 

GUS works on journal. It works but still needs some love, quoting GUS:

> Text is not properly aligned to the book and the book is not properly aligned/scaled on the screen.