{
   "author": "Okulo",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-09-24T18:55:10+00:00",
   "title": "A Matter of Time",
   "type": "post",
   "url": "/2014/matter-time/"
}
With the release of 0.32.0 out of the way, the dapper development team marched on to get working on 0.33.0. Already have they solved almost 50 bugs and implemented some cool features, few as may be left.

Zini has continued working on OpenCS. One exciting feature is something that will make the life of many modders a whole lot easier. When you&#8217;re creating a mod, you need to test it out. But why would you start Morrowind manually and either navigate to the spot where your mod content happens to be or load a saved game in that spot over and over again? Wouldn&#8217;t it be easier if you can let OpenCS do the work for you? With this feature, it takes a mere press of a button for OpenCS to not only start OpenMW, but also place the player in the exact spot that OpenCS&#8217;s camera was in.

OpenMW itself has had some new features implemented as well, such as music when the player levels up (YAY!) or when the player dies (Awww). Bugs ranging from weird ([paying a 0 gold fine after doing hard time][1]) to downright nasty ([A couple of NPCs drowning themselves after quest completion][2]) have been fixed.

We&#8217;re barely a week after the release of 0.32.0 and already 0.33.0 is shaping up to be another major release. When we&#8217;re looking at the past few releases, we are seeing a positive trend: more and more issues are being closed.  
[0.29.0][3] had 67 issues closed. [0.30.0][4] consisted of 84 closed issues. [0.31.0][5] stands tall at a recordbreaking 183 issues squished and [0.32.0][6] was definitely something to write home about: a whopping 144 issues.

Not all issues are created equal though. Features are few but tend to get more difficult now that the i&#8217;s are getting dotted and the t&#8217;s are getting crossed. The bugs, on the other hand, are usually small but numerous and the time between releases have been getting gradually longer. We used to have monthly releases, but the last release took more than two months. So let&#8217;s have some real talk here. Though the large changelogs and huge numbers may be cool and all, none of us are here just to impress people on the Internet. And you, dear reader, know that OpenMW is a project with qualities that run deeper than the superficiality of numbers or shaders, otherwise you would not have been following this massive undertaking. You and I, we are people who care a lot about practicality.

And so is the team. They knew that in order to get the most out of everyone&#8217;s time, they needed to take a step back and reevaluate the release schedule. Having a long release cycle doesn&#8217;t just look cool on the changelist, but also gives the team the opportunity to well and truly iron out the nasty bugs and implement the more difficult features without feeling rushed by an approaching deadline. On the other hand, a short release cycle means that all our players will get updates sooner. It really helps testing when everyone has an up-to-date version of the project. There&#8217;s a balance that needs to be struck, especially now that we&#8217;re coming up to 1.0.

Speaking of testing, it is noteworthy that there have been plenty of occasions where it wasn&#8217;t someone on the development team spotting these issues. These people may not know a word of C++ but still do incredibly important work by testing OpenMW. Whenever they run into problems with quests or gameplay issues in general, they report their findings to the team. These are the people that play an integral role in making sure that those of us waiting for 1.0 will have the best experience possible.

So many thanks and three blessings to our testers/bugreporters so far. Your keen eyes and invaluable contributions deserve our acknowledgement and gratitude.

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=2473" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://bugs.openmw.org/issues/1892
 [2]: https://bugs.openmw.org/issues/1911
 [3]: https://bugs.openmw.org/versions/20
 [4]: https://bugs.openmw.org/versions/22
 [5]: https://bugs.openmw.org/versions/23
 [6]: https://bugs.openmw.org/versions/24