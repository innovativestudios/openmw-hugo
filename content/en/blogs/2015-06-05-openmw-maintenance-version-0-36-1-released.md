{
   "author": "raevol",
   "categories": [
      "Maintenance",
      "Release"
   ],
   "date": "2015-06-05T20:52:53+00:00",
   "title": "OpenMW Maintenance Version 0.36.1 Released!",
   "type": "post",
   "url": "/2015/openmw-maintenance-version-0-36-1-released/"
}
The OpenMW team announces the release of maintenance version 0.36.1. This version addresses a regression that caused additional startup scripts to fail to launch. Grab it from our [Downloads Page][1] for all operating systems.

**Known Issues:**

  * Crash when trying to view cell in render view in OpenMW-CS on OSX
  * Crash when switching from full screen to windowed mode on D3D9

**Changelog:**

  * Fixed additional startup scripts failing to launch

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=2927" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://openmw.org/downloads/