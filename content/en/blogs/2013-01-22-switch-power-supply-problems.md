{
   "author": "Lukasz Gromanowski",
   "categories": [
      "Maintenance"
   ],
   "date": "2013-01-22T02:45:01+00:00",
   "tags": [
      "maintenance"
   ],
   "title": "Switch and power supply problems",
   "type": "post",
   "url": "/2013/switch-power-supply-problems/"
}
Hello,  
OpenMW website was off-line for about three and half hour due to switch and rack power supply failure in OVH/MyDevil.net server room. Problem is solved and (I hope) everything is OK now. Sorry for inconvenience.