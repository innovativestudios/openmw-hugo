{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized",
      "Week's review"
   ],
   "date": "2013-10-21T11:23:50+00:00",
   "title": "Week in review",
   "type": "post",
   "url": "/2013/week-review-72/"
}
Greetings!

As I promised we can go back to the old tradition of weekly reviews. Sadly this week was not exactly outstanding.

Let&#8217;s start with something tasty for fedora users: OpenMW is now in the fusion repository, making it radically easier to install and test. Kudos for everyone who somehow helped to achieve this.

Other than that: rainChu (a new developer, who joined us recently) just managed to implement exteinguishing portable light sources (that is mostly torches). Anyway: torches will become useless after a period of time, or after going underwater ─ just like in vanilla.

Zini just started to implement everything related to the dialogues handling inside OpenCS. Interesting fact: as you may know, vanilla Morrowind allowed to play sound file while in dialogue. However, not all dialogue records types allowed that. OpenMW can extend this feature to cover topic, greeting and persuasion. This would be very nice, don&#8217;t you think?

And that would be it, sadly.