{
   "author": "raevol",
   "categories": [
      "Release"
   ],
   "date": "2012-11-10T17:52:15+00:00",
   "tags": [
      0.19,
      "death",
      "fast travel",
      "launcher",
      "record saving",
      "release",
      "skill gain",
      "sleep window",
      "traveling",
      "WeirdSexy"
   ],
   "title": "OpenMW 0.19.0 Released!",
   "type": "post",
   "url": "/2012/openmw-0-19-0-released/"
}
The OpenMW team is proud to announce the release of version 0.19.0! Release packages for Ubuntu are now available via our [Launchpad PPA][1]. Release packages for other platforms are available on our [Download page][2]. This version introduces Sleeping, Potions, DEATH!!, Spell Creation, Travel, and many other features.

Check out our release video by WeirdSexy:  


**Known Issues:**

  * Launcher crashes on OS X < 10.8
  * Extreme shaking can occur during exterior cell transitions for some users

**Changelog:**

  * Implemented sleep/wait
  * Further implementation for alchemy/potions
  * Implemented death
  * Implemented spell creation and spell creation window
  * Implemented travel and travel dialogue
  * Implemented first layer of global map
  * Implemented trainer window
  * Implemented skill increase from skill books
  * Implemented ESM/ESP record saving
  * Fix for Character shaking in 3rd person mode near the origin
  * Implemented gamma correct rendering (does not work without shaders)
  * Fix for fortify attribute effects on the last 3 attributes
  * Fix for NCC flag handling, fixes some collision issues
  * Sort birthsign menu alphabetically
  * Various fixes/cleanup for the launcher
  * Fix for sound listener position updating incorrectly
  * Implemented dynamically generating splash image list
  * Fix for markers interfering with raycasting
  * Fix for crash after picking up items from an NPC

 [1]: https://launchpad.net/~openmw/+archive/openmw
 [2]: https://code.google.com/p/openmw/downloads/list