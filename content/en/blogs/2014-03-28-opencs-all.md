{
   "author": "Lukasz Gromanowski",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-03-28T05:18:09+00:00",
   "title": "OpenCS above all!",
   "type": "post",
   "url": "/2014/opencs-all/"
}
I&#8217;ll start a bit differently this time &#8211; with OpenCS instead of the usual OpenMW.

Thanks to SirHerrbatka and Zini&#8217;s efforts, OpenCS now features the functionality mentioned in last week&#8217;s post. For your viewing pleasure, the preview and editing views in all their glory:

[<img loading="lazy" src="https://i0.wp.com/openmw.org/wp-content/uploads/2014/03/zrzut103.png?resize=300%2C240&#038;ssl=1" alt="zrzut103" width="300" height="240" class="alignnone size-medium wp-image-3524" srcset="https://i0.wp.com/openmw.org/wp-content/uploads/2014/03/zrzut103.png?resize=300%2C240&ssl=1 300w, https://i0.wp.com/openmw.org/wp-content/uploads/2014/03/zrzut103.png?resize=1024%2C819&ssl=1 1024w, https://i0.wp.com/openmw.org/wp-content/uploads/2014/03/zrzut103.png?w=1280&ssl=1 1280w" sizes="(max-width: 300px) 100vw, 300px" data-recalc-dims="1" />][1]

Beautiful! And all of this is in the main branch!

Aside from that, we have fixes, fixes, fixes, and Scrawl. Like every other week, Scrawl dealt with some nasty bugs (this time having to do with NPCs not falling down). Slothlife, meanwhile, has included support for saving the current weather state in savegame files. I think that loading and saving the state of a character&#8217;s spells list should also appear soon.

And that&#8217;s all folks, until next time!

 [1]: https://i0.wp.com/openmw.org/wp-content/uploads/2014/03/zrzut103.png?ssl=1