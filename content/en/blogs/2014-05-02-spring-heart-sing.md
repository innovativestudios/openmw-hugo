{
   "author": "Nekochan",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-05-02T19:25:14+00:00",
   "openid_comments": [
      "a:1:{i:0;i:584;}"
   ],
   "title": "It’s spring and all hearts sing…",
   "type": "post",
   "url": "/2014/spring-heart-sing/"
}
Except for Sir Herrbatka&#8217;s, who started off the spring with a flu. We also sympathise with those suffering from allergies, who are now walking around with red eyes and runny noses. But despite that we still love this season, and even with all this ruckus surrounding it, we promise that we&#8217;re not going to stop development. All our top coders are still working for hours on end. We&#8217;re a veritable beehive of activity, which shows in the work that has been done.

Let&#8217;s start with bugfixes: there are a lot of them. I would&#8217;ve liked to leave it at that, but then you might feel cheated, so I have to mention at least some of our accomplishments:

&#8211; You can now loot corpses without being considered a thief.  
&#8211; The time it takes to drown is no longer hardcoded.  
&#8211; You can no longer repair items, recharge enchanted items or create potions in the middle of a fight.  
&#8211; Any many more fixes&#8230;

This update isn&#8217;t very feature rich, but you have probably gotten used to that by now. After all, OpenMW still has a lot of bugs that need to be fixed. But that doesn&#8217;t mean it&#8217;s completely devoid of features &#8211; there&#8217;s still some news for you:

&#8211; Depending on how dark it is, NPCs might use torches.  
&#8211; When NPCs are done chasing something or someone, they will now return to their default position.  
&#8211; NPCs can now fight along the vertical axis, so you can no longer chug a levitation potion and pummel those poor, defenceless NPCs into oblivion.

Another good news:

<h2 style="text-align: center">
  <a href="http://www.linuxgameawards.org/news/project-of-the-month-may-2014-winner">Project of the Month May 2014 &#8211; Winner</a>
</h2>

<p style="text-align: center">
  <a href="https://i0.wp.com/openmw.org/wp-content/uploads/2014/05/potm1405-winner-banner-600370x224.jpg?ssl=1"><img loading="lazy" class="alignnone size-medium wp-image-3592" src="https://i0.wp.com/openmw.org/wp-content/uploads/2014/05/potm1405-winner-banner-600370x224-300x112.jpg?resize=300%2C112&#038;ssl=1" alt="potm1405-winner-banner-600[370]x224" width="300" height="112" data-recalc-dims="1" /></a>
</p>

<p style="text-align: center">
  The third installment of the Project of the Month is over and the game project that could collected the most votes and thus the Project of the Month May 2014 is:
</p>

<h1 style="text-align: center">
  OpenMW
</h1>

<p style="text-align: center">
  Get the results from <a href="http://www.linuxgameawards.org">http://www.linuxgameawards.org</a>
</p>

<p style="text-align: center">
  The <a href="http://www.linuxgameawards.org/supporters">http://www.linuxgameawards.org/supporters</a>  Linux Game Award supporters will promote OpenMW for one Month starting 1st May.
</p>

<p style="text-align: center">
  <a href="https://i0.wp.com/openmw.org/wp-content/uploads/2014/05/potm1405_banner.jpg?ssl=1"><img loading="lazy" class="size-medium wp-image-3596 aligncenter" src="https://i0.wp.com/openmw.org/wp-content/uploads/2014/05/potm1405_banner-300x112.jpg?resize=300%2C112&#038;ssl=1" alt="potm1405_banner" width="300" height="112" data-recalc-dims="1" /></a>
</p>

<p style="text-align: center">
  :)
</p>

&nbsp;

<p style="text-align: center">