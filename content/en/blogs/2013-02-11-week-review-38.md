{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-02-11T14:51:42+00:00",
   "title": "week in review",
   "type": "post",
   "url": "/2013/week-review-38/"
}
And so we meet again readers,

Chris still works on animations and although there is still much to be done we have swimming animations working. The rest needs movement solver to be expanded and working, good that the code of movement solver is small and simple ─ if any of you have experience with 3D video games and want to help this area is open, Chris will be happy to see some sort of help for sure.

But we have great news for both Chris and all OpenMW users. Let&#8217;s give voice to the mystical leader of OpenMW, Zini:

> I merged the multiple ESX branch into next today (already been to Solstheim). The whole thing is still a bit rough and I fully expect more bugs to show up over the coming weeks. But unless something is going completely wrong, we should have mostly working support for multiple ESX files in 0.22.0.

Scrawl reports that we have performance regression related as it seems to the multiple data files support.

Also, less important news:

PotatoesMaster works now on the importing archives listed in the openmw.cfg

dougmencken, the owner of the near extincted PowerPC personal computers was able to run OpenMW. Good news for this two, lone in the dark, surrendered by the sea of x86 processors and unable to upgrade owners of big endian machines. This won&#8217;t get into the master probably any time soon if ever but still, we can see an interesting collection of funny bugs.

Example 1: Pink font and blue window frames that actually looks <a title="click me" href="https://dl.dropbox.com/u/2899105/openmwrightmousebuttonm.png" target="_blank">cute</a>.

Example 2: Apple patented round corners so for legal reason sun is <a title="click me" href="https://dl.dropbox.com/u/2899105/quadratishsonne02.png" target="_blank">squared</a>.

Example 3: I told him to not eat this <a title="click me" href="https://dl.dropbox.com/u/2899105/screenshot001zu.png" target="_blank">mushroom</a>&#8230;

Good news is that Zini works now on the editor, we have even a basic GMST support and the work on open file dialog is now finished.