{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-12-31T08:06:25+00:00",
   "title": "Week in review",
   "type": "post",
   "url": "/2013/3325/"
}
Such a active week!

First of, any interesting news. As you may know, number of content files in vanilla Morrowind engine is limited to the 255. 255 seems to be a large number in this context, at least for me, yet still ─ some users reported that they where able to cross this limit. This is not a problem anymore with OpenMW ─ we don&#8217;t have content file limit anymore, though we have a dependency count limit. No content file can depend on more than 255 files. I really don&#8217;t think that this is not enough but if somehow in fact it is not enough… zini can remove this limit as well.

Other than that: although bugs are still legion, our team with smart mind and brave spirit dived into dark abandoned source files to slay many of them. Really, it&#8217;s nice to see that we are somehow able large number accumulated bugs. Especially when there are only three non-implemented skills.

Scrawl also focuses on bugfixes. He probably could implement marksman (one of not implemented skills) at this point, however we miss formulas! We do not know how to calculate chance to hit, or how to calculate damage from projectiles. If someone could help us here, it would be very helpful.

Sadly, progress on physics is slow, but it comes with no surprise: bullet is difficult to use, physics in video game is a difficult topic with quite a lot of crazy linear algebra involved and in general, everything here is just annoying.

And to spoil your eyes, another portion of specular, paralax, and normal mapped <a href="https://plus.google.com/u/0/b/105268208967912754238/photos/105268208967912754238/albums/5960234896492093137" title="click me" target="_blank">terrain!</a>