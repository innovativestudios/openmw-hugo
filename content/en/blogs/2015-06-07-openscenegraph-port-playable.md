{
   "author": "scrawl",
   "categories": [
      "Uncategorized"
   ],
   "date": "2015-06-07T17:22:25+00:00",
   "title": "OpenSceneGraph port now playable",
   "type": "post",
   "url": "/2015/openscenegraph-port-playable/"
}
During the past three months, the OpenMW team has been hard at work porting their codebase away from the Ogre3D engine and towards the OpenSceneGraph rendering toolkit. The [previous post][1] talks about our motivations.

We are pleased to announce the porting efforts are finally bearing fruit &#8211; as in, all gameplay-essential features have been ported across, so users can enjoy a legitimate game of Morrowind in the [OpenMW-osg][2] development branch. 

Some advanced features &#8211; shaders, distant terrain, shadows, and water reflections &#8211; have not been ported yet. However, even this early, it is safe to say the transition has been a massive success, in more ways than we initially imagined. Players will find the new OpenMW loads faster, improves framerate, looks more like the original game, and fixes a host of long-standing bugs that proved difficult to address within our old framework.

Hold on&#8230; _improved framerate_? Let&#8217;s put that to the test&#8230;

## The first benchmark

Our test environment shall be:  
GeForce GTX 560 Ti/PCIe/SSE2, AMD Phenom(tm) II X4 955 Processor × 4, Linux 3.13.0-24-generic x86_64  
1680&#215;1050, full screen, no AA, 16x AF, no water reflections, no shadows, no shaders  
Maximum view distance as per the in-game slider

[<img loading="lazy" src="https://i0.wp.com/openmw.org/wp-content/uploads/2015/06/osg_bench-300x188.png?resize=300%2C188&#038;ssl=1" alt="osg_bench" width="300" height="188" class="alignnone size-medium wp-image-4399" data-recalc-dims="1" />][3]  
_The testing scene: good old (laggy) Balmora  
_ 

<table cellspacing="8">
  <tr>
    <td>
    </td>
    
    <td>
      OpenMW
    </td>
    
    <td>
      OpenMW-osg
    </td>
  </tr>
  
  <tr>
    <td>
      Framerate avg.
    </td>
    
    <td>
      49
    </td>
    
    <td>
      <strong>75</strong>
    </td>
  </tr>
  
  <tr>
    <td>
      Loading time avg.
    </td>
    
    <td>
      7s
    </td>
    
    <td>
      <strong>3.4s</strong>
    </td>
  </tr>
  
  <tr>
    <td>
      System memory avg.
    </td>
    
    <td>
      344.6mb
    </td>
    
    <td>
      <strong>277.1mb</strong>
    </td>
  </tr>
</table>

Unsurprisingly, the OSG port wins on all three counts. The framerate improvement is decent, though still far short of the 3-4x improvement we saw in [earlier tests][4] with a single model. There is no reason to be concerned, however &#8211; you should take these numbers with a grain of salt:

  1. **The comparison is unfair**. New rendering features are included in the OSG branch, that bring us closer to vanilla Morrowind compatibility, but affect performance as well. For example, we now dynamically expand bounding boxes based on running animations, which fixes the infamous bug of cliff racers disappearing under certain angles (Bug 455), but is taxing on frame rate as well. Likewise, we have ditched _static geometry batching_, a performance optimization that was causing a multitude of issues, mainly [incorrect lighting][5] and [scripted movement of objects not working][6]. Even under all this new workload, the OSG port is _still_ faster!
  2. **Expect higher performance gains for advanced graphical features**. The comparison was made with the minimum graphics settings, for the simple reason that the advanced settings (shadows, reflections, etc.) do not exist in the OSG branch yet. We expect that once those advanced features are ported, their impact on framerate will be much lower than before, simply due to the new renderer scaling a lot better with regards to GPU workload. Draw submissions are now offloaded to a worker thread, so the graphical complexity of the scene does not block the main thread from performing work in the meantime such as culling, physics, scripting, and animation.
  3. **The _real_ optimization phase hasn&#8217;t even begun yet**. The main focus for now has been to get the game back to a playable state, and that only happened a few days ago. Now, there are plenty of optimization opportunities on the horizon. Our new rendering framework gives us more control over how the scene graph is structured, and how updates are dispatched, something we are only just starting to take advantage of. Planned optimizations in the near future include: 
      * Move skinning updates to a worker thread.
      * Move particle updates to a worker thread.
      * Share state across different NIF files.
      * Enable culling for particle emitters/programs.
      * Integrate a model optimizer. Morrowind&#8217;s models unfortunately contain plenty of redundant nodes, redundant transforms, and redundant state, which impacts rendering performance. The original engine runs an &#8220;optimizer&#8221; pass over the models upon loading them into the engine. We should implement a similar optimizer pass. OpenSceneGraph provides the osgUtil::Optimizer that might prove useful for this very purpose.
      * Create a more balanced scene graph, e.g. a quad tree, to reduce the performance impact of culling and scene queries.
  4. **Rendering is not the only bottleneck**. Assuming an N times faster renderer would lead to an N times faster OpenMW is wrong. We have other systems contending for frame time, and now that our renderer is faster, these other bottlenecks are becoming ever more apparent. In particular, the physics and animation systems are currently the worst two offenders. Some preliminary optimizations for these systems have made it into the OSG port, but we have no doubts there is more room for improvement.

With that said, better performance is certainly not the only change users can look forward to:

## Preliminary changelog

**Rendering improvements**

  * Non-uniform NPC scaling (Bug 814):  
    Certain NPCs are now scaled along their X and Y axes, giving them a more bulky appearance, just like in vanilla Morrowind. Previous versions of OpenMW were unable to support this kind of scaling, due to an Ogre3D limitation. 
  * Improved rendering precision: solves large coordinate precision issues, exhibited by flickering/shaking when the player travels too far from the world origin. [Video comparison][7] 
  * Removed static geometry batching: fixes incorrect lighting (Bug 385), fixes scripted movement of objects (Bug 602), and improves cell loading time.
  * Vanilla-accurate transparency settings: Previous versions of OpenMW were using transparency settings not quite accurate to vanilla Morrowind, in an effort to facilitate static geometry batching. That has been fixed &#8211; users will notice that foliage and other transparent objects now have softer edges.
  * Added _small feature culling_ option: In addition to culling off-screen objects, we can now also cull objects that would be smaller than a single pixel when rendered. The visual change is virtually unnoticeable, so we have enabled the option by default.

<table cellspacing="16">
  <tr>
    <td>
      <a href="https://i0.wp.com/openmw.org/wp-content/uploads/2015/06/osg_scaling.png?ssl=1"><img loading="lazy" src="https://i0.wp.com/openmw.org/wp-content/uploads/2015/06/osg_scaling-300x156.png?resize=300%2C156&#038;ssl=1" alt="osg_scaling" width="300" height="156" class="alignnone size-medium wp-image-4383" data-recalc-dims="1" /></a>
    </td>
    
    <td>
      <a href="https://i0.wp.com/openmw.org/wp-content/uploads/2015/06/osg_transparency.png?ssl=1"><img loading="lazy" src="https://i0.wp.com/openmw.org/wp-content/uploads/2015/06/osg_transparency-300x156.png?resize=300%2C156&#038;ssl=1" alt="osg_transparency" width="300" height="156" class="alignnone size-medium wp-image-4386" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td align="center">
      <em>NPC scaling comparison</em>
    </td>
    
    <td align="center">
      <em>Transparency comparison</em>
    </td>
  </tr>
</table>

**Re-designed NIF loader**

  * Support for non-uniform scaling in NIF files (Bug 2052)
  * Fixed limitation on the number of nodes in NIF files (Bug 2187)
  * Fixed animations &#8220;freezing&#8221; when the object is culled (Bug 2151)
  * Redesigned skinning algorithm to allow for a more efficient scene graph
  * Dynamically expand bounding boxes based on skeletal animation; fixes certain creatures disappearing under certain view angles (Bug 455)
  * NIF scene graphs are now a shared resource; drastically improves loading time.
  * Animation text keys are now a shared resource

**Physics rewrite**

  * When compiled with Bullet 2.83 or later, take advantage of btScaledBvhTriangleMeshShape for efficient shape instancing
  * Remove the &#8220;detailed&#8221; Bullet raycasting shapes, replaced by direct raycasting on the scene graph (see &#8220;New raycasting&#8221; section), vastly reducing memory usage
  * Use a btCollisionWorld in place of a btDynamicsWorld, to avoid unnecessary updates for functionality that we are not using
  * Map collision objects by pointer instead of by name

**New raycasting**

  * Use the osgUtil::IntersectionVisitor for direct raycasting on the scene graph
  * Support for raycasting against animated meshes, fixing inaccurate cursor selection for actors (Bug 827)
  * Rewrote item selection logic for the inventory preview doll to use raycasting instead of a selection buffer, improving response time

**Improved loading screen**

  * Loading screen now renders in the draw thread, thus no longer blocks the loading procedure
  * Increased loading screen FPS so the loading bar moves more smoothly
  * Upload OpenGL objects in a background thread while the area loads, using osgUtil::IncrementalCompileOperation

**Improved SDL2 support**

SDL2, the library we are using for cross-platform input and window creation, has been more closely integrated with the rendering system. Practical benefits for the user include: 

  * The antialiasing option finally works on Linux systems (Bug 2014)
  * SDL2 is now responsible for creating the graphics context, meaning new display APIs, such as Wayland and Mir on Linux, are automatically supported with no extra maintenance overhead for the OpenMW team.

[<img loading="lazy" src="https://i0.wp.com/openmw.org/wp-content/uploads/2015/06/osg_antialiasing-300x188.png?resize=300%2C188&#038;ssl=1" alt="osg_antialiasing" width="300" height="188" class="alignnone size-medium wp-image-4379" data-recalc-dims="1" />][8]  
_8x antialiasing in action_ 

**Profiling overlay**

A nice side effect of using OpenSceneGraph is access to their top-notch profiling tools. With the &#8216;F3&#8217; key, an on-screen overlay appears that shows more information with every key press.

[<img loading="lazy" src="https://i0.wp.com/openmw.org/wp-content/uploads/2015/06/osg_profiling-300x130.png?resize=300%2C130&#038;ssl=1" alt="osg_profiling" width="300" height="130" class="alignnone size-medium wp-image-4376" data-recalc-dims="1" />][9]  
_Profiling overlay &#8211; the colored bars represent OpenSceneGraph&#8217;s internal threads, the white bars OpenMW logic_

**Passive fixes**

The new OpenMW sports a unified OpenGL renderer on all our platforms. Rendering via Direct3D is no longer supported, easing the maintenance and support overhead for the OpenMW team.

Practically speaking, we have thus &#8220;fixed&#8221; Bug 2186 (garbage pixels on the minimap on Windows) and Bug 1647 (crash when switching to windowed mode on Windows).

**Miscellaneous changes**

Finally, we do have some bonus changes that are not strictly related to the OpenSceneGraph transition, but were committed to the OSG branch anyway in an effort to reduce merge conflicts:

  * Tweaked the activation range for lights (Bug 1813)
  * Fixed NiGravity strength (Bug 2147)
  * Implemented controller extrapolation modes (Bug 1871)
  * Implemented the NiPlanarCollider particle processor (Bug 2149)
  * Added UI scaling option:

<table cellspacing="16">
  <tr>
    <td>
      <a href="https://i0.wp.com/openmw.org/wp-content/uploads/2015/06/osg_ui_scale_1.png?ssl=1"><img loading="lazy" src="https://i0.wp.com/openmw.org/wp-content/uploads/2015/06/osg_ui_scale_1-300x188.png?resize=300%2C188&#038;ssl=1" alt="osg_ui_scale_1" width="300" height="188" class="alignnone size-medium wp-image-4393" data-recalc-dims="1" /></a>
    </td>
    
    <td>
      <a href="https://i0.wp.com/openmw.org/wp-content/uploads/2015/06/osg_ui_scale_2.png?ssl=1"><img loading="lazy" src="https://i0.wp.com/openmw.org/wp-content/uploads/2015/06/osg_ui_scale_2-300x188.png?resize=300%2C188&#038;ssl=1" alt="osg_ui_scale_2" width="300" height="188" class="alignnone size-medium wp-image-4394" data-recalc-dims="1" /></a>
    </td>
  </tr>
  
  <tr>
    <td align="center">
      <em>Normal UI scale</em>
    </td>
    
    <td align="center">
      <em>2x UI scale, same resolution</em>
    </td>
  </tr>
</table>

**Code maintenance / restructuring / cleanup**

The codebase has considerably lost weight, which is likely interesting to developers, though not so interesting to end-users.

  * Physics code moved to a new &#8220;mwphysics&#8221; subsystem
  * Removed scene node names, i.e. the RefData::getHandle identifier
  * Removed OpenEngine
  * Removed platform wrapper
  * Removed shiny

In total, ~23.000 lines of code were removed:  
`<br />
git diff upstream/master --shortstat<br />
 689 files changed, 24051 insertions(+), 47695 deletions(-)<br />
` 

## What&#8217;s next?

Phew, that was a lot to take in &#8211; even at this stage, the list of improvements is massive, so our first priority should be to merge the port into the main branch, get our various nightly builds up and running again, then get a new release out.

Speaking for the longer term, we are not even close to unleashing the full potential of our new rendering engine. The next steps would be to further improve performance, then work on restoring shaders, distant terrain, water reflections and shadows. Our new NIF loader facilitates the implementation of background cell loading, which was originally planned as a post-1.0 improvement &#8211; now, it would be trivial to do, so we might see this feature pre-1.0 after all. 

In the meantime though, on the graphics front, there is now nothing stopping us from releasing the long awaited OpenMW 1.0, so perhaps efforts should be diverted to fix the remaining [OpenMW 1.0 blockers][10] first, get version 1.0 out the door, then port the advanced graphical features for version 1.1. We might decide this on a case-by-case basis. In particular, the water reflections should be a lot easier to port than shadows, which weren&#8217;t working particularly well in the Ogre branch anyway. 

If you have an opinion on the matter, feel free to comment. Though regardless of the priorities we decide on, these are certainly exciting times ahead!

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=2931" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://openmw.org/2015/announcing-switch-openscenegraph/
 [2]: https://github.com/scrawl/openmw/tree/osg
 [3]: https://i0.wp.com/openmw.org/wp-content/uploads/2015/06/osg_bench.png?ssl=1
 [4]: https://forum.openmw.org/viewtopic.php?f=2&t=2760&start=20
 [5]: https://bugs.openmw.org/issues/385
 [6]: https://bugs.openmw.org/issues/602
 [7]: https://www.youtube.com/watch?v=wybVYwQPVmY
 [8]: https://i0.wp.com/openmw.org/wp-content/uploads/2015/06/osg_antialiasing.png?ssl=1
 [9]: https://i0.wp.com/openmw.org/wp-content/uploads/2015/06/osg_profiling.png?ssl=1
 [10]: https://bugs.openmw.org/projects/openmw/roadmap#openmw-1.0