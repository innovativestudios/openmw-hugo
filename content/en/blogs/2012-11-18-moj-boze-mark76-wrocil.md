{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2012-11-18T18:34:25+00:00",
   "title": "O mój Boże! Mark76 wrócił!",
   "type": "post",
   "url": "/2012/moj-boze-mark76-wrocil/"
}
Welcome!

We had quite a nice week. A new developer Gohan (real name Marcin Hulist) joined us and started implementing AI dummy packages. This is his first attempt at AI. Hopefully we will see more AI oriented tasks after we implement the walking animations and maybe some elements of combat.

Zini finished implementing all the filter dialogs. Finally, everything in the dialogs should work,, unless there are other, currently unknown, features missing.

Thanks to greye we can now change races and the race menu window updates accordingly. The fact that it was possible to implement this feature also means that we can get a lot of other important tasks done.

Scrawl (trivia: did you know that Scrawl is the top remover of code in the OpenMW project?) continued work on the global map. This week he’s brought us the explored area overlay. We decided to a go slightly different path then vanilla Morrowind, but the effect should be cool enough.

Gus is working on weapon rendering. This is already working, well kind of… The weapon can be seen and that’s serious improvement, but there is still much to be done.

mark76 is slowly making progress the last couple of weeks on supporting multiple esm/esp files. We have support for multiple esm context in the cell store and basic loading references. This is nice progress, but better not to expect this to be resolved overnight!

lgro is back and plans on working this coming week on text formatting for books. The formatting is currently not working for some languages. For example, in the German version of Morrowind the tags are currently spliced between pages, which looks bad.

trombonecot, another new developer, bravely stepped into the fray. He is going to implement the failed action feature. Way to go!

We always need to recruit new developers to balance the losses. For example, jhooks1 has been absent for some time. He has been a great contributor to the project, putting so much effort into the animation and physics systems. He was also active for a very long time, even completely alone when the team was weak. We hope you are doing well J! Thanks for your amazing work and we hope to hear from you soon.

-See you next week