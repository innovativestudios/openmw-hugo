{
   "author": "lysol",
   "categories": [
      "Uncategorized"
   ],
   "date": "2019-03-25T19:41:26+00:00",
   "title": "Get Morrowind for free, today only!",
   "type": "post",
   "url": "/2019/get-morrowind-for-free-today-only/"
}
We&#8217;re a bit late to the party now, but there&#8217;s still a few hours left to go: Bethesda gives away Morrowind for free today (March 25th), and only today. You need a Bethesda.net account to get the code, otherwise it&#8217;s fully free!

Get it <a aria-label=" (opens in a new tab)" rel="noreferrer noopener" href="https://elderscrolls.bethesda.net/en/tes25" target="_blank">here</a> before it&#8217;s too late!

Remember that you need a copy of Morrowind to be able to play Morrowind through the OpenMW and/or TES3MP engine, so if you don&#8217;t own Morrowind yet, this is a perfect chance to get it for, well, nothing. :)