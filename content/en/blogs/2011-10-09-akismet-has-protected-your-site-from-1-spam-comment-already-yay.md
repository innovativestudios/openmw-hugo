{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2011-10-09T10:34:53+00:00",
   "showReadingTime": true,
   "summary": "Jhooks is excited again&#8230; He finally figured out how to fix the death animations!!!",
   "tags": [
      "animation",
      "Gus",
      "jhooks",
      "journal",
      "nico",
      "refactoring",
      "Zini"
   ],
   "title": "“Akismet has protected your site from 1 spam comment already.” Yay!",
   "url": "2011/akismet-has-protected-your-site-from-1-spam-comment-already-yay"
}
And the positioning works now properly (previously every one was placed above the ground). Videos can be found on our wiki, media section (that dark, abandoned place where until now only videos from nicolay openmw dwell).

Zini is doing his refactoring, spreading the word about proper coding meanwhile. If you are coder you should care.

And GUS wants to start journal task next week. That would be very good (to not say awesome) since we have a little problem with lack of gui features that looks good on screens and screencasts.

And that&#8217;s pretty much everything I&#8217;m afraid.