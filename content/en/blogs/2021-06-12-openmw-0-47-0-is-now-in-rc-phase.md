{
  "author": "lysol",
  "categories": [
    "Uncategorized"
  ],
  "date": "2021-06-12T14:57:00+00:00",
  "showReadingTime": true,
  "summary": "Finally, after much hard work to get all the cool new features ready for release, we have our first round of Release Candidates, or &#8220;RC&#8221; for short.",
  "title": "OpenMW 0.47.0 is now in RC-phase!",
  "url": "/2021/openmw-0-47-0-is-now-in-rc-phase/"
}
RCs are simply release binaries for testing, to make sure everything is in order for a release. That&#8217;s right, testing! So we would be _**very**_ grateful if you would download an RC for your OS of choice, test it a bit to see if it works without any issues and report any findings you make to our Gitlab [issue tracker][1]. Please make sure to check first that whatever you find is not on the tracker already.

Thank you and we&#8217;ll see you again on the day of the release!

Downloads are found [here][2].

[1]: https://gitlab.com/groups/OpenMW/-/issues
[2]: https://forum.openmw.org/viewtopic.php?p=71357#p71357