{
   "author": "raevol",
   "categories": [
      "Uncategorized"
   ],
   "date": "2017-03-04T04:53:27+00:00",
   "title": "Forging Ahead with the OpenMW Example Suite",
   "type": "post",
   "url": "/2017/forging-ahead-with-the-es/"
}
OpenMW is known as being the free, open source reimplementation of the engine for TESIII: Morrowind. In addition to being the go-to engine for playing Morrowind, the goal of OpenMW is to develop our own improved implementation of the construction set, the OpenMW-CS, as well as make OpenMW a platform to create new games on.

The Example Suite has long stood as OpenMW&#8217;s main effort to create a standalone game to demonstrate the capabilities of the OpenMW engine, without any dependencies on the Morrowind content files. This set of content will be a freely available game that players will be able to run immediately with the OpenMW engine.

Recently, our team member DestinedToDie has stepped up to be the dedicated 3D lead for this project. He has put together a basic layout of the first area with placeholder assets.

<blockquote class="imgur-embed-pub" lang="en" data-id="Blynpmv">
  <p>
    <a href="http://imgur.com/Blynpmv">View post on imgur.com</a>
  </p>
</blockquote>



DestinedToDie has written up a [design document][1] of his vision for the Example Suite.

In order to make this project into a reality, DestinedToDie has [opened a patreon][2] to fund his efforts. Supporting his patreon will enable him to dedicate time to this project, and move it towards a playable state. As an alternative option to Patreon, he also has a [Paypal account][3].

All efforts in the Example-Suite are for the benefit of everyone under the CC-BY-3.0 license, meaning that others are free to contribute to the project or spin-off their own game, even commercially.

Here&#8217;s looking forward to when OpenMW can be played &#8220;out of the box&#8221; with the freely available Example Suite!

##### [Want to leave a comment?][4]

 [1]: http://forum.openmw.org/viewtopic.php?f=28&p=46098#p46098
 [2]: https://www.patreon.com/destinedtodie
 [3]: http://www.paypal.me/DestinedToDie
 [4]: https://forum.openmw.org/viewtopic.php?f=38&t=4181