{
   "author": "Thunderforge",
   "categories": [
      "Maintenance"
   ],
   "date": "2018-06-19T02:04:52+00:00",
   "title": "OpenMW code, bug tracker, and wiki are now on GitLab!",
   "type": "post",
   "url": "/2018/openmw-code-bug-tracker-wiki-gitlab/"
}
One pain point for OpenMW has been that contributing means signing up for multiple accounts. Notably, our code repository, bug tracker, and wiki are on completely separate servers with separate login processes. Additionally, some developers prefer GitHub due to its popularity and some prefer GitLab due to it being open source, just like this project. So we&#8217;re reorganizing tools to make contributing easier.

Starting today, OpenMW code will be [available on GitLab][1] while being bidirectionally mirrored [with GitHub][2]. This means that our open source project is now hosted on an open source platform, while still allowing the countless GitHub contributors who have submitted code over the years to continue to do so on that platform if they choose. In addition, our Redmine issue tracker has been retired and all the issues have been [migrated to GitLab][3]. Our wiki is still in the process of being migrated, but will be [hosted on GitLab as well][4]. This allows everyone to use a single account for tracking issues, contributing code, and writing wiki documentation.

We hope that this will make the process of contributing to OpenMW better for everyone, and look forward to all sorts of new contributions!

<div class="content">
  <div class="content">
    <h5>
      <a href="//forum.openmw.org/viewtopic.php?f=38&t=5244" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>
    </h5>
  </div>
</div>

 [1]: https://gitlab.com/OpenMW/openmw/
 [2]: https://github.com/OpenMW/openmw
 [3]: https://gitlab.com/OpenMW/openmw/issues
 [4]: https://gitlab.com/OpenMW/openmw/wikis/home