{
   "author": "Okulo",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-10-16T11:05:47+00:00",
   "title": "On my head, they keep falling",
   "type": "post",
   "url": "/2014/head-falling/"
}
It&#8217;s been a while since the last update, so you&#8217;re probably curious what has happened in the meantime. Well, we&#8217;ve got 108 closed issues on the 0.33.0 tracker now and many more bugs have been fixed that did not appear on there. Let&#8217;s look at a few.

One particularly interesting bug left Dagoth Ur dead. In Morrowind you were supposed to defeat seven of Dagoth Ur&#8217;s cronies before facing the big bad himself. A script would be running that lowered Dagoth Ur&#8217;s health by 50 points every time you killed one of them, making for a total of 350 points of damage to the Sharmat. In vanilla Morrowind, this script was broken so the damage was not done. However, OpenMW didn&#8217;t have this bug in the first place, so the script ran as instructed. The result was that Dagoth Ur, who only had 300 health, would already be lying dead in his cavern before you had even reached him. Ironically, the effects of the bug needed to be replicated to make the game function as intended.

There was also an issue with the rain, a visual effect that is actually all smoke and mirrors. You wouldn&#8217;t (or rather shouldn&#8217;t) notice, but when it rains in-game, it doesn&#8217;t rain everywhere. There&#8217;s no reason to, really; it would be unneccessarily taxing your system for little (visual) gain. Instead, it only rains where the player, you, is walking. In first person you wouldn&#8217;t see the difference anyway. But once you switched you third person, you could see how [extremely local the rain actually was][1]. That&#8217;s been fixed now. Rain will fall wherever your camera is positioned, not wherever your character is.

Another issue was with followers. When teleporting or traveling via silt strider your follower would be sent to your destination first. After that, you would go. This happened almost simultaneously, so normally you&#8217;d never notice. But since your follower has the exact same destination as you, you would be placed on top of your follower&#8217;s head. In outside areas that was silly at best, but if you were transported to a destination inside a building (such as one of the Mages Guild halls), you would end up not just on top of your follower, but with the top part of your body clipping through the ceiling. That is fixed now, too, so no more [Philadelphia Experiment][2]-esque fusings in the guild halls.

Some news from the OpenCS corner, too. [Terrain rendering has been included][3], which means you can now properly view the terrain from OpenCS. Those squares with text you see on the screenshot are the x,y-coordinates and the name of the cell. This way you know exactly where you are as you are flying over the terrain.

cc9cii has spent some time cleaning up code and Lazaroth has been playing around with shaders. He wondered what it would look like if water changed its behaviour based on the weather. For example, when it rains, that should be reflected in the water, right? He has already [replicated the effect][4], but right now only one such effect can be applied to the water itself. Right now it is not possible to apply different effects to the water surface depending on the current weather, but Scrawl mentioned that with some work that should not be a big issue. It&#8217;s definitely work that is worth doing, because not only does it allow developers to change the shape of the water surface, it also allows for other cool effects, like streets getting a nice sheen from the rain.

Speaking of Scrawl, he has started a blog of his own. This blog will not only chronicle the more technical sides of OpenMW, but also that of other projects he&#8217;s working on. As of now, his most recent post is about porting OpenMW to Ogre 2.0, which _should_ give OpenMW a nice performance boost. If you&#8217;ve got a little background in software development, this is bound to interest you. [Check out Scrawl&#8217;s blog for more of his coding (mis)adventures][5].

That&#8217;s it for now. Version 0.33.0 is making good progress and the sheer amount of fixes has prompted talk of another release soon, so stay tuned.

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=2516" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://bugs.openmw.org/attachments/download/608/2014-07-26_00007.jpg
 [2]: https://en.wikipedia.org/wiki/Philadelphia_Experiment
 [3]: https://github.com/OpenMW/openmw/pull/312
 [4]: http://i.imgur.com/JNfv62K.jpg
 [5]: http://scrawl.bplaced.net/blog/