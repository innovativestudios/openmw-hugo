{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2011-09-18T11:53:53+00:00",
   "showReadingTime": true,
   "summary": "jhooks was able to make hands open and close. And it&#8217;s the good news. The bad news is that with all this animation stuff performance is quite poor.",
   "tags": [
      "bullet",
      "Gus",
      "jhooks",
      "K1ll",
      "physics"
   ],
   "title": "The last days of summer",
   "url": "2011/the-last-days-of-summer"
}
Solving performance problems is not strait forward but in theory It looks quite simple: find the bottleneck, part of procedure that runs slowest; then try to fix it. If needed: repeat. GUS already demonstrated this tactic. The worst thing that we can discover is the bottleneck function that can&#8217;t run faster but investigation just started and nothing like that must happen.

But at least It works! Keep this in the mind all the time.

Bullet optimisation also slowly moves forward. K1ll still suffers poor performance on his system and helps GUS to find the problem. The situation is not clear at the moment, this can be bug in the bullet itself.

OpenMW 0.11.1 is late because We lack linux builder. There are no word to express what I think about it.