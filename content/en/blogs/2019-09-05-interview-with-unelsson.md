{
   "author": "lysol",
   "categories": [
      "Uncategorized"
   ],
   "comment": true,
   "date": "2019-09-05T10:47:45+00:00",
   "showReadingTime": true,
   "source": {
      "forumID": 3,
      "topicID": 6340
   },
   "title": "Interview with unelsson",
   "url": "/2019/interview-with-unelsson/"
}
Hello again!

Today we have a different kind of a news post for you – namely an interview with one of our developers, unelsson. I, Lysol that is, had a nice short chat with him the other day. He has been working on our content file editor OpenMW-CS, both on features that have been added already and also on more experimental features that are yet to be introduced


**Who are you?**

My name is Uoti Huotari, I’m a circus instructor and performer from Finland, as well as a rpg designer, physical therapist and robotics hobbyist. Oh yeah, I do code stuff to OpenMW-CS too

**Not to get too off-topic, but a circus instructor that is into robotics? This sounds really interesting. Anything to show us?**

Well, I&#8217;m really proud of my _snapuswipe_, posted that at my [Instagram][1].

**So to move on, when and how did you discover the project?**

I think I saw a release video in Youtube some years ago, and as a fan of Morrowind decided to give OpenMW a try. I got hooked by the stability and performance of the engine, no crashes, and the usability too

**You&#8217;ve mostly been working with OpenMW-CS while most developers always found it more interesting to work with the engine instead. Why?**

It doesn’t really matter what I work on, as long as I am doing something interesting and useful. My goals have been not only to develop OpenMW, but also to develop my coding skills, and get some good feedback and guidance from people who know better than me.

My secret plan always was to fit in by listening to project lead and other developers, and do whatever is required to push the project forwards. By doing that, I hope to gain better feedback, which will in turn help me to gain personal coding skills. It’s also nice to just contribute with something useful, coding is nonetheless a hobby for me, and it’s just inherently rewarding to participate by being useful in the ways I can

**Yes, speaking of developing your coding skills, how much experience did you have when you began coding for the CS then? Do you feel you have evolved as a developer since then?**

My dad taught me to code BASICA when I was five years old, and I coded kazillion small games and personal projects with various languages, like QBASIC, Turbo Pascal and C/C++ until I was a teenager. Later I got interested in robotics, which got me learning some Python, rpg design drew me into writing web apps with Javascript, and html seems to be everywhere.

OpenMW is written mostly with C++, and frankly, I couldn’t remember how to write a Hello World -program when I started. Participating has developed my understanding of C++ enormously… Just insanely much

**Has OpenMW lived up to what you expected when you became involved with the project?**

It’s always been more than I ever expected, a stable and easy way to play Morrowind on modern Linux computer, not to mention that you can easily use mods, there are shadows (omg!), and me too, with zero coding education, I’ve actually been able to learn and contribute. It’s really more than I could ever ask for.

But I did expect faster development, especially on the OpenMW-CS side. There have however been some unexpected delays and things in-between that require solving before more features can be added. I don’t feel like OpenMW is failing though, it’s nonetheless a remarkable project, and it kind of keeps going forward even when it seems like nothing is happening

**What kind of cool stuff do you expect (or hope) OpenMW will give us in the future?**

I think the various missing edit modes should be added to OpenMW-CS, but improved versions from vanilla. I hope to contribute so that OpenMW-CS can be better than the vanilla CS. On the engine side, I really hope that some sort of batching is developed, as that will improve performance, and might allow things like procedurally generated grass, but I really have no idea how that can be done, or when it will happen

**Do you plan to do any of the OpenMW-CS things in the near future?**

Uhh.. Yes? No? I really want to complete the rest of the edit modes, but it’s probably not the best idea to rush things. I’m quite busy with other stuff in my life right now, but it’s not like I have no time at all, coding is after all a place to rest in between all the flipping around, hanging from the trapeze and throwing stuff in the air

**Thanks!**

I have also interviewed Capostrophic, and that interview is more or less ready to be published. But we&#8217;ll save that for another time. Also, there is a lot going on on GitLab/GitHub right now, so hopefully we can tell you more about that soon. Until then, thank you for reading.

[1]: https://www.instagram.com/p/BrAnag9HUdS/