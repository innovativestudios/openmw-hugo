{
   "author": "Lukasz Gromanowski",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-03-11T20:55:57+00:00",
   "openid_comments": [
      "a:1:{i:0;i:549;}"
   ],
   "title": "Did he fire six arrows or only five?",
   "type": "post",
   "url": "/2014/fire-arrows-five/"
}
Are there any among us who prefer ranged weapons over close quarters combat with the enemy? Are there any among us who prefer bows, crossbows, or maybe even throwing weapons? Yeah, certainly there are, and certainly they&#8217;ll be glad to hear that they can now shoot and throw at will! OpenMW now supports all nasty, harmful things relating to the &#8220;Marksman&#8221; skill category. Of course, the one behind all this is none other than Scrawl.

However, that&#8217;s not all.

Gus contributed another portion of fixes and improvements intended for the still slightly ailing artificial intelligence of NPCs. Zini improves 3D navigation in the rendering panel of OpenCS, while SirHerrbatka is working on the records editing view &#8211; which you can see on the following screenshot: 

[<img loading="lazy" src="https://i0.wp.com/openmw.org/wp-content/uploads/2014/03/zrzut961.png?resize=300%2C240&#038;ssl=1" alt="zrzut96" width="300" height="240" class="alignnone size-medium wp-image-3490" srcset="https://i0.wp.com/openmw.org/wp-content/uploads/2014/03/zrzut961.png?resize=300%2C240&ssl=1 300w, https://i0.wp.com/openmw.org/wp-content/uploads/2014/03/zrzut961.png?resize=1024%2C819&ssl=1 1024w, https://i0.wp.com/openmw.org/wp-content/uploads/2014/03/zrzut961.png?w=1280&ssl=1 1280w" sizes="(max-width: 300px) 100vw, 300px" data-recalc-dims="1" />][1] 

See you next week!

 [1]: https://i0.wp.com/openmw.org/wp-content/uploads/2014/03/zrzut961.png?ssl=1