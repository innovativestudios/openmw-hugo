{
  "author": "lysol",
  "categories": [
    "Uncategorized"
  ],
  "comment": true,
  "date": "2020-02-10T10:19:19+00:00",
  "showReadingTime": true,
  "source": {
    "forumID": 3,
    "topicID": 6693
  },
  "title": "OpenMW won GOTY award!",
  "url": "/2020/openmw-won-goty-award/"
}
Hello everyone

We are very proud to announce that the website [GamingOnLinux][1] has revealed OpenMW as the winner of their [Game of the Year awards][2] in the _Favourite FOSS game engine reimplementation_ category.

Competing against other respected and important projects like [ScummVM][3] and [OpenRA][4], this means a lot, and we are very honoured. A BIG thank you to everyone who voted for OpenMW!

[1]: http://www.gamingonlinux.com
[2]: https://www.gamingonlinux.com/goty.php?module=category&category_id=7
[3]: https://www.scummvm.org/
[4]: https://www.openra.net/