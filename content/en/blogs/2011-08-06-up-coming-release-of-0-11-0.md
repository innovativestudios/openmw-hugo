{
   "author": "hircine",
   "categories": [
      "Week's review"
   ],
   "date": "2011-08-06T11:53:35+00:00",
   "showReadingTime": true,
   "tags": [
      "0.11.0",
      "morrowind",
      "release"
   ],
   "title": "Up coming Release of 0.11.0",
   "url": "2011/up-coming-release-of-0-11-0"
}
OpenMW, the reimplementation of the Engine of popular video game The Elder Scrolls: Morrowind.

We have an upcoming release version 0.11.0 (to be released in the next 7-14 days)

Which fixes several bugs, as well as improving performance. New features include, tab-completion in the console, Message Box GUI and a Launcher.

&nbsp;

This is largely a maintenance release, kill bugs, improve performance and add a few new things.

0.12.0 should be more feature packed release.

Although we have had a round at speeding things up there are still a number of things we can do to improve performance for the engine.

The Launcher although allows selecting multiple esm/esp files, OpenMW currently only supports one esm. Esp file loading is yet to be introduced.

&nbsp;

For The following release(0.12.0); The User interface will receive a lot of attention. Including The NPC Dialogue System.