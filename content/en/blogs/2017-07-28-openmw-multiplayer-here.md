{
   "author": "DestinedToDie",
   "categories": [
      "Uncategorized"
   ],
   "date": "2017-07-28T21:47:50+00:00",
   "title": "OpenMW multiplayer is here!",
   "type": "post",
   "url": "/2017/openmw-multiplayer-here/"
}
**Edit: **Note that this is an old post about an older version of TES3MP. You can get a newer one from [here.][1]



**multiplayer**: &#8220;M&#8217;Aiq does not know this word. You wish others to help you in your quest? Coward! If you must, search for the Argonian Im-Leet, or perhaps the big Nord, Rolf the Uber. They will certainly wish to join you.&#8221; &#8211; M&#8217;Aiq the Liar

M&#8217;Aiq may have been teasing us back in 2002, but today you really can meet the Argonian Im-Leet as well as Rolf the Uber. This is all thanks to OpenMW&#8217;s sister project TES3MP and its developers [Koncord][2] and [David Cernat][3]. Previously, tes3mp only had pvp, but a recent patch added NPC synchronization. As if that weren&#8217;t enough, they also decided to include sync for player faction, journal, topic, time, and plug-ins&#8230; You can pretty much play the whole game now, if you just keep in mind that scripts still work locally (as they do in single-player) and custom items made with alchemy/enchanting/spellmaking do not save on the server. [Download tes3mp version 0.6.0][4] and be sure to read developer announcement [here!][5]

[<img loading="lazy" class="alignnone wp-image-5615" src="https://i0.wp.com/openmw.org/wp-content/uploads/2017/07/tes3mp-2017-screenshot-1.png?resize=716%2C403&#038;ssl=1" alt="" width="716" height="403" srcset="https://i0.wp.com/openmw.org/wp-content/uploads/2017/07/tes3mp-2017-screenshot-1.png?resize=1024%2C576&ssl=1 1024w, https://i0.wp.com/openmw.org/wp-content/uploads/2017/07/tes3mp-2017-screenshot-1.png?resize=300%2C169&ssl=1 300w, https://i0.wp.com/openmw.org/wp-content/uploads/2017/07/tes3mp-2017-screenshot-1.png?resize=768%2C432&ssl=1 768w, https://i0.wp.com/openmw.org/wp-content/uploads/2017/07/tes3mp-2017-screenshot-1.png?w=1280&ssl=1 1280w" sizes="(max-width: 716px) 100vw, 716px" data-recalc-dims="1" />][6]

Installation is super easy. If you already have OpenMW installed, you need only download tes3mp and unzip the folder to a destination of your choice and you&#8217;re ready to go. In case you haven&#8217;t got OpenMW previously installed, it&#8217;s sufficient to run the launcher wizard and point it at morrowind.esm, it&#8217;s that easy. After that&#8217;s done, you can start your own server simply by running tes3mp-server.exe. You&#8217;ll have to open a port so other players can join or use a networking program like Hamachi. Or if you&#8217;re simply looking to join a server, launch tes3mp-browser.exe and pick your server of choice.

[<img loading="lazy" class="alignnone wp-image-5616" src="https://i0.wp.com/openmw.org/wp-content/uploads/2017/07/tes3mp-2017-screenshot-2.jpg?resize=716%2C382&#038;ssl=1" alt="" width="716" height="382" srcset="https://i0.wp.com/openmw.org/wp-content/uploads/2017/07/tes3mp-2017-screenshot-2.jpg?resize=300%2C160&ssl=1 300w, https://i0.wp.com/openmw.org/wp-content/uploads/2017/07/tes3mp-2017-screenshot-2.jpg?resize=1024%2C547&ssl=1 1024w, https://i0.wp.com/openmw.org/wp-content/uploads/2017/07/tes3mp-2017-screenshot-2.jpg?resize=768%2C410&ssl=1 768w, https://i0.wp.com/openmw.org/wp-content/uploads/2017/07/tes3mp-2017-screenshot-2.jpg?resize=1536%2C820&ssl=1 1536w, https://i0.wp.com/openmw.org/wp-content/uploads/2017/07/tes3mp-2017-screenshot-2.jpg?w=1728&ssl=1 1728w" sizes="(max-width: 716px) 100vw, 716px" data-recalc-dims="1" />][7]

As you can see, there are quite a few options available. A server can be private and password-protected or free for anyone to join. It can have a list of plug-ins, in which case I also need to download the specified mods if I wish to join it. The admin can either require that you have .esp files with the exact same hash as the one he is using or allow for any .esp file so long as the name matches. An admin can also enable or disable console for the entire server, or just for a specific player if need be. Additionally, it is up to him to set the difficulty for players joining the server, as well as the starting location via LUA script.

LUA scripting allows for quite a few interesting tweaks. During the alpha testing phase one of the modders scripted a new game mode &#8211; battle royale. It was set up in a way that players would spawn in a waiting cell and when a minimum of 3 players had joined the server, a 60 second count-down would start. Once the numbers ticked down, players were teleported to random cities around Vvardenfel with 25 minutes to arm themselves to the best of their ability before abruptly being teleported into an arena where they&#8217;d duke it out. Imagine the possibilities here!

But let&#8217;s pretend that I&#8217;m launching the vanilla tes3mp and see what awaits us. The first thing greeting me is the outside of Pelagiad and a box asking for my name and password. After entering them, the info is sent to the server, which creates a .json file where my character is stored. A moment later, I fill in my race, class and birthsign and am teleported to Balmora.

Now that I&#8217;ve properly logged in, I am immediately brought up to date with every faction, faction reputation and quest. If a player has joined thieves guild, then I&#8217;ll also be included in thieves guild. If a person is an archmage of the mages guild, then everyone is an archmage. If someone did a quest, I&#8217;ll have his journal entries in my book. I open up my journal and see that a player has started the fighters guild rat quest, killed the rats, got the 100 gold reward, but not yet reported to Eydis Fire-Eye.

I make my way to the fighters guild. As I enter the fighters guild cell, the server sends me info from a cell.json file, which contains all of the changes other players have made to the cell. This might be a dead NPC, a dropped item or a looted item. In my case I find that someone has emptied the fighters guild supply chest. This is multiplayer, after all. I turn in the unfinished rat quest and get an advancement to apprentice.

I ask for more orders and get the egg poacher quest. It may be a little too much for my freshly made character to handle, so I ask in game chat if anyone wants to accompany me. 2 players join and we go to the eggmine. Player 1 has a ping of 356, player 2 has a ping of 72 and my ping is 201. Because player 2 has the lowest ping of us all, he becomes the authority of the cell. When a kwama forager attacks us, player 2 (authority) sends information about the forager&#8217;s movements to the server and the server distributes info back to player 1 and me. This is how the synchronization of NPCs and creatures is handled.

After fighting a bunch of kwama foragers and bringing the egg poachers to justice, we are all wounded to an extent. Player 1 needs 7 hours to fully heal, player 2 needs only 1 hour and I need 3 hours to be fully rested. So how is that resolved? The server keeps the time and our individual times are always synchronized to that. When I rest for 3 hours, the resting bar will appear and I&#8217;ll start healing as my individual time appears to pass, but as soon as the bar is filled, my client re-synchronizes back into server time.

After we are done, we decide to go back to Balmora. My buddies were smart enough to bring almsivi intervention scrolls, but I didn&#8217;t. Instead of running all the way back to Balmora, I decide to kill myself by writing /suicide in chat and triggering a LUA script that ends my life. But don&#8217;t worry, I&#8217;ll respawn in the nearest temple or imperial shrine, which just so happens to be Balmora temple right now.

We return to the guild of fighters for our reward. A quest may be completed only once and the reward is only given to one player. We split the gold we got for tracking down the egg poachers. Similarly, an item may only be picked up once. However the server admin may reset cells and quests by deleting cell and world .json files, making it possibly to do quests and obtain items you might have previously missed.

We start pvp-ing each other. Bad idea, because the citizens of Balmora report our unlawful behavior! A guard runs to me and gives me an ultimatum &#8211; either pay gold or go to jail. Since time is never stopped in a multiplayer environment, the other 2 players are free to continue playing while I sort my business out with the guard. He may not know it, but I&#8217;m the Nerevarine (or at least one of them) and there&#8217;s a price to pay for so rudely interrupting my business.

I loot the armor off the dead guard and equip it. And then it hits me. I look indistinguishable from a Hlaalu Guard. I turn off my caps lock and start walking around as if I&#8217;m wandering in an AI pattern. Nobody will be able to tell the difference &#8211; at least not until they get too close and I&#8217;ve already drawn my blade. If you ever come to visit Balmora. Remember. I&#8217;ll be there. Walking. In an AI pattern. Ready to strike unexpectedly.

With how far tes3mp has come in such a relatively short time, I dare say that it has shown that implementing multiplayer and other projects as such on OpenMW is the preferable route. People have waited years and years for a fan-made implementation of some form of multiplayer for TES3: Morrowind, Oblivion, Skyrim, Fallout 3, Fallout 3 &#8211; New Vegas and tes3mp is the first to deliver not only functional NPC sync, but also a mountain of features no other similar project has been able to deliver yet. It only took 2 developers and an Open Source engine to do what even large teams of professional coders struggle to achieve on a closed source TES engine.

OpenMW has been building up to something like this as well as other possible projects for a long time, so it&#8217;s a great joy to finally see it forked for a successful multiplayer project. The dream of being able to play Morrowind with your friends is now achieved! But don&#8217;t expect us to stop yet. The developers of tes3mp will continue improving it even further, OpenMW main fork continues to receive pull requests and we welcome any who would like to contribute to making OpenMW better or want to make their own forked project similar to tes3mp.

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=4463" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://github.com/TES3MP/openmw-tes3mp/releases
 [2]: https://www.patreon.com/Koncord
 [3]: https://www.patreon.com/davidcernat
 [4]: https://github.com/TES3MP/openmw-tes3mp/releases/download/tes3mp-0.6.0/tes3mp.Win64.release.0.6.0.zip
 [5]: https://steamcommunity.com/groups/mwmulti#announcements/detail/1441567597386587897
 [6]: https://i0.wp.com/openmw.org/wp-content/uploads/2017/07/tes3mp-2017-screenshot-1.png?ssl=1
 [7]: https://i0.wp.com/openmw.org/wp-content/uploads/2017/07/tes3mp-2017-screenshot-2.jpg?ssl=1