{
  "author": "psi29a",
  "categories": [
    "Uncategorized"
  ],
  "date": "2020-06-05T14:37:54+00:00",
  "showReadingTime": true,
  "title": "A rumble in the deep",
  "url": "/2020/rumble-deep/"
}
Let&#8217;s just jump right into it! We&#8217;ve recently forked off 0.46.0 into its own branch and started our release candidate (RC) process. This will be officially our biggest release yet! If you would like to help us test, feel free to join us on the [Forums](https://forum.openmw.org/), [IRC](https://webchat.freenode.net/?channels=openmw&uio=OT10cnVlde) or [Discord](https://discord.gg/bWuqq2e).

{{<figure src="https://i0.wp.com/openmw.org/wp-content/uploads/2020/06/screenshot038.png?resize=1119%2C629&#038;ssl=1" caption="Sunrise">}}
{{<figure src="https://i0.wp.com/openmw.org/wp-content/uploads/2020/06/screenshot037.png?resize=1119%2C629&#038;ssl=1" caption="Sunset">}}