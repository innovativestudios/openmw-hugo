{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2011-11-20T19:36:18+00:00",
   "tags": [
      "animation",
      "GUI",
      "journal",
      "mygui"
   ],
   "title": "mwrender… again",
   "type": "post",
   "url": "/2011/mwrender-again/"
}
This is how it looks my rss reader:

[<img loading="lazy" src="https://i0.wp.com/openmw.org/wp-content/uploads/2011/11/38-300x240.png?resize=300%2C240" alt="" title="akregator" width="300" height="240" class="aligncenter size-medium wp-image-137" srcset="https://i0.wp.com/openmw.org/wp-content/uploads/2011/11/38.png?resize=300%2C240&ssl=1 300w, https://i0.wp.com/openmw.org/wp-content/uploads/2011/11/38.png?resize=1024%2C819&ssl=1 1024w, https://i0.wp.com/openmw.org/wp-content/uploads/2011/11/38.png?w=1280&ssl=1 1280w" sizes="(max-width: 300px) 100vw, 300px" data-recalc-dims="1" />][1]

Mwrender all the time. At last the end of this task is near. Than animations + journal and 0.12.0 is coming. It&#8217;s safe to say that 0.12.0 will have this two major features. 

It supposed to be GUI time but It won&#8217;t. Maybe it&#8217;s a fault of poor documentation of mygui, maybe lack of support, maybe We done wrong choosing it but it&#8217;s not a time to look backward. 

It&#8217;s time to look forward!

PS  
I hope that 0.12.0 will come soon so I won&#8217;t have to write about mwrender all the time. ;-)

 [1]: https://i0.wp.com/openmw.org/wp-content/uploads/2011/11/38.png