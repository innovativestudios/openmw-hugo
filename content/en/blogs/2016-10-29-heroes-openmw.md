{
   "author": "DestinedToDie",
   "categories": [
      "Uncategorized"
   ],
   "date": "2016-10-29T09:51:38+00:00",
   "title": "Heroes of OpenMW",
   "type": "post",
   "url": "/2016/heroes-openmw/"
}
For a long time scrawl has been the main driving force behind OpenMW. So when the news came that he&#8217;d be going on a hiatus to improve his health, many of us thought that the progress would halt, and that the next releases of OpenMW would only include bug-fixes, if anything&#8230;

But we were so utterly wrong! Out of the woodworks appeared Allofich and MiroslavR. The duo has not only solved many bugs but also implemented several new features. In addition, scrawl is still active in the community &#8211; accepting pull requests, reading the forums and occasionally closing an issue when his health allows it. While the pace is a bit slower now, our project still makes progress, and OpenMW steadily continues its journey towards version 1.0.

[<img loading="lazy" class="wp-image-4741 aligncenter" src="https://i0.wp.com/openmw.org/wp-content/uploads/2016/10/bugtracker-300x133.jpg?resize=675%2C299&#038;ssl=1" alt="bugtracker" width="675" height="299" data-recalc-dims="1" />][1]

On the OpenMW-CS side of things there have also been several improvements. Previously, the scene window was a static view of the world where you couldn&#8217;t really do anything besides looking around with the camera. Aeslywinn came in, reworked the camera system, implemented pathgrids, water rendering and object movement, rotation and scaling sub modes. While he has now left the OpenMW project to do other great things, he has also left our editor with the capability to create new worlds.

[<img loading="lazy" class="wp-image-4742 aligncenter" src="https://i0.wp.com/openmw.org/wp-content/uploads/2016/10/Castle-300x161.jpg?resize=691%2C371&#038;ssl=1" alt="castle" width="691" height="371" data-recalc-dims="1" />][2]

It should be noted that, although what you see in the above screenshot was made using OpenMW-CS, there are still some features that need to implemented for the editor. Remember &#8211; If you have skills in C++ or QT and are interested in contributing to the OpenMW project, then you too can be an OpenMW hero.

[Want to leave a comment?][3]

 [1]: https://i0.wp.com/openmw.org/wp-content/uploads/2016/10/bugtracker.jpg?ssl=1
 [2]: https://i0.wp.com/openmw.org/wp-content/uploads/2016/10/Castle.jpg?ssl=1
 [3]: https://forum.openmw.org/viewtopic.php?f=38&p=43228#p43228