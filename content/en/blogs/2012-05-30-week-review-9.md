{
   "author": "jvoisin",
   "date": "2012-05-30T09:50:36+00:00",
   "tags": [
      "Aleksandar",
      "animation",
      "Chris",
      "Edmondo",
      "Gus",
      "jhooks",
      "Mark76",
      "multiple esm",
      "scrawl",
      "spells",
      "spyboot"
   ],
   "title": "The week in review",
   "type": "post",
   "url": "/2012/week-review-9/"
}
Hello everyone!

We are pretty close from a release, so things are pretty calms this week.

**scrawl**, while improving the GUI, has worked mainly on options support, like refactorisation of the launcher, preliminary implementation of ingame options, refactoring of options-support backend&#8230;

**edmondo** has proposed to clean a little bit the cmake script, but his patches were rejected. As **zini** said:

> We don&#8217;t have any formatting rules for cmake files and considering that even with the currently limited number of code layout and naming rules (some) people have difficulties following them, I don&#8217;t see how adding more will be of any help. But without new rules the cmake files will quickly degenerate into the inconsistent state we have now.
> 
> On top of that these kind of code cleanup usually leads to a lot of merge problems. Therefore they should be avoided unless absolutely necessary.

**gus** is currently working on objects rotation and scaling, to allow you to decorate your house, or do books tower!

**jhooks** and **Chris** are, as always, trying to improve animations.

**Aleksandar** is trying to implement the spell window.

**mark76**: is doing great on multiples ESM support, you&#8217;ll soon be able to walk into Mournhold!

**spyboot** is our new German translator (and maybe a future developer? who knows&#8230;), be nice with him!