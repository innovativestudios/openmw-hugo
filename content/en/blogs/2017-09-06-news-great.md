{
   "author": "DestinedToDie",
   "categories": [
      "Uncategorized"
   ],
   "date": "2017-09-06T08:02:24+00:00",
   "title": "News of great things to come",
   "type": "post",
   "url": "/2017/news-great/"
}
<div class="content">
  <p>
    It has been slightly over a month since the release of OpenMW 0.42.0. If you look back to when 0.41.0 was released, you&#8217;ll see that it took us 8 months to deliver the new version, making it perhaps one of the lengthiest releases in our history, yet in its size it isn&#8217;t that different from preceding versions.
  </p>
  
  <p>
    That is not to say that OpenMW developers have been slacking off, however. Or that during the course of those 8 months we had much less progress. The reason for this delay was that our video editor&#8217;s computer broke down at a rather unfortunate and inconvenient time. When discussing whether the release should done without including a video, the decision was made that we would halt the release process until hardware issues were solved. After all, if anyone wants to play the up to date version of OpenMW, they needn&#8217;t wait for the release because they can use our nightlies, which we have recently made more visible by including them in the <a class="postlink" href="https://openmw.org/downloads/">downloads page</a>.
  </p>
  
  <p>
    The good news is that while the release process was in limbo, work on the next version had already started and by the end of it had reached a point where we almost had enough to release version 0.43.0 the day after 0.42.0. At the time of this writing, we are sitting at 85 closed issues, making this release already larger than the previous. <a class="postlink" href="https://bugs.openmw.org/versions/52">Check out the bugtracker</a> for more detail.
  </p>
  
  <p>
    A little more than a month ago we also saw the release of TES3MP version 0.6.0 and more recently a <a class="postlink" href="http://steamcommunity.com/groups/mwmulti#announcements/detail/1443822567710521369">stability patch 0.6.1</a>, with the next version possibly landing very soon at mid-September. Since TES3MP is built upon the OpenMW engine, we received part of the media attention and had a burst of new developer applications afterwards.
  </p>
  
  <p>
    With new people on the board, there has been increased increased interest in improving our code documentation. One of our new coders is looking into possibly extending the .OSG animations or making it possible for OpenMW to read more than just Morrowind&#8217;s version of .nif files. Scrawl has released his incomplete experiments with shadows and an old member by the name of AnyOldName3 is trying to get them working, though he urges me to include that there&#8217;s no guarantee he&#8217;ll succeed. In addition, we&#8217;ve had the return of Aeslywinn, who in the past did some great work on OpenMW-CS and is back to continue where he left off.
  </p>
  
  <p>
    A new feature of his that&#8217;s already been merged with OpenMW gives modders the comfort of being able to reload assets in OpenMW-CS without having to close and re-open the program. His next undertaking seems to be land records, which would be a step towards being able to edit landscape, the main missing feature in OpenMW-CS.
  </p>
  
  <p>
    While we have enough issues closed to produce a rather sizable release, no call for the release process to start has been made yet. After all, we just released 0.42.0 only a brief time ago. It is likely that the upcoming version will be bigger than usual and will arrive in about two months, with the mentioned land records and other features currently being worked on possibly included. Overall, areas that remained stagnant a few months ago are now getting visible development and things are looking good for the OpenMW project.
  </p>
</div>

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=4620" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>