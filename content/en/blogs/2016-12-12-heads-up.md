{
   "author": "DestinedToDie",
   "categories": [
      "Uncategorized"
   ],
   "date": "2016-12-12T04:58:28+00:00",
   "title": "Just a heads-up",
   "type": "post",
   "url": "/2016/heads-up/"
}
OpenMW version 0.41.0 might be coming out in about a week or two.

To prepare for it, our new release manager raevol is putting together a changelog. Atahualpa is working on the release videos. Meanwhile, our developers have begun work on OpenMW version 0.42.0.

By the way, you can already do a test drive on OpenMW version 0.41.0. Technically it&#8217;s not released yet, but during the release process, we try to give our Release Candidate packages some quick testing to see if they work out of the box. They are scattered around the last pages of [this thread][1]{.postlink}.

Sometimes our RCs have problems with OpenMW-CS, other times someone forgot to include an important component. If you encounter any problems, please tell us about it in the [OpenMW 0.41.0 thread][1]{.postlink}. And if everything runs smooth, let us know anyway, so we can be sure that the RCs have been tested.

[Want to comment?][2]

 [1]: https://forum.openmw.org/viewtopic.php?f=20&t=3768
 [2]: https://forum.openmw.org/viewtopic.php?f=38&t=3989