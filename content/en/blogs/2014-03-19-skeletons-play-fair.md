{
   "author": "Lukasz Gromanowski",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-03-19T22:19:29+00:00",
   "title": "Skeletons don’t play fair",
   "type": "post",
   "url": "/2014/skeletons-play-fair/"
}
OpenMW v0.29.0 has been released &#8211; the best release since the previous one! :D Just look at the list of changes &#8211; not only is its sheer size impressive, it also includes the long awaited option to save the game. Of course, most of you already know this very well, so let&#8217;s move on to the events of the last week.

An interesting new feature in the OpenCS editor is the initial implementation of the model preview pane. As the name suggests, it is a tool for previewing objects without having to place them in the cell rendering window.

I&#8217;m not sure if this absolutely essential feature was available in the original Construction Set, but we have it!

In addition, another portion of AI patches have appeared in OpenMW&#8217;s repository and characters under the influence of levitation and water walking magic are now able to use these spells to move over areas of water. Also, creatures can handle ranged combat &#8211; beware of skeleton-archers with enchanted, paralyzing arrows! Probably every veteran player has already experienced the charm of this weapon ;)

Till next time!