{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2012-02-19T10:11:09+00:00",
   "tags": [
      "dialogue",
      "equiping",
      "Gus",
      "jhooks",
      "lgromanowski",
      "rendering",
      "scrawl",
      "skybox",
      "water",
      "weather",
      "Zini"
   ],
   "title": "New roadmap!",
   "type": "post",
   "url": "/2012/224/"
}
Let&#8217;s start with **important news**: We decided that **openmw should focus on smaller, but more frequent releases**. It seems that every single release of openmw up to now was delayed badly. And that&#8217;s bad since It means that **features must wait very long for testing.** Bugs can be tricky to fix when a lot happens in the code meanwhile.

**In openmw 0.13.0 We won&#8217;t see terrain rendering** as It needs to wait for new version of OGRE to be added without unneeded pain. Waiting does not go well with new concept of frequent releasing. **But we have another killer feature: dialogue GUI!** GUS did very well on this one, and did already a lot ─ the rest **needs fixing another partially broken stuff and Zini is on it just after finishing equipping item task** (jhooks1 wouldn&#8217;t forgive delaying it).

And water rendering. **Water is mega-important and easily seen feature.** Jhooks1 did awesome job again. **This will be include in new version of openmw as well.**

This two features would be enough for openmw 0.13.0 on they own. **Any other big features needs to be moved** for 0.14.0 or even 0.15.0.

And Let&#8217;s not forget about the openmw 0.12.0. **We are almost done here.** Just a little more tuning and We are ready. It&#8217;s all in lgro hands. This are good hands (frequently washed). ;-)

Recently new developer joined us. **Welcome Scrawl**, one of the <a href="http://code.google.com/p/vdrift-ogre/" title="vdrift-ogre" target="_blank">vdrift-ogre</a> developers where He plays the role of graphics guy! Although advance graphics features needs to wait for openmw post 1.0.0 **there are still tasks to be done, even in rendering.** We still miss particles, need a proper skybox, weather&#8230; 

PS  
**ElderTroll** wrote <a href="http://freegamer.blogspot.com/2012/02/morrowind-open-source-pngines-who-they.html" title="article" target="_blank">article</a> for the freegamer about open source morrowind engines.