{
   "author": "Lukasz Gromanowski",
   "categories": [
      "Maintenance"
   ],
   "date": "2014-12-06T08:47:47+00:00",
   "title": "Web server change",
   "type": "post",
   "url": "/2014/web-server-change/"
}
Hi,  
tomorrow night (7.12.2014) I will be moving website to the new web server (same hosting provider, but different web server), so between 20.00-23.00 CET blog, wiki, bugtracker, and forum will be temporary in read-only mode (or closed if read-only mode won&#8217;t be possible).

Sorry for inconveniences.