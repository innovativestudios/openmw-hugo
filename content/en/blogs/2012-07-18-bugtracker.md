{
   "author": "Lukasz Gromanowski",
   "categories": [
      "Maintenance"
   ],
   "date": "2012-07-18T11:43:51+00:00",
   "tags": [
      "bugtracker",
      "lgromanowski"
   ],
   "title": "Bugtracker",
   "type": "post",
   "url": "/2012/bugtracker/"
}
Hello,  
Bugtracker has been moved from RootNode to OVH server last night and since then there is only one valid URL: <a href="http://bugs.openmw.org" title="http://bugs.openmw.org" target="_blank">http://bugs.openmw.org</a>. Please don&#8217;t use <a href="http://bugs.openmw.org" title="http://bugs.openmw.org" target="_blank">http://bugs.openmw.org:81/</a> (URL with :81, port number, suffix), it won&#8217;t work any more.