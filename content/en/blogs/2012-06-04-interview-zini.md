{
   "author": "jvoisin",
   "categories": [
      "Uncategorized"
   ],
   "date": "2012-06-04T15:18:21+00:00",
   "tags": [
      "ace",
      "interview",
      "scrawl",
      "Zini"
   ],
   "title": "Interview!",
   "type": "post",
   "url": "/2012/interview-zini/"
}
<div>
  Since our <a href="http://freegamer.blogspot.fr/2009/09/openmw-interview-with-nicolay-korslund.html">last interview</a> is 3 years old, time has come for new one with some of our developers: zini, scrawl and Ace.</p> 
  
  <div>
    <p>
      <strong>Hello, who are you?</strong>
    </p>
    
    <div>
      <blockquote>
        <p>
          <strong>Zini</strong>
        </p>
        
        <p>
          Zini. More mundanely know as Marc Zinnschlag. Lead Developer of OpenMW a.k.a. the Benevolent Dictator.
        </p>
      </blockquote>
      
      <blockquote>
        <p>
          <strong>Scrawl</strong>
        </p>
        
        <p>
          I&#8217;m a hobby programmer mainly interested in graphics programming and <a href="https://en.wikipedia.org/wiki/Shader" title="shader">shader</a> development.
        </p>
      </blockquote>
    </div>
    
    <blockquote>
      <p>
        <strong>Ace</strong>
      </p>
      
      <p>
        I happen to be Ace, the resident Windows builder.
      </p>
    </blockquote>
    
    <p>
      <strong>How did you discover the project?</strong>
    </p>
    
    <div>
      <blockquote>
        <p>
          <strong>Zini</strong>
        </p>
        
        <p>
          I honestly don&#8217;t remember. Been a long time ago. I guess I read about it in some forum. OGRE maybe?
        </p>
      </blockquote>
      
      <blockquote>
        <p>
          <strong>Scrawl</strong>
        </p>
        
        <p>
          Initially I discovered it a long time ago (some time in 2009 or so when the mailing list was created). Don&#8217;t remember where I saw it, probably a blogpost or article. Since then I always followed the news updates and was very excited about it (Morrowind being my favorite RPG and disappointment with the newer Bethesda games), but I never felt I had the skills to contributing. Back then all I knew was a little web development, I didn&#8217;t know C++ (or even D)
        </p>
      </blockquote>
      
      <blockquote>
        <p>
          <strong>Ace</strong>
        </p>
        
        <p>
          I think I was discussing open-source remakes with a couple of friends when I found it, it was still in D back then.
        </p>
      </blockquote>
    </div>
    
    <p>
      <strong>Why are you contributing ?</strong>
    </p>
    
    <div>
      <blockquote>
        <p>
          <strong>zini</strong>
        </p>
        
        <p>
          I am one of the developers of a Morrowind-TC (total conversion), namely <a href="http://cfkasper.de/ultima/index.php">U9: Redemption</a>. That obviously made OpenMW interesting to me. While we were working on Redemption we ran into more and more problems, because of the inherent bugginess of Morrowind. It was manageable at first, but got worse over time.
        </p>
        
        <p>
          I started by doing some bug reporting and privately providing an internal snapshot of Redemption to Nico (the previous lead developer) for testing. Later I started to help with coding.
        </p>
        
        <p>
          And then we ran into one big disastrous bug in Morrowind, that we somehow did not notice before :<br /> In Morrowind, if you move a NPC to a different cell via a script instruction and the NPC&#8217;s original cell wasn&#8217;t loaded, the NPC will be loaded only partially. That will result in odd behaviour and a crash, if you talk to the NPC.
        </p>
        
        <p>
          We honestly did not know that and it didn&#8217;t show up in the (small scale) test setups we had. Broke at least a dozen features.<br /> At this point it became clear, that we wouldn&#8217;t be able to release the game with the features and at the quality level we had planned. So I shifted a lot of free time from Redemption to OpenMW and even took over leadership when Nico dropped out.
        </p>
      </blockquote>
      
      <blockquote>
        <p>
          <strong>scrawl</strong>
        </p>
        
        <p>
          In the meantime I had gathered quite some knowledge about game development in C++ (I started making some small games in python and then moved on to C++ as I got interested in 3D graphics). I have also worked a lot with the OGRE engine which is used by OpenMW (and I think is a pretty cool engine).
        </p>
        
        <p>
          I have started contributing to OpenMW a few months ago because I felt someone with an expertise on graphics was missing &#8211; most easily supported by the fact that in 4 years of development, no one even bothered to make a simple sky renderer (which was one of my first contributions).
        </p>
      </blockquote>
      
      <blockquote>
        <p>
          <strong>Ace</strong>
        </p>
        
        <p>
          The project needed a windows package and at the time I happened to have a fully working build environment.
        </p>
      </blockquote>
    </div>
    
    <p>
      <strong>What are you currently working on?</strong>
    </p>
    
    <div>
      <blockquote>
        <p>
          <strong>Zini</strong>
        </p>
        
        <p>
          <a href="http://openmw.org/forum/viewtopic.php?f=20&t=711&p=6261&hilit=release#p6261">Planning</a>, scheming, giving directions and assistance, <a href="http://openmw.org/forum/viewtopic.php?f=7&t=720&p=5760">worrying</a>, <a href="http://openmw.org/forum/viewtopic.php?f=6&t=625">swearing</a> and occasionally cleaning up other people&#8217;s garbage. The usual thing a lead developer does.
        </p>
        
        <p>
          On the coding side of the job I am currently busy with sorting out the last bits of fallout from a rendering-subsystem refactoring we had a couple of releases ago.
        </p>
      </blockquote>
      
      <blockquote>
        <p>
          <strong>scrawl</strong>
        </p>
        
        <p>
          I will have to hold my horses with implementing kick-ass graphics until OpenMW 1.0 is released (more below). So currently I&#8217;m mostly working on GUI tasks. I&#8217;m quite familiar with <a href="http://mygui.info/" title="MyGUI">MyGUI</a> (the GUI engine used by OpenMW) and have only recently cleared up a big amount of incomplete or non-optimal things in the GUI. Now I want to implement all missing GUI windows completely and in all detail (currently working on the alchemy window)
        </p>
      </blockquote>
      
      <blockquote>
        <p>
          <strong>Ace</strong><br /> I&#8217;m working on getting the loaded ESM records to save again, haven&#8217;t had much progress in a while (I blame university) but I&#8217;m hoping to have it done before the 0.16 release.
        </p>
      </blockquote>
    </div>
    
    <p>
      <strong>What&#8217;s the weirdest bug you&#8217;ve encountered so far?</strong>
    </p>
    
    <div>
      <blockquote>
        <p>
          <strong>Zini</strong>
        </p>
        
        <p>
          Nothing major. As bugs go, OpenMW has been pretty tame so far.
        </p>
        
        <p>
          Well, it is a bug in Morrowind and not in OpenMW, so it doesn&#8217;t really matches the question but anyway: In Morrowind, if you move a NPC to a different cell via a script instruction and the NPC&#8217;s original cell wasn&#8217;t loaded, the NPC will be loaded only partially. That will result in odd behaviour and a crash, if you talk to the NPC.<br /> We honestly did not know that and it didn&#8217;t show up in the (small scale) test setups we had. Broke at least a dozen features.
        </p>
      </blockquote>
      
      <blockquote>
        <p>
          <strong>scrawl</strong>
        </p>
        
        <p>
          Can&#8217;t answer this one really, nothing big that i&#8217;ve encountered. The code base is pretty well organized thanks to our pragmatic leader which apparently has saved us from a lot of trouble.
        </p>
      </blockquote>
      
      <blockquote>
        <p>
          <strong>Ace</strong>
        </p>
        
        <p>
          I happened upon a weird one related to windows building around the time the audio refactoring happened, maybe a bit later. At the time the project built without a single error but still failed to link because of a supposedly missing function. I dug through the file in question for a while, checking and double checking that the function in question did in-fact exists.
        </p>
        
        <p>
          In the end, after testing everything I could think of (Including renaming the function, moving it, changing arguments), I decided to try switching the forward declaration of the argument type from a struct to a class.<br /> And lo and behold, suddenly the linker can find the function and successfully link the executable.
        </p>
      </blockquote>
    </div>
    
    <p>
      <strong>What do you dislike about openmw?</strong>
    </p>
    
    <div>
      <blockquote>
        <p>
          <strong>Zini</strong>
        </p>
        
        <p>
          Nothing major again. The project had a somewhat messy start, but we have that under control now and its nothing more than a minor annoyance.<br /> I don&#8217;t like how the work on the <a href="http://openmw.org/forum/viewtopic.php?f=7&t=720&p=5760">planned editor is delayed</a> again and again. Everything else looks reasonably good.
        </p>
      </blockquote>
      
      <blockquote>
        <p>
          <strong>scrawl</strong>
        </p>
        
        <p>
          The lack of progress, overall. A wrong programming language being chosen following a rewrite in another language causing a huge slowdown. Developers going MIA and not coming back. Of course you can&#8217;t demand anything like that from an open source project, but I still find it a bit disappointing that after 4 years there is still no playable version. Luckily there&#8217;s been a substantial increase of development speed in the last months, so I&#8217;m very optimistic that OpenMW will eventually be released (but of course, never finished).
        </p>
      </blockquote>
      
      <blockquote>
        <p>
          <strong>Ace</strong>
        </p>
        
        <p>
          The fact that it&#8217;s not in a fully playable state yet.
        </p>
      </blockquote>
    </div>
    
    <p>
      <strong>How do you see a future of the project?</strong>
    </p>
    
    <div>
      <blockquote>
        <p>
          <strong>Zini</strong>
        </p>
        
        <p>
          Finishing OpenMW 1.0 obviously, which will be a functionally complete replacement for Morrowind. After that we need to focus on a new editor, because having a construction set replacement available at source level is a requirement for almost any substantial improvement we are going to do after 1.0.
        </p>
        
        <p>
          Then I would like to gradually improve OpenMW beyond what Morrowind can do. Extending existing features, moving away from hardcoded functionality, removing limits. That kind of stuff.
        </p>
      </blockquote>
      
      <blockquote>
        <p>
          <strong>scrawl</strong>
        </p>
        
        <p>
          The future.. I guess that&#8217;s the part we&#8217;re all most excited about and keeps us motivated to work on OpenMW. After OpenMW 1.0 is released, I want to bring the technical graphics in line with newer games such as Skyrim. Some people might think that improving graphics is not possible without providing better assets, but there&#8217;s a lot of stuff to go with. Some inspirations can be found in the <a href="http://sourceforge.net/apps/mediawiki/morrgraphext/index.php?title=Main_Page">MGE</a> (Morrowind Graphics Extender) project.
        </p>
        
        <p>
          But, from a developers point of view MGE totally SUCKS. Being able to design the whole rendering backend from the ground-up enables us a lot more possibilities. Only to mention a few things, that are all easily possible with OGRE and that I&#8217;m most excited about:
        </p>
        
        <ul>
          <li>
            Seamless and infinite world <a href="https://en.wikipedia.org/wiki/Level_of_detail">LOD</a>
          </li>
          <li>
            Realtime soft shadows, deferred lighting with point light shadows, <a href="https://en.wikipedia.org/wiki/Ambient_occlusion">Ambient occlusion</a>
          </li>
          <li>
            Volumetric sunlight scattering (<a href="https://en.wikipedia.org/wiki/Crepuscular_rays">Godrays</a>)
          </li>
          <li>
            True High Dynamic Range (<a href="https://en.wikipedia.org/wiki/High_dynamic_range_imaging">HDR</a>)
          </li>
          <li>
            High quality water rendering with real, moving geometry
          </li>
        </ul>
        
        <p>
          Once these basic things are done, it really depends on the modders if the graphics will be improved further. Supporting newer mapping techniques (e.g. normal/parallax/specular maps) is no big deal at all, but there have to be some modders who actually produce these assets.<br /> Being able to bring OpenMW to state-of-the-art graphics will not only increase immersion with the game world, I also think that it might lower the &#8220;entry barrier&#8221; to play morrowind and attract more players who didn&#8217;t discover morrowind for themselves yet (&#8220;Ugh, that looks like 2002&#8230; I&#8217;m not playing that&#8221;)
        </p>
      </blockquote>
      
      <blockquote>
        <p>
          <strong>Ace</strong>
        </p>
        
        <p>
          Hopefully breathing more life into the Morrowind modding scene, I&#8217;ve had a lot of fun in there. Even though none of my mods have ever been released to the public.
        </p>
      </blockquote>
    </div>
    
    <p>
      <strong>If you could change anything about Morrowind, what would it be?</strong>
    </p>
    
    <div>
      <blockquote>
        <p>
          <strong>Zini</strong>
        </p>
        
        <p>
          If you mean Morrowind as a game: Everything or nothing. I often hear people saying that Morrowind is a great game. I don&#8217;t agree with that. I think Morrowind is coming damn close to being a great game in every aspect, but it just doesn&#8217;t manage to make the final couple of steps, in literally every aspect (okay, almost; the UI sucks and the lore rocks).
        </p>
        
        <p>
          If you mean Morrowind as a modding/development platform: The whole implementation and the editor (which is actually what we are doing with OpenMW).
        </p>
      </blockquote>
      
      <blockquote>
        <p>
          <strong>scrawl</strong>
        </p>
        
        <p>
          Making it open-source. There are so many things wrong about morrowind that it can&#8217;t simply be fixed by changing &#8220;one&#8221; thing. If Bethesda released the sources that would simplify our work a lot (also considering all the game mechanics formulae that we have to <a href="http://openmw.org/forum/viewtopic.php?f=2&t=766&start=20">reverse-engineer</a>)
        </p>
      </blockquote>
      
      <blockquote>
        <p>
          <strong>Ace</strong>
        </p>
        
        <p>
          I would change their rendering engine so the game doesn&#8217;t run slower than Skyrim on my high-end desktop.
        </p>
      </blockquote>
    </div>
    
    <p>
      <strong>One more thing to say?</strong>
    </p>
    
    <blockquote>
      <p>
        <strong>Ace</strong><br /> Avoid angering the gazebo gods.
      </p>
    </blockquote>
    
    <p>
      <strong>Thank you for your time.</strong>
    </p>
  </div>
</div>