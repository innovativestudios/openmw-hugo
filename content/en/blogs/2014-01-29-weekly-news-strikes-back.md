{
   "author": "Lukasz Gromanowski",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-01-29T03:27:19+00:00",
   "title": "Weekly news strikes back!",
   "type": "post",
   "url": "/2014/weekly-news-strikes-back/"
}
Open a terminal, and go to the OpenMW directory. Run git pull, and now git log – we&#8217;ll see what our hard-working developers coded. Oh, no! There&#8217;s no commits this week! How is this possible? Did some cataclysm occur? Did work on OpenMW stop?

Of course not, we just haven&#8217;t managed to complete work on any new features this week, but that doesn&#8217;t mean we&#8217;re lazy.

First, and I think most importantly: for our Slavic brothers (and sisters!) who are using the Cyrillic alphabet – Greye was working on improved support for characters from outside the UTF-8 character set (mostly the ones which aren&#8217;t boring variations on the Latin alphabet). This should improve and facilitate the fun of OpenMW not only for Russians, Ukrainians, and Belorussians, but also Asian nations – perhaps, at the end, we can manage to break the Japanese away from playing Final Fantasy?

Pvdk introduced functionality for displaying the OpenMW version in the launcher, but wait – it&#8217;s not only a version number, but also a git hash. It&#8217;s hard to get more accurate than that. Even if someone complains about it &#8211; please bear in mind that for Linux users it&#8217;s quite simple to get and compile OpenMW sources, but for Windows users it&#8217;s a bit different. They&#8217;re mostly using pre-compiled binaries (Ace&#8217;s nightly builds) and they&#8217;ll probably warmly welcome such a version indicator.

How about Scrawl? Scrawl is commonly known for being the main point of each weekly update. This time he&#8217;s focused on the saving feature. As SirHerrbatka mentioned in the last weekly news, there&#8217;s still a lot of work to do on that feature, and it&#8217;s good that Scrawl is working on this. It&#8217;s hoped that OpenMW version 0.29.0 will introduce relatively complete functionality for loading and saving the state of the game.

OpenCS is also growing &#8211; it&#8217;ll be possible to clone records soon. Also, Graffy introduced significant changes to the file selection window of the game.