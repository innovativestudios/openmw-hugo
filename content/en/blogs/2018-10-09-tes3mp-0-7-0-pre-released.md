{
   "author": "DestinedToDie",
   "categories": [
      "Uncategorized"
   ],
   "date": "2018-10-09T01:33:53+00:00",
   "title": "TES3MP 0.7.0 pre-released",
   "type": "post",
   "url": "/2018/tes3mp-0-7-0-pre-released/"
}
Version 0.7.0 of TES3MP, a fork of OpenMW that provides players with multiplayer experience, has been pre-released! [Full details and download here.][1] But in short, what&#8217;s so good about this? Well, for starters, here are a few new features to pique your interest:

&#8211; Custom spells, potions and enchantments can now be created through regular gameplay, and their details are sent to other players on a need-to-know basis, preventing unnecessary packet spam.  
&#8211; Spells cast through items are now synchronized.  
&#8211; On strike enchantments are now partially synchronized.  
&#8211; Weather is now fully synchronized, with sudden transitions when teleporting to other areas and gradual transitions when walking across regions.

If you want to experience Morrowind with friends and strangers, come join us!

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=5467" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://steamcommunity.com/groups/mwmulti#announcements/detail/1708445194436273527