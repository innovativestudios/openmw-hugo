{
  "author": "lysol",
  "categories": [
    "Uncategorized"
  ],
  "comment": true,
  "date": "2020-11-16T19:55:20+00:00",
  "showReadingTime": true,
  "source": {
    "forumID": 38,
    "topicID": 7207
  },
  "summary": "Unless you live close to the equator or are located in the southern hemisphere, you might have had to rake the leaves from your lawn once or twice now. Our developers don&#8217;t seem to have lawns however, because they&#8217;ve been adding code like they have nothing else to do. Let&#8217;s dig into what has happened in the last two months:",
  "title": "The Autumn in Review: September-October 2020",
  "url": "/2020/the-autumn-in-review-september-october-2020/"
}

Petr Mikheev, aka ptmikheev, is a fairly new developer in the community. He has made himself very well known, though, with all his hard work. You might remember him being mentioned in the previous post regarding his implementation of a new, more modern 3rd-person camera view. More recently, he has added optional first-person head-bobbing to the game as well as a [smooth movement][1] feature which makes movement animations, well, smoother. But let&#8217;s not forget one very important thing: Lua scripting. Petr has been talking with David C, the main developer of tes3mp, about how Lua should be implemented into OpenMW. The resulting ideas were presented in a thread [here][2]. Check it out if you are interested in these things!

Frederic Chardon, aka fr3dz10, is another fairly new developer. Actually, he did submit a few merge requests about a year ago, but now he&#8217;s dropped the mic by submitting his ["Async physics"][3] merge request. Physics and collision detection has been a big performance bottleneck in OpenMW for a long time. While the issue is not completely solved yet, this MR still improves performance by moving the game&#8217;s physics from the main thread to one or more other threads, making OpenMW even better at using multiple cores. The setting is optional and you can configure the number of additional threads in the OpenMW launcher. fr3dz10 has worked on several other issues in the last weeks, but this is the most noticeable one for end users.

Our hammer-wielding penguin akortunov (Andrei Kortunov) has continued his quest of making OpenMW support groundcover with better performance. The big question is whether groundcover should be supported using [paging][4] or [instancing][5]. But what does paging and instancing even mean? Andrei explained it like this:



>With paging, you merge different grass instances on the fly to large shapes and cache them. So your CPU needs some time to merge all instances and some additional RAM to store data. With instancing, you tell GPU to render the same mesh in the same place many times, and then move every instance via shader.

* We are currently measuring the differences between the two methods. Stay tuned for future updates on this topic.
* The great Capostrophic (Alexei Dobrohotov), who had been developing in the top gear for quite a while now, switched down to a lower gear for a while due to important matters in real life. He has, however, returned lately with some really cool stuff regarding his work to make OpenMW support assets of later Bethesda games. For example, the compressed archive format Bethesda started using in Skyrim Special Edition (SSE), called LZ4, is now supported in OpenMW. This is not only a prerequisite for running SSE using our engine in the future &#8212; the format itself is more modern and decompresses much faster than the older BSA format, which could turn out to be very interesting for Morrowind modders and asset creators in general, too. Capostrophic has recently pushed many more changes to improve OpenMW&#8217;s handling of the NIF file format, but these are rather abstract in nature and will be discussed in detail when there are new features available that make use of them.
* Assumeru or "Evil Eye" has implemented a lot of under-the-hood changes which most of you won&#8217;t even notice. However, they are still very important! So what has been done, then? Region sounds now use settings imported from Morrowind.ini to determine the interval at which they need to be played. Levelled items in untouched containers are now randomly generated from their respective lists when the game is loaded, mimicking the original engine. Scripts can now manipulate base records in containers, i.e., it is possible to add and remove items from all untouched containers. To support these changes, scripts can now add levelled item lists to actors and containers. Finally, vanilla-style overstocking has been implemented as well.
* Thanks to the joint forces consisting of psi29a (Bret Curtis), AnyOldName3 (Chris Djali), and johnnyhostile (Hristos Triantafillou), we now have automatic builds for Windows, Linux, Mac, and even Android on GitLab for every merge request that is opened. This means you can go to <em>any</em> merge request on GitLab and get a fresh build for your OS of choice to test any not-yet-implemented feature you like or hunt down any freshly hatched bug.
* Unelsson (Uoti Huotari) has been working on the hard task of getting the [dae/Collada][6] 3D model format to work correctly in OpenSceneGraph, the backbone behind OpenMW&#8217;s rendering. Lately, he has been making really good progress, and the basics of animation support are already covered. There is still a lot of work left to be done, but he is slowly getting there!

Okay, this post is getting way longer than planned, so let&#8217;s stop it here. There are still plenty of other things we could have talked about since this autumn has been a real blast for OpenMW&#8217;s development.

See you next time and have a great winter (or, again, summer if you are located in the southern hemisphere :)).

[1]: https://gitlab.com/OpenMW/openmw/-/merge_requests/282
[2]: http://forum.openmw.org/viewtopic.php?f=6&t=7149
[3]: https://gitlab.com/OpenMW/openmw/-/merge_requests/248
[4]: https://github.com/OpenMW/openmw/pull/3010
[5]: https://github.com/OpenMW/openmw/pull/3023
[6]: https://gitlab.com/OpenMW/openmw/-/merge_requests/252
[7]: https://openmw.org/downloads/