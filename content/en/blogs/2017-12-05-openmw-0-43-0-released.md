{
   "author": "raevol",
   "categories": [
      "Release"
   ],
   "date": "2017-12-04T23:51:03+00:00",
   "title": "OpenMW 0.43.0 Released!",
   "type": "post",
   "url": "/2017/openmw-0-43-0-released/"
}
The OpenMW team is proud to announce the release of version 0.43.0! Grab it from our [Downloads Page][1] for all operating systems. This release brings a host of new features and bug fixes, including AI resurfacing to breathe, improved AI combat behavior and keyboard shortcuts for menus.

Check out the [release video][2] and the [OpenMW-CS release video][3] by the masterful Atahualpa, and see below for the full list of changes.





**Known Issues:**

  * Shadows are not re-implemented yet
  * To use the Linux targz package binaries, Qt4 and libpng12 must be installed on your system
  * On macOS, OpenMW-CS is able to launch OpenMW only if OpenMW.app and OpenMW-CS.app are siblings

**New Features:**

  * Implemented AI resurfacing to breathe (#1374)
  * Implemented rain and snow induced water ripples (#452, 4156)
  * Implemented fade to black on cell transitions (#824)
  * Implemented starting in an unnamed exterior cell with &#8211;start (#3941)
  * Implemented AI using &#8220;WhenUsed&#8221; enchantments (#3953)
  * Implemented stacks of ingredients or potions remaining grabbed after using one (#4082)
  * Implemented keyboard navigation for menus (#3400)
  * Implemented an option to show the success rate while enchanting (#3492)
  * Implemented an option to show projectiles&#8217; damage in tooltips (#2923)
  * Implemented an option to show melee weapons&#8217; reach and speed in tooltips (#2923)
  * Implemented a &#8220;launch OpenMW&#8221; tickbox on the Windows installer (#2258)
  * Implemented the ability to close the inventory while dragging items (#1692, #3099)
  * OpenMW-CS: Implemented LTEX record table (#933)
  * OpenMW-CS: Implemented LAND record table (#936)
  * OpenMW-CS: Implemented highlighting occurrences of a variable in a script (#2509)
  * OpenMW-CS: Implemented using one resource manager per document (#2748)
  * OpenMW-CS: Implemented shortcuts and context menu options for commenting code out and uncommenting code respectively (#3274)
  * OpenMW-CS: Implemented an option in user settings to reset settings to their defaults (#3275, #3682)
  * OpenMW-CS: Implemented reloading data files (#3530)
  * ess-Importer: Implemented converting projectiles (#2320)

**Bug Fixes:**

  * Water visibility is now consistent across different setting configurations (#815)
  * Autosaves are now created when waiting, to match vanilla behavior (#1452)
  * Container Close button no longer loses key focus when an item is clicked (#1555)
  * Guards no longer become aggressive towards creatures that are beyond their alarm radius (#2405)
  * Players can no longer change their weapon or spell during attack or spellcasting, though weapons can still be sheathed (#2445)
  * UI window pinning now persists between saves and boots (#2489, #2834)
  * First person view now uses the correct body texture with the &#8220;Better Bodies&#8221; mod (#2594)
  * Stat review menu now reads correctly from the player&#8217;s stats (#2628)
  * Player states are now correctly reset when a save is loaded (#2639)
  * Rain and snow weather effects no longer move with the player (#2698)
  * Implemented some missing creature swim animations (#2704)
  * &#8220;Owned&#8221; crosshair now does not mark objects which can be activated without triggering a crime (#2789)
  * Drag-and-drop is now ended if the item is removed (#3097)
  * GetDetected can now be used without a reference (#3110)
  * Fixed poor performance when resizing the dialog window (#3126)
  * Fixed how ampersands in filenames are handled by the Launcher and Wizard (#3243)
  * Improved water reflection rendering along shorelines (#3365)
  * Improved AI usage of Dispel magic effect (#3341)
  * Disposing of corpses no longer breaks related quests (#3528)
  * Framerate limit is now enforced in videos and menus (#3531)
  * Only one copy of a given sound effect may now be played at a time (#3647)
  * Stationary NPCs resume returning to their position after a game is loaded if they had moved (#3656)
  * Added a fade-out for music transitions (#3665)
  * Spellcasting effects now disappear when resting (#3679)
  * Filled soul gems no longer count against the number of empty soulgems a merchant restocks of the same type (#3684)
  * Fixed magicka calculation during character generation (#3694)
  * Guards will now try to arrest the player when attacked, instead of just fighting back (#3706)
  * Death counts are now checked to be for valid actors before they are loaded (#3720)
  * Malformed inequality operators are now handled as equality to match vanilla and give a warning (#3744)
  * Yagrum Bagarn no longer twitches (#3749)
  * DisableLevitation now removes Levitate visual spell effect indicators (#3766)
  * Script commands now run if they were in voiced dialog scripting (#3787)
  * Added enabled check to animation script commands (#3793)
  * Default sound buffer size was increased (#3794)
  * RemoveItem behavior adjusted (#3796)
  * Godmode adjusted to match vanilla (#3798)
  * Animations now loop correctly in the &#8220;Animated Morrowind&#8221; mod (#3804)
  * Fixed enchantment point calculation to match vanilla (#3805)
  * Missing book art now logs a warning instead of showing a pink rectangle (#3826)
  * Fixed &#8220;Windows Glow&#8221; mod so window textures are not darker than they should be (#3833)
  * Bodyparts with multiple NiTriShapes are now handled correctly (#3835)
  * InventoryStore::purgeEffect() now removes all effects with argument ID (#3839)
  * Fixed fatigue loss calculations when jumping (#3843)
  * Underwater effects are now not applied to scripted &#8220;say&#8221; commands (#3850)
  * Actors no longer try to speak underwater (#3851)
  * Fixed a crash caused by an NPC being spawned by two routines at once (#3864)
  * Fixed an exploit where constant effect enchantments could be made with any soul gem by switching the soul gem during enchanting (#3878)
  * Fixed jail state not being cleared when a game is loaded (#3879)
  * Journal no longer displays empty entries (#3891)
  * An empty line is no longer displayed between a topic and the actual dialog text in the dialog window (#3892)
  * PositionCell in dialog no longer closes the dialog window (#3898)
  * &#8220;Could not find Data Files location&#8221; dialog no longer appears multiple times (#3906)
  * Fixed a case where the user could get stuck when installing without a CD (#3908)
  * Adjustments made to the Morrowind Content Language dropdown in the settings pane of the launcher (#3909)
  * Scroll in launcher window can no longer be cut off by resizing the window (#3910)
  * NC text key on nifs now works correctly (#3915)
  * Menu click sound no longer plays for right clicks (#3919)
  * Improved AI targetting algorithm to choose targets based on attack effectiveness (#3921)
  * AI now avoids enemy hits while casting self-ranged spells (#3922)
  * Copy/Paste now uses the correct command key on macOS (#3934)
  * AI attack strength is now reset when starting a new attack (#3935)
  * Changed spell priority calculation to match vanilla (#3937)
  * UI sounds are no longer distorted under water (#3942)
  * Fixed the game not pausing when minimized on Windows (#3943)
  * Vendors now confiscate stolen items that the player tries to sell them back (#3944)
  * Fixed some character preview rendering issues (#3955)
  * EditEffectDialog close button now updates range button and area slider properly (#3956)
  * Fixed some issues with body part rendering with mods (#3957)
  * Clothing value is now unsigned, preventing some value overflow issues (#3960)
  * Fatigue is now consumed if your encumbrance equals your maximum carry weight exactly (#3963)
  * Journal sounds are now played when flipping pages and closing the journal (#3974)
  * Fixed containers instantly closing when opened with a controller and the cursor over the close button (#3978)
  * Fixed performance issue when casting spells, especially on new landmasses such as Tamriel Rebuilt (#3981)
  * Corrected down and up sounds played when trading (#3982)
  * NPCs taunted to aggression no longer continue dialog with the player (#3987)
  * Journal can no longer be opened at the main menu (#3991)
  * Fixed Dispel to only cancel spell effects (#3995)
  * Fixed missing header inclusion breaking OpenBSD clang build (#4002)
  * Inventory avatar now fits within display border (#4003)
  * Manis Virmaulese now speaks to the player before attacking (#4004)
  * AI spawn position is now reset if they are moved to another cell (#4010)
  * Keypresses consumed by the UI are no longer processed again by the game (#4016)
  * GetPCRunning and GetPCSneaking now check if the PC is actually moving (#4017)
  * Music track shuffling is improved (#4024)
  * Spells with copy/pasted names no longer sort to the top of the spell list (#4025)
  * NPC archers now switch to another weapon to attack when they run out of arrows (#4033)
  * Reloading a save game while falling no longer prevents fall damage (#4049)
  * Draw animation is no longer played when a player equips a new weapon while already holding one (#4056)
  * Disposition is now updated correctly when dialog is closed (#4076)
  * Alchemy skill increases now take effect immediately after potion creation, instead of after the next batch is started (#4079)
  * GetResist commands now work like in vanilla (#4093)
  * Level-up messages for levels past 20 are now used (#4094)
  * Fixed error in framelistener when taking all items from a corpse (#4095)
  * Default 0 float precision is now used in float formatting (#4096)
  * Cycling through weapons now only selects weapons that can be equipped (#4104)
  * A scroll bar has been added to the birthsign effects list at character creation (#4105)
  * Changed inventory sort order to be more consistent (#4112)
  * Fixed unexpected &#8220;Resolution not supported in fullscreen&#8221; messages (#4113)
  * Fixed pickpocketing behavior to match vanilla (#4131)
  * Fixed a case where NPCs would not equip a second ring (#4155)
  * NPCs now do not autoequip clothing of the same price (#4165)
  * Multiple GUI and AI fixes and improvements
  * OpenMW-CS: Fixed issues with using # characters in loaded configuration files (#3045)
  * OpenMW-CS: Fixed camera position jumping when switching camera modes (#3709)
  * OpenMW-CS: &#8220;Map Color&#8221; field in Cells table now works correctly (#3971)
  * OpenMW-CS: Name is now reported correctly as OpenMW-CS on macOS (#4027)
  * OpenMW-CS: New greetings now work in-game (#4037)
  * OpenMW-CS: Merging of Land and Land Texture records fixed (#4074)
  * OpenMW-CS: Fixed narrow left pane in Preferences window (#4107)

##### <a target="_blank" href="https://forum.openmw.org/viewtopic.php?f=38&t=4801">Want to leave a comment?</a>

 [1]: https://openmw.org/downloads/
 [2]: https://www.youtube.com/watch?v=F0MPLXdl0y4
 [3]: https://www.youtube.com/watch?v=88ywx8BSyKg