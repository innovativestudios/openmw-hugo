{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2011-09-11T16:14:20+00:00",
   "showReadingTime": true,
   "summary": "OpenMW 0.11.0 (actually it&#8217;s called 0.11.1) is coming! This time ALMOST for sure. It certainly took a while to finish but don&#8217;t get the wrong idea? It didn&#8217;t stopped work on openmw 0.12.0.",
   "tags": [
      "0.11.0",
      "animation",
      "jhooks",
      "launcher"
   ],
   "title": "OpenMW 0.11.1",
   "url": "2011/openmw-0-11-1"
}
First thing that you may noticed is brand new OpenMW launcher. It&#8217;s rather useless at the moment as OpenMW can&#8217;t even load multiple esm&#8217;s.

Another features: the message box GUI and basic tab completion in the console. OpenMW 0.11.1 should also run faster with added optimization.

Work on crucial features takes a long time to finish. Some things just can&#8217;t be finished faster, especially when developer have no official documentation and often just crawls in the dark. Just look at the story behind animations? jhooks still has hard time to figure out how all the stuff works.

But yes, openmw 0.11.1 is close.

And openmw 0.12.0 will be a giant milestone, not terrible faraway. Actually large portion of work was already done, what you should know If you read news regularly.