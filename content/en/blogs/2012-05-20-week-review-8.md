{
   "author": "jvoisin",
   "categories": [
      "Week's review"
   ],
   "date": "2012-05-20T17:31:38+00:00",
   "tags": [
      "animation",
      "Chris",
      "Gus",
      "jhooks",
      "scrawl",
      "Zini"
   ],
   "title": "The week in review",
   "type": "post",
   "url": "/2012/week-review-8/"
}
This week&#8217;s theme is either _&#8220;The Inventory: barter window, inventory, item stacking and tooltips&#8221;_ or maybe &#8220;Thank You **scrawl** and **gus**&#8220;. Although inventory and barter windows are not 100% ready they are working and will soon be perfected. Zini has done the backend, and scrawl rely on it to implement visible functionalities: collaboration and teamwork are what makes openmw move forward!

**gus** fixed a bug in collision detection (since his task was stolen by **scrawl** ;) ) which caused invisible barriers under Vivec&#8217;s lanterns that blocked player movement.

**zini**

  * Various fixes
  * More background technical stuff, this time related to magical effects!
  * Ho, we&#8217;ve never mentioned this in a post, but **zini** is our [BDFL][1]. He manages branches and merging them into the master branch, he accepts or refuses patches, and prioritizes feature development, and offers technical advice to developers&#8230; **zini** is **The Guardian of OpenMw**. Thanks to him, the code is (mostly) clean and efficient, and the project is moving forward! If you like the project, then you may want to give a quick [thank you][2] to **zini** for making it possible.

**scrawl**:

  * Thanks to **gus&#8217;s** preliminary work, you can now drop item on the ground! WOOOOOOO!
  * [Drag&#8217;n&#8217;drop][3] from/to the inventory.
  * The inventory [works][4] (also thanks to **gus** too) : Items stacking, tooltips, filtering, &#8230;
  * Item count selection dialog
  * Many gold related features (eg. You can&#8217;t sell gold anymore !)
  * Implementation of [trading][5] with NPC ( in 3 fuckin&#8217; days !).
  * You can now talk with the [mudcrab merchant][6] !
  * You can&#8217;t put your items in &#8220;organic&#8221; stuff anymore.
  * Max capacity for containers
  * You can now see faction and birth-sign in the stat window

**jhooks** and **chris** are still working on animations. They are currently [experimenting][7] with various strategies to get robes and skirts to render correctly through Ogre3d.

 [1]: https://en.wikipedia.org/wiki/Benevolent_Dictator_For_Life
 [2]: http://openmw.org/forum/viewtopic.php?f=2&t=531
 [3]: http://openmw.org/wiki/index.php?title=File:015-5.png
 [4]: http://openmw.org/wiki/index.php?title=File:015-4.png
 [5]: http://openmw.org/wiki/index.php?title=File:015-6.png
 [6]: http://www.uesp.net/wiki/Morrowind:Mudcrab_Merchant
 [7]: http://openmw.org/forum/viewtopic.php?f=6&t=655&start=60#p6092