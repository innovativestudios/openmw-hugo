{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-01-31T14:37:23+00:00",
   "title": "week in review",
   "type": "post",
   "url": "/2013/week-review-36/"
}
Hi again.

This week is not full of ground breaking news to be honest, in majority it&#8217;s just progress in the same areas as a week ago. Blunted2night is still on the journal task, but he also started cleaning up the worldclass.

Animations are currently under heavy development by Chris. He is doing mostly well, even if remaining tasks are still numerous.

wheybags just finished with the work on the script for containers, and everything is merged into main branch now.

pvdk is rewriting the way the launcher handles the settings files.

Developers also fixed quite large number of bugs but that&#8217;s all for now.