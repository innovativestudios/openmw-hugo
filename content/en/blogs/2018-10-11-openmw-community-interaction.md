{
   "author": "psi29a",
   "categories": [
      "Uncategorized"
   ],
   "date": "2018-10-11T16:35:56+00:00",
   "title": "OpenMW’s take on community interaction",
   "type": "post",
   "url": "/2018/openmw-community-interaction/"
}
We&#8217;d hope that this would go without saying, but sometimes it needs to be _explicitly_ said&#8230; **do unto others as you would have them do unto you.** In other words, help us help you. We have a commitment to ourselves to try to help, where we can, to be kind and patient. We ask that also in return. This is our hobby too, our labor of love, be it OpenMW, TES3MP, MWSE&#8230; we all want to get along even if we disagree with each other. If we feel that our good will is being abused, then we&#8217;ll stop providing help, warn and eventually ban individuals who can&#8217;t follow the Golden Rule. This also includes those who troll other projects, for whatever reason.

We hereby invoke [Wheaton&#8217;s Law][1].

Cheers,  
The OpenMW Team

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=5467" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: http://www.wheatonslaw.com/