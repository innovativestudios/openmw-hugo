{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2012-10-29T11:13:02+00:00",
   "tags": [
      "alchemy",
      "animations",
      "Chris",
      "death",
      "editor",
      "Eli2",
      "fast travel",
      "Greye",
      "Gus",
      "launcher",
      "pvdk",
      "Zini"
   ],
   "title": "the week in review",
   "type": "post",
   "url": "/2012/week-review-26/"
}
Ruminations of alchemy, animations, and death

The Alchemy implementation is complete. Bug testing has revealed some issues that will surely get resolved soon.

Zini is also working on the &#8220;Death&#8221; branch. There was a bug where everything/everyone was insta-killed. That is no longer a problem, but there are more to be fixed.

The newly finished quick travel services in the &#8220;Travel Dialogue&#8221; branch is being refactored by Gus.

Pvdk continues working on our launcher with the support for profiles now back!

Greye is working in the &#8220;Storedevel&#8221; branch on lower level structures of the engine, rather than on user visible features ─ but that doesn&#8217;t make it unimportant. In fact quite opposite: without it we would be screwed in many ways.

Chris is digging around in the animations. The number of commits increases, the number of issues decreases but the goal of fully implemented animations is still far away…

The OpenMW engine is so big and interrelated that every feature requires quite a lot of work…

Eli2 as usual continues to work on the editor. Stopgap json serializer has been replaced with the qt variant. There was also Qt5 experiment ─ not very important from practical point of view but some of you may be interested that &#8220;It works&#8221;.