{
   "author": "Nekochan",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-06-03T20:42:38+00:00",
   "title": "Sir_Herrbatka wants to be the Flower Girl…",
   "type": "post",
   "url": "/2014/sir_herrbatka-girl-flower/"
}
If you haven&#8217;t noticed yet, we have just released 0.30.0. Great news, right? Everyone who can read English has been lucky enough to be able to enjoy the changelog, but our Polish-only readers aren&#8217;t that lucky as Sir_Herrbatka has simply been short on time. So instead of the full changelog in Polish, he did the next best thing and gave his readers a quick overview of [Zini&#8217;s forum post on the progress of the project][1].

> As seen here we are pretty close to becoming feature complete. All is going well and I don&#8217;t really have much to say about OpenMW.
> 
> OpenCS is another case. We still lacking manpower. It seems right now sirherrbatka and I are doing the bulk of the work, which is just not enough. I certainly don&#8217;t want to distract the OpenMW development by pushing people towards OpenCS. But if there are any inactive or retired OpenMW developers, I would be very happy to see some un-retire themself and help us out a bit (new people are also welcome, of course).

So that&#8217;s where stand now. Honestly, I&#8217;ve gotten the feeling that our newsposts have gotten a bit stale. Until version 1.0.0 rolls around, I won&#8217;t have much to tell you in terms of exciting changes in the project, let alone the nitty-gritty details of their implementation. On the other hand, there _is_ still lots of work to do to bring whole thing to a fully playable state.

That means that there&#8217;s no reason for us to stop working! Scrawl is super(radio)active; like a superhero straight from the silver screen, he is squishing bugs one after another. Didimaster also fixed lots of bugs and Puppykevin is working on adding facial animations. If you have tried our alpha releases, you know exactly what we mean by that.

Zini implemented a very important function in the editor: OpenCS now supports record references, which is a fundamental and necessary function for the further development of OpenCS.

The biggest news I want to share with you doesn’t concern OpenMW, but **Lgro**, developer during the day, master of martial arts at night and our reliable admin all the time: He is getting married! So I wish him happiness and prosperity in his new way of life. And just a note to all the female readers who are now undoubtedly wailing loudly and gnashing their teeth after seeing Lgro slip away: I heard Scrawl is still available!

Anyway, feel free to add your own wishes for Mr. and Mrs. Gromanowski.

 [1]: https://forum.openmw.org/viewtopic.php?p=24802#p24802