{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-12-31T08:10:16+00:00",
   "title": "Year in review",
   "type": "post",
   "url": "/2013/year-review/"
}
OpenMW is now almost playable thanks to our efforts in the last 12 months. We are almost there&#8230; Still missing few skills but we have managed to implement artificial inteligence. We have also introduced support for multiple data files &#8211; many users waited impatiently for this&#8230; We have also changed few things. We moved away from pmove to our own custom written controller. Thanks to this walking animation works and controlls the movement. Modern effects were added. Normal mapping is a major improvement and brand new distant terrain look great ─ even without any objects. New (drastically improved) implementation of input was introduced. We have implemented new game button. We have completely re-evaluated the usage of OpenCS editor. The old version was scrapped and new one was introduced. We have solved hundreds upon hundreds of bugs and small annoyances&#8230;

Damn&#8230; That was a lot of work&#8230; but thanks to our fantastic team it was progressing fast and smooth. At some stage the work was going so well we honestly believed that we will be able to release version 1.0 before the end of this year! That wont happen unfortunatelly but we are close&#8230; Very close&#8230; Year 2014 brings new challanges for the OpenMW team ─ ie. improving performance, optimizing code, adding new functions to the OpenCS editor, implementing missing OpenMW features and squashing as many bugs as possible&#8230; I am sure we can handle those tasks and then&#8230; it will be OpenMW 1.0 release time&#8230; I am waiting impatiently! Are You?