{
  "author": "lysol",
  "categories": [
    "Uncategorized"
  ],
  "comment": true,
  "date": "2020-06-26T14:21:51+00:00",
  "showReadingTime": true,
  "source": {
    "forumID": 3,
    "topicID": 7014
  },
  "summary": "Hello again, We hope you are all having a pleasant time with the new release! While you're busy killing vicious cliff racers, our team is busy with further improving the engine and the editor.",
  "title": "OpenMW Spotlight: Turning the Pages",
  "url": "/2020/openmw-spotlight-turning-the-pages/"
}

There is one particular feature that recently got merged that we want to bring to your attention: object paging. What&#8217;s that, you ask? Well, object paging is a system developed by bzzt, the man behind the improved distant-land feature introduced in the latest release. This system takes several objects that are close to each other and merges them into a single object. This is done in real time during gameplay and shouldn&#8217;t be noticeable to the end user. But we can hear you thinking: why should I care about this? &#8211; Because it reduces draw calls! &#8211; Okay, and &#8230;? &#8211; Because draw calls are the main bottleneck if you want to display statics (buildings, rocks, trees, etc.) over a larger distance. &#8211; Wait! Are you saying &#8230; distant land with distant statics? &#8211; [Yes, we are.](https://imgur.com/a/XT4s0DM)

One feature that was developed in the same merge request as object paging is the &#8220;active-grid object paging&#8221;. It is basically the same feature, but applied to the player&#8217;s active cell(s). This means that the system will not only merge objects in distant cells, but also those in the cells your character is located in. This increases performance, especially in draw-heavy areas like cities and towns. Yay!

> #### [2020-05-01 20-53-49](https://streamable.com/t4kots)
>
> Watch 2020-05-01 20-53-49 on Streamable.


There is an issue with this feature as of now however, which is why we decided to set the &#8220;active-grid object paging&#8221; setting to &#8220;false&#8221; by default. The issue is that OpenMW still has a hard limit of 8 lights per object, a limitation inherent in the old fixed-function pipeline of Morrowind that OpenMW faithfully supports for legacy reasons. The limitation is sometimes an issue even without object paging, but you don&#8217;t generally notice it with vanilla Morrowind&#8217;s assets. If you start to merge several objects into a single one however, the limitation really starts to show: Lights will pop up and disappear frequently which simply looks bad. Once the light limit is removed (for which there exists a sizeable work in progress), active-grid object paging will become a lot more interesting.


> #### [2020-05-09 13-37-58](https://streamable.com/3lfx2v)
>
> Watch 2020-05-09 13-37-58 on Streamable.


The feature can now be tested in all freshly baked nightly builds in your neighbourhood. There are still things that need to be addressed in future merge requests. For example, we still don&#8217;t have a system to generate lower-detail meshes from high-detail ones to be used in the distance. This isn&#8217;t really an issue if you&#8217;re mostly using vanilla assets as they are very low in detail for modern standards anyway. For mods that use high-detail assets, however, this might be an issue. Work will continue on this. In any case, please help us out by installing the latest nightly build for your operating system and putting object paging to the acid test!