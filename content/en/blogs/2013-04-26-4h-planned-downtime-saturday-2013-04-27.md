{
   "author": "Lukasz Gromanowski",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-04-26T03:27:01+00:00",
   "title": "4h of planned downtime on Saturday (2013.04.27)",
   "type": "post",
   "url": "/2013/4h-planned-downtime-saturday-2013-04-27/"
}
Hello,  
I would like to inform that due to main shell server upgrade in MyDevil.net there will be about 4h downtime on Sunday (2013.04.27) between 00:30 and 04:30 CET. Sorry for inconvenience.