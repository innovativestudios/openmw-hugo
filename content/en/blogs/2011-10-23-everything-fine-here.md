{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2011-10-23T09:00:27+00:00",
   "showReadingTime": true,
   "summary": "Hello! jHooks finished his vertex morphing and wants to merge his animations into master branch, but then there is a problem: mwrender refactoring is not finished.",
   "tags": [
      "animation",
      "Gus",
      "jhooks",
      "journal",
      "mwrender",
      "refactoring",
      "vertex morphing"
   ],
   "title": "Everything fine here",
   "url": "2011/everything-fine-here"
}
jHooks finished his vertex morphing and wants to merge his animations into master branch, but then there is a problem: mwrender refactoring is not finished. Even more, it&#8217;s not moving forward. But then the task was reassigned to jhooks himself and mwrender refactoring is progressing again.

Jhooks is my hero. Seriously.

And GUS is working on journal, even if He did not commit any code yet that&#8217;s just because it&#8217;s still messy. Do you try to taste unfinished cake?

Yes, I know that you do. Don&#8217;t even try to lie.