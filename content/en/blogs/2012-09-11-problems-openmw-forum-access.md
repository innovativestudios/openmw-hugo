{
   "author": "Lukasz Gromanowski",
   "categories": [
      "Maintenance"
   ],
   "date": "2012-09-11T03:49:19+00:00",
   "tags": [
      "forum",
      "lgromanowski",
      "maintenance",
      "server"
   ],
   "title": "Possible problems with OpenMW forum access.",
   "type": "post",
   "url": "/2012/problems-openmw-forum-access/"
}
Hello,  
I&#8217;ve just enabled some small server-side anti-spambot script today, so if you encounter any problems with OpenMW forum access (HTTP 403 error), then please let me know (via IRC, GitHub, e-mail). Sorry for inconvenience.