{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2012-07-10T17:02:53+00:00",
   "tags": [
      "alchemy",
      "editor",
      "Eli2",
      "formulas",
      "launcher",
      "Myckel",
      "pvdk",
      "refactoring",
      "scrawl",
      "serpentine",
      "texture",
      "WeirdSexy",
      "Zini"
   ],
   "title": "30 june 2012 review",
   "type": "post",
   "url": "/2012/30-june-2012-review/"
}
Hi, all

After an extensive phase of testing and five release candidates later, 0.16.0 is out. **WeirdSexy** made a video can be viewed in the previous post. It&#8217;s interesting that many users discover our project via YouTube, probably more than through Google. I shouldn&#8217;t be surprised since this is the Web 2.0 era after all.

Other people are pushing OpenMW forward in other ways. In terms of features, I need to mention **Myckel**. The situation makes me a little ashamed because **Myckel** is not a programmer, but he is reverse engineering the formulas related to alchemy. It&#8217;s an important project, so important that zini says its the only thing holding back progress with this implementing alchemy. But it&#8217;s not just alchemy, there are many formulas that are still not worked out. There is nothing stopping ME from doing this task besides laziness. The situation is even worse &#8212; because most of the formulas on our wiki were put there by a single person. Something is not right.

**Zini** can&#8217;t progress with the alchemy task yet, so he started another round of refactoring. This time it&#8217;s the MW-subsystem. All information important for developers can be found in this topic: [viewtopic.php?f=6&t=863][1]

There is bad news because animating with OGRE3D is stuck in place again. Hopefully not for long this time.

**pvdk** is on-line and so the launcher is seeing modifications again. Configuration options are available as well a better graphics menu for it thanks to serpentine. Maybe less important but still nice.

**scrawl** is focused on few bug fixes right now. He already fixed infamous black textures bug on DirectX and now he is creating new changes in the graphics.

**Eli2** who previously worked (toying?) on head tracking experiment now is working (toying?) on the editor. I would L O V E to see awesome stuff right there so go for it **Eli2**!

 [1]: http://openmw.org/forum/viewtopic.php?f=6&t=863 "http://openmw.org/forum/viewtopic.php?f=6&t=863"