{
   "author": "Nekochan",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-06-12T21:46:19+00:00",
   "title": "Junk data…",
   "type": "post",
   "url": "/2014/junk-data/"
}
Welcome back.

The project advances like Armstrong in his best years on doping. What, you think cycling is boring? Yeah, it bores me too, but it does make the analogy pretty applicable.

You see, at this point development of OpenMW is repetitive, like the seasons in a year, so each week I need to write about the same things. Scrawl continues his fight against bugs, Greye refactors AI code, Pvdk works on the launcher (as well as the Morrowind installer), Zini is making changes to OpenCS – and it&#8217;s nigh impossible to tell you about the nature of these changes if you don&#8217;t already know how these applications are structured.

But lucky me, I still have some interesting stuff to share.

First, if you wanted to be the first person to finish Morrowind on our engine&#8230; you&#8217;re too late! Rebel-rider beat you to it&#8230; and found some [really weird bugs][1] along the way.

Second, Scrawl mentioned that he is struggling with some mods that are not quite well-made. Initially we just nodded our heads as if we understood what he was talking about, but we didn&#8217;t expect to see things like [this][2].

Yes, apparently some mods have &#8220;junk data&#8221;. Poor Scrawl. :(

 [1]: https://bugs.openmw.org/issues/1457
 [2]: https://openmw.org/wp-content/uploads/2014/06/screenshot_19-00-00.png