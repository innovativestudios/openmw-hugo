{
   "author": "raevol",
   "categories": [
      "Release"
   ],
   "date": "2013-03-01T00:17:38+00:00",
   "title": "OpenMW 0.21.0 Released!",
   "type": "post",
   "url": "/2013/openmw-0-21-0-released/"
}
The OpenMW team is proud to announce the release of version 0.21.0! Release packages for Ubuntu are now available via our [Launchpad PPA][1]. Release packages for other platforms are available on our [Download page][2]. This release introduces video playback, improvements to cell load time, and parsing for escape sequences in dialogue and message boxes.

Check out WeirdSexy&#8217;s release video:  


**Known Issues:**

  * No sound when playing videos on OS X
  * Extreme shaking may occur during cell transitions for some users (enable anti-aliasing as a possible workaround)
  * Launcher crash on OS X < 10.8

**Changelog:**

  * Various dialogue, trading, and disposition fixes and improvements
  * Torch flickering improved to better match vanilla Morrowind
  * Fix for attribute fluctuation when infected with Ash Woe Blight
  * Adjusted activation range to better match vanilla Morrowind
  * Fixes for the Journal UI
  * Fixed crash caused by Golden Saint models
  * Fix for beast races being able to wear shoes
  * Fix for background music not playing
  * Fix for meshes without certain node names not being loaded
  * Fix for incorrect terrain shape on inital cell load
  * Fix for MWGui::InventoryWindow creating a duplicate player actor at the origin
  * Added video playback
  * Added support for escape sequences in message box and dialogue text
  * Added AI related script functions, note that AI is not functional yet
  * Implemented fallbacks for necessary ini values in the importer, unused in OpenMW as of yet
  * Implemented execution of scripts of objects in containers/inventories in active cells
  * Cell loading performance improvements
  * Removed broken GMST contamination fixing mechanism

 [1]: https://launchpad.net/~openmw/+archive/openmw
 [2]: https://code.google.com/p/openmw/downloads/list