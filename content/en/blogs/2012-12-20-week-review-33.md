{
   "author": "jvoisin",
   "categories": [
      "Week's review"
   ],
   "date": "2012-12-20T16:10:42+00:00",
   "title": "The week in review",
   "type": "post",
   "url": "/2012/week-review-33/"
}
Hello,

Sir_**Herrbatka **is still offline, so I&#8217;m back once again.

The big news is that version 0.20.0 is out now. This is our ninth release during 2012 and that&#8217;s pretty good for a fan project! Release packages for Ubuntu are now available via our [Launchpad PPA][1]. Release packages for other platforms are available on our [Download page][2]. This release brings a near-complete implementation of the dialogue system, visual player race changes in character creation, and many other fixes and improvements.



**Known Issues:**  
&#8211; Extreme shaking may occur during cell transitions for some users (enable anti-aliasing as a possible workaround)  
&#8211; The disposition implementation is severely broken and will give wrong numbers in many cases  
&#8211; Entering a cell containing a Golden Saint model will crash the game  
&#8211; A journal regression causes journal updates to not display correctly  
**  
Changelog:**  
&#8211; Implemented all missing dialogue filters  
&#8211; Implemented Mercantile skill  
&#8211; Implemented Persuasion dialogue  
&#8211; Initial implementation of AI package classes  
&#8211; Implemented 2nd layer for Global map  
&#8211; Implemented Animation pausing while game is paused  
&#8211; Implemented Player race now changes visually during character creation  
&#8211; Fixed Cell reloading when using teleport doors that link to the same cell  
&#8211; Fixed &#8220;0 a.m.&#8221; to display as &#8220;12 a.m.&#8221;  
&#8211; Fixed text persisting in the &#8220;name&#8221; field of the potion/spell creation window  
&#8211; Fixed starting date of new games  
&#8211; Fixed console window close behavior  
&#8211; Fixed container window formatting to better accommodate long item names  
&#8211; Fixed some topics not automatically being added to the known topic list  
&#8211; Fixed auto-move working despite DisablePlayerControls being set  
&#8211; Fixed rest dialogue opening again after resting  
&#8211; Fixed double removal of ingredients when creating potions  
&#8211; Various engine code and scripting improvements

&nbsp;

In version 0.21.0 news **Chris** and **Scrawl** are working to improve the video player&#8217;s reliability, quality and performance. Since their work is pretty complex, I won&#8217;t try explaining it there. Feel free to check their commits in git if you&#8217;re interested in the details.

**Zini** is still working on the editor.

Thanks to **BrotherBrick**, you are now able to compile against boosts libraries statically on Linux !.

**Corristo** is trying to make OpenMW buildable as a universal 32/64 bit on MacOS.

And as usual, some bugs where fixed.

 [1]: https://launchpad.net/~openmw/+archive/openmw
 [2]: https://code.google.com/p/openmw/downloads/list