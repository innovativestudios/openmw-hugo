{
   "author": "DestinedToDie",
   "categories": [
      "Uncategorized"
   ],
   "date": "2017-03-15T14:19:21+00:00",
   "title": "Distant terrain",
   "type": "post",
   "url": "/2017/distant-terrain/"
}
[<img loading="lazy" class="wp-image-4922 aligncenter" src="https://i0.wp.com/openmw.org/wp-content/uploads/2017/03/budmNsK-300x169.jpg?resize=665%2C374&#038;ssl=1" alt="" width="665" height="374" data-recalc-dims="1" />][1]

What&#8217;s this? I can see the Ghostfence all the way from Ald-Ruhn and my fps is above 10. This is OpenMW and not MGE XE, right?

This is OpenMW alright and [what you&#8217;re seeing][2] is made possible by the newest addition from scrawl &#8211; distant terrain. This new feature is available in the latest OpenMW commit and if you&#8217;re on windows, you can download it from [Ace&#8217;s nightly builds][3] to try it out yourself.

So what does it do? In short, it [simplifies the land mesh][4], nothing else. It doesn&#8217;t touch on scripts, NPCs and statics, but having simplified land certainly lightens the workload to a point where we can have some wiggle room with our view distance. Distant statics is another feature that will have to be implemented at a later stage.

You may have noticed that I&#8217;ve taken use of this in some of the screenshots and loaded enough statics to create the illusion that there are objects [as far as the eye can see][5]. You can only spot [empty land][6] if I purposefully look at something very distant.

Finally, here is how you set this up:

`[Camera]<br />
# Determines how much land is rendered. Add another 6 or 66 to it.<br />
viewing distance = 6666.0``<br />
` 

````

`[Terrain]<br />
# The on-switch to distant terrain. Set this to True.<br />
distant terrain = True`

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=4221" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://i0.wp.com/openmw.org/wp-content/uploads/2017/03/budmNsK.jpg?ssl=1
 [2]: http://i.imgur.com/IuRDXv7.jpg
 [3]: https://forum.openmw.org/viewtopic.php?f=20&t=1808#p18839
 [4]: http://i.imgur.com/YklBk3E.jpg
 [5]: http://i.imgur.com/HLZzgDp.jpg
 [6]: http://i.imgur.com/pAg1oKv.jpg