{
   "author": "Okulo",
   "categories": [
      "Uncategorized"
   ],
   "date": "2016-03-31T12:01:42+00:00",
   "title": "The Forgotten Releases: 0.38.0",
   "type": "post",
   "url": "/2016/forgotten-releases-0-38-0/"
}
And here&#8217;s the last update we owe you. Below you&#8217;ll find two more videos. The first one will handle OpenMW, and the other will spend some time on OpenMW-CS.





Phew, now that that is out of the way, we can move on to 0.39.0. The next version is still cooking, but if you want to go check it out, you can always try the nightlies for [Windows][1] and [Linux][2]. They already include support for those [fancy new shaders][3] too!

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=3445" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://forum.openmw.org/viewtopic.php?f=20&t=1808
 [2]: https://launchpad.net/~openmw/+archive/ubuntu/openmw-daily
 [3]: https://wiki.openmw.org/index.php?title=TextureModding