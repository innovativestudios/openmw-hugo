{
   "author": "lysol",
   "categories": [
      "Uncategorized"
   ],
   "date": "2019-02-04T18:53:09+00:00",
   "title": "Time to make stuff official",
   "type": "post",
   "url": "/2019/time-to-make-stuff-official/"
}
Hello again.

First of all: Sorry for the delay of 0.45.0. It is more or less ready for release and has been for quite some time. Real life just caught up for raevol and Atahualpa, our release manager and release video producer respectively. We want each release to be seen and downloaded by as many people as possible, so it&#8217;s important to us to get the PR done right – and we all love the videos. As soon as they are done with the things occupying them, we will have our release ready for you.

Anyway, there is one thing we want to make official to you all. Those of you active on the forums might have already figured out that the long-term plan for TES3MP, the multiplayer fork of OpenMW, is to get it merged into OpenMW as an optional multiplayer feature of the engine. The multiplayer part will then probably be renamed to OpenMW-MP. When the merging is going to happen has not yet been decided, but a good guess is that work on TES3MP integration will probably start soon after OpenMW 1.0.0 has been released.

Multiplayer integration also means that a server side scripting system, like the one present in TES3MP, will eventually find its way into OpenMW. This is, of course, a huge undertaking, so it might take a while before we can release more details on how this system is going to work.

Anyway, since TES3MP is now officially a partner of OpenMW (and, internally, has been for quite some time), you&#8217;ll see more news posts regarding that project in the future. In the meantime, we highly recommend visiting [the official TES3MP subreddit][1] and [TES3MP&#8217;s steam group][2].

The TES3MP news of the week is that TES3MP will soon have a macOS client! This has been much requested for a long time and will allow our dear Apple users to join the growing Morrowind multiplayer community. Already there: TES3MP now provides 32-bit builds on Windows for users of older operating systems or computers. You can download TES3MP [here][3].

But what about news for regular OpenMW you ask? There has been a lot of work on our engine too. – But that is something to discuss in a future news post.

I&#8217;ll give you one thing though: [Shadows are being reviewed][4] and, after that, are going to be merged into the OpenMW master. Soon™ in a nightly near you!

See you again next week!

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=5712" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://www.reddit.com/r/tes3mp
 [2]: https://steamcommunity.com/groups/mwmulti
 [3]: https://github.com/TES3MP/openmw-tes3mp/releases
 [4]: https://i.imgur.com/CALH4g1.png