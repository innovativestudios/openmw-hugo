{
   "author": "Lukasz Gromanowski",
   "categories": [
      "Maintenance"
   ],
   "date": "2012-01-13T12:17:43+00:00",
   "tags": [
      "lgromanowski",
      "maintenance"
   ],
   "title": "Power failure in datacenter",
   "type": "post",
   "url": "/2012/power-failure-in-datacenter/"
}
Hi,  
as you probably noticed openmw.org was off-line yesterday and today that was caused by some strange power failure in datacenter used by Rootnode.net.  
More detailed info is in http://rootnodestatus.net but unfortunately it is still in Polish language only. All data was restored from backups, so there should be no loss except uptime.

I will monitor situation and write another status post on weekend.