{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-11-26T17:51:00+00:00",
   "title": "Week in review",
   "type": "post",
   "url": "/2013/week-review-77/"
}
TL;DR Another good week, with progress on long awaited features.

The 0.28.0 will be either very good or truly amazing. This week gus work on combat AI was merged and even now, in this very moment work on new features goes on!

Let&#8217;s start with saving, that little nifty feature that allows you to actually quit Morrowind without worrying about loosing your hero. Zini is working on it currently, and I bet this will be ready for the 0.28.0. Please note, that saving and loading was meant to be one of the last things to implement before 1.0.0. So yes, we are getting to this long awaited moment.

Scrawl still works on magic. He implemented bound and summon effects, vampirism, sun damage, absorb, reflect. In fact majority of magic effects is implemented now! He also corrected various aspects of container implementation, implemented recharging of the magical items. Great job scrawl!

Work on our manual also rolls on. Although it is still very &#8220;quick and dirty&#8221; it slowly grows to cover missing issues. At some point, probably soon after the release of 1.0.0 it will actually become valuable help to all users: both die-hard veterans of construction set and all of those who just want to begin creating content for openmw engine.