{
   "author": "Okulo",
   "categories": [
      "Uncategorized"
   ],
   "date": "2015-01-06T19:08:31+00:00",
   "title": "The Year of OpenMW",
   "type": "post",
   "url": "/2015/year-openmw/"
}
Happy 2015 everyone! Hopefully you had some great meals during the holidays and partied hard on New Year&#8217;s Eve because now that January has rolled around, it&#8217;s time for you to hit that treadmill again and work off those extra kilos you gained. Hop to it!

OpenMW continues to gain ground. In 2014 we have gone from 0.27.0 to 0.34.0. Don&#8217;t remember 0.27.0? It was the version where [Acrobatics and God Mode were implemented][1]. The Morrowind community has also been noticing OpenMW a bit more. As part of his _Morrowind Modding Interviews_ series, Darkelfguy had an interview with our project leader, Zini. [You should totally check it out.][2] Not only do you get to hear Zini speak perfect Angleutsch, you also get to learn some more about Zini himself, OpenMW and the Morrowind modding community in general.

Scrawl has been working hard throughout december on OpenMW. This guy has knowhow &#8211; and a lot of it. A lot of us fans do not have any background in programming and wouldn&#8217;t know a pointer from a variable, so getting into the inner workings of OpenMW is not quite what we do on the frontpage. But it _is_ damn interesting. That&#8217;s why Scrawl has a really nice blog in which he talks about what he has been up to. His latest post is about parting OpenMW to Ogre 2.0. [You should definitely check it out if you want to learn more about the nitty gritty details of development and what makes OpenMW tick][3]. I&#8217;ll let Scrawl himself talk about what he has been doing throughout December:

> The first couple of days were spent catching up with the backlog of easy to fix bugs that had accumulated. Over the course of the two weeks, many more of these were discovered and fixed, some of which can be attributed to Tinker&#8217;s efforts regarding a mod testing wiki page. I applaud this idea and must say I am very happy with the amount of polish we are getting these days.
> 
> A few NIF features were added: XYZ rotation keys, used among others by the &#8220;Containers Animated&#8221; mod are now supported.
> 
> The sound system was updated to account for various range limiting quirks in MW&#8217;s sound system. This fixes excessively loud ambient sound volumes that were often noted by testers, and also bug #244 &#8220;Clipping/static in relation to the ghostgate sound&#8221; which is one of the most long-standing issues we have fixed in recent times.
> 
> The missing hotkeys for cycling the equipped weapon/spell have been implemented. That means all controls from the original game are now added; another category we can strike off the list. Nice! We have the head tracking for NPCs in place now, which means they will turn their head to follow the nearest actor. Hopefully this makes them seem a little less robotic. I might do more tweaking on it later, but I think it looks as good as in the original game already.
> 
> Lastly, a dialogue script testing switch was added. We often get bugs regarding script compilation errors, since OpenMW&#8217;s script compiler is more strict than the original game&#8217;s. There has been a &#8211;script-all switch to compile all scripts in the game and check for errors for a while now, but this didn&#8217;t cover dialogue scripts. Now we can test dialogue scripts too, and several bugs were already found as result of this.
> 
> All in all, 0.35 is shaping up to an awesome release already. Not in terms of numbers, but the amount of polish we are getting and also the fixed ambient sound volume issue should be very appreciated by players.

But that&#8217;s not all. This was just a selection of what Scrawl has written. Check out the full posts [here][4] and [here][5] to see what else Scrawl has done and how you as a player will notice!

Here&#8217;s hoping 2015 will be just as productive as 2014 &#8211; for you, me and OpenMW alike. See you all next time.

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=2696" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://www.youtube.com/watch?v=6ea8NPedKsk
 [2]: https://www.youtube.com/watch?v=d8XSeMYgmU4
 [3]: http://scrawl.bplaced.net/blog/
 [4]: https://forum.openmw.org/viewtopic.php?p=29582#p29582
 [5]: https://forum.openmw.org/viewtopic.php?p=30036#p30036