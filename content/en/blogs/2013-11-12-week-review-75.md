{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-11-12T06:58:41+00:00",
   "title": "Week in review",
   "type": "post",
   "url": "/2013/week-review-75/"
}
Welcome!

We have reached to new milestone in OpenMW. You were able to steal Fargoths ring, you were able to kill Fargoth. But now, you are able to also use Scroll of Icarian Flight in OpenMW! This is a spectacular. Just watch the [video][1]!

In other words: fortify spell effects are finally working. This was programmed by scrawl. Scrawl is right now looking forward to implement touch ranged magic spells.

We also have other interesting contributions to talk about: zini continues to work on dialogue handling in the editor and graffy is reworking OpenCS settings in order to allow adding new features in the future.

And finally: We are getting close to the new version madness, with few new features, new editor, and plenty of bugfixes.

 [1]: https://www.youtube.com/watch?v=-6l_B98P7P8 "video"