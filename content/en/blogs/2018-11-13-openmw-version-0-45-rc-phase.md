{
   "author": "lysol",
   "categories": [
      "Uncategorized"
   ],
   "date": "2018-11-13T16:31:31+00:00",
   "title": "OpenMW version 0.45 is in RC phase",
   "type": "post",
   "url": "/2018/openmw-version-0-45-rc-phase/"
}
Greetings! We just want to inform you all that OpenMW version 0.45 is finally in RC phase, which means it will soon™ be released to you all.

One important thing, just so you don&#8217;t get too excited just yet: neither [shadows][1] nor [recastnavigation][2] (better AI pathfinding, basically) will be included in the release, even though we all hoped so before. Don&#8217;t get too sad though, because recastnavigation is actually merged in master. This means that you can try it out already today in the latest nightlies! The implementation of shadows is not merged yet however, and it is very hard to say exactly when it will get merged, but probably quite soon™. Some bugs take longer time than you might expect before they are solved.

So why were recastnavigation not included in 0.45 then? Basically, it is such a huge update that we need to test it thoroughly before it can get included in a stable release. The alternative would have been to delay the release of 0.45, but that release is so huge already that we still want to get it out for you all. If you do test the implementation of recastnavigation in a nightly, be sure to [report any bugs you find][3]!

Otherwise, have a little more patience and you&#8217;ll have a new OpenMW version soon.

Until next time!

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=5542" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://openmw.org/2018/openmw-launcher-advanced-settings-menu/
 [2]: https://openmw.org/2018/solution-ai-problems/
 [3]: https://gitlab.com/OpenMW/openmw/issues