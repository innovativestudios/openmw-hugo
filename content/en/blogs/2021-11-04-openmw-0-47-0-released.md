{
  "author": "Atahualpa",
  "categories": [
    "Uncategorized"
  ],
  "comment": true,
  "date": "2021-11-04T17:49:23+00:00",
  "showReadingTime": true,
  "summary": "Another year of busy and fruitful development lies behind us &#8212; and the OpenMW team is proud to announce the release of version 0.47.0 of our open-source engine!",
  "source": {
    "forumID": 3,
    "topicID": 7588
  },
  "title": "OpenMW 0.47.0 Released!",
  "url": "2021/openmw-0-47-0-released"
}
Grab it from our [Downloads Page][1] for all supported operating systems.

With over 180 solved issues and a plethora of new features, this release is on par with the enormous 0.46.0 release we had last year. Brace yourself for object paging which allows OpenMW to finally display distant statics, proper support for groundcover mods, an improved lighting system, more efficient and robust physics, the new, optional over-the-shoulder camera, and much, much more!

In addition, countless bugs have been solved &#8212; both for the vanilla game and for a variety of mods to ensure even better mod compatibility in the new version.

Check out the [release announcement video][2] by the hacking Atahualpa and the slashing johnnyhostile, and see below for the full list of changes. There are also a [German release video][3] and a [German changelog][4] available.

# {{<youtube id="_9sUhduT-K4" autoplay="false">}}
# {{<youtube id="9Cwdf7fwO6k" autoplay="false">}}

**Known Issues:**

* On macOS, launching OpenMW from OpenMW-CS requires &#8216;OpenMW.app&#8217; and &#8216;OpenMW-CS.app&#8217; to be siblings
* Lighting of spellcasting particles looks dull
* Pathfinding during the &#8220;Tribunal&#8221; expansion quest &#8220;Infidelities&#8221; is broken and the journal may not get updated because Taren Andoren hasn&#8217;t yet reached his target spot; as a workaround, waiting for one hour will allow Taren to reach his target spot and the journal to be correctly updated
* Performance of enchanted ranged weapons (e.g., darts) isn&#8217;t as good as other projectiles&#8217; performance

**New Engine Features:**

* _by akortunov_
* [[#5524][5]] Failed scripts are flagged as &#8216;inactive&#8217; and ignored rather than being removed &#8212; until the game is reloaded or restarted
* [[#5580][6]] An NPC&#8217;s service refusal (e.g., for training, trading, or enchanting) can be filtered by service type to allow more complex NPC reactions instead of the vanilla &#8220;all or nothing&#8221; behaviour
* [[#5642][7]] Arrows can be attached to an actor&#8217;s skeleton instead of the bow mesh to allow implementing left-handed bows and actor-dependent shooting animations
* [[#5813][8]] Improved handling of groundcover mods via grass instancing
* _by AnyOldName3_
* [[#4899][9]] Alpha-to-coverage anti-aliasing &#8212; this improves the look of alpha-tested objects, e.g., foliage from &#8220;Morrowind Optimization Patch&#8221;
* [[#4977][10]] Show default icon if an item&#8217;s icon is not found
* _by Assumeru_
* [[#2404][11]] Levelled lists can be placed in containers
* [[#2798][12]] Base records are mutable, i.e., modifying an instance of a base record (e.g., a guard NPC) will now affect all instances sharing the same base record (e.g., all other clones of that guard NPC)
* [[#5730][13]] Add option to support graphic herbalism to &#8216;Advanced&#8217; tab in the launcher
* [[#5771][14]] &#8216;ori&#8217; console command displays the mesh&#8217;s data source as well as whether the x-prefixed version is used
* _by bzzt_
* Proper underwater shadows when refraction is enabled; disabled if &#8216;refraction scale&#8217; setting is not 1.0 _[additions by AnyOldName3 and madsbuvi]_
* [[#2386][15]] Optional: Render distant static objects via object paging, i.e., by merging objects which are close to each other; ON by default but &#8216;Distant land&#8217; needs to be enabled _[corrections and polishing by psi29a]_
* [[#2386][15]] Optional: Object paging in active cells (3&#215;3 grid around player character); ON by default _[corrections and polishing by psi29a]_
* _by Capostrophic_
* [[#5362][16]] Dialogue for splitting item stacks displays the name of the trapped soul for stacks of soul gems
* [[#5445][17]] Handle &#8216;NiLines&#8217; property of NIF meshes
* [[#5545][18]] Optional: Allow the player character to steal from unconscious (knocked-down) NPCs during combat; OFF by default
* [[#5579][19]] Support for inverse-order rotation (z, y, x) in &#8216;SetAngle&#8217; function
* [[#5649][20]] Support Skyrim SE&#8217;s compressed BSA format
* [[#5672][21]] Setting &#8216;Stretch menu background&#8217; which makes background images fit the full screen is also available in the launcher
* _by CedricMocquillon_
* [[#1536][22]] Show current level-up attribute multipliers in &#8216;Level&#8217; tooltip
* [[#4486][23]] Handling and logging of crashes on Windows
* [[#5297][24]] Search bar in the launcher&#8217;s &#8216;Data Files&#8217; tab
* [[#5486][25]] Optional: Determine a trainer&#8217;s offered skills and their respective limits by evaluating base values instead of modified ones; OFF by default
* [[#5519][26]] Reorganised and expanded &#8216;Advanced&#8217; tab in the launcher to integrate most MCP-like settings
* [[#5814][27]] &#8216;bsatool&#8217; can add to or create new BSA archives (supports all BSA types)
* _by elsid_
* [[#4917][28]] Objects too small to change the navigation mesh no longer trigger updates of that mesh which improves performance
* [[#5500][29]] Scene loading ends only when enough navigation mesh tiles around the player character have been generated
* [[#6033][30]] Automatically add existing path grid to navigation mesh to further improve pathfinding
* [[#6033][30]] Partially remove fallbacks to old pathfinding to avoid, e.g., actors following other enemies into lava
* [[#6034][31]] Calculate optimal navigation paths based on an actor&#8217;s individual movement speed on land and in water
* _by fr3dz10_
* [[#2159][32]] Optional: Grey out dialogue topics which are &#8212; at least in that moment &#8212; exhausted and highlight newly added topics; OFF by default (colours are customisable)
* [[#4201][33]] Projectile-projectile collision, including targeted spells (a successful hit cancels both projectiles); emulates vanilla behaviour
* [[#5563][34]] Optional: Physics are run in one or several background threads to improve performance; OFF by default
* _by glassmancody.info_
* [[#5828][35]] New, shader-based, customisable lighting system which removes the limit of eight light sources per object
* _by jefetienne_
* [[#5692][36]] Filter in the spell window also displays items and spells with matching magic effects &#8212; not only those with matching names
* _by ptmikheev_
* Improved vanity mode in third-person view
* [[#390][37]] Optional: New over-the-shoulder look in 3rd-person view; OFF by default
* [[#390][37]] Optional: Auto-switch shoulder in narrow passages; ON by default but &#8216;View over the shoulder&#8217; needs to be enabled
* [[#2686][38]] Logged info in &#8216;openmw.log&#8217; displays timestamps
* [[#4894][39]] Optional: NPCs avoid collisions when moving; OFF by default
* [[#4894][39]] Optional: NPCs give way to moving actors if they are idle; ON by default but &#8216;NPCs avoid collisions&#8217; needs to be enabled
* [[#5043][40]] Optional: Customisable head bobbing in first-person mode; OFF by default
* [[#5457][41]] Optional: More realistic diagonal character movement; OFF by default
* [[#5610][42]] Optional: Actors move and turn smoother; OFF by default
* _by simonmeulenbeek_
* [[#5511][43]] Add audio settings to &#8216;Advanced&#8217; tab in the launcher
* _by TescoShoppah_
* [[#3983][44]] Installation wizard provides link to FAQ section regarding buying Morrowind.
* _by unelsson_
* [[#5456][45]] Basic support for &#8216;Collada&#8217; animation format
* _by wareya_
* [[#5762][46]] &#8216;Movement solver&#8217; is now much more robust
* [[#5910][47]] Fall back to true delta time if needed to prevent physics death spirals in game

**New Editor Features:**

* _by Atahualpa_
* [[#6024][48]] Customisable primary and secondary selection modes for &#8216;terrain land editing&#8217; mode (see [#3171][49])
* _by olcoal_
* [[#5199][50]] Improved and configurable colours in 3D view
* _by unelsson_
* [[#832][51]] Proper handling of deleted references
* [[#3171][49]] Instance drag selection with the following modes: &#8216;centred cube&#8217;, &#8216;cube corner to corner&#8217;, &#8216;centred sphere&#8217;
* [[#3171][49]] Configurable actions for instance drag selection: &#8216;select only&#8217;, &#8216;add to selection&#8217;, &#8216;remove from selection&#8217;, &#8216;invert selection&#8217;

**Engine Bug Fixes:**

* _by akortunov_
* [[#3714][52]] Resolved conflicts between &#8216;SpellState&#8217; and &#8216;MagicEffects&#8217; functionality of the engine
* [[#3789][53]] Fixed a crash which could occur when updating active magic effects
* [[#4021][54]] Attributes and skills are now stored as floating-point values
* [[#4623][55]] Improved implementation of the Corprus disease
* [[#5108][56]] Prevent save-game bloating by using an appropriate fog texture format
* [[#5165][57]] Active spell effects now use real-time updates instead of timestamps to avoid problems with time scale and scripted daytime changes
* [[#5387][58]] &#8216;Move&#8217; and &#8216;MoveWorld&#8217; commands now correctly update the moved object&#8217;s cell
* [[#5499][59]] Game logic for rank advancement now properly considers the two favoured skills of a faction
* [[#5502][60]] Dead zone of analogue sticks can now be configured in the &#8216;openmw.cfg&#8217; file
* [[#5619][61]] Key presses are now ignored during savegame loading
* [[#5975][62]] Controllers for &#8220;sheath&#8221; meshes (used by weapon sheathing mods) are now disabled to prevent, e.g., accidentally playing shooting animations for bows in &#8220;HQ Arsenal&#8221; mod
* [[#6043][63]] An NPC&#8217;s &#8216;shield&#8217; animation is now cancelled when a light source is equipped instead of a shield
* [[#6047][64]] In-game mouse bindings can no longer be triggered when controls are disabled, e.g., when loading a savegame
* _by AnyOldName3_
* [[#2069][65]] &#8216;NiFlipControllers&#8217; now only affect the base texture which, e.g., solves issues with the fireflies in the &#8220;Fireflies Invade Morrowind&#8221; mod
* [[#2976][66]] Fixed issues with the priority of OpenMW&#8217;s local and global config files
* [[#4631][67]] GPUs which don&#8217;t support an anti-aliasing value of 16 now always fall back to a lower value if &#8217;16&#8217; is set in the settings
* [[#5391][68]] Bodies from &#8220;Races Redone&#8221; mod are now correctly shown on the inventory&#8217;s paper doll
* [[#5688][69]] Water shader no longer breaks in interior cells when indoor shadows are disabled
* [[#5906][70]] Sun-glare effect now works with &#8216;Mesa&#8217; drivers and AMD GPUs
* _by Assumeru_
* [[#2311][71]] Targeting non-unique actors in scripts is now supported
* [[#2473][72]] Selling respawning items to vendors now increases the amount that respawns
* [[#3862][73]] Random contents of containers are now determined in a similar way as in the original engine
* [[#3929][74]] Random loot in a container no longer respawns when the player character takes the loot and trades with the container&#8217;s owner afterwards
* [[#4039][75]] Followers no longer form a train of actors but rather follow their leader at the same distance
* [[#4055][76]] If a script is running as a global script, newly triggered local instances of that script will now be initialised with the global script&#8217;s variable state
* [[#5300][77]] NPCs carrying torches now switch to using a shield when they enter combat _[inspired by Capostrophic&#8217;s work]_
* [[#5423][78]] Bigger creature followers &#8212; e.g., Guars &#8212; no longer bump into their leader
* [[#5469][79]] Scripted rotations or displacements of large objects no longer reset the local map&#8217;s fog of war
* [[#5661][80]] Region sounds now use fallback values to determine the minimum and maximum time between sounds
* [[#5661][80]] There now is a chance to play no region sound at all if the sum of all sound chances for the current region is below 100 %
* [[#5687][81]] Bound items covering the same equipment slot no longer freeze the game if they expire at the same time, e.g., while resting
* [[#5835][82]] Scripts can now set AI values (&#8216;AI Hello&#8217;, &#8216;AI Alarm&#8217;, &#8216;AI Fight&#8217;, &#8216;AI Flee&#8217;) to negative numbers
* [[#5836][83]] Dialogue conditions containing negative AI values now work
* [[#5838][84]] Teleport doors to non-existent wilderness cells no longer break the local map
* [[#5840][85]] NPCs being hit due to attacking an enemy with an active &#8216;fire/frost/shock shield&#8217; effect now trigger the corresponding damage sound effect
* [[#5841][86]] Free spells can now be cast even if the caster has no magicka left
* [[#5871][87]] Users with Russian keyboard layout can now use &#8216;Ё&#8217; in input fields without opening the console
* [[#5912][88]] &#8216;Summon&#8217; effects that failed to summon anything are no longer removed
* [[#5923][89]] Clicking on blank spaces in the journal can no longer open topics on the next page
* [[#5934][90]] Morrowind legacy madness: The item count in &#8216;AddItem&#8217; commands is now transformed into an unsigned value, i.e., negative numbers will overflow and result in a positive number of items being added
* [[#5991][91]] Scripts can now open books and scrolls in the inventory
* [[#6007][92]] Corrupt video files no longer crash the game
* [[#6016][93]] Sneaking or jumping NPCs no longer stop and turn to greet the player character
* _by Capostrophic_
* [[#4774][94]] Guards no longer ignore attacks of invisible player characters but rather initiate dialogue and flee if the player resists being arrested
* [[#5358][95]] Initiating dialogue with another actor without closing the dialogue window no longer clears the dialogue history in order to allow, e.g., emulation of three-way dialogue via &#8216;ForceGreeting&#8217; command
* [[#5363][96]] &#8216;Auto Calc&#8217; flag for enchantments is now properly treated as flag in OpenMW-CS and in OpenMW&#8217;s &#8216;esmtool&#8217;
* [[#5364][97]] Scripts which try to start a non-existent global script now skip that step and continue execution instead of breaking
* [[#5367][98]] Selecting already equipped spells or magic items via hotkey no longer triggers the &#8216;equip&#8217; sound to play
* [[#5369][99]] &#8216;Scale&#8217; argument in levelled creature lists is now considered when spawning creatures from such lists
* [[#5370][100]] Morrowind legacy madness II: Using a key on a trapped door/container now only disarms the trap if the door/container is actually locked
* [[#5397][101]] NPC greetings are now properly reset when leaving and re-entering an area
* [[#5403][102]] Visual spell effects affecting an actor now also play during that actor&#8217;s death animation
* [[#5415][103]] Environment maps now properly work for equipped items, e.g., those of the &#8220;HiRez Armors&#8221; mod
* [[#5416][104]] Junk non-node root records in NIF meshes are now handled more gracefully to allow certain modded assets to be loaded
* [[#5424][105]] Creatures now head-track the player character
* [[#5425][106]] Magic effects which are not applied once now have a minimum duration of 1 second
* [[#5427][107]] &#8216;GetDistance&#8217; function no longer stops script execution when there is no object found for the given ID
* [[#5427][107]] &#8216;GetDistance&#8217; function and the engine&#8217;s object search now log improved warning messages regarding missing objects of a given ID
* [[#5435][108]] Enemy attacks can now hit the player character when collision is disabled
* [[#5441][109]] Fixed the priority of certain animations to allow enemies to push the player character in first-person view if the player just holds the attack button
* [[#5451][110]] Magic projectiles now instantly disappear when their caster vanishes or dies
* [[#5452][111]] Auto-walking is no longer included in savegames, i.e., the player character will stand still again after loading
* [[#5484][112]] Items with a base value of 0 can no longer be sold for 1 gold
* [[#5485][113]] Successful intimidation attempts now always increase an NPC&#8217;s disposition by at least a minimum margin
* [[#5490][114]] Hits to the &#8216;left carry&#8217; slot are now redistributed to the &#8216;left pauldron&#8217; slot or the &#8216;cuirass&#8217; slot if the actor has no shield equipped
* [[#5525][115]] Case-insensitive search in the inventory window now also works for non-ASCII characters
* [[#5603][116]] When switching to constant-effect enchanting in the enchantment window, all effect ranges are now reset to &#8216;self&#8217; if possible and incompatible effects are removed
* [[#5604][117]] OpenMW&#8217;s NIF loaders can now correctly handle NIF files with multiple root nodes
* [[#5611][118]] Repair hammers with 0 uses can now be used only once before they break, while lockpicks and probes with 0 uses do nothing at all &#8212; thanks, Bethesda!
* [[#5622][119]] Fixed the priority of the main-menu window to prevent the in-game console from becoming unresponsive
* [[#5627][120]] Book parser now considers images and formatting tags after the last &#8216;end of line&#8217; tag to correctly display certain in-game books and scrolls added by mods
* [[#5633][121]] Negative spell effects received before switching to god mode no longer continue to harm the player character
* [[#5639][122]] Tooltips no longer cover message boxes
* [[#5644][123]] Active summon effects on the player character no longer cause crashes during game initialisation
* [[#5656][124]] Characters no longer use the standing animation when blocking attacks in sneak mode
* [[#5695][125]] Actors casting a target spell at themselves via script now aim for their feet rather than casting the spell in their current target&#8217;s direction
* [[#5706][126]] AI sequences &#8212; e.g., for patrolling NPCs &#8212; no longer stop looping when a savegame is reloaded
* [[#5758][127]] Paralysed actors who are underwater now float to the surface
* [[#5758][127]] Levitating actors who get paralysed now fall to the ground
* [[#5869][128]] Guards now only initiate the arrest dialogue if the player character is in line of sight
* [[#5877][129]] Transparency of magic-effect icons is now properly reset to prevent &#8220;empty&#8221; icons from appearing in certain situations
* [[#5902][130]] &#8216;NiZBufferProperty&#8217; now handles &#8216;depth test&#8217; flag
* [[#5995][131]] UV offset in &#8216;NiUVController&#8217; &#8212; which is used in vanilla Morrowind to simulate liquid movement &#8212; is now properly calculated
* _by ccalhoun1999_
* [[#5101][132]] Hostile followers no longer follow the player character through teleport doors or when they use travel services
* _by davidcernat_
* [[#5422][133]] The player character no longer loses all spells if resurrected via the &#8216;resurrect&#8217; console command
* _by elsid_
* [[#4764][134]] Synchronise main thread and rendering thread to avoid errors with particles, e.g., in the water-ripple effect
* [[#5479][135]] Fixed an issue with pathfinding that led to NPCs standing rooted to the ground instead of wandering about
* [[#5507][136]] Sound volume settings are now always restricted to the range [0.0, 1.0] and are no longer potentially unclamped in response to in-game volume changes.
* [[#5531][137]] Fleeing actors are now correctly rotated which, e.g., prevents cliff racers from diving underwater when fleeing
* [[#5914][138]] &#8216;Navigator&#8217; now builds &#8220;limited&#8221; paths for actors with far-away destinations to ensure pathfinding even over greater distances (outside the navigation mesh)
* [[#6294][139]] Fixed a crash caused by an empty path grid
* _by fr3dz10_
* [[#3372][140]] Magic bolts and projectiles now always collide with moving targets
* [[#4083][141]] Doors now mimic vanilla Morrowind&#8217;s behaviour when colliding with an actor during their open/close animation _[partially fixed by elsid]_
* [[#5472][142]] Zero-lifetime particles are now handled properly and a related potential zero division in &#8216;NiParticleColorModifier&#8217; that caused crashes on non-PC platforms, e.g., when using Melchior Dahrk&#8217;s &#8220;Mistify&#8221; mod, has been fixed
* [[#5548][143]] &#8216;ClearInfoActor&#8217; script function now removes the correct topic from an actor
* [[#5739][144]] Saving and reloading the savegame prior to hitting the ground no longer prevents fall damage
* _by glassmancody.info_
* [[#5899][145]] Exiting the game without having closed all modal windows no longer leads to a crash
* [[#6028][146]] NIF particle systems now properly inherit the particle count from their &#8216;NiParticleData&#8217; record which, e.g., solves issues with the &#8220;I Lava Good Mesh Replacer&#8221; mod
* _by kyleshrader_
* [[#5588][147]] Clicking on an empty journal page no longer triggers topic entries to show
* _by madsbuvi_
* [[#5539][148]] Resizing of the game window no longer breaks when switching from lower resolution to full-screen resolution
* _by mp3butcher_
* [[#1952][149], [#3676][150]] &#8216;NiParticleColorModifier&#8217; in NIF files is now properly handled which solves issues regarding particle effects, e.g., smoke and fire
* _by ptmikheev_
* [[#5557][151]] Moving diagonally using a controller&#8217;s analogue stick no longer results in slower movement speed compared to keyboard input
* [[#5821][152]] OpenMW now properly keeps track of NPCs which were added by a mod and moved to another cell, even if the mod&#8217;s load-order position is changed
* _by SaintMercury_
* [[#5680][153]] Actors now properly aim magic projectiles which, e.g., prevents bull netches from shooting over the player character&#8217;s head
* _by Tankinfrank_
* [[#5800][154]] Equipping a constant-effect ring no longer unequips a cast-on-use ring which is currently selected in the spell window
* _by wareya_
* [[#1901][155]] Actors&#8217; collision behaviour now more closely replicates vanilla Morrowind&#8217;s behaviour
* [[#3137][156]] Walking into a wall no longer prevents the player character from jumping
* [[#4247][157]] Actors can now walk up certain steep stairs thanks to the usage of AABB (&#8220;axis-aligned bounding box&#8221;) collision shapes
* [[#4447][158]] Switching to AABB collision shapes also prevents the player character from looking through certain walls
* [[#4465][159]] Overlapping collision shapes no longer cause NPCs to twitch
* [[#4476][160]] Player character no longer floats in the air during scenic travel in abot&#8217;s &#8220;Gondoliers&#8221; mod
* [[#4568][161]] Actors can no longer push each other out of level boundaries when there are too many of them in one spot
* [[#5431][162]] Prevent physics death spirals in scenes with a huge amount of actors
* [[#5681][163]] Player characters now properly collide with wooden bridges instead of getting stuck or passing through them

**Editor Bug Fixes:**

* _by akortunov_
* [[#1662][164]] OpenMW-CS no longer crashes if there are non-ASCII characters in a file path or in a configuration path
* _by Atahualpa_
* [[#5473][165]] Cell borders are now properly redrawn when undoing/redoing changes made to the terrain
* [[#6022][166]] Terrain selection grid is now properly redrawn when undoing/redoing changes made to the terrain
* [[#6023][167]] Objects no longer block terrain selection in &#8216;terrain land editing&#8217; mode
* [[#6035][168]] Circle brush no longer selects terrain vertices outside its perimeter
* [[#6036][169]] Brushes no longer ignore vertices at NE and SW corners of a cell
* _by Capostrophic_
* [[#5400][170]] Verifier no longer checks for alleged &#8216;race&#8217; entries in non-skin body parts
* [[#5731][171]] Skirts worn by NPCs are now properly displayed in 3D view
* _by unelsson_
* [[#4357][172]] Sorting in &#8216;Journal Infos&#8217; and &#8216;Topic Infos&#8217; tables is now disabled; you may adjust the order of records manually
* [[#4363][173]] Clone function for &#8216;Journal Infos&#8217; and &#8216;Topic Infos&#8217; records now allows you to edit the new record&#8217;s &#8216;ID&#8217; respectively
* [[#5675][174]] Object instances are now loaded and saved with the correct master index to prevent overwritten objects from appearing in game
* [[#5703][175]] OpenMW-CS will no longer flicker and crash on XFCE desktop environments
* [[#5713][176]] OpenMW-CS now properly renders &#8216;Collada&#8217; models
* [[#6235][177]] OpenMW-CS no longer crashes when changes to the terrain height are undone and then redone
* _by Yoae_
* [[#5384][178]] Deleting an instance in 3D view now correctly updates all active 3D views

**Miscellaneous:**

* _by akortunov_
* [[#5026][179]] Prevent data races with &#8216;rain intensity uniforms&#8217;
* [[#5480][180]] Drop Qt4 support in favour of Qt 5.12 or later
* _by AnyOldName3_
* [[#4765][181]] Avoid binding an OSG array from multiple threads in OpenMW&#8217;s &#8216;ChunkManager&#8217;
* [[#5551][182]] Windows: OpenMW no longer forces a reboot during installation
* [[#5904][183]] Mesa: Fixed OpenSceneGraph issue regarding RTT method (&#8220;render-to-texture&#8221;)
* _by CedricMocquillon_
* [[#5520][184]] Improved handling of combo box &#8216;Start default character at&#8217; in the launcher to avoid warning messages and possible memory leaks
* _by fr3dz10_
* [[#5980][185]] Check for and enforce double-precision &#8216;Bullet&#8217; during configuration _[addition by akortunov]_
* _by glebm_
* [[#5807][186]] Fixed crash on ARM machines caused by incorrect handling of frame allocation in &#8216;osg-ffmpeg-videoplayer&#8217;
* [[#5897][187]] Updated &#8216;MyGUI&#8217; to prevent errors when dynamically linking it

[1]: https://openmw.org/downloads/
[2]: https://www.youtube.com/watch?v=_9sUhduT-K4
[3]: https://www.youtube.com/watch?v=9Cwdf7fwO6k
[4]: https://openmw.org/2021/openmw-0-47-0-veroffentlicht/
[5]: https://gitlab.com/OpenMW/openmw/-/issues/5524
[6]: https://gitlab.com/OpenMW/openmw/-/issues/5580
[7]: https://gitlab.com/OpenMW/openmw/-/issues/5642
[8]: https://gitlab.com/OpenMW/openmw/-/issues/5813
[9]: https://gitlab.com/OpenMW/openmw/-/issues/4899
[10]: https://gitlab.com/OpenMW/openmw/-/issues/4977
[11]: https://gitlab.com/OpenMW/openmw/-/issues/2404
[12]: https://gitlab.com/OpenMW/openmw/-/issues/2798
[13]: https://gitlab.com/OpenMW/openmw/-/issues/5730
[14]: https://gitlab.com/OpenMW/openmw/-/issues/5771
[15]: https://gitlab.com/OpenMW/openmw/-/issues/2386
[16]: https://gitlab.com/OpenMW/openmw/-/issues/5362
[17]: https://gitlab.com/OpenMW/openmw/-/issues/5445
[18]: https://gitlab.com/OpenMW/openmw/-/issues/5545
[19]: https://gitlab.com/OpenMW/openmw/-/issues/5579
[20]: https://gitlab.com/OpenMW/openmw/-/issues/5649
[21]: https://gitlab.com/OpenMW/openmw/-/issues/5672
[22]: https://gitlab.com/OpenMW/openmw/-/issues/1536
[23]: https://gitlab.com/OpenMW/openmw/-/issues/4486
[24]: https://gitlab.com/OpenMW/openmw/-/issues/5297
[25]: https://gitlab.com/OpenMW/openmw/-/issues/5486
[26]: https://gitlab.com/OpenMW/openmw/-/issues/5519
[27]: https://gitlab.com/OpenMW/openmw/-/issues/5814
[28]: https://gitlab.com/OpenMW/openmw/-/issues/4917
[29]: https://gitlab.com/OpenMW/openmw/-/issues/5500
[30]: https://gitlab.com/OpenMW/openmw/-/issues/6033
[31]: https://gitlab.com/OpenMW/openmw/-/issues/6034
[32]: https://gitlab.com/OpenMW/openmw/-/issues/2159
[33]: https://gitlab.com/OpenMW/openmw/-/issues/4201
[34]: https://gitlab.com/OpenMW/openmw/-/issues/5563
[35]: https://gitlab.com/OpenMW/openmw/-/issues/5828
[36]: https://gitlab.com/OpenMW/openmw/-/issues/5692
[37]: https://gitlab.com/OpenMW/openmw/-/issues/390
[38]: https://gitlab.com/OpenMW/openmw/-/issues/2686
[39]: https://gitlab.com/OpenMW/openmw/-/issues/4894
[40]: https://gitlab.com/OpenMW/openmw/-/issues/5043
[41]: https://gitlab.com/OpenMW/openmw/-/issues/5457
[42]: https://gitlab.com/OpenMW/openmw/-/issues/5610
[43]: https://gitlab.com/OpenMW/openmw/-/issues/5511
[44]: https://gitlab.com/OpenMW/openmw/-/issues/3983
[45]: https://gitlab.com/OpenMW/openmw/-/issues/5456
[46]: https://gitlab.com/OpenMW/openmw/-/issues/5762
[47]: https://gitlab.com/OpenMW/openmw/-/issues/5910
[48]: https://gitlab.com/OpenMW/openmw/-/issues/6024
[49]: https://gitlab.com/OpenMW/openmw/-/issues/3171
[50]: https://gitlab.com/OpenMW/openmw/-/issues/5199
[51]: https://gitlab.com/OpenMW/openmw/-/issues/832
[52]: https://gitlab.com/OpenMW/openmw/-/issues/3714
[53]: https://gitlab.com/OpenMW/openmw/-/issues/3789
[54]: https://gitlab.com/OpenMW/openmw/-/issues/4021
[55]: https://gitlab.com/OpenMW/openmw/-/issues/4623
[56]: https://gitlab.com/OpenMW/openmw/-/issues/5108
[57]: https://gitlab.com/OpenMW/openmw/-/issues/5165
[58]: https://gitlab.com/OpenMW/openmw/-/issues/5387
[59]: https://gitlab.com/OpenMW/openmw/-/issues/5499
[60]: https://gitlab.com/OpenMW/openmw/-/issues/5502
[61]: https://gitlab.com/OpenMW/openmw/-/issues/5619
[62]: https://gitlab.com/OpenMW/openmw/-/issues/5975
[63]: https://gitlab.com/OpenMW/openmw/-/issues/6043
[64]: https://gitlab.com/OpenMW/openmw/-/issues/6047
[65]: https://gitlab.com/OpenMW/openmw/-/issues/2069
[66]: https://gitlab.com/OpenMW/openmw/-/issues/2976
[67]: https://gitlab.com/OpenMW/openmw/-/issues/4631
[68]: https://gitlab.com/OpenMW/openmw/-/issues/5391
[69]: https://gitlab.com/OpenMW/openmw/-/issues/5688
[70]: https://gitlab.com/OpenMW/openmw/-/issues/5906
[71]: https://gitlab.com/OpenMW/openmw/-/issues/2311
[72]: https://gitlab.com/OpenMW/openmw/-/issues/2473
[73]: https://gitlab.com/OpenMW/openmw/-/issues/3862
[74]: https://gitlab.com/OpenMW/openmw/-/issues/3929
[75]: https://gitlab.com/OpenMW/openmw/-/issues/4039
[76]: https://gitlab.com/OpenMW/openmw/-/issues/4055
[77]: https://gitlab.com/OpenMW/openmw/-/issues/5300
[78]: https://gitlab.com/OpenMW/openmw/-/issues/5423
[79]: https://gitlab.com/OpenMW/openmw/-/issues/5469
[80]: https://gitlab.com/OpenMW/openmw/-/issues/5661
[81]: https://gitlab.com/OpenMW/openmw/-/issues/5687
[82]: https://gitlab.com/OpenMW/openmw/-/issues/5835
[83]: https://gitlab.com/OpenMW/openmw/-/issues/5836
[84]: https://gitlab.com/OpenMW/openmw/-/issues/5838
[85]: https://gitlab.com/OpenMW/openmw/-/issues/5840
[86]: https://gitlab.com/OpenMW/openmw/-/issues/5841
[87]: https://gitlab.com/OpenMW/openmw/-/issues/5871
[88]: https://gitlab.com/OpenMW/openmw/-/issues/5912
[89]: https://gitlab.com/OpenMW/openmw/-/issues/5923
[90]: https://gitlab.com/OpenMW/openmw/-/issues/5934
[91]: https://gitlab.com/OpenMW/openmw/-/issues/5991
[92]: https://gitlab.com/OpenMW/openmw/-/issues/6007
[93]: https://gitlab.com/OpenMW/openmw/-/issues/6016
[94]: https://gitlab.com/OpenMW/openmw/-/issues/4774
[95]: https://gitlab.com/OpenMW/openmw/-/issues/5358
[96]: https://gitlab.com/OpenMW/openmw/-/issues/5363
[97]: https://gitlab.com/OpenMW/openmw/-/issues/5364
[98]: https://gitlab.com/OpenMW/openmw/-/issues/5367
[99]: https://gitlab.com/OpenMW/openmw/-/issues/5369
[100]: https://gitlab.com/OpenMW/openmw/-/issues/5370
[101]: https://gitlab.com/OpenMW/openmw/-/issues/5397
[102]: https://gitlab.com/OpenMW/openmw/-/issues/5403
[103]: https://gitlab.com/OpenMW/openmw/-/issues/5415
[104]: https://gitlab.com/OpenMW/openmw/-/issues/5416
[105]: https://gitlab.com/OpenMW/openmw/-/issues/5424
[106]: https://gitlab.com/OpenMW/openmw/-/issues/5425
[107]: https://gitlab.com/OpenMW/openmw/-/issues/5427
[108]: https://gitlab.com/OpenMW/openmw/-/issues/5435
[109]: https://gitlab.com/OpenMW/openmw/-/issues/5441
[110]: https://gitlab.com/OpenMW/openmw/-/issues/5451
[111]: https://gitlab.com/OpenMW/openmw/-/issues/5452
[112]: https://gitlab.com/OpenMW/openmw/-/issues/5484
[113]: https://gitlab.com/OpenMW/openmw/-/issues/5485
[114]: https://gitlab.com/OpenMW/openmw/-/issues/5490
[115]: https://gitlab.com/OpenMW/openmw/-/issues/5525
[116]: https://gitlab.com/OpenMW/openmw/-/issues/5603
[117]: https://gitlab.com/OpenMW/openmw/-/issues/5604
[118]: https://gitlab.com/OpenMW/openmw/-/issues/5611
[119]: https://gitlab.com/OpenMW/openmw/-/issues/5622
[120]: https://gitlab.com/OpenMW/openmw/-/issues/5627
[121]: https://gitlab.com/OpenMW/openmw/-/issues/5633
[122]: https://gitlab.com/OpenMW/openmw/-/issues/5639
[123]: https://gitlab.com/OpenMW/openmw/-/issues/5644
[124]: https://gitlab.com/OpenMW/openmw/-/issues/5656
[125]: https://gitlab.com/OpenMW/openmw/-/issues/5695
[126]: https://gitlab.com/OpenMW/openmw/-/issues/5706
[127]: https://gitlab.com/OpenMW/openmw/-/issues/5758
[128]: https://gitlab.com/OpenMW/openmw/-/issues/5869
[129]: https://gitlab.com/OpenMW/openmw/-/issues/5877
[130]: https://gitlab.com/OpenMW/openmw/-/issues/5902
[131]: https://gitlab.com/OpenMW/openmw/-/issues/5995
[132]: https://gitlab.com/OpenMW/openmw/-/issues/5101
[133]: https://gitlab.com/OpenMW/openmw/-/issues/5422
[134]: https://gitlab.com/OpenMW/openmw/-/issues/4764
[135]: https://gitlab.com/OpenMW/openmw/-/issues/5479
[136]: https://gitlab.com/OpenMW/openmw/-/issues/5507
[137]: https://gitlab.com/OpenMW/openmw/-/issues/5531
[138]: https://gitlab.com/OpenMW/openmw/-/issues/5914
[139]: https://gitlab.com/OpenMW/openmw/-/issues/6294
[140]: https://gitlab.com/OpenMW/openmw/-/issues/3372
[141]: https://gitlab.com/OpenMW/openmw/-/issues/4083
[142]: https://gitlab.com/OpenMW/openmw/-/issues/5472
[143]: https://gitlab.com/OpenMW/openmw/-/issues/5548
[144]: https://gitlab.com/OpenMW/openmw/-/issues/5739
[145]: https://gitlab.com/OpenMW/openmw/-/issues/5899
[146]: https://gitlab.com/OpenMW/openmw/-/issues/6028
[147]: https://gitlab.com/OpenMW/openmw/-/issues/5588
[148]: https://gitlab.com/OpenMW/openmw/-/issues/5539
[149]: https://gitlab.com/OpenMW/openmw/-/issues/1952
[150]: https://gitlab.com/OpenMW/openmw/-/issues/3676
[151]: https://gitlab.com/OpenMW/openmw/-/issues/5557
[152]: https://gitlab.com/OpenMW/openmw/-/issues/5821
[153]: https://gitlab.com/OpenMW/openmw/-/issues/5680
[154]: https://gitlab.com/OpenMW/openmw/-/issues/5800
[155]: https://gitlab.com/OpenMW/openmw/-/issues/1901
[156]: https://gitlab.com/OpenMW/openmw/-/issues/3137
[157]: https://gitlab.com/OpenMW/openmw/-/issues/4247
[158]: https://gitlab.com/OpenMW/openmw/-/issues/4447
[159]: https://gitlab.com/OpenMW/openmw/-/issues/4465
[160]: https://gitlab.com/OpenMW/openmw/-/issues/4476
[161]: https://gitlab.com/OpenMW/openmw/-/issues/4568
[162]: https://gitlab.com/OpenMW/openmw/-/issues/5431
[163]: https://gitlab.com/OpenMW/openmw/-/issues/5681
[164]: https://gitlab.com/OpenMW/openmw/-/issues/1662
[165]: https://gitlab.com/OpenMW/openmw/-/issues/5473
[166]: https://gitlab.com/OpenMW/openmw/-/issues/6022
[167]: https://gitlab.com/OpenMW/openmw/-/issues/6023
[168]: https://gitlab.com/OpenMW/openmw/-/issues/6035
[169]: https://gitlab.com/OpenMW/openmw/-/issues/6036
[170]: https://gitlab.com/OpenMW/openmw/-/issues/5400
[171]: https://gitlab.com/OpenMW/openmw/-/issues/5731
[172]: https://gitlab.com/OpenMW/openmw/-/issues/4357
[173]: https://gitlab.com/OpenMW/openmw/-/issues/4363
[174]: https://gitlab.com/OpenMW/openmw/-/issues/5675
[175]: https://gitlab.com/OpenMW/openmw/-/issues/5703
[176]: https://gitlab.com/OpenMW/openmw/-/issues/5713
[177]: https://gitlab.com/OpenMW/openmw/-/issues/6235
[178]: https://gitlab.com/OpenMW/openmw/-/issues/5384
[179]: https://gitlab.com/OpenMW/openmw/-/issues/5026
[180]: https://gitlab.com/OpenMW/openmw/-/issues/5480
[181]: https://gitlab.com/OpenMW/openmw/-/issues/4765
[182]: https://gitlab.com/OpenMW/openmw/-/issues/5551
[183]: https://gitlab.com/OpenMW/openmw/-/issues/5904
[184]: https://gitlab.com/OpenMW/openmw/-/issues/5520
[185]: https://gitlab.com/OpenMW/openmw/-/issues/5980
[186]: https://gitlab.com/OpenMW/openmw/-/issues/5807
[187]: https://gitlab.com/OpenMW/openmw/-/issues/5897