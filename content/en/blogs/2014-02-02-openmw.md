{
   "author": "Lukasz Gromanowski",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-02-02T20:54:04+00:00",
   "title": "Meanwhile in OpenMW.",
   "type": "post",
   "url": "/2014/openmw/"
}
So, dear reader, for the first time (or once again if you&#8217;re long-time lurker) you visited OpenMW blog to find out what&#8217;s going on in the project and to check if we didn&#8217;t craftily released version 1.0.0 in your absence. First of all, I can tell you that nothing like this has happened. More detailed information can be found later in the text – people interested don&#8217;t need to be encouraged, and others already left the page, so let&#8217;s cut to the chase.

Scrawl, one of the main developers, managed to complete work on the area magic effects, and has introduced a number of significant general improvements as well. It&#8217;s worth noting the change in NPC&#8217;s navigation: NPC&#8217;s will no longer change direction instantly, instead turning their bodies gradually in accordance with the laws of physics. Gus also contributed to achieve this effect.

Zini has just returned to work on the game saving feature – the new thing is the ability of saving changes of the containers content.

Graffy continues to work on the content file selector window for OpenCS and OpenMW launcher. Currently selector&#8217;s model is ready, and few elements in a selector&#8217;s view were improved.

Records cloning in OpenCS works as expected.

In my opinion, we are making significant progress.

And that&#8217;s all folks!