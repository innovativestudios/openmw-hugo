{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2013-03-12T14:16:00+00:00",
   "title": "Week in review",
   "type": "post",
   "url": "/2013/week-review-42/"
}
Welcome once again readers,

Do you remember the last week when I wrote that Tamriel Rebuilt textures are bad? This is not the case anymore: TR just updated their data and now all textures are dds, not bmp or tga. Kudos!

Let&#8217;s get back to OpenMW.

This week was dominated by Scrawl Related Activity (SRA), on top of many contributions from other developers.

The first of graphics branch was merged into the main branch, along with (!) 75 commits of it. This means that many things are now fixed in the main branch, and graphics have improved that much more, especially lights in interiors looks impressive.

It&#8217;s hard to list every fix from this branch and this week we had an even larger number of solved bugs by other developers. I was told to not call bugfixes &#8220;boring&#8221;, even if it&#8217;s so tempting. Developers work hard to introduce it into OpenMW after all.

But still, It&#8217;s impossible to list everything. OpenMW recently advanced rapidly and it&#8217;s not possible to make a detailed post describing everything in short, Internet Reader Friendly (IRF) text anymore.

Let&#8217;s get back to SRA.

Scrawl added new features:  
Allow zooming camera in vanity or preview mode with the mousewheel.  
Implemented sneaking animation and pickpocket (not skill itself yet).  
Race selection preview: render only the head, and focus the camera on its node.  
Dispose corpse.  
Added BM trees to transparency overrides  
Batching bloodmoon&#8217;s trees.

Scrawl also solved loads of bugs and now he works on proper raycasting.

But it&#8217;s not everything this week. Gus has been working on AI. He is making a good progress: NPCs can go to the closest waypoint already.

Blunted2night still commits to his journal branch. Blunted2night can take as much time to complete the feature as he wants to and it looks like he aims for perfection.

Zini works with graffy76 on openCS and the project is advancing quite nicely, probably not fast enough to be ready for OpenMW 1.0.0 but still not bad. Needless to say, Zini, aside from merging dozens of pull requests, also found some time to work on the OpenMW itself to solve a few more bugs. Greye has also helped us to get rid of a few additional errors.

Pvdk presented to us launcher with ini importer integration. We like it. We guarantee you&#8217;ll like it just as well.