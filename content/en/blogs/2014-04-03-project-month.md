{
   "author": "Lukasz Gromanowski",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-04-03T05:13:09+00:00",
   "openid_comments": [
      "a:1:{i:0;i:564;}"
   ],
   "title": "Project of the month",
   "type": "post",
   "url": "/2014/project-month/"
}
We have been nominated for [&#8220;Project of the month&#8221; at Linux Game Awards][1] &#8211; please vote! :D

 [1]: http://www.linuxgameawards.org/game/openmw "Linux Game Awards - Project of the month"