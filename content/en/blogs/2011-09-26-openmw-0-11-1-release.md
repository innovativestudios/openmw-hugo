{
   "author": "raevol",
   "categories": [
      "Release"
   ],
   "date": "2011-09-26T16:16:00+00:00",
   "showReadingTime": true,
   "summary": "I&#8217;m proud to announce that OpenMW 0.11.1 is now released!",
   "tags": [
      "0.11.0",
      "release"
   ],
   "title": "OpenMW 0.11.1 Release",
   "url": "2011/openmw-0-11-1-release"
}
Release packages can be found on the [Google Code page][1].

Please note that with the loss of our Linux builder, no Linux package is available. Linux users can download the source and build the release themselves. If you have Debian packaging experience and would like to help out, please let us know!

Changelog:
* Launcher implemented
* Misc. code cleanup
* Some optimizations implemented, further optimizations are planned
* Fix to allow resource loading outside of BSA files
* Fix to search for openmw.cfg in the correct locations
* Added the TCL alias for ToggleCollision
* Added missing cfg file support for some command line options
* Added reporting of resulting state to Toggle-type script instructions
* Fix for some NPC IDs being interpreted as Topic IDs
* Implemented back-end for player journal
* Implemented MessageBox
* Implemented tab-completion in console
* Implemented handling multiple data directories
* Fix for accessing objects in cells via ID with mixed or upper case IDs
* Fixed unicode conversion issue to allow localized encoding of gui strings

[1]: http://code.google.com/p/openmw/downloads/list