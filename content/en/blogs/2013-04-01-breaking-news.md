{
   "author": "sir_herrbatka",
   "categories": [
      "Uncategorized"
   ],
   "date": "2013-04-01T13:40:43+00:00",
   "title": "Breaking news!",
   "type": "post",
   "url": "/2013/breaking-news/"
}
Thanks to hard work of our programmers We finally nailed down bug that prevented Vivec from saying something all these years. He surely feels relief now.

<div style="width: 410px" class="wp-caption alignnone">
  <a href="https://i0.wp.com/wiki.openmw.org/images/8/86/VWGpYKd.png?ssl=1"><img loading="lazy" alt="" src="https://i0.wp.com/wiki.openmw.org/images/8/86/VWGpYKd.png?resize=400%2C300&#038;ssl=1" width="400" height="300" data-recalc-dims="1" /></a>
  
  <p class="wp-caption-text">
    See?
  </p>
</div>