{
   "author": "DestinedToDie",
   "categories": [
      "Uncategorized"
   ],
   "date": "2016-11-15T08:02:19+00:00",
   "title": "Bump mappers assemble!",
   "type": "post",
   "url": "/2016/bump-mappers-assemble/"
}
With the announcement of OpenMW version 0.39 around 5-6 months ago, some users booted their game up to find an unpleasant surprise &#8211; shiny rocks. But they were told that this wasn&#8217;t a bug at all. Everything was working as intended.

[<img loading="lazy" class="wp-image-4759 alignnone" src="https://i0.wp.com/openmw.org/wp-content/uploads/2016/11/WAaXCcy-300x188.jpg?resize=699%2C438&#038;ssl=1" alt="waaxccy" width="699" height="438" data-recalc-dims="1" />][1]

So what led to this? Morrowind was initially released in 2002, a time when texturing techniques were not as advanced, and there were no normal maps. As years went by, the community created more and more complex mods on an engine that was frozen solid to the state that it started with a decade ago. At a certain point, the methods began to exceed engine limitations and newer technologies simply couldn&#8217;t be applied anymore.

Since the Morrowind engine isn&#8217;t open source, it&#8217;s not like someone could just edit the code and update it to modern standards. But what they could do is essentially &#8220;hack&#8221; the engine by datamining for information and make code injectors. This spawned the loved MGE XE distant lands, Morrowind Script Extender, and, last but not least, Morrowind Code Patch. While they have served the community well, they&#8217;ve also produced some unwanted side effects such as crashes, instability, memory leaks, and strange methods of texturing.

The latter in particular is caused by Morrowind Code Patch, and is also the reason for our shiny rocks. MCP enables a sort of &#8220;fake&#8221; bump mapping by using an environmental map to give textures the appearance of being bump-mapped. But no other engine does it this way &#8211; and if you were to port a &#8220;fake&#8221; bump-mapped mesh into Unity engine, Skyrim, or OpenMW, a shiny weird-looking object is what you would get.

So what do we do with these shiny rocks? Do we throw them out? No! Fortunately, they can be redeemed, and we now have [Lysol&#8217;s detailed and comprehensive guide][2] to lead you through the process. Most bump maps can be fixed with a few easy steps that even the average player with no modding experience can handle.

[<img loading="lazy" class="wp-image-4758 alignnone" src="https://i0.wp.com/openmw.org/wp-content/uploads/2016/11/mAjCbzS-300x188.jpg?resize=703%2C440&#038;ssl=1" alt="majcbzs" width="703" height="440" data-recalc-dims="1" />][3]

But only the author of a mod reserves the right to upload their content, so any user who wanted to play your mod would have to repeat the same process. If you are an author, I encourage you to take a little time to update your mod to OpenMW. Or if you&#8217;re short on time, post on our forums and give permission for users to update it and see if someone picks it up. Let&#8217;s rid Vvardenfell of those shiny rocks!

[Want to leave a comment?][4]

 [1]: https://i0.wp.com/openmw.org/wp-content/uploads/2016/11/WAaXCcy.jpg?ssl=1
 [2]: https://forum.openmw.org/viewtopic.php?f=40&t=3922
 [3]: https://i0.wp.com/openmw.org/wp-content/uploads/2016/11/mAjCbzS.jpg?ssl=1
 [4]: https://forum.openmw.org/viewtopic.php?f=38&t=3935