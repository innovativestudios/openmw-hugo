{
   "author": "Nekochan",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-06-29T12:02:23+00:00",
   "title": "Breaking news…",
   "type": "post",
   "url": "/2014/breaking-news-2/"
}
The usual things going on:

  * Mountains of bugs fixed. (Almost three dozen bugs disappeared from the tracker just this week!)
  * Since we&#8217;re nearing completion, we&#8217;ve taken a good look at how we can optimize OpenMW. As a result, the game runs noticeably smoother now.
  * Pvdk is working on the Morrowind installer for those of us who are not on Windows systems. These little side-projects might not have a direct result on the gameplay, but are still part of OpenMW and will need to be in good shape.
  * Scrawl discovered a few new absurdities about Morrowind&#8230;

Sorry for the rush. The next news post will be as fat as deep-fried butter. ;)