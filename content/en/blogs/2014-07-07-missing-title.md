{
   "author": "Nekochan",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-07-07T15:42:33+00:00",
   "title": "Brace for impact!",
   "type": "post",
   "url": "/2014/missing-title/"
}
It is done! Ladies and gentlemen:

**OpenCS&#8217;s rendering lives!**

To tell you the truth, it&#8217;s not totally done yet, but what has been done is significant and pretty exciting. Our editor really starts to feel like a full-featured application now! Well, it isn&#8217;t yet, of course, but it feels awesome, thanks to Zini.

Sir_herrbatka is still wrestling with some Drag & Drop issues, and he&#8217;s working on implementing nested tables. Right now they are only implemented for containers, so you are able to add and remove items. Unfortunately, the bad news is that you won&#8217;t see this feature until 0.32.0. But the good news is the reason for that: 0.31.0 is almost upon us!

That&#8217;s right! Tons of bugs have been fixed and we&#8217;ve locked the main branch down for 0.31.0. It will be released very shortly, after some last minute bug fixing so please be patient. Soon you&#8217;ll have a taste of it. :)