{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2011-09-25T10:17:47+00:00",
   "showReadingTime": true,
   "summary": "Welcome! OpenMW 0.11.1 is coming, but the road is slow and painful.",
   "tags": [
      "animation",
      "corristo",
      "jhooks",
      "rendering",
      "terrain",
      "Zini"
   ],
   "title": "What’s up?",
   "type": "post",
   "url": "2011/whats-up"
}
Since Linux builder vanished zini started to be &#8220;officially pissed off&#8221; and decided that there will be no binary for OpenMW 0.11.1. If you want to test OpenMW 0.11.1 on Linux you will have to build it on your own. Of course it&#8217;s possible that binary may pop-up later but lack of Linux builder won&#8217;t stop us anymore.

Among Linux world it&#8217;s common to distribute software in the form of deb, rpm, tar.gz and source code or just distribute source code and tell package maintainer &#8220;build it and upload to the repo&#8221;. Closed source apps (like softmaker office) are distributed without source code, just with binary packages, simply because they are closed source and you can&#8217;t build it yourself. We can just stick with the source code for now.

jhooks reached the point when everything related to the npc animations works (that includes robes and tails) but so slow that it&#8217;s not usable for video game. Now it&#8217;s time to figure out how to get better performance but the situation is rather complicated.

Zini was working on &#8220;cell handling improvements&#8221; and was able to get nice results. That should improve game saving and loading time.

Corristo is investigating terrain rendering, and I&#8217;m very happy to see this since OpenMW now definitely lacks terrain. It can take ages to implement, though.