{
   "author": "Okulo",
   "categories": [
      "Maintenance",
      "Uncategorized"
   ],
   "date": "2015-06-13T06:07:29+00:00",
   "title": "Base-building",
   "type": "post",
   "url": "/2015/base-building/"
}
As the feature list is getting shorter and shorter, our development team&#8217;s attention has been shifting towards issues that are a lot bigger than things like NPCs not doing what they are supposed to do.

Scrawl, for example, is slaving away at porting the project from Ogre3D to OpenSceneGraph, a relatively unknown but immensely flexible and expansive 3d engine which will not only allow OpenMW to run smoother than before, but also allows for better handling of materials and stitching the bodyparts of NPCs together (which is not nearly as gruesome as it sounds). [Scrawl himself has told us][1] about the what and why of OSG, and yes, I _could_ tell you all that he is making good progress, but pictures speak louder than words.

<div style="text-align: center">
  <a href="https://i0.wp.com/scrawl.bplaced.net/temp/screen22-16-24.png"><img style="width: 500px" src="https://i0.wp.com/scrawl.bplaced.net/temp/screen22-16-24.png" data-recalc-dims="1" /></a><br /> <a href="https://i0.wp.com/scrawl.bplaced.net/temp/screen22-07-19.png"><img style="width: 500px" src="https://i0.wp.com/scrawl.bplaced.net/temp/screen22-07-19.png" data-recalc-dims="1" /></a>
</div>

It&#8217;s almost a throwback to version 0.13.0, but don&#8217;t worry; unlike [the project way back when in 2012][2], much of the program is already in place right now, so we promise it won&#8217;t take three years to catch up. I mean, just between the time that I started writing this update and the time this got posted [Scrawl got NPCs to render][3]. Things are progressing _fast_.

Meanwhile, OpenMW-CS has been developing nicely as well. Our very own modding suite has gotten two more features that you _have_ to see if you are a modder.

A plenty large code submission by cc9ii, based on some hard work by sirherrbatka, will give you access to stuff that you didn&#8217;t have access to before. Take NPCs, for example. Their settings were previously accessible only through the main table, but there are some things that were not contained therein. Selecting _Edit record_ from the right click menu would bring up some more stuff to change (such as if this NPC would buy certain items), but cc9ii&#8217;s contribution expands that even further. By editing the NPC, you can now put stuff in his inventory, check out the paths they take while walking through the world, [and much more][4].

Zini has [announced with the proper fanfare][5] that Global Search is now working. When confronted with a huge list of stuff, trying to find that lovely lime-ware platter from the Census and Excise Office is like searching for a needle in a haystack. Previously, it was possible to filter using a specific syntax, but why make it difficult? With Global Search implemented, you can instantly find that one object with nary a thought. What&#8217;s more, this global search function will be extended with a replace function, so you don&#8217;t have to edit all those potions one by one.

Finally, a bit of community stuff as well. Sterling of the Old School RPG YouTube channel has started his Let&#8217;s Play of Morrowind through the OpenMW engine. If you want to see how LPs are done or are just curious about how OpenMW runs in practice, go ahead and watch the first video below. Don&#8217;t forget to [check out his channel afterwards][6] for more content.



That it&#8217;s for this update. The team is ever active, and we can&#8217;t wait to tell you more in our next update. See you next time.

##### <a href="//forum.openmw.org/viewtopic.php?f=38&t=2859" title="Want to leave a comment?" target="_blank">Want to leave a comment?</a>

 [1]: https://openmw.org/2015/announcing-switch-openscenegraph/
 [2]: https://www.youtube.com/watch?v=9gOqBU2oZnk
 [3]: http://i.imgur.com/jwhQAdr.png
 [4]: https://github.com/OpenMW/openmw/pull/553
 [5]: https://twitter.com/marczinnschlag/status/582217189966610434
 [6]: https://www.youtube.com/channel/UC6cCf0rNe7XK8V1rPcW9Bkw