{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2011-11-13T17:29:01+00:00",
   "tags": [
      "mwrender",
      "yacoby"
   ],
   "title": "Skyrim is here, but so what?",
   "type": "post",
   "url": "/2011/skyrim-is-here-but-so-what/"
}
Nothing really new or really exiting. Just the same old development in mwrender plus minor improvements (as He defined it) from yacoby &#8220;Use alternate storage location for modified object position&#8221;.

And I won&#8217;t write more since everybody is playing Skyrim right now and have no time to read my news. :(