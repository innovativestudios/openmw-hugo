{
   "author": "Lukasz Gromanowski",
   "categories": [
      "Uncategorized"
   ],
   "date": "2015-06-13T06:00:41+00:00",
   "openid_comments": [
      "a:1:{i:0;i:541;}"
   ],
   "title": "An ode to (unit) tests.",
   "type": "post",
   "url": "/2014/ode-unit-tests/"
}
Version 0.29.0 will be released soon — momentary impediments are the bugs that were detected during the RC testing phase. I don&#8217;t want to delve into the details, but, if anyone needs to support the thesis that (unit) testing is critical, OpenMW is a perfect example. Fortunately, things are moving in the right direction.

Scrawl&#8217;s latest idea is to streamline the process of starting a new game — terrain data can be loaded in another thread, so we don&#8217;t have to watch another episode of &#8220;The Bold and the Beautiful&#8221; while waiting for the start of OpenMW.

Additionally, Pvdk is trying to please grumblers who can&#8217;t tolerate Wine on their system. Wine is very helpful with the installation of Morrowind, but I must admit that installing it just for this purpose seems over the top. After all, you can just unpack game archives (there is even a useful library for doing this).

As I&#8217;ve mentioned in previous weeks, Pvdk is creating a user-friendly installer for Morrowind. The installer will be enriched by new features as well as other minor improvements: it&#8217;ll verify what has already been installed and won&#8217;t install the Bloodmoon add-on before Tribunal. It will also properly save the morrowind.ini file.

SirHerrbatka has introduced the ability to automatically create filters using the drag&#8217;n&#8217;drop feature in OpenCS. The way the expression is created ensures that it can be adapted to the columns which hold corresponding records. For example, when dragging a faction record, let&#8217;s say, &#8220;the Fighter&#8217;s Guild&#8221;, into the Referencables&#8217; filter field, it&#8217;ll automatically appear in the filter, so that the table will display only NPCs belonging to the Fighter&#8217;s Guild.

The new method is much more efficient — the filter creation process speeds up noticeably (even if you type very quickly). The possibilities of this function are a bit more expansive than the example above and eventually it&#8217;ll be described in much more detail in the OpenCS manual.

And last but not least [here you can read Raevol&#8217;s SCaLE 12x Talk Debrief][1] and [here][2] you can download slides from SCaLE 12x Talk.

Till next time!

 [1]: https://forum.openmw.org/viewtopic.php?f=4&t=2053 "SCaLE 12x Talk Debrief"
 [2]: http://downloads.openmw.org/presentations/SCaLE12x/OpenMW_SCaLE12x.odp "Slides from OpenMW SCaLE 12x Talk"