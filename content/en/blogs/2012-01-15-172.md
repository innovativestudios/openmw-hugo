{
   "author": "Lukasz Gromanowski",
   "categories": [
      "Maintenance"
   ],
   "date": "2012-01-15T14:36:47+00:00",
   "tags": [
      "forum",
      "lgromanowski",
      "maintenance",
      "wiki"
   ],
   "type": "post",
   "url": "/2012/172/"
}
Hello,  
Thursday and Friday openmw.org unavailability, as I mentioned in previous post, was caused by power loss in <a href="http://ecatel.co.uk/?p=home" title="Ecatel datacenter" target="_blank">Ecatel datacenter</a> in Holland which is used by Rootnode.net (our hosting), but that unfortunately was not all (Friday 13th?). There was also APC power switch and some KVMs failure which caused some problems with data on shell and web servers, so RN admins restored it from backup. I don&#8217;t have access to shell (yet) so I can&#8217;t verify if all data was restored, but main page, forum, wiki and bug tracker works, so I hope everything is OK.

Among this, RN admins planned some bigger maintenance works at Monday and Wednesday (16/17.01.2012). Maintenance status will be posted on <a href="http://twitter.com/marcinhlybin" title="Rootnode twitter" target="_blank">twitter</a>. They will add and start testing new (faster, better) web cluster which will be used for web hosting for RN users in near future. Also there will be HDD upgrades (but only on shell servers so openmw.org pages will not be affected), and some network reconfiguration. So, please be prepared for some &#8220;unexpected&#8221; things, like few minutes of web page unavailability. And there is yet another thing, there will be a dedicated machine for Java in RN (it&#8217;s currently tested) &#8211; whats this mean for us? We could setup in future <a href="http://jenkins-ci.org/" title="Jenkins CI" target="_blank">Jenkins Continuous Integration server</a> which could do for us some automatic binaries building and packaging :D