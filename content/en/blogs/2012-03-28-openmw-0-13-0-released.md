{
   "author": "raevol",
   "categories": [
      "Uncategorized"
   ],
   "date": "2012-03-28T17:42:08+00:00",
   "tags": [
      0.13,
      "release"
   ],
   "title": "OpenMW 0.13.0 Released!",
   "type": "post",
   "url": "/2012/openmw-0-13-0-released/"
}
Hot on the heels of 0.12.0, the OpenMW team is proud to announce the release of version 0.13.0! Release packages for Ubuntu are now available via our [Launchpad PPA][1]. Release packages for other platforms are available on our [Download page][2]. This release notably includes functional NPC dialogue, and beautiful sky!

Please note:

&#8211; On OSX, the path to the application cannot contain spaces, or the launcher will not work properly.

Changelog:

&#8211; NPC Dialogue window and mechanics implemented  
&#8211; Reimplemented sky rendering, added weather effects  
&#8211; Wireframe mode added  
&#8211; Fix for sounds broken in 0.12.0  
&#8211; Fix for 3D sounds  
&#8211; Added sounds for weather, doors, containers, picking up items, and journal  
&#8211; Various code cleanup and improvements  
&#8211; Fixed an Ogre crash at the Dren plantation  
&#8211; Several launcher improvements  
&#8211; Added fade to black effect for cutscenes  
&#8211; Added backend for equipping items  
&#8211; Fix to stop ASCII 16 character from being added to console on its activation in OSX  
&#8211; Fixed collision shapes being out of place  
&#8211; Fixed torch lights not being visible past a short distance  
&#8211; Fixed some transparency rendering problems

 [1]: https://launchpad.net/~openmw/+archive/openmw
 [2]: http://code.google.com/p/openmw/downloads/list