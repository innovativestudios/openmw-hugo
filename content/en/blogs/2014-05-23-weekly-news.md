{
   "author": "Nekochan",
   "categories": [
      "Uncategorized"
   ],
   "date": "2014-05-23T15:15:47+00:00",
   "title": "Almost weekly news",
   "type": "post",
   "url": "/2014/weekly-news/"
}
&nbsp;

There are still many outstanding bugs, both big and small. From time to time we are even haunted by HUGE bugs – and there&#8217;s just no escaping those! Our friendly developers have added an incredible amount of features and with those come bugs that need to be fixed, so it does make sense that they&#8217;re in there.

As I keep saying week after week, OpenMW it’s almost ready and we haven&#8217;t even seen new bugs pop up when we try to fix one of them. Now that&#8217;s pure luck. At this point you can get a good idea of how far along we are by looking at the amount of problems we have solved. So, sure &#8211; maybe the news this week is not extremely exciting but we won&#8217;t release 1.0.0 if these bugs are in there… nor even just 0.30.0 if you don&#8217;t like to look into the not too distant future.

Speaking of which, 0.30.0 still isn’t ready, but we are very close. K1ll pushed a whole bunch of corrections to his repository that fix some issues with certain Linux installations and Scrawl just smashed all records with the amount of bugfixes he delivered. Other developers have also been working hard, of course. Last week was very active, but other than bug-fixes we don&#8217;t really have much to show for it. The only bit other bit of news is that two weeks ago Zini talked about improving support for loading the game files to the editor.

And that is all for now. Just be patient and wait for release 0.30.0 – we&#8217;re so close I can almost taste it!

<img loading="lazy" class="alignnone" src="https://i0.wp.com/i.imgur.com/YOmDuDV.jpg?resize=656%2C337&#038;ssl=1" alt="" width="656" height="337" data-recalc-dims="1" /> 

&nbsp;

&nbsp;