{
  "author": "Atahualpa",
  "categories": [
    "OpenMW Videos"
  ],
  "comment": true,
  "date": "2021-03-30T21:26:48+00:00",
  "source": {
    "forumID": 38,
    "topicID": 7423
  },
  "showReadingTime": true,
  "summary": "Starting with version 0.13.0, each and every main release of our engine has been accompanied by a release commentary. For me and many others, the release videos have always been something to look forward to, a strangely satisfying means to highlight OpenMW&#8217;s progress throughout the ages &#8212; and a great opportunity to mess around with our beloved Fargoth!",
  "tags": [
    "Video Special",
    "Videos",
    "Visual History"
  ],
  "title": "OpenMW Special Video: 10,000 YouTube Subscribers",
  "url": "/2021/openmw-special-video-10000-youtube-subscribers/"
}

Our channel has now reached 10,000 subscribers, a truly gigantic number for such a niche project like OpenMW. A huge thank-you to all of you for your continuous support! Rest assured, though, that we are not reaching for the stars here and that we will still concentrate on publishing release commentaries in the future instead of featuring puppies or kittens. But a little celebration seems to be appropriate, which is why I present you with an updated version of our 2013 video &#8220;A Visual History of Changes to OpenMW&#8221;! Sit back, relax, and have fun. &#8212; And thanks again for your support!