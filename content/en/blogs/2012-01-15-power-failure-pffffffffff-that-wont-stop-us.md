{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2012-01-15T16:01:58+00:00",
   "tags": [
      "configuration",
      "jhooks",
      "lgromanowski",
      "rendering",
      "terrain",
      "water",
      "yacoby"
   ],
   "title": "Power failure? pffffffffff! That won’t stop us.",
   "type": "post",
   "url": "/2012/power-failure-pffffffffff-that-wont-stop-us/"
}
It seems that the only task left for new release openmw is configuration/directory handling. lgro is currently on it so It looks like we will see new version on the loose soon with basic journal, properer character rendering and animations finally working (to mention the most spectacular features).

Actually work for 0.13.0 already started. Yaccoby is taken over terrain rendering task and jhooks1 is working on water rendering.

Short reminder here: yaccoby is the guy behind old terrain rendering in old openmw. It was totally custom implementation, since the current ogre3d lib did not exist at the time and available solutions sucked a bit. Currently there is no reason to write custom terrain rendering since:

  * new lib does the job good enough
  * more code in separate libs = less code in openmw = less work on code maintaining
  * this code should get improvements over a time from ogre3d community that we could use
And yaccoby seems to excited to work on feature that can make pretty pictures

While Yaccoby is doing element of earth jhooks1 started element of water (sadly We dropped codenames so We can&#8217;t name 0.12.0 &#8220;It moves&#8221; and 0.13.0 &#8220;ElementalWind&#8221;). No hydratex here: It requires decent hardware to run smoothly and that&#8217;s not really acceptable for openmw.

Although openmw will require better hardware than morrowind It shouldn&#8217;t need newest graphic cards to run at all. For example yaccoby wants to use shader model 2 for terrain, and although Morrowind did not require that kind of hardware today it&#8217;s unusual to find less capable PC. Even my crappy, old Geforce 7300 GT is a shader model 3.

Oh, my geforce is doing what I want and I&#8217;m not complaining at all. It&#8217;s just old. I don&#8217;t think that anyone uses older graphic card than this.