{
   "author": "sir_herrbatka",
   "categories": [
      "Week's review"
   ],
   "date": "2011-10-30T09:18:33+00:00",
   "tags": [
      "Gus",
      "jhooks",
      "journal",
      "mwrender",
      "physics",
      "refactoring"
   ],
   "title": "Oh yes",
   "type": "post",
   "url": "/2011/oh-yes/"
}
Hello

Jhooks is moving forward with MWRender refactoring task, meanwhile fighting physics bugs. Well, the fight was successful thanks to common effort of developers. Sadly refactoring starts to be harder since the easier part is done.

GUS is still doing journal task, although &#8220;journal task&#8221; name does not gives really right feeling about what it is. Besides journal the code should be useful for books, since books are quite similar to the journal ─ both parses text and displays it in pages. Difference is mostly related to extra journal stuff, like index and disallowing to break entry on two pages.