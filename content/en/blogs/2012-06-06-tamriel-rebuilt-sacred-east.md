{
   "author": "jvoisin",
   "categories": [
      "Uncategorized"
   ],
   "date": "2012-06-06T12:10:37+00:00",
   "title": "Tamriel Rebuilt – Sacred East",
   "type": "post",
   "url": "/2012/tamriel-rebuilt-sacred-east/"
}
Omg, Sacred East was [released][1]!

<img loading="lazy" src="https://i0.wp.com/i.imgur.com/EjLJl.jpg?resize=682%2C511" alt="cover" width="682" height="511" data-recalc-dims="1" /> 

For those who don&#8217;t know, Tamriel Rebuilt is the enormous fan effort to complete the province of Morrowind, piece by piece.  
Their goal is to faithfully create Morrowind&#8217;s mainland to the vision set down in Bethesda Softworks official TES lore.

Here is the anouncement :

> Sacred East is now available for your enjoyment! Sacred East features some of the most lush terrain seen anywhere on Morrowind to date. With dense jungles, sparkling rivers, and impressive plantations, Sacred East is an adventure into the frontier of a previously-unexplored frontier: the lands of Great House Indoril. The Indoril may be known as the pious and religious among all the Great Houses, but they are not immune to terror, and it will be up to you to unravel the mysteries plaguing Almalexia&#8217;s favored, as they call themselves. Prepare yourself for amazement, intrigue, and more than a little cross-country backpacking!
> 
> Sacred East includes an **updated version** of Telvannis & Antediluvian Secrets, as well as TR_Data (the data files required to play our content!). This release contains faction quests for Antediluvian Secrets, and miscellaneous quests for most settlement areas in Sacred East. Detailed lists and guides for quests may follow this release.

Here&#8217;s to the day when you&#8217;ll be able to play Tamriel Rebuilt&#8217;s completed masterpiece in OpenMW!

 [1]: http://tamriel-rebuilt.org/ "TR"