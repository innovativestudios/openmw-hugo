{
   "date": "2022-01-13T00:49:42-06:00",
   "draft": false,
   "title": "Download",
   "toc": true
}

## Release Versions

These versions are official releases that have been thoroughly tested.

### Windows / MacOS / Linux
* [Download from GitHub](https://github.com/OpenMW/openmw/releases/latest) (Available at the end of the changelog)
* [Download from Site File Repository](https://downloads.openmw.org/?C=M&O=D)

### Other Download Locations
* Windows: [Install via Chocolatey](https://chocolatey.org/packages/openmw)
* Ubuntu: [Official Ubuntu Repo](https://packages.ubuntu.com/search?keywords=openmw) / [Launchpad PPA](https://launchpad.net/~openmw/+archive/openmw)
* Debian: [Official Debian Repo](https://packages.debian.org/sid/openmw)
* Arch Linux: [Install from [Community] Repository](https://www.archlinux.org/packages/?sort=&q=openmw)
* Gentoo: [Install from Gentoo Repository](https://packages.gentoo.org/package/games-engines/openmw)
* OpenSU SE: [Install from OpenSU SE Repository](https://software.opensuse.org/download.html?project=games&package=openmw)
* Fedora: [Install from RPM Fusion Repository](https://admin.rpmfusion.org/pkgdb/package/free/openmw/)

Also available as a [Flatpak](http://flatpak.org/) package from [Flathub](https://flathub.org/). You can either [Click to Download](https://flathub.org/repo/appstream/org.openmw.OpenMW.flatpakref), or use the command line:

` flatpak install --from https://flathub.org/repo/appstream `
` /org.openmw.OpenMW.flatpakref `

## Development Builds
These are development builds of OpenMW with changes not found in the latest release. Linux and macOS downloads include OpenMW-CS

**Note:** These builds contain the newest features but are also significantly less-tested than the release builds.

If you find an issue while using an Unreleased Build, please visit our issue tracker to see if it's been reported, and report it if it has not been.

* [Windows Engine](https://gitlab.com/OpenMW/openmw/-/jobs/artifacts/master/raw/OpenMW_MSVC2019_64_Engine_Release_master.zip?job=Windows_MSBuild_Engine_Release)
* [OpenMW-CS](https://gitlab.com/OpenMW/openmw/-/jobs/artifacts/master/raw/OpenMW_MSVC2019_64_CS_Release_master.zip?job=Windows_MSBuild_CS_Release)
* [Make Sure Visual C++ Redist is Installed](https://aka.ms/vs/16/release/vc_redist.x64.exe)

Packaged Linux Distro Unreleased Builds:

* [Ubuntu Linux Launchpad PPA](https://launchpad.net/~openmw/+archive/ubuntu/openmw-daily)
* [Arch Linux AUR](https://aur.archlinux.org/packages/?O=0&K=openmw)

## OpenMW User Manual
Latest Version of the [OpenMW User Manual](https://openmw.readthedocs.io/en/master/manuals/index.html)

## OpenMW-CS User Manual
Latest Version of the [OpenMW-CS User Manual](https://openmw.readthedocs.io/en/master/manuals/openmw-cs/index.html)