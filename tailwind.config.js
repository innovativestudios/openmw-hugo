const colors = require('tailwindcss/colors');

module.exports = {
	content: ['./content/**/*.{html,md}', './themes/openMW/layouts/**/*.{html,md}'],
	theme: {
		colors: {
			transparent: 'transparent',
      current: 'currentColor',
			black: colors.black,
			white: colors.white,
			slate: colors.slate,
			gray: colors.gray,
			zinc: colors.zinc,
			neutral: colors.neutral,
			stone: colors.stone,
			red: colors.red,
			orange: colors.orange,
			amber: colors.amber,
			yellow: colors.amber,
			lime: colors.lime,
			green: colors.green,
			emerald: colors.emerald,
			teal: colors.teal,
			cyan: colors.cyan,
			sky: colors.sky,
			blue: colors.blue,
			indigo: colors.indigo,
			violet: colors.violet,
			purple: colors.purple,
			fuchsia: colors.fuchsia,
			pink: colors.pink,
			rose: colors.rose,
			openMW: {
				500: '#cd3700',
				600: '#eb3f00'
			},
			openMW_dark: {
				500: '#48361d',
				600: '#654b28'
			}
		},
		extend: {
			typography: {
				DEFAULT: {
					css: {
						'code::before': {content: ''},
						'code::after': {content: ''}
					}
				}
			}
		}
	},
	variants: {
		display:['group-hover']
	},
	plugins: [
    require('autoprefixer'),
    require('postcss-nested'),
		require('@tailwindcss/typography'),
		require('tailwindcss')
	]
}